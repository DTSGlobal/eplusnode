
$(function () {
    var self = this;
    var userThings = new DTS.Things();
    var user = new DTS.Molecule();
    var navigationItems = new DTS.Things();
    var parentItems = [];
    var subMenuItems = [];
    var childItems = [];
    var baseurl = window.location.pathname;
    var universeId = "3b6a2fa8-2890-499e-8ec7-f81e11034853";

    userThings.getUserMolecule(userEmail, function(){

            user = userThings.molecules[0];
            var userId = user.header.exposedId;

            navigationItems.getNavigation(universeId, self.buildNavigation)

    });

    self.getQueryVariable = function (variable) {

        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }
    };

    var id = self.getQueryVariable('id');
    if(id == "" || id == undefined){
        id="00000000-0000-0000-0000-000000000000";
    }

    $('#id').val(id);

    var formId = self.getQueryVariable('formId');
    
    $('#formId').val(formId);

    if(!baseurl.includes("purposebuilt") && !baseurl.includes("UserDynamic")){
        var kpiHeader = `<div class="container-fluid body" id="bodyContainer" style="background-color: #F2F4F6">
                            <div class="card-deck" style="background-color: #F2F4F6">
                                <div class="card kpi-container" data-kpi-id="KPI1" style="background-color: #F2F4F6">
                                    <div class="card-block" style="background-color: #F2F4F6">
                                        <h5 class="card-title" id="title1"><b>30-Day Booking Breakdown<b></h5>
                                        <div class="card-deck" id="container1" style="background-color: #F2F4F6">

                                            <h4 id="loadingTitle"></h4>
                                            <canvas id="myCanvas"></canvas>
                                            <div id="myLegend"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`

        $('#loadingTitle').append('<h4 class="text-center" id="loadtitle">Loading KPIs...</h4>');
        $('#loadingTitle').append('<i class="fa fa-spin fa-spinner" id="myspinner"></i>');

        $('#loadingTitle').append(kpiHeader);

        var today = new Date()
        var convertToday = today.toISOString();
        today.setDate(today.getDate() - 30);
        var dateString = today.toISOString().split('T')[0];
        

        var myMethods = {};
        var allContainerItems = new DTS.Things();
        var myCanvas = document.getElementById("myCanvas");
        myCanvas.width = 500;
        myCanvas.height = 500;

        var ctx = myCanvas.getContext("2d");
        var queryAttributes = {};
        queryAttributes.attributeName = 'Containercontainer_invoice_date';
        queryAttributes.DateMin = dateString;
        queryAttributes.DateMax = convertToday;
        var request = {};
        request.attributes = [];
        request.attributes.push(queryAttributes);
        request.DomainObjectId = 'e2a3840f-b06b-4e27-87b9-881f5f6ab7fc';//Container Object
        request.ExistsOnly = true;
        request.friendlyColumns = [];
        request.friendlyColumns = [
            'ValueExistsOnOff',
            'ContainerChangeMethod'
        ];
        request.RowCount = 500;
        request.pageNumber = 1;

        allContainerItems.InitializeByQuery(request, function () {
            addtoMethodList(allContainerItems);

        });
    }


    function drawLine(ctx, startX, startY, endX, endY) {
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(endX, endY);
        ctx.stroke();
    };



    function drawPieSlice(ctx, centerX, centerY, radius, startAngle, endAngle, color) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(centerX, centerY);
        ctx.arc(centerX, centerY, radius, startAngle, endAngle);
        ctx.closePath();
        ctx.fill();
    };


    function addtoMethodList(allContainerItems) {
        var airMethod = 0;
        var seaMethod = 0;
        var bulkMethod = 0;
        var directMethod = 0;
        var foreignMethod = 0;

        allMethods = allContainerItems.molecules;
      
        for (var i = 0; i < allMethods.length; i++){

            currentContainer = allMethods[i];

            currentMethod = currentContainer.atom('ContainerChangeMethod').displayValue;
     
            if (currentMethod == 'A') {
                airMethod++;
            }
            if (currentMethod == 'S') {
                seaMethod++;
            }
            if (currentMethod == 'B') {
                bulkMethod++;
            }
            if (currentMethod == 'D') {
                directMethod++;
            }
            if (currentMethod == 'F') {
                foreignMethod++;
            }
          
        }
       
        myMethods = {
            "Air": airMethod,
            "Containers": seaMethod,
            "Break Bulk": bulkMethod,
            "Vendor Direct": directMethod,
            "Foreign" : foreignMethod
        };

        var myLegend = document.getElementById("myLegend");

        var myPiechart = new Piechart(
            {
                canvas: myCanvas,
                data: myMethods,
                colors: ["#fde23e", "#f16e23", "#57d9ff", "#937e88", "#53BF44"],
                legend: myLegend
            }
        );
        $('#myspinner').remove();
        $('#loadtitle').remove();
        myPiechart.draw();

    };

    function Piechart(options) {
        this.options = options;
        this.canvas = options.canvas;
        this.ctx = this.canvas.getContext("2d");
        this.colors = options.colors;

        this.draw = function () {
            var total_value = 0;
            var color_index = 0;
            for (var categ in this.options.data) {
                var val = this.options.data[categ];
                total_value += val;
            }

            var start_angle = 0;
            for (categ in this.options.data) {
                val = this.options.data[categ];
                var slice_angle = 2 * Math.PI * val / total_value;

                drawPieSlice(
                    this.ctx,
                    this.canvas.width / 2,
                    this.canvas.height / 2,
                    Math.min(this.canvas.width / 2, this.canvas.height / 2),
                    start_angle,
                    start_angle + slice_angle,
                    this.colors[color_index % this.colors.length]

                );

                start_angle += slice_angle;
                color_index++;

            }

            if (this.options.doughnutHoleSize) {
                drawPieSlice(
                    this.ctx,
                    this.canvas.width / 2,
                    this.canvas.height / 2,
                    this.options.doughnutHoleSize * Math.min(this.canvas.width / 2, this.canvas.height / 2),
                    0,
                    2 * Math.PI,
                    "#ff0000"

                );
            }

            start_angle = 0;
            for (categ in this.options.data) {
                val = this.options.data[categ];
                slice_angle = 2 * Math.PI * val / total_value;
                var pieRadius = Math.min(this.canvas.width / 2, this.canvas.height / 2);
                var labelX = this.canvas.width / 2 + (pieRadius / 2) * Math.cos(start_angle + slice_angle / 2);
                var labelY = this.canvas.height / 2 + (pieRadius / 2) * Math.sin(start_angle + slice_angle / 2);

                if (this.options.doughnutHoleSize) {
                    var offset = (pieRadius * this.options.doughnutHoleSize) / 2;
                    labelX = this.canvas.width / 2 + (offset + pieRadius / 2) * Math.cos(start_angle + slice_angle / 2);
                    labelY = this.canvas.height / 2 + (offset + pieRadius / 2) * Math.sin(start_angle + slice_angle / 2);
                }

                var labelText = Math.round(val);
                this.ctx.fillStyle = "black";
                this.ctx.font = "bold 20px Arial";
                this.ctx.fillText(labelText + " Bookings", labelX, labelY);
                start_angle += slice_angle;
            }

            if (this.options.legend) {
                color_index = 0;
                var legendHTML = "";
                for (categ in this.options.data) {
                    legendHTML += "<div><span style='display:inline-block;width:20px;background-color:" + this.colors[color_index++] + ";'>&nbsp;</span> " + categ + "</div>";
                }
                this.options.legend.innerHTML = legendHTML;
            }
        }
    }

    
    $('.fa-spinner').remove();
    

    self.buildNavigation = function(){
        
        var currentURLinNavItems = false;
        for (var x = 0; x < navigationItems.molecules.length; x++) {
            var thisNavItem = navigationItems.molecules[x];
            var isUserPage = false;
            var exists = thisNavItem.ValueExistsOnOff;
            if (exists) {
                if (thisNavItem.NavigationItemName == 'User Management') {
                    isUserPage = true;
                }
                if (isUserPage) {
                    if (user.Usernameadmin) {
                        parentItems.push(thisNavItem);
                    }
                } else {
                    if (thisNavItem.NavigationParentExposedId == undefined){
                        parentItems.push(thisNavItem);
                    } else {
                        subMenuItems.push(thisNavItem);

                    }
                }
            }
        }

        var getFormId;
        var getCustom = "";
        for (var o = 0; o < subMenuItems.length; o++) {
            var thisSubItem = subMenuItems[o];
            for (var b = 0; b < thisSubItem.attributes.length; b++) {
                var checkAttr = thisSubItem.attributes[b];
                if (checkAttr.name == 'Navigation Item Name') {
                    getFormId = checkAttr.valueThingId;
                }
                if (checkAttr.name == 'Navigation Item Custom URL') {
                    if (checkAttr.valueText != undefined) {
                        getCustom = checkAttr.valueText;
                    }
                }
                if (baseurl.includes(getCustom) || baseurl.includes(getFormId)) {
                    currentURLinNavItems = true;
                }
            }
        }

        navPane = '';

        for (var i = 0; i < parentItems.length; i++) {

            var menu = parentItems[i];
            var menuId = menu.header.exposedId;
            var menuName = menu.NavigationItemName;
        
            var menuLink = `<li class="nav-item dropdown" id="` + menuId + `">
                            <a href="#" class="nav-link">
                                <span>`+ menuName + `</span>
                            </a>
                            <nav class="submenu">
                                <ul class="submenu-items">`;

            childItems = [];

            for (var p = 0; p < subMenuItems.length; p++) {
                var thisSub = subMenuItems[p];
                for (var m = 0; m < thisSub.attributes.length; m++) {
                    var thisAttr = thisSub.attributes[m];
                    if (thisAttr.name == 'Navigation Parent') {
                        if (thisAttr.valueThingId.toLowerCase() == menuId.toLowerCase()) {
                            childItems.push(thisSub);
                        }
                    }
                }
            }

            for (var a = 0; a < childItems.length; a++) {
                var subMenu = childItems[a];
                var subMenuName = '';
                var customURL = '';
                var formId = '';
                for (var g = 0; g < subMenu.attributes.length; g++) {
                    var subAttr = subMenu.attributes[g];
                    if (subAttr.name == 'Navigation Item Name') {
                        subMenuName = subAttr.valueText;
                    }
                    if (subAttr.name == 'Navigation Item Name') {
                        formId = subAttr.valueThingId;
                    }
                    if (subAttr.name == 'Navigation Item Custom URL') {
                        if (subAttr.valueText != undefined) {
                            customURL = subAttr.valueText;
                        }
                    }
                }
                var subMenuStr = '';

                if (customURL != '') {
                    subMenuStr = `<li class="submenu-item">
                               <a href="`+ customURL + `" class="submenu-link pagelink">
                                        `+ subMenuName + `
                               </a>
                          </li>`;
                } else {
                    subMenuStr = `<li class="submenu-item">
                                <a href="/UserDynamic?formId=`+ formId + `" asp-action="` + subMenu.header.exposedId + `" class="submenu-link pagelink">
                                    `+ subMenuName + `
                                </a>
                            </li>`;
                }
                menuLink += subMenuStr;
            }
            menuLink += `</ul></nav></li>`;

            $('#setNavItemsHere').append(menuLink);

        }

        var logoutLink = `<li class="nav-item">
                        <a class="nav-link pagelink user-profile-nav-item" href="/Account/Logout">Logout</a>
                      </li>`;

        $('#setNavItemsHere').append(logoutLink);

        $(".dropdown").on('click', function (e) {
            var getElement = e.currentTarget.id;
            var thisElement = $('#' + getElement);
            if (thisElement.hasClass('show-submenu')) {
                thisElement.removeClass('show-submenu');
                return;
            } else {
                thisElement.addClass('show-submenu');
                return;
            }
        });

    };
});
