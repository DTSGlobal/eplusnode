$(function () {

    var DynamicForm = function () {
        console.log('UserDynamic initializing');
        var self = this;
        var domainThing = new DTS.Molecule();
        self.thisThing = new DTS.Molecule();
        var headerForm = new DTS.Molecule();
        var headerFormElements = new DTS.Things();
        var isCreate = false;
        var utilThing = new DTS.Things();
        var request = {};
        var headerSearchResults = DTS.Things();
        var baseQueryThing = new DTS.Things();
        var isSearch = false;
        var allowSave = false;
        var gridDropDowns = {};
        var allowExportToExcel = false;
        var allowInLineDelete = false;
        var childForms = new DTS.Things();
        var childFormResults = {};
        var expandElements = [];
        var selectedSortField = '';
        var selectedSortDirection = '';
        var accessoryData = {}; //This is the extra data for ValueThings for a given query. Need to persist it for when we add a row to a table;
        var headerId = '';
        var companyId = $('#navigationCompanyId').val();
        var projectId = $('#sessionProject').val();
        var page = 1;
        var totalResultCount = 0;
        var codeThings = new DTS.Things();
        //Chevron-only
        //var userThing = {};
        var newPersonnelThing = {};
        var currentUniverse = DTS.Molecule();
        var timelineMolecule = DTS.Molecule();
        var selectedSequence = 0;
        var valueThingExposedIds = [];
        self.rowMolecule = new DTS.Molecule();

        var twoColumnLayout = `<div class="main-body mt-4">
                                        <h2 id="header"></h2>
                                        <div class="row" id="headerRow">
                                            <div class="col-sm-12 col-md-8 col-lg-3" style="margin-left:3%" id="columnOne">
                                            </div>
                                            <div class="col-sm-12 col-md-8 col-lg-3 offset-2 offset-md-3" id="columnTwo">
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left:3%" id="contentSeparator">
                                        </div>
                                        <div class="row" style="margin-left:3%" id="pbResultsGrid">

                                            
                                        </div>
                                    </div>
                                </div>`;

        $('#loadingTitle').append(twoColumnLayout);

        self.initialize = function () {

            var baseURL = window.location.href.toLowerCase();
            var startIds = baseURL.indexOf("userdynamic/");
            var basePath = baseURL.substring(startIds + 12);
            var basePathArray = basePath.split('/');
            var formId = $('#formId').val();

            utilThing.getForm(formId, function (result) {

                id=$('#id').val()

                utilThing.removeInactive();
                currentUniverse.CreateByExposedId(utilThing.molecules[0].header.au.exposedId);

                headerForm = utilThing.byTypeName('Form')[0];

                if (headerForm.Type === 'Custom') {
                    self.renderUserJS();
                    return;
                }

                isSearch = headerForm.Type == 'Search';
                headerFormElements.molecules = utilThing.byTypeName('Form Field');
                headerSearchResults.molecules = utilThing.byPropertyValue('Type', 'Result');

                headerSearchResults.sortThingsByMultipleAttributes([
                    {
                        property: 'Sequence',
                        dir: 'asc'
                    }
                ]);
                domainThing = utilThing.byExposedId(headerForm.DomainObjectExposedId);
                if (isSearch) {
                    allowSave = headerForm.AllowEdit;
                    self.populateSearchResultDropDowns();

                    var allowExportToExcelAtom = headerForm.atomByPropertyName('AllowExport');
                    allowExportToExcel = allowExportToExcelAtom.valueLong == 1;
                }

                if (id=="00000000-0000-0000-0000-000000000000") {
                    self.thisThing.CreateByType(domainThing, eMESA.returnGuid(), '');
                    headerId = self.thisThing.header.exposedId;
                    self.loadFormHeaderElements(self.thisThing);


                }
                else {
                    self.thisThing.CreateByExposedId(id, function (result) {
                        self.loadFormHeaderElements(self.thisThing);
                        self.loadTimelineElements();
                    });

                }

                //Get list of child forms (drill down, since child forms can have child forms)
                var childFormId = headerForm.atomByPropertyName('Title');

                //Need child forms to refer back to their parents, which is how it should have been set up 
                //in the first place
                var lastHeaderId = headerForm.header.exposedId;
                if (childFormId.valueThingId != undefined && childFormId.valueThingId != null && childFormId.valueThingId != "fa2f459b-f277-46c6-8dd8-578785f35884") {
                    let childForm = utilThing.byExposedId(childFormId.valueThingId);

                    childForm.parentFormExposedId = lastHeaderId;
                    childForms.molecules.push(childForm);
                    lastHeaderId = childForm.header.exposedId;
                    childFormId = childForm.atomByPropertyName('Title');
                    if (childFormId.valueThingId != undefined && childFormId.valueThingId != null) {
                        let childForm = utilThing.byExposedId(childFormId.valueThingId);
                        if (childForm == undefined) {
                            childForm = new DTS.Molecule();
                            childForm.CreateByExposedId(childFormId.valueThingId, function () {
                                childForm.parentFormExposedId = lastHeaderId;
                                childForms.molecules.push(childForm);
                                lastHeaderId = childForm.header.exposedId;
                                childFormId = childForm.atomByPropertyName('Title');
                                utilThing.molecules.push(childForm);

                                var allQueryAttributes = [];
                                var queryAttributes = {};
                                queryAttributes.attributeName = 'Form';
                                queryAttributes.valueThingId = lastHeaderId;
                                allQueryAttributes.push(queryAttributes);

                                //queryAttributes = {};
                                //queryAttributes.attributeName = 'Type';
                                //queryAttributes.valueText = 'Result';
                                //allQueryAttributes.push(queryAttributes);

                                //queryAttributes = {};
                                //queryAttributes.attributeName = 'Input Type';
                                //queryAttributes.valueText = 'Label';
                                //allQueryAttributes.push(queryAttributes);

                                var request = {};
                                request.attributes = [];
                                request.attributes.push(queryAttributes);
                                request.columns = [];

                                request.DomainObjectId = '0101b7fa-7ea1-4d6c-b000-4dc480484020';
                                request.ExistsOnly = true;
                                request.friendlyColumns = [];
                                request.friendlyColumns = [
                                    'ValueExistsOnOff',
                                    'Type',
                                    'Form',
                                    'Column',
                                    'Link To Form',
                                    'Sequence',
                                    'Input Type',
                                    'Attribute Id',
                                    'Display Label',
                                    'Attribute Id'

                                ];
                                request.RowCount = 1000;
                                request.pageNumber = 1;
                                var formChildFields = new DTS.Things();
                                formChildFields.InitializeByQuery(request, function () {
                                    formChildFields.removeInactive();
                                    for (var z = 0; z < formChildFields.molecules.length; z++) {
                                        var thisField = formChildFields.molecules[z];
                                        if (thisField.Type == 'Result') {
                                            utilThing.molecules.push(formChildFields.molecules[z]);
                                        }
                                    }
                                });

                            });
                        } else {
                            childForm.parentFormExposedId = lastHeaderId;
                            childForms.molecules.push(childForm);
                            lastHeaderId = childForm.header.exposedId;
                            childFormId = childForm.atomByPropertyName('Title');
                        }
                    }
                    
                }

                if (childForms.molecules.length != 0) {
                    self.createHeaderFormChildForm(childForms.molecules[0]);
                }
            });
        };

        //This is a simple text-binary-text converter; Might want to put this in site.js or Things class
        var ABC = {
            toAscii: function (bin) {
                return bin.replace(/\s*[01]{8}\s*/g, function (bin) {
                    return String.fromCharCode(parseInt(bin, 2))
                });
            },
            toBinary: function (str, spaceSeparatedOctets) {
                return str.replace(/[\s\S]/g, function (str) {
                    str = ABC.zeroPad(str.charCodeAt().toString(2));
                    return !1 == spaceSeparatedOctets ? str : str + " "
                })
            },
            zeroPad: function (num) {
                return "00000000".slice(String(num).length) + num
            }
        };

        self.createHeaderFormChildForm = function (childForm) {

            var childFormAttribute = childForm.atomByPropertyName('Title');
            var childFormDisplayFields = new DTS.Things();
            childFormDisplayFields.molecules = utilThing.ByValueThingId('Form', childForm.header.exposedId);



            var fromParent = utilThing.byExposedId(childForm.parentFormExposedId);
            childFormAttribute = fromParent.atomByPropertyName('Title');
            var childFormValueThingAttributeId = childFormAttribute.valueGuid;


            var request = {};
            var attributes = [];

            for (var i = 0; i < valueThingExposedIds.length; i++) {
                var valueThingExposedId = valueThingExposedIds[i];
                var queryAttribute = {};
                queryAttribute.AttributeId = childFormValueThingAttributeId;
                queryAttribute.ValueThingId = valueThingExposedId;
                attributes.push(queryAttribute);
            }

            request.attributes = attributes;

            request.ExistsOnly = true;
            var resultsToReturn = $('#resultsCount').val();
            var FKs = [];
            var resultColumns = [];
            for (var y = 0; y < childFormDisplayFields.molecules.length; y++) {

                var searchResult = childFormDisplayFields.molecules[y];
                if (searchResult.AttributeId == '00000000-0000-0000-0000-000000000000') {
                    var thingFK = searchResult.atomByPropertyName('DisplayLabel');
                    var fkAttribute = searchResult.atomByPropertyName('AttributeId');

                    resultColumns.push(thingFK.valueThingId);
                    FKs.push(eMESA.fkQueryObjectFriendly(thingFK.valueThingName, fkAttribute.valueThingName));
                }
                else {
                    resultColumns.push(searchResult.AttributeId);
                }
            }

            fromParent = utilThing.byExposedId(childForm.parentFormExposedId);
            childFormAttribute = fromParent.atomByPropertyName('Title');
            childFormValueThingAttributeId = childFormAttribute.valueGuid;
            resultColumns.push(childFormValueThingAttributeId);

            request.columns = resultColumns;
            request.ForeignKeys = FKs;
            request.DomainObjectId = childForm.DomainObjectExposedId;
            request.ExistsOnly = true;
            request.RowCount = 500;
            request.pageNumber = 1;

            var childQueryThing = new DTS.Things();
            childQueryThing.InitializeByQuery(request, function (results) {

                console.log('Child results received');
                childFormResults[childForm.DomainObjectExposedId] = childQueryThing;
                
            });

        };

        self.renderUserJS = function () {

            $('#columnOne').remove();
            $('#columnTwo').remove();
            $('#bodyContainer').append(`<div id = "userContent"></div>`);
            $('#header').append(headerForm.Title);
            document.title = headerForm.Title;

            let myJS = ABC.toAscii(headerForm.UserScriptsBinary);
            $("body script").last().after(
                `<script id="myJS">${myJS}</script>`);

        };

        self.appendUserJS = function () {

            let myJS = ABC.toAscii(headerForm.UserScriptsBinary);
            $("body script").last().after(
                `<script id="myJS">${myJS}</script>`);

        };

        self.distinctFormAttributeIds = function () {

            var returnAttributeIds = [];
            for (var i = 0; i < headerFormElements.molecules.length; i++) {

                if (!returnAttributeIds.includes(headerFormElements.molecules[i].AttributeId) && headerFormElements.molecules[i].ValueExistsOnOff) {
                    returnAttributeIds.push(headerFormElements.molecules[i].AttributeId);
                }
            }
            return returnAttributeIds;
        };

        self.activeSequencesOnForm = function () {

            var sequences = [];
            var thisFormIds = self.distinctFormAttributeIds();
            var sequences = timelineMolecule.distinctSequenceIdsForThing(thisFormIds);
            return sequences;
        };


        self.loadTimelineElements = function () {

            if (headerForm.TimelineEnabled) {

                timelineMolecule.getFullById(self.thisThing.header.exposedId, function (result) {

                    var sequences = self.activeSequencesOnForm();
                    var tickStr = '<datalist id="ticks">';
                    for (var i = 0; i < sequences.length; i++) {
                        tickStr += `<option>${sequences[i]}</option>`;
                    }
                    tickStr += '</dataset>';

                    var timelineDiv = `<div class = "row" id="timelineRow">
                                                    <div class = "col-md-6" style="padding-bottom:30px">
                                                        <input id="timelineRange" type="range" min="${sequences[0]}" max="${sequences[sequences.length - 1]}" value="${sequences[sequences.length - 1]}" list="ticks" style="width:1000px" />
                                                    <p id="rangeResults"></p>    
                                                    </div>
                                                </div>
                                                `;
                    $('#headerRow').before(tickStr);
                    $('#headerRow').before(timelineDiv);

                    var $timeline = $('#timelineRange');
                    $('#timelineRange').on('change', $timeline, self.timelineRangeChanged);
                    $('#timelineRange').on('input', $timeline, function (e) {
                        $('#rangeResults').empty();
                        $('#rangeResults').append(e.target.value);
                    });
                });
            }
        };

        self.populateSearchResultDropDowns = function () {
            //Populating drop-downs individually/atomically doesn't make sense when there might be dozens of the same one in a results grid
            //so let's go get them now
            for (var i = 0; i < headerSearchResults.molecules.length; i++) {
                var displayElement = headerSearchResults.molecules[i];
                var displayTypeAtom = displayElement.atomByPropertyName('InputType');

                var formElement = headerSearchResults.molecules[i];
                var elementType = 'label';
                if (displayTypeAtom.valueLong == 1) {
                    elementType = self.thingElementName(displayTypeAtom.valueText);
                }

                if (elementType == 'select') {

                    var displayFields = '';
                    var valueThing = {};
                    var thingFK = displayElement.atomByPropertyName('DisplayLabel');
                    var fkAttribute = displayElement.atomByPropertyName('AttributeId');
                    var atom = domainThing.atomById(thingFK.valueThingId);
                    if (atom != undefined) {
                        valueThing = utilThing.byExposedId(atom.typeOfAttributeExposedId);
                        displayFields = fkAttribute.valueThingName;

                    }
                    else {

                        displayFields = self.displayFieldsForFormElement(displayElement, valueThing);
                    }
                    var displayItems = new DTS.Things();
                    var columns = [];
                    var queryAttributes = {};
                    queryAttributes.attributeName = 'ValueExistsOnOff';
                    queryAttributes.BigIntValue = 1;

                    var request = {};
                    request.attributes = [];
                    request.attributes.push(queryAttributes);
                    request.columns = columns;//[];
                    request.DomainObjectId = atom.typeOfAttributeExposedId;
                    request.friendlyColumns = displayFields.split(',');
                    request.friendlyColumns.push('ValueExistsOnOff');

                    //If we have a parent/child dependency between two elements, we need
                    //to include that element in the child's query;
                    request.RowCount = 150000;
                    request.pageNumber = 1;

                    displayItems.InitializeByQuery(request, function (results) {

                        codeThings.molecules = codeThings.molecules.concat(results);
                    });
                }
            }
        };

        self.thingElementName = function (formElementName) {

            switch (formElementName) {

                case 'FreeText':
                    return 'textbox';
                    break;
                case 'TextBox':
                    return 'textbox';
                    break;
                case 'Dropdown':
                    return 'select';
                    break;
                case 'Autocompleter':
                    return 'autocompleter';
                    break;
                case 'Label':
                    return 'label';
                    break;
                case 'TextArea':
                    return 'textarea';
                    break;
                case 'Radio':
                    return 'radio';
                    break;

            }
        };

        self.timelineRangeChanged = function (e) {

            var sequences = self.activeSequencesOnForm(); //timelineMolecule.distinctSequenceIdsForThing();
            //console.log('Sequence count' + sequences.length);
            var idx = -1;
            var lastVal = -1;
            var thisVal = -1;
            //console.log('Incoming selection ' + e.target.value);
            for (var i = 0; i < sequences.length; i++) {

                thisVal = Math.abs(sequences[i] - e.target.value);
                var nextVal = 0;
                if (sequences.length > i + 1) {
                    nextVal = sequences[i + 1];
                }
                if (thisVal < lastVal || thisVal == 0 || (i == 0 && thisVal < nextVal)) {
                    idx = i;
                }

                lastVal = thisVal;
            }

            if (idx > -1) {

                $('#timelineRange').val(sequences[idx]);
                selectedSequence = sequences[idx];
            }
            console.log('selectedSequence: ' + selectedSequence);

            var atts = timelineMolecule.atomsBySequenceId(selectedSequence);

            var thisTime = new DTS.Molecule();

            var baseTime = JSON.parse(JSON.stringify(self.thisThing));
            thisTime.header = baseTime.header;
            thisTime.attributes = atts;
            thisTime.mapToObject();
            self.loadFormHeaderElements(thisTime);
        };

        self.loadFormHeaderElements = function (forThisThing) {

            $('#columnOne').empty();
            $('#columnTwo').empty();
            $('#contentSeparator').empty();
            SaveElements.formElements = [];
            $('#header').empty();
            $('#header').append(headerForm.Title);
            document.title = headerForm.Title;
            headerFormElements.sortThingsByMultipleAttributes([
                {
                    property: 'Column',
                    dir: 'asc'
                }
                , {
                    property: 'Sequence',
                    dir: 'asc'
                }
            ]);

            var propertyList = [];

            //for (var i = 0; i < thisThing.attributes.length; i++) {

            //    console.log('Thing: ' + domainThing.header.exposedId + ' Attribute: ' + domainThing.attributes[i].label + ' exposedId: ' + domainThing.attributes[i].exposedId);
            //}

            for (var i = 0; i < headerFormElements.molecules.length; i++) {

                var formElement = headerFormElements.molecules[i];
                if (formElement.Type == 'Result' || formElement.AttributeId == null) {
                    continue;
                }

                if (formElement.Column < 1 || formElement.Column > 2) {
                    formElement.Column = 1;
                }
                var appendTo = '';
                if (formElement.Column == 1) {
                    appendTo = 'columnOne';
                }
                if (formElement.Column == 2) {
                    appendTo = 'columnTwo';
                }
                if (formElement.Column == null) {
                    appendTo = 'columnOne';
                }
                var displayFields = '';
                var atom = domainThing.atomById(formElement.AttributeId);
                if (atom == undefined || atom.name == undefined) {
                    continue;
                }

                //Check to see if the attribute we're looking for exists yet
                if (selectedSequence > 0) {

                    var historicalAtom = forThisThing.atomById(formElement.AttributeId);
                    if (historicalAtom == undefined || historicalAtom.name == undefined) {
                        continue;
                    }
                }

                var propertyName = atom.name.replace(/\s/g, '');

                if (propertyList.includes(propertyName)) {
                    continue;
                }
                else {
                    propertyList.push(propertyName);
                }

                var displayFields = '';
                var searchFields = '';
                var valueThing = new DTS.Molecule();
                var isMultiSelect = false;

                var acSortDefinition = {};
                if (atom.dataType == 'ValueThingId') {
                    if (headerForm.Type == 'Search') {
                        isMultiSelect = true;
                    }
                    if (atom.typeOfAttributeExposedId == undefined) {
                        continue;
                    }
                    valueThing = utilThing.byExposedId(atom.typeOfAttributeExposedId);

                    var onHydrateCallback = null;
                    //if (SiteLoginAdministrator == 'False' && (valueThing.header.name == 'Code Company' || valueThing.header.name == 'Project') && headerForm.Title != 'Search RFI') {
                    //    onHydrateCallback = self.applyFilters;
                    //}

                    displayFields = self.displayFieldsForFormElement(formElement, valueThing);
                    searchFields = self.searchFieldsForFormElement(formElement, valueThing);
                }

                var elementType = '';
                if (atom.dataType == 'ValueDatetime') {
                    elementType = 'datepicker';
                }
                else {
                    elementType = self.thingElementName(formElement.InputType);
                }
                displayFields = displayFields.replace(/\s/g, '');

                let isFreeText = formElement.InputType == 'FreeText';


                new DTS.ThingElements().addElement({
                    elementType: elementType,
                    label: formElement.Label,
                    thing: forThisThing,
                    property: propertyName,
                    appendTo: appendTo,
                    prependTo: null,
                    includeFormGroup: true,
                    displayFields: displayFields,
                    searchFields: searchFields,
                    valueThing: valueThing,
                    isMultiSelect: isMultiSelect,
                    isSearch: isSearch,
                    onHydrateCallback: onHydrateCallback,
                    autocompleteTypeId: atom.typeOfAttributeExposedId,
                    autocompleteMinimumCharacters: 2,
                    addHandler: self.resetPage,
                    isFreeText: isFreeText
                });

                var thisAtom = self.thisThing.atomByPropertyName(propertyName);
                if (thisAtom.valueSequenceId == selectedSequence) {

                    var thisElement = SaveElements.formThingByThingAndAtom(self.thisThing, thisAtom);
                    //var thisDomElement = $('#' + thisElement[0].id);
                    $('#' + thisElement[0].id).animate({
                        backgroundColor: 'yellow'
                    }, 500);

                }

            }



            if (isSearch) {

                var searchObjectLabel = headerForm.Title;
                var searchButtonStr = '<div>';
                searchButtonStr += '<div class="row mt-3" id="searchButtonContainer">';
                searchButtonStr += '<div class="col-12"><input type="submit" class="btn btn-block btn-lg btn-outline-primary clickable dynamicSearch" id="searchButton" value="' + searchObjectLabel + '" style="width:450px" /></div>';
                searchButtonStr += '</div>';
                if (allowSave) {
                    searchButtonStr += '<div class="row mt-3"><span class="pull-left"><div class="col-12"><button class="btn btn-outline-success save-changes-button" id="saveGridResults" style="">Save Changes</button></span></div></div>';
                    //$('#contentSeparator').append(searchButtonStr);
                }
                if (allowExportToExcel) {
                    searchButtonStr += '<div class="row mt-3"><div class="col-12"><span class="pull-left"><button class="btn btn-block btn-lg btn-info clickable" id="exportToExcelButton">Download to Excel</button></span></div></div>';
                }

                searchButtonStr += '<div class="row" id="resultsCountContainer" >';
                searchButtonStr += '<div class="col-12">';
                searchButtonStr += '<label><strong>Show</strong> <select name="resultsCount" id="resultsCount" aria-controls="DataTables_Table_0" class="form-control input-sm">';
                searchButtonStr += '<option value="25">25</option>';
                searchButtonStr += '<option value="100">100</option>';
                searchButtonStr += '<option value="500">500</option>';
                searchButtonStr += '</select> entries</label></div>';
                searchButtonStr += '</div>';
                searchButtonStr += '</div>';//</div></div>

                //$('#bodyContainer').append(searchButtonStr);
                $('#contentSeparator').append(searchButtonStr);
                var $searchButton = $('#searchButton');
                $searchButton.on('click', $searchButton, self.search);

                if (allowSave) {
                    var $saveButton = $('#saveGridResults');
                    $saveButton.on('click', $saveButton, self.saveThings);
                    $saveButton.hide();
                    //$('#saveGridResults').attr('disabled', 'disabled')
                    SaveElements.saveBtnId = $saveButton;
                }
                if (allowExportToExcel) {
                    var $exportButton = $('#exportToExcelButton');
                    $exportButton.on('click', $exportButton, self.exportToExcel);
                    $exportButton.hide();
                }
                var resultSelect = $('#resultsCount');
                $(resultSelect).on('change', $(resultSelect), self.resultCountChanged);

                //CHEVRON ONLY
                //if (!SiteLoginAdministrator) {

                //}

            }
            else {
                var saveObjectLabel = 'Save ' + headerForm.Title;
                var saveButtonStr = '<input type="submit" class="button btn btn-success" id="saveButton" value="' + saveObjectLabel + '" />'
                $('#contentSeparator').append(saveButtonStr);
                var $saveButton = $('#saveButton');
                $saveButton.on('click', $saveButton, self.saveThings);
                SaveElements.saveBtnId = $saveButton;
            }

            if (headerForm.UserScriptsBinary != null) {
                self.appendUserJS();
            }


        };

        self.applyFilters = function (thingElement) {

            if (thingElement.atom.typeOfAttributeExposedId == '6f11d44a-a629-4a1e-80f5-5b29d482e25a' && companyId.length > 0) {//Company
                $('#' + thingElement.id + ' option[value=' + companyId + ']').prop('selected', true);
                $('#' + thingElement.id).triggerHandler('change');
                $('#' + thingElement.id).attr('disabled', 'disabled');
            }
            if (thingElement.atom.typeOfAttributeExposedId == 'd29fb72b-9718-4004-8d3c-4c9f8315d822' && projectId.length > 0) {//Company
                $('#' + thingElement.id + ' option[value=' + projectId + ']').prop('selected', true);
                $('#' + thingElement.id).triggerHandler('change');
                $('#' + thingElement.id).attr('disabled', 'disabled');
            }
        };

        self.resetPage = function () {
            page = 1;
        };

        self.resultCountChanged = function (e) {

            page = 1;
            self.search(e);
        };

        self.search = function (e) {

            e.stopPropagation();
            e.preventDefault();

            $('#searchButton').attr('disabled', 'disabled');

            self.molecules = [];
            totalResultCount = 0;

            var attributes = [];
            if (SaveElements.filterMolecules.length > 0) {
                for (var i = 0; i < SaveElements.filterMolecules[0].attributes.length; i++) {

                    var selectedFilterAttributes = SaveElements.filterMolecules[0].attributes[i];
                    var queryAttribute = {};
                    queryAttribute.AttributeId = selectedFilterAttributes.exposedId.replace('max', '');

                    switch (selectedFilterAttributes.dataType) {
                        case 'ValueText':
                            if (selectedFilterAttributes.valueText.length > 0) {
                                queryAttribute.ValueText = selectedFilterAttributes.valueText;
                                attributes.push(queryAttribute);
                            }
                            break;
                        case 'ValueLong':
                            if (selectedFilterAttributes.valueLong != null) {
                                queryAttribute.ValueLong = selectedFilterAttributes.valueLong;
                                attributes.push(queryAttribute);
                            }
                            break;
                        case 'ValueDatetime':
                            //If we've already set a min or max value for this attribute, find it
                            var matchAttribute = {};
                            var attributeExists = false;
                            for (var y = 0; y < attributes.length; y++) {
                                if (attributes[y].AttributeId == queryAttribute.AttributeId) {
                                    queryAttribute = attributes[y];
                                    attributeExists = true;
                                    break;
                                }
                            }
                            if (selectedFilterAttributes.isMax != undefined && selectedFilterAttributes.isMax) {
                                queryAttribute.DateMax = selectedFilterAttributes.valueDateTime;
                            }
                            else {
                                queryAttribute.DateMin = selectedFilterAttributes.valueDateTime;
                            }
                            if (!attributeExists) {
                                attributes.push(queryAttribute);
                            }
                            break;
                        case 'ValueExistsOnOff':

                            break;
                        case 'ValueBoolean':
                            queryAttribute.BigIntValue = selectedFilterAttributes.valueLong;
                            attributes.push(queryAttribute);
                            break;
                        case 'ValueLabel':
                            queryAttribute.ValueText = SaveElements.filterMolecules[0].header.label;
                            attributes.push(queryAttribute);
                            break;
                        case 'ValueThingId':
                            if (selectedFilterAttributes.valueThingId != null) {
                                if (Array.isArray(selectedFilterAttributes.valueThingId)) {
                                    for (var x = 0; x < selectedFilterAttributes.valueThingId.length; x++) {
                                        var valueThingqueryAttribute = {}
                                        valueThingqueryAttribute.AttributeId = selectedFilterAttributes.exposedId;
                                        valueThingqueryAttribute.ValueThingId = selectedFilterAttributes.valueThingId[x];
                                        attributes.push(valueThingqueryAttribute);
                                    }
                                }
                                else {
                                    queryAttribute.AttributeId = selectedFilterAttributes.exposedId;
                                    queryAttribute.ValueThingId = selectedFilterAttributes.valueThingId;
                                    attributes.push(queryAttribute);
                                }
                            }
                            break;
                        case 'ValueFloat':
                            var matchAttribute = {};
                            var attributeExists = false;
                            for (var y = 0; y < attributes.length; y++) {
                                if (attributes[y].AttributeId == queryAttribute.AttributeId) {
                                    queryAttribute = attributes[y];
                                    attributeExists = true;
                                    break;
                                }
                            }

                            if (selectedFilterAttributes.isMax != undefined && selectedFilterAttributes.isMax) {
                                queryAttribute.FloatMax = selectedFilterAttributes.valueFloat;
                            }
                            else {
                                queryAttribute.FloatMin = selectedFilterAttributes.valueDecimal;
                            }
                            if (!attributeExists) {
                                attributes.push(queryAttribute);
                            }
                            break;
                        case 'ValueBigint':
                            break;
                        default:
                            break;
                    }
                }
            }

            request.attributes = attributes;
            var attributeSet = false;
            var resultsToReturn = $('#resultsCount').val() - 1;
            var FKs = [];
            var resultColumns = [];
            for (var y = 0; y < headerSearchResults.molecules.length; y++) {

                var searchResult = headerSearchResults.molecules[y];
                if (searchResult.AttributeId == '00000000-0000-0000-0000-000000000000') {
                    var thingFK = searchResult.atomByPropertyName('DisplayLabel');
                    var fkAttribute = searchResult.atomByPropertyName('AttributeId');

                    resultColumns.push(thingFK.valueThingId);
                    FKs.push(eMESA.fkQueryObjectFriendly(thingFK.valueThingName, fkAttribute.valueThingName));
                }
                else {
                    if (!resultColumns.includes(searchResult.AttributeId)) {
                        resultColumns.push(searchResult.AttributeId);
                        var nativeAtom = domainThing.atomById(searchResult.AttributeId);
                        if (nativeAtom != undefined && selectedSortField == '') {
                            selectedSortField = searchResult.AttributeId;
                            selectedSortDirection = 'Ascending';
                        }

                    }
                }
            }

            var sortKeys = [];
            var sortKey = {
                OrderNumber: 1,
                AttributeId: selectedSortField,
                SortOrder: selectedSortDirection
            };
            sortKeys.push(sortKey);
            request.columns = resultColumns;
            request.ForeignKeys = FKs;
            request.DomainObjectId = headerForm.DomainObjectExposedId;
            //request.ExistsOnly = true;
            request.RowCount = resultsToReturn;
            request.pageNumber = page;
            request.SortKeys = sortKeys;

            $('#pbResultsGrid').empty();
            $('#pbResultsGrid').append('<i class="fa fa-spin fa-spinner" id="loadSpinner"></i>');

            baseQueryThing.InitializeByQuery(request, function (results) {

                baseQueryThing.removeInactive();
                baseQueryThing.removeDuplicates();
                console.log('Results received');

                var tableTitle = headerForm.DomainObjectName.replace(/\s/g, '') + '_results';
                self.displayQueryResults({
                    data: baseQueryThing,
                    tableTitle: tableTitle,
                    depth: 0,
                    appendTo: 'pbResultsGrid',
                    forForm: headerForm,
                    level: 1
                });
            });
        };

        self.childFormExpanded = function (e) {
            e.preventDefault();
            var $target = $(e.target);
            var expandId = e.target.id;
            var ids = expandId.split('_expand_');
            var rowMoleculeExposedId = ids[0];
            var valueThingExposedId = ids[1];

            var tableTitle = rowMoleculeExposedId + '_results';
            var expandKey = tableTitle + '_holder';

            if (expandElements.includes(expandKey)) {

                if ($target.hasClass('fa-compress')) {
                    $('#' + expandKey).hide();
                }
                else {
                    $('#' + expandKey).show();
                }
            }
            else {

                var childForm = childForms.byExposedId(valueThingExposedId);
                var theseChildResults = childFormResults[childForm.DomainObjectExposedId];

                var fromParent = utilThing.byExposedId(childForm.parentFormExposedId);
                var childFormAttribute = fromParent.atomByPropertyName('Title');
                var childFormValueThingAttributeId = childFormAttribute.valueGuid;

                var childFormFields = self.resultFieldsByHeaderFormId(childForm.header.exposedId);
                childFormFields.sortThingsByMultipleAttributes(
                    [
                        {
                            property: 'Sequence',
                            dir: 'asc'
                        }
                    ]
                );

                var valueThingAttributeName = theseChildResults.molecules[0].atomById(childFormValueThingAttributeId).name;
                var results = new DTS.Things();
                results.molecules = theseChildResults.ByValueThingId(valueThingAttributeName, rowMoleculeExposedId);

                //Should sort by the first field in the child form
                var sortFieldName = results.molecules[0].atomById(childFormFields.molecules[0].AttributeId).name;

                results.sortThingsByMultipleAttributes([
                    {
                        property: sortFieldName,
                        dir: 'asc'
                    }
                ]);

                var appendTo = 'row_' + rowMoleculeExposedId;
                var appendElement = $('#' + appendTo);

                self.displayQueryResults({
                    data: results,
                    tableTitle: tableTitle,
                    depth: 1,
                    appendTo: 'row_' + rowMoleculeExposedId,
                    forForm: childForm,
                    isSubTable: true,
                    level: 0
                });

                expandElements.push(expandKey);
            }

            if ($target.hasClass('fa-compress')) {

                $target.removeClass('fa-compress').addClass('fa-expand').click(false);
            }
            else {
                $target.removeClass('fa-expand').addClass('fa-compress').click(false);

            }
        };

        self.valueThingSearch = function (childForm, valueThingExposedIds) {

            var childFormAttribute = childForm.atomByPropertyName('Title');
            var childFormDisplayFields = new DTS.Things();
            childFormDisplayFields.molecules = utilThing.ByValueThingId('Form', childForm.header.exposedId);

            var fromParent = utilThing.byExposedId(childForm.parentFormExposedId);
            var childFormAttribute = fromParent.atomByPropertyName('Title');
            var childFormValueThingAttributeId = childFormAttribute.valueGuid;


            var request = {};
            var attributes = [];

            for (var i = 0; i < valueThingExposedIds.length; i++) {
                var valueThingExposedId = valueThingExposedIds[i];
                var queryAttribute = {};
                queryAttribute.AttributeId = childFormValueThingAttributeId;
                queryAttribute.ValueThingId = valueThingExposedId;
                attributes.push(queryAttribute);
            }

            request.attributes = attributes;

            request.ExistsOnly = true;
            var resultsToReturn = $('#resultsCount').val();
            var FKs = [];
            var resultColumns = [];
            for (var y = 0; y < childFormDisplayFields.molecules.length; y++) {

                var searchResult = childFormDisplayFields.molecules[y];
                if (searchResult.AttributeId == '00000000-0000-0000-0000-000000000000') {
                    var thingFK = searchResult.atomByPropertyName('DisplayLabel');
                    var fkAttribute = searchResult.atomByPropertyName('AttributeId');

                    resultColumns.push(thingFK.valueThingId);
                    FKs.push(eMESA.fkQueryObjectFriendly(thingFK.valueThingName, fkAttribute.valueThingName));
                }
                else {
                    resultColumns.push(searchResult.AttributeId);
                }
            }

            var fromParent = utilThing.byExposedId(childForm.parentFormExposedId);
            var childFormAttribute = fromParent.atomByPropertyName('Title');
            var childFormValueThingAttributeId = childFormAttribute.valueGuid;
            resultColumns.push(childFormValueThingAttributeId);

            request.columns = resultColumns;
            request.ForeignKeys = FKs;
            request.DomainObjectId = childForm.DomainObjectExposedId;
            request.ExistsOnly = true;
            request.RowCount = 500;
            request.pageNumber = 1;

            var childQueryThing = new DTS.Things();
            childQueryThing.InitializeByQuery(request, function (results) {

                console.log('Child results received');
                childFormResults[childForm.DomainObjectExposedId] = childQueryThing;
                for (var i = 0; i < valueThingExposedIds.length; i++) {

                    //fa fa-expand clickable

                    var resultMoleculeExposedId = valueThingExposedIds[i];
                    var $expandButton = $('#' + resultMoleculeExposedId + '_expand_' + childForm.header.exposedId);
                    $expandButton.removeClass('fa-spin');
                    $expandButton.removeClass('fa-spinner');

                    //Only display the expand icon if there actually are child results to display
                    var fromParent = utilThing.byExposedId(childForm.parentFormExposedId);
                    var childFormAttribute = fromParent.atomByPropertyName('Title');
                    var childFormValueThingAttributeId = childFormAttribute.valueGuid;

                    var valueThingAttributeName = childQueryThing.molecules[0].atomById(childFormValueThingAttributeId).name;
                    var results = new DTS.Things();
                    results.molecules = childQueryThing.ByValueThingId(valueThingAttributeName, resultMoleculeExposedId);

                    if (results.molecules.length > 0) {
                        $expandButton.addClass('fa-expand clickable').click(false);
                        $expandButton.on('click', $expandButton, self.childFormExpanded);
                    }
                }
            });

        };

        self.displayQueryResults = function (args) {

            $('#searchButton').attr('disabled', false);

            var childForm = {};
            //data, tableTitle, depth, appendTo
            if (allowSave) {
                var $saveButton = $('#saveGridResults');
                $saveButton.show();
            }
            if (allowExportToExcel) {
                $('#exportToExcelButton').show();
            }

            $('#loadSpinner').remove();
            //$('#' + args.appendTo).empty();

            var hasExpandButton = false;
            var allowInlineCreate = false;
            if (args.forForm != undefined) {
                var childFormId = args.forForm.atomByPropertyName('Title');
                if (childFormId.valueThingId != undefined && childFormId.valueThingId != null && childFormId.valueThingId != "fa2f459b-f277-46c6-8dd8-578785f35884") {
                    hasExpandButton = true;
                    childForm = childForms.byExposedId(childFormId.valueThingId);
                }
                allowInlineCreate = args.forForm.AllowEdit;
                allowInLineDelete = args.forForm.AllowInLineDelete;
                //allowInlineCreate = domainObjectId.valueLong == 1;
            }
            var displayColumnHeaders = [];
            if (hasExpandButton) {
                displayColumnHeaders.push('tableaction');
            }
            var rowThings = args.data.byTypeId(args.forForm.DomainObjectExposedId); //data.byTypeName('Asset');

            if (rowThings.length > 0) {
                totalResultCount = rowThings[0].resultCount;
            } else {
                totalResultCount = 0;
            }

            var childFormDisplayFields = self.resultFieldsByHeaderFormId(args.forForm.header.exposedId);
            accessoryData[args.forForm.DomainObjectExposedId] = args.data;

            //Need to keep track of which header fields are native and which aren't
            //Ones that are native we'll add a sort icon to
            var headerSort = {};

            for (var i = 0; i < childFormDisplayFields.molecules.length; i++) {

                var headerColumn = childFormDisplayFields.molecules[i];
                if (headerColumn.ValueExistsOnOff == 1) {
                    var fixLabel = headerColumn.DisplayLabel;
                    var replaceLabel = fixLabel.replace('$', '');
                    var replaceOther = replaceLabel.replace('%', '');
                    displayColumnHeaders.push(replaceLabel);

                    if (domainThing.atomById(headerColumn.AttributeId) != undefined) {
                        headerSort[replaceLabel] = 'sorting_' + headerColumn.AttributeId;
                    }
                    else {
                        headerSort[replaceLabel] = 'nosort';
                    }
                }
            }

            //Table header
            new DTS.ThingElements().addTableDynamic({
                headerLabel: '',
                id: args.tableTitle,
                headerFields: displayColumnHeaders,
                appendTo: args.appendTo,
                isSubTable: args.isSubTable,
                level: args.level,
                allowDelete: allowInLineDelete,
                hasExpandButton: hasExpandButton,
                headerSort: headerSort,
                toggleHandler: self.toggleSort
            });

            var tableId = 'DataTables_Table_' + args.tableTitle;

            //DataTables_Table_WorkOrder_results
            var tableAppendTo = args.tableTitle + 'body';

            var valueThingIds = [];
            var appendAddButton = false;
            for (var x = 0; x < rowThings.length; x++) {
                var rowMolecule = rowThings[x];

                if ((x == rowThings.length - 1) && allowInlineCreate) {
                    appendAddButton = true;
                }
                self.renderRowForMoleculeAndChildForm({
                    rowMolecule: rowMolecule,
                    forForm: args.forForm,
                    tableAppendTo: tableAppendTo,
                    allowDelete: allowInLineDelete,
                    hasExpandButton: hasExpandButton,
                    childForm: childForm,
                    childFormDisplayFields: childFormDisplayFields,
                    data: args.data,
                    appendAddButton: appendAddButton
                });

                if (hasExpandButton) {
                    //Bind the expand button
                    valueThingIds.push(rowMolecule.header.exposedId);
                }

            }

            if (args.level == 1) {
                var resultStr = '<div class="container" id="resultDetails" >';//<hr>'
                resultStr += '<div class = "row justify-content-lg-left" id="resultDetailsRow" ><div class = "col-lg-2">';

                var resultsToReturn = $('#resultsCount').val();
                if (totalResultCount == 0) {
                    resultStr += 'No results found';
                }
                else {

                    var minDisplay = 1;

                    if (page > 1) {

                        minDisplay = Number(page) * Number(resultsToReturn);

                        if (minDisplay > totalResultCount) {
                            minDisplay = totalResultCount - Number(rowThings.length);
                        }
                    }

                    var maxDisplay = (Number(minDisplay) + Number(resultsToReturn)) - 1;
                    if (maxDisplay > totalResultCount) {
                        maxDisplay = totalResultCount;
                    }
                    if (Number(minDisplay) + Number(resultsToReturn) == totalResultCount) {
                        maxDisplay = totalResultCount;
                    }
                    resultStr += 'Showing ' + minDisplay + ' - ' + maxDisplay + ' of ' + totalResultCount + ' results';
                }
                $('#pbResultsGrid').append(resultStr);

                $('#resultDetailsRow').append('<div class = "col-lg-2" >&nbsp;</div >');
                if (totalResultCount > 0) {
                    self.renderPaginationElement('resultDetailsRow');
                }
            }

            if (childForm.header != undefined) {
                self.valueThingSearch(childForm, valueThingIds);
            }
        };

        self.renderPaginationElement = function (appendTo) {

            var appendStr = '';
            var resultsToReturn = $('#resultsCount').val();
            var elementsToRender = 8; //totalResultCount / resultsToReturn;
            var allElementstoRender = totalResultCount / resultsToReturn;
            var midPoint = totalResultCount / 2;
            var resultTally = page * resultsToReturn;

            if (resultsToReturn >= totalResultCount) {
                return;
            }

            var rounded = Math.round(allElementstoRender);
            if ((allElementstoRender - rounded) > 0) {
                allElementstoRender++;
            }
            allElementstoRender = Math.round(allElementstoRender);

            var incrementer = 7;
            if (allElementstoRender < 8) {
                incrementer = allElementstoRender;
            }

            var modResult = totalResultCount % 7;

            appendStr += '<div class = "col-lg-8">';
            appendStr += '<div class="form-inline" >';

            var displayElem = 0;

            if (page > 1) {//allElementstoRender > 7 && 
                appendStr += '<div class="form-group">';
                appendStr += '<label class="control-label col-sm-2" id="searchResult_previous">Previous</label>';
                appendStr += '</div>';
            }

            var styleTag = '';
            var displayNumber = 0;

            var pagingElements = [];
            for (var i = 1; i <= incrementer; i++) {

                if (resultTally < midPoint) {
                    if (page == 1) {
                        displayNumber = i;
                    }
                    else if (page > 5) {
                        displayNumber = Math.abs((6 - page)) + i + 1;
                    }
                    else {
                        displayNumber = i;
                    }
                }
                else {
                    if (page == 1) {
                        displayNumber = i;
                    }
                    else if (page > 2) {
                        displayNumber = (allElementstoRender - (7 - i)) - (allElementstoRender - page);// //Math.abs(((Number(page)+1)- Number(allElementstoRender))) + i + 1;
                        console.log('Page: ' + page + ' displayNumber: ' + displayNumber);
                    }
                }

                if (page == displayNumber) {
                    styleTag = 'color="blue"';
                }
                else {
                    styleTag = 'color="black"';
                }

                if (allElementstoRender > 7) {

                    if (resultTally > midPoint) {

                        if (i == 2) {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  >...</label>';
                            appendStr += '</div>';
                        }
                        else if (i == 7) {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  ' + styleTag + ' ><font ' + styleTag + ' id="searchResult_' + allElementstoRender + '">' + allElementstoRender + '</font></label>';
                            appendStr += '</div>';
                            pagingElements.push(allElementstoRender);
                        }

                        else if (i == 1) {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  ' + styleTag + ' ><font ' + styleTag + ' id="searchResult_' + i + '">' + i + '</font></label>';
                            appendStr += '</div>';
                            pagingElements.push(i);
                        }
                        else {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  ><font ' + styleTag + ' id="searchResult_' + displayNumber + '">' + displayNumber + '</font></label>';
                            appendStr += '</div>';
                            pagingElements.push(displayNumber);
                        }

                    }
                    else {
                        if (i == 6) {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  >...</label>';
                            appendStr += '</div>';
                        }
                        else if (i == 7) {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  ' + styleTag + ' ><font ' + styleTag + ' id="searchResult_' + allElementstoRender + '">' + allElementstoRender + '</font></label>';
                            appendStr += '</div>';
                            pagingElements.push(allElementstoRender);
                        }
                        else if (i == 1) {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  ' + styleTag + ' ><font ' + styleTag + ' id="searchResult_' + i + '">' + i + '</font></label>';
                            appendStr += '</div>';
                            pagingElements.push(i);
                        }
                        else {
                            appendStr += '<div class="form-group">';
                            appendStr += '<label class="control-label col-sm-2"  ><font ' + styleTag + ' id="searchResult_' + displayNumber + '">' + displayNumber + '</font></label>';
                            appendStr += '</div>';
                            pagingElements.push(displayNumber);
                        }
                    }
                }
                else {
                    if (page == i) {
                        styleTag = 'color="blue"';
                    }
                    else {
                        styleTag = 'color="black"';
                    }


                    appendStr += '<div class="form-group">';
                    appendStr += '<label class="control-label col-sm-2"  ><font ' + styleTag + ' id="searchResult_' + i + '">' + i + '</font></label>';
                    appendStr += '</div>';
                    pagingElements.push(i);
                }
            }


            if (page < allElementstoRender) {//allElementstoRender > 7 && 
                appendStr += '<div class="form-group">';
                appendStr += '<label class="control-label col-sm-2" id="searchResult_next" >Next</label>';
                appendStr += '</div>';
            }

            appendStr += '</div>';
            appendStr += '</div>';
            $('#' + appendTo).append(appendStr);

            for (var i = 0; i < pagingElements.length; i++) {
                var pageNumber = pagingElements[i];
                var $label = $('#searchResult_' + pageNumber);
                $label.on('click', $label, self.pageSelected);
            }
            $('#searchResult_next').on('click', $label, self.pageSelected);
            $('#searchResult_previous').on('click', $label, self.pageSelected);

            //allElementstoRender
        };

        self.pageSelected = function (e) {

            var pageOrigin = e.target.id.replace('searchResult_', '');
            console.log('pageOrigin: ' + pageOrigin);
            if (pageOrigin == 'previous') {
                page--;
            }
            else if (pageOrigin == 'next') {
                page++;
            }
            else {
                page = pageOrigin;
            }
            console.log('Page: ' + page);
            self.search(e);
        };

        self.renderRowForMoleculeAndChildForm = function (args) {

            var rowMolecule = args.rowMolecule;
            var childForm = args.childForm;
            var tableAppendTo = args.tableAppendTo;
            var hasExpandButton = args.hasExpandButton;
            var childFormDisplayFields = args.childFormDisplayFields;
            var appendAddButton = args.appendAddButton;
            var forForm = args.forForm;

            var rowStyle = '';
            var rowStr = '<tr row class ="' + rowStyle + '" id="row_' + rowMolecule.header.exposedId + '" > ';

            if (hasExpandButton) {
                rowStr += '<td style="width:50px">';
                rowStr += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i9 class="fa fa-spin fa-spinner" id="' + rowMolecule.header.exposedId + '_expand_' + childForm.header.exposedId + '"  ></i>';
                rowStr += '</td>';
            }

            if (args.allowDelete) {
                rowStr += '<td class = "custom-row-actions" id= "customRowActions' + rowMolecule.header.exposedId + '">';
                rowStr += '<i class="fa fa-times-circle fa-lg delete-custom-row:before" id="' + rowMolecule.header.exposedId + '_delete' + '"  data-thing-id="' + rowMolecule.header.exposedId + '" data-thing-type="pallets" data-thing-order="Palletpallet_id" style="display:inline-block; color: darkred" ></i>';
                
            }

            for (var i = 0; i < childFormDisplayFields.molecules.length; i++) {
                var formElement = childFormDisplayFields.molecules[i];
                var displayLabel = formElement.DisplayLabel.replace(/\s/g, '');
                var fixLabel = displayLabel.replace('$', '');
                var replaceLabel = fixLabel.replace('%', '');
                //rowStr += '<td style="width:50px">';
                //rowStr += '<i class="fa fa-times-circle fa-lg delete-custom-row:before" id="' + boxItem.header.exposedId + '_delete' + '"  data-thing-id="' + boxItem.header.exposedId + '" data-thing-type="boxItems" style="display:inline-block; color: darkred" ></i>';
                //rowStr += '</td>';
                rowStr += '<td style="padding: 12px 12px; min-width:75" id = "' + rowMolecule.header.exposedId + replaceLabel + '"></td>'; //Placeholder for each TD; adding formElements below
            }
            rowStr += '</tr>';

            $('#' + tableAppendTo).append(rowStr);

            if (args.allowDelete) {
                var $deleteButton = $('#' + rowMolecule.header.exposedId + '_delete');
                $deleteButton.on('click', $deleteButton, self.deleteRow);

            }

            for (var i = 0; i < childFormDisplayFields.molecules.length; i++) {
                var onHydrateCallback = null;

                var displayElement = childFormDisplayFields.molecules[i];

                var fixElementLabel = displayElement.DisplayLabel.replace(/\s/g, '');
                var fixElementLabel1 = fixElementLabel.replace('$', '');
                var replaceElementLabel = fixElementLabel1.replace('%', '');
                var rowAppendTo = rowMolecule.header.exposedId + replaceElementLabel;

                var labelThing = new DTS.Molecule();
                var atom = {};
                var displayFields = '';
                var valueThing = {};

                if (displayElement.AttributeId == '00000000-0000-0000-0000-000000000000') {
                    var thingFK = displayElement.atomByPropertyName('DisplayLabel');
                    var fkAttribute = displayElement.atomByPropertyName('AttributeId');
                    atom = rowMolecule.atomById(thingFK.valueThingId);
                    if (atom != undefined) {
                        valueThing = utilThing.byExposedId(atom.typeOfAttributeExposedId);
                        displayFields = fkAttribute.valueThingName;
                        if (atom.valueThingId != undefined) {
                            labelThing = args.data.byExposedId(atom.valueThingId);
                        }

                    }
                }
                else {
                    atom = rowMolecule.atomById(displayElement.AttributeId);
                    if (atom != undefined) {
                        valueThing = utilThing.byExposedId(atom.typeOfAttributeExposedId);
                        displayFields = self.displayFieldsForFormElement(displayElement, valueThing);

                    }
                }
                if (atom == undefined || atom.name == undefined || displayElement.ValueExistsOnOff == 0) {
                    continue;
                }

                var displayTypeAtom = displayElement.atomByPropertyName('InputType');
                var elementType = 'label';
                if (displayTypeAtom.valueLong == 1) {
                    elementType = self.thingElementName(displayTypeAtom.valueText);
                }


                var propertyName = atom.name.replace(/\s/g, '');
                var includeFormGroup = false;
                var hrefLink = '';
                if (displayElement.LinkToForm != '00000000-0000-0000-0000-000000000000') {

                    hrefLink = '<a href="/userdynamic/' + displayElement.LinkToForm + '/' + rowMolecule.header.exposedId + '" target="_blank" > ';
                }

                var displayAtom = displayElement.atomByPropertyName('Column');
                if (displayAtom.name == 'Column' && displayAtom.valueText != null && displayAtom.valueText.length > 0 && displayAtom.existsOnOff) {
                    hrefLink = '<a href="' + displayAtom.valueText + '?id=' + rowMolecule.header.exposedId + '" target="_blank">';
                }
                displayFields = displayFields.replace(/\s/g, '');

                var dropDownContents = [];
                if (elementType == 'select') {//&& gridDropDowns[valueThing.header.exposedId] != undefined
                    dropDownContents = codeThings.byTypeId(valueThing.header.exposedId); //gridDropDowns[valueThing.header.exposedId];
                }


                new DTS.ThingElements().addElement({
                    elementType: elementType,
                    label: displayElement.Label,
                    thing: rowMolecule,
                    property: propertyName,
                    appendTo: rowAppendTo,
                    prependTo: null,
                    includeFormGroup: includeFormGroup,
                    displayFields: displayFields,
                    searchFields: self.searchFieldsForFormElement(displayElement, valueThing),
                    valueThing: valueThing,
                    isMultiSelect: false,
                    labelThing: labelThing,
                    hrefLink: hrefLink,
                    dropDownContents: dropDownContents,
                    autocompleteTypeId: atom.typeOfAttributeExposedId,
                    onHydrateCallback: onHydrateCallback,
                    autocompleteMinimumCharacters: 3
                });

                if (appendAddButton && (i == childFormDisplayFields.molecules.length - 1)) {

                    var appendButtonStr = '<td style="text-align: right" ><button class="btn btn-sm btn-outline-success add-custom-row" id="new_' + forForm.header.exposedId + 'TABLEAPPENDKEY' + tableAppendTo + '"  >New ' + forForm.DomainObjectName + '</button></td>';
                    $('#row_' + rowMolecule.header.exposedId).append(appendButtonStr);

                    var $addButton = $('#new_' + forForm.header.exposedId + 'TABLEAPPENDKEY' + tableAppendTo);
                    $addButton.on('click', $addButton, self.addRowToTable);

                }
            }

            SaveElements.PopulateForeignKeyDependencies();
        };

        self.deleteRow = function (e) {

            var $target = e.target.id;
            var targetId = $target.replace('_delete', '');

            var thing = baseQueryThing.byExposedId(targetId);
            thing.ValueExistsOnOff = 0;
            thing.syncMoleculeValue('ValueExistsOnOff');
            SaveElements.addThingToSave(thing, thing.atomByPropertyName('ValueExistsOnOff'));

            var row = $('#row_' + targetId);
            row.remove();

        };

        self.addRowToTable = function (e) {
            e.preventDefault();
            e.stopPropagation();

            var childFormIdArray = e.target.id.replace('new_', '').split('TABLEAPPENDKEY');
            var childFormId = childFormIdArray[0];
            var tableAppendTo = childFormIdArray[1];
            var childForm = utilThing.byExposedId(childFormId);
            var rowMoleculeType = utilThing.byExposedId(childForm.DomainObjectExposedId);
            //var rowMolecule = new DTS.Molecule();

            self.rowMolecule.CreateByType(rowMoleculeType, eMESA.returnGuid(), '');
            var data = accessoryData[childForm.DomainObjectExposedId];
            var childFormDisplayFields = self.resultFieldsByHeaderFormId(childForm.header.exposedId);


            $('#' + e.target.id).remove();

            self.renderRowForMoleculeAndChildForm({
                rowMolecule: self.rowMolecule,
                forForm: childForm,
                tableAppendTo: tableAppendTo,
                hasExpandButton: false,
                allowDelete: allowInLineDelete,
                childFormDisplayFields: childFormDisplayFields,
                data: data,
                appendAddButton: true
            });

            //Chevron-only
            //if (domainThing.header.name == 'Personnel') {
            //    newPersonnelThing = rowMolecule;
            //}

        };

        self.resultFieldsByHeaderFormId = function (headerFormId) {

            var childFormDisplayFields = new DTS.Things();
            childFormDisplayFields.molecules = utilThing.ByValueThingId('Form', headerFormId);
            childFormDisplayFields.molecules = childFormDisplayFields.byPropertyValue('Type', 'Result');
            childFormDisplayFields.sortThingsByMultipleAttributes([
                {
                    property: 'Sequence',
                    dir: 'asc'
                }
            ]);

            return childFormDisplayFields;
        };

        self.toggleSort = function (e) {
            page = 1;

            var expandId = e.target.id.replace('sorting_', '');

            //var newSortField = domainThing.atomByPropertyName(expandId).exposedId;
            var newSortField = expandId;//domainThing.atomById(expandId).exposedId;

            if (newSortField == selectedSortField) {
                if (selectedSortDirection == 'Ascending') {
                    selectedSortDirection = 'Descending';
                }
                else {
                    selectedSortDirection = 'Ascending';
                }
            }
            else {
                selectedSortField = newSortField;
                selectedSortDirection = 'Ascending';
            }
            self.search(e);
        };

        self.saveThings = function (e) {
            e.stopPropagation();
            e.preventDefault();
            var saveObjectLabel = '';

            if (isSearch) {
                saveObjectLabel = 'Saving...';
            }
            else {
                'Saving ' + headerForm.Title + '... ';
            }
            $('#bodyContainer').block({ message: saveObjectLabel });
            SaveElements.save(self.postSave);
        };

        self.postSave = function (results) {

            var currentURL = window.location.href;
            if (!currentURL.includes(headerId) && !isSearch) {

                var newUrl = currentURL;
                if (newUrl.slice(-1) != '/') {
                    newUrl += '/';
                }
                newUrl += headerId;
                history.replaceState(null, "", newUrl);

            }
            $('#bodyContainer').unblock();

            //Chevron-specific for user-management
            var newPersonnelThing = self.rowMolecule;

            if (domainThing.header.name == 'Personnel' && newPersonnelThing.header != null) {

                var newUser = new DTS.Molecule();

                newUser.CreateByType(userThing, eMESA.returnGuid(), '');
                newUser.EmailAddress = newPersonnelThing.email;
                newUser.Label = newPersonnelThing.email;
                newUser.Auth0UserId = newPersonnelThing.email;
                newUser.UserProfileUserName = newPersonnelThing.email;
                newUser.Personel = newPersonnelThing;
                newUser.PersonelExposedId = newPersonnelThing.header.exposedId;
                newUser.CurrentUniverse = currentUniverse;
                newUser.CurrentUniverseExposedId = currentUniverse.header.exposedId;
                newUser.header.au.name = newPersonnelThing.header.au.name;
                newUser.header.au.exposedId = newPersonnelThing.header.au.exposedId;
                newUser.ApplicationUniverse = currentUniverse;
                //newUser.ApplicationUniverseExposedId = '917EDCF1-C1B1-4563-87DA-7A76068C09DA';
                newUser.applyMappedValues();

                var saveThing = new DTS.Things();
                saveThing.createOrModifyBySet([newUser], function (result) {

                    var universeArray = [
                        'FEEFD678-1612-490E-97FA-00A670AF5E4B',
                        '155E6E2A-911E-4213-B44D-1BD21498F154',
                        '917EDCF1-C1B1-4563-87DA-7A76068C09DA',
                        'BA97BF2E-40E8-4391-BC63-986FB6575C1C',
                        '3D01548A-A564-4CCE-87EE-D4B029907C3D'
                    ];


                    var saveThings = [];
                    for (var i = 0; i < universeArray.length; i++) {
                        var thisUniverse = universeArray[i];
                        var atom = result[0].atom('ValueSecurityLevel');
                        var saveThing = new DTS.Molecule(); //JSON.parse(JSON.stringify(selectedDetailThing));
                        var fromThing = JSON.parse(JSON.stringify(result[0]));
                        saveThing.header = fromThing.header;

                        var valueSecurityAtom = JSON.parse(JSON.stringify(atom));
                        valueSecurityAtom.securityLevel = 100;
                        valueSecurityAtom.au.exposedId = thisUniverse;
                        saveThing.attributes = [valueSecurityAtom];
                        saveThing.header.au.exposedId = thisUniverse;
                        saveThings.push(saveThing);
                    }

                    var utilThing = new DTS.Things();
                    utilThing.molecules = saveThings;
                    utilThing.saveSecurityLevelForThingAttribute(true, true, function (results) {

                        console.log('Results: ' + results);
                    });

                    newPersonnelThing.UserId = result[0];
                    var savePersonnel = new DTS.Things();
                    savePersonnel.createOrModifyBySet([newPersonnelThing], function (result) {

                        console.log('Personnel saved');

                    });

                    //newPersonnelThing = null;


                });
                

            }

            if (domainThing.header.name == 'Personnel') {
                if (newPersonnelThing.header != undefined) {
                    let userId = newPersonnelThing.header.exposedId;
                } else {
                    for (var a = 0; a < results.length; a++) {

                        let userId = results[a].header.exposedId;

                        $.ajax('/NativeMoleculeDAL/InvalidatePersonnelOnChange',
                            {
                                method: 'POST',
                                data: {
                                    userId
                                }
                            });
                    }
                }


               
            }

            if (ThingInterface.postSaveCallback != undefined) {

                console.log('Calling custom post-save');
                //ThingInterface.postSaveCallback(results);
            }
        };


        self.displayFieldsForFormElement = function (formElement, valueThing) {
            var displayFields = '';

            if (formElement.AutocompleterDisplayFields != null && formElement.AutocompleterDisplayFields.length > 0 && formElement.AutocompleterDisplayFields != '00000000-0000-0000-0000-000000000000') {

                var displayArray = formElement.AutocompleterDisplayFields.split(',');
                for (var x = 0; x < displayArray.length; x++) {

                    if (x > 0) {
                        displayFields += ',';
                    }
                    var displayFieldToAppend = valueThing.atomById(displayArray[x]).name;
                    displayFields += displayFieldToAppend;
                }
            }
            displayFields = displayFields.replace(/\s/g, '');
            //if (displayFields == '') {
            //    displayFields = 'Label';
            //}
            return displayFields;
        };

        self.searchFieldsForFormElement = function (formElement, valueThing) {

            var searchFields = '';
            if (formElement.AutocompleterSearchFields != null && formElement.AutocompleterSearchFields.length > 0 && formElement.AutocompleterSearchFields != '00000000-0000-0000-0000-000000000000') {
                var searchArray = formElement.AutocompleterSearchFields.split(',');

                for (var x = 0; x < searchArray.length; x++) {

                    var searchAttribute = searchArray[x];
                    if (x > 0) {
                        searchFields += ',';
                    }
                    var searchFieldToAppend = valueThing.atomById(searchAttribute).name;
                    searchFields += searchFieldToAppend;
                }
            }
            return searchFields;
        };

        self.exportToExcel = function (e) {
            e.stopPropagation();
            e.preventDefault();

            var tableId = 'DataTables_Table_';
            tableId += headerForm.DomainObjectName;
            tableId += '_results';
            tableId = tableId.replace(/ /g, '');
            //Asset_results'
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById(tableId); // id of table
            for (j = 2; j < tab.rows.length; j++) {
                var row = tab.rows[j];
                var rowStr = '<tr>';
                if (j > 3) {
                    for (var x = 0; x < row.childNodes.length; x++) {
                        var childNodes = row.childNodes[x];

                        var thisChild = $(childNodes.childNodes);

                        if (thisChild.is("input")) {
                            rowStr += '<td>' + thisChild[0].value + '</td>';
                        }
                        else if (thisChild.is("label")) {

                        }
                        else if (thisChild.is("select")) {
                            rowStr += '<td>' + thisChild[0].selectedOptions[0].innerText + '</td>';
                        }
                        else {

                            if (thisChild[0] != undefined && thisChild[0].childNodes != undefined) {
                                var radioContainer = thisChild[0].childNodes;

                                if (radioContainer[1] != undefined) {
                                    var isChecked = radioContainer[1].checked;
                                    if (isChecked) {
                                        rowStr += '<td>True</td>'
                                    }
                                    else {
                                        rowStr += '<td>False</td>'
                                    }
                                }
                            }
                        }
                    }
                    tab_text = tab_text + rowStr + '</tr>';
                }
                else {
                    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                }
                  //tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "export.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return sa;

        };
    };

    var dynamicForm = new DynamicForm();
    dynamicForm.initialize();

    window.userDynamic = dynamicForm;

});
