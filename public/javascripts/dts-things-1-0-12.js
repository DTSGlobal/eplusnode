"use strict";


if (window.DTS === undefined) {
    window.DTS = {};
}

DTS.ThingInterface = function () {

    var self = this;
    self.postSaveCallback = undefined;

    self.elementByThingAndAttribute = function (thing, attribute) {

        var element = SaveElements.formThingByThingAndAtom(thing, attribute)[0];
        var returnElement = $('#' + element.id);
        return returnElement;
    };

    self.elementContainerByThingAndAttribute = function (thing, attribute) {

        var element = SaveElements.formThingByThingAndAtom(thing, attribute)[0];
        var returnElement = $('#' + element.id + '_formGroup');
        return returnElement;
    };

};


DTS.ThingRegistry = function () {

    var self = this;
    self.thingConstants = {

        UniverseTypeId: '155e6e2a-911e-4213-b44d-1bd21498f154',
        UserTypeThingId: '0e4138fa-cf2d-4109-8915-5f5e9ded8f88'
    };
};

DTS.SaveElements = function () {
    /*
    As its name suggests, this class and its functions handles saving elements from a form.
    By default, ThingElements instances will save any updated values (as the user enters them)
    based on blur, change, etc. events. These updated Thing/molecule combinations are stored in
    SaveEleemnts.molecules.
    The Save function calls the service methods to save these methods to the database, retrieves updated values
    back, and is responsible for refreshing the values of any form elements registed via self.formElements and self.baseFormData

    */

    

    var self = this;
    self.molecules = [];
    self.formElements = [];
    self.elementIds = [];
    self.baseFormData = [];
    self.saveResults = [];
    var utilThing = new DTS.Things();
    self.fkElems = [];
    self.fkThings = [];
    self.fkThingIds = [];
    self.textContacts = [];
    self.selectContents = new DTS.Things();
    self.saveBtnId = '';
    self.userDefaultType = '';
    self.userDefaultThings = '';
    self.isLogInAdmin = false;
    self.filterMolecules = [];
    self.helpTexts;
    var helpTextTypeId = '578a4efb-043d-47c6-8aed-0cb00bc48701';
    var helpTextType = new DTS.Molecule();
    var helpTexts = new DTS.Things();
    
    /*
    The handler events for ThingElements call this function with updated values for thing/atom combinations.
    This function is responsible for maintaining an array of saved values, either by additions or updates.
    */
    self.formThingByThingAndAtom = function (thing, atom) {

        var returnElements = [];
        for (var i = 0; i < self.formElements.length; i++) {

            var formElement = self.formElements[i];
            if (formElement.thing.header.exposedId == thing.header.exposedId && formElement.atom.exposedId == atom.exposedId) {
                returnElements.push(formElement);
            }
        }
        return returnElements;
    }
    //addThingToFilterList uses logic similar to SaveElements.addThingToSave to bind and gather values
    //We use these values for query filters; we also have logic in here to save range values for floats and dates
    self.addThingToFilterList = function (thing, atom) {

        var matchFound = false;
        var existingMolecule = {};

        var baseMolecule = JSON.parse(JSON.stringify(thing));
        var saveMolecule = new DTS.Molecule();
        saveMolecule.header = baseMolecule.header;
        saveMolecule.attributes = baseMolecule.attributes;

        if (self.filterMolecules.length == 0) {
            saveMolecule.attributes = [];
            saveMolecule.attributes.push(atom);
            self.filterMolecules.push(saveMolecule);
        }
        else {

            //see if we have this thing/attribute in our save list yet
            for (var i = 0; i < self.filterMolecules.length; i++) {

                var matchMol = self.filterMolecules[i];
                if (matchMol.header.exposedId == thing.header.exposedId) {

                    var attExists = false;
                    var matchAtt = {};
                    for (var a = 0; a < matchMol.attributes.length; a++) {

                        if (matchMol.attributes[a].exposedId == atom.exposedId) {
                            matchMol.attributes[a] = atom;
                            if (atom.dataType == 'ValueLabel') {
                                matchMol.header.label = atom.displayValue;
                                //console.log('New label value: ' + matchMol.header.label);
                            }
                            attExists = true;
                            break;
                        }
                    }
                    if (!attExists) {
                        matchMol.attributes.push(atom);
                    }
                    matchFound = true;
                    existingMolecule = matchMol;
                }
            }

            if (!matchFound) {
                saveMolecule.attributes = [];
                saveMolecule.attributes.push(atom);
                self.filterMolecules.push(saveMolecule);

            }
        }
    };

    self.addThingToSave = function (thing, atom) {

        var matchFound = false;
        var existingMolecule = {};

        var baseMolecule = JSON.parse(JSON.stringify(thing));
        var saveMolecule = new DTS.Molecule();
        saveMolecule.header = baseMolecule.header;
        saveMolecule.attributes = baseMolecule.attributes;

        if (self.molecules.length == 0) {
            saveMolecule.attributes = [];
            saveMolecule.attributes.push(atom);
            self.molecules.push(saveMolecule);
        }
        else {

            //see if we have this thing/attribute in our save list yet
            for (var i = 0; i < self.molecules.length; i++) {

                var matchMol = self.molecules[i];
                if (matchMol.header.exposedId == thing.header.exposedId) {
                    matchMol.header.label = thing.header.label;
                    var attExists = false;
                    var matchAtt = {};
                    for (var a = 0; a < matchMol.attributes.length; a++) {

                        if (matchMol.attributes[a].exposedId == atom.exposedId) {
                            matchMol.attributes[a] = atom;
                            attExists = true;
                            break;
                        }
                    }
                    if (!attExists) {

                        matchMol.attributes.push(atom);
                    }
                    matchFound = true;
                    existingMolecule = matchMol;
                }
            }

            if (!matchFound) {
                saveMolecule.attributes = [];
                saveMolecule.attributes.push(atom);
                self.molecules.push(saveMolecule);

            }
        }
    };

    //adds helpText icons to labels/texts - Grabs all their information after form load

    self.makeHelpText = function () {

        helpTextType.CreateByExposedId(helpTextTypeId, self.addHelpTextsToForm);
    };

    self.addHelpTextsToForm = function () {

        var helpTextTypeID = '578a4efb-043d-47c6-8aed-0cb00bc48701';
        var allQueryAttributes = [];

        var page = window.location.pathname;

        var queryAttributes = {};
        queryAttributes.attributeName = 'Page';
        queryAttributes.valueText = page;
        allQueryAttributes.push(queryAttributes);

        var request = {};
        request.attributes = [];
        request.attributes = allQueryAttributes;
        request.columns = [];

        request.DomainObjectId = helpTextTypeID;
        request.ExistsOnly = true;
        request.friendlyColumns = [];
        request.friendlyColumns = [
            'ValueExistsOnOff',
            'Page',
            'Element',
            'Text'
        ];
        request.RowCount = 100;
        request.pageNumber = 1;

        helpTexts.InitializeByQuery(request, function () {
            //Make the help text part of the elements - so we can get the property, thing, etc of the element. Then, on the click we can handle that event in the elements class as well.
            SaveElements.helpTexts = helpTexts;
            //if (helpTexts.molecules.length == 0 && SaveElements.isLogInAdmin)
            for (var s = 0; s < SaveElements.formElements.length; s++) {
                var currentElement = SaveElements.formElements[s];
                if (currentElement.addHelpText) {
                    var currentProperty = currentElement.exposedProperty;
                    var isHelpTextforProperty = false;
                    for (var m = 0; m < helpTexts.molecules.length; m++) {
                        var currentText = helpTexts.molecules[m];
                        var currentTextProperty = currentText.Element;
                        if (currentTextProperty != undefined && currentProperty != undefined) {
                            if (currentTextProperty.toLowerCase() == currentProperty.toLowerCase()) {
                                isHelpTextforProperty = true;
                                if (currentElement.helpTextAdded) {
                                    currentText.ValueExistsOnOff = 0;
                                    currentText.applyMappedValues();
                                    SaveElements.molecules.push(currentText);
                                    SaveElements.addThingToSave(currentText, currentText.atomByPropertyName('ValueExistsOnOff'));
                                } else {
                                    currentElement.helpTextAdded = true;
                                }
                                if (currentText.ValueExistsOnOff) {
                                    var appendTextTo = currentElement.id;
                                    if (currentText.Text == undefined || currentText.Text == '') {
                                        if (SaveElements.isLogInAdmin) {
                                            new DTS.ThingElements().addElement({
                                                elementType: 'helpText',
                                                thing: currentText,
                                                property: 'Text',
                                                appendTo: appendTextTo,
                                                prependTo: null
                                            });
                                        }
                                    } else {

                                        new DTS.ThingElements().addElement({
                                            elementType: 'helpText',
                                            thing: currentText,
                                            property: 'Text',
                                            appendTo: appendTextTo,
                                            prependTo: null
                                        });
                                    }
                                }

                            }
                        }

                    }

                    if (!isHelpTextforProperty) {

                        if (SaveElements.isLogInAdmin) {
                            var helpText = new DTS.Molecule();
                            helpText.CreateByType(helpTextType, eMESA.returnGuid(), 'newHelpText');
                            helpText.Page = window.location.pathname;
                            helpText.Element = currentElement.exposedProperty;
                            helpText.applyMappedValues();
                            helpTexts.molecules.push(helpText);
                            SaveElements.molecules.push(helpText);
                            SaveElements.addThingToSave(helpText, helpText.atomByPropertyName('Page'));
                            SaveElements.addThingToSave(helpText, helpText.atomByPropertyName('Element'));
                            SaveElements.helpTexts.molecules.push(helpText);

                            var appendTextTo = currentElement.id;

                            new DTS.ThingElements().addElement({
                                elementType: 'helpText',
                                thing: helpText,
                                property: 'Text',
                                appendTo: appendTextTo,
                                prependTo: null
                            });
                        }

                    }
                }
            }

        });

    };

    /*
    Saves the results.
    Create and update events are all handled via save, and can be pass in as sets.
    The post-save callback updates form elements (selects, textboxes, radio buttons, etc.) with updated values
    that the server returns in response to the save call
    */
    self.save = function (callback) {

        var t = new DTS.Things();
        console.log('Start save');
        //check if anything is required and make sure it's been populated
        var areRequirementsFilled = true;
        for (var i = 0; i < SaveElements.formElements.length; i++) {

            var currentElement = SaveElements.formElements[i];
            var isRequired = currentElement.required;
            if (isRequired) {
                var property = currentElement.exposedProperty;

                if (property != undefined) {
                    var valueByAtom = currentElement.thing.atomByPropertyName(property);
                    var textValue = '';

                    if (currentElement.exposedElementType == 'select') {
                        textValue = $('#' + currentElement.id).find('option:selected').text();
                    }
                    else {
                        textValue = $('#' + currentElement.id).val();
                    }

                    if (valueByAtom.dataType == 'ValueThingId') {
                        if (valueByAtom.valueThingId == undefined || valueByAtom.valueThingId == ('(none selected)')) {
                            console.log(currentElement);
                            areRequirementsFilled = false;
                            var changePropertyColor = document.getElementById(currentElement.id);
                            if (changePropertyColor != undefined) {
                                changePropertyColor.style.borderColor = '#D92121';
                            }
                        }
                        else {
                            var changePropertyColor = document.getElementById(currentElement.id);
                            if (changePropertyColor != undefined) {
                                changePropertyColor.style.borderColor = '#00CC99';
                            }
                        }
                    } else {
                        var isPropertyNull = currentElement.thing[property];
                        if (isPropertyNull == undefined) {
                            console.log(currentElement);
                            areRequirementsFilled = false;
                            var changePropertyColor = document.getElementById(currentElement.id);
                            if (changePropertyColor != undefined) {
                                changePropertyColor.style.borderColor = '#D92121';
                            }
                        } else {
                            var changePropertyColor = document.getElementById(currentElement.id);
                            if (changePropertyColor != undefined) {
                                changePropertyColor.style.borderColor = '#00CC99';
                            }
                        }
                    }
                } else {
                    textValue = $('#' + currentElement.id).val();
                    if (textValue == '(Please Select)') {
                        console.log(currentElement);
                        areRequirementsFilled = false;
                        var changePropertyColor = document.getElementById(currentElement.id);
                        if (changePropertyColor != undefined) {
                            changePropertyColor.style.borderColor = '#D92121';

                        }
                    } else {
                        var changePropertyColor = document.getElementById(currentElement.id);
                        if (changePropertyColor != undefined) {
                            changePropertyColor.style.borderColor = '#00CC99';
                        }
                    }
                }
            }
        }
        if (!areRequirementsFilled) {
            var result = 'UnfilledRequirements';
            callback(result);
            return;
        }
        t.createOrModifyBySet(self.molecules, function (result) {
            if (result != 'Unauthorized') {
                console.log('Save complete');
                var postprocessThing = new DTS.Things();
                postprocessThing.AppendMolecules(result);
                self.saveResults = postprocessThing.molecules;
                self.molecules.length = 0;
                self.updateBaseFormDataWithResults();
                self.RefreshAllElements();

            }
            console.log('Post-save complete');
            if (callback != undefined) {
                callback(result);
            }
        });
    };

    self.updateBaseFormDataWithResults = function () {
        for (var i = 0; i < self.baseFormData.length; i++) {
            var baseDataToUpdate = self.baseFormData[i];
            for (var j = 0; j < self.saveResults.length; j++) {

                var replacementThing = self.saveResults[j];
                var index = -1;
                if (baseDataToUpdate.exposedIdExists(replacementThing.header.exposedId)) {
                    var thingToReplace = baseDataToUpdate.byExposedId(replacementThing.header.exposedId);
                    self.applyValueThingsToReplacement(replacementThing);
                    index = baseDataToUpdate.molecules.indexOf(thingToReplace);
                    baseDataToUpdate.molecules[index] = replacementThing;
                    break;

                }

                for (var z = 0; z < self.formElements.length; z++) {

                    if (index > -1 && baseDataToUpdate.molecules[index].header.exposedId == self.formElements[z].thingId) {
                        self.formElements[z].refresh(baseDataToUpdate.molecules[index]);
                    }
                }
            }
        }
    };

    self.applyValueThingsToReplacement = function (replacementThing) {

        for (var i = 0; i < self.baseFormData.length; i++) {

            replacementThing.applyValueThings(self.baseFormData[i].molecules);
            replacementThing.applyValueThings(self.saveResults);
        }

    };

    //self.RefreshAllElements = function (updatedThings) {
    self.RefreshAllElements = function () {


        for (var i = 0; i < self.formElements.length; i++) {

            for (var x = 0; x < self.saveResults.length; x++) {

                var updatedThing = self.saveResults[x];
                if (updatedThing.header.exposedId == self.formElements[i].thingId) {
                    self.formElements[i].refresh(updatedThing);
                }
            }
        }
    };

    self.RefreshFormElementsAllForThing = function (updatedThing) {

        for (var i = 0; i < self.formElements.length; i++) {

            if (updatedThing.header.exposedId == self.formElements[i].thingId) {
                self.formElements[i].refresh(updatedThing);
            }

        }

    };

    self.RefreshFormElementByExposedId = function (updatedThing, property) {
        for (var i = 0; i < self.formElements.length; i++) {

            if (updatedThing.header.exposedId == self.formElements[i].thingId && property == self.formElements[i].exposedProperty) {
                self.formElements[i].refresh(updatedThing);
            }

        }
    };

    self.RefreshValueThingDependenciesForThing = function (thingToUpdate) {

        var elementsToUpdate = [];

        for (var i = 0; i < self.formElements.length; i++) {

            if (self.formElements[i].hasValueThingDependencies) {
                elementsToUpdate.push(self.formElements[i]);
            }
        }

        for (var x = 0; x < elementsToUpdate.length; x++) {

            if (elementsToUpdate[x].atom.valueThingId == thingToUpdate.header.exposedId) {

                var elem = elementsToUpdate[x];
                if (elem.exposedElementType == 'label') {
                    elem.populateLabelWithResults(thingToUpdate);

                }
                if (elem.exposedElementType == 'autocompleter') {
                    elem.populateAutocompleterWithResults(thingToUpdate);
                }
            }
        }
        var exists = false;

        for (var k = 0; k < self.fkThings.length; k++) {

            if (self.fkThings[k].header.exposedId == thingToUpdate.header.exposedId) {

                self.fkThings[k] = thingToUpdate;
                exists = true;

            }

        }

        if (!exists) {

            self.fkThings.push(thingToUpdate);
            self.fkThingIds.push(thingToUpdate.header.exposedId);

        }

    };

    self.RefreshValueThingDependencies = function () {

        if (utilThing.molecules.length == 0) {

            for (var i = 0; i < self.fkThings.length; i++) {

                utilThing.molecules.push(self.fkThings[i]);
            }

        }
        else {
            utilThing.molecules.concat(self.fkThings);

        }

        for (var i = 0; i < utilThing.molecules.length; i++) {

            var fkThing = utilThing.molecules[i];

            for (var x = 0; x < self.fkElems.length; x++) {

                if (self.fkElems[x].atom.valueThingId == fkThing.header.exposedId) {

                    var elem = self.fkElems[x];
                    if (elem.exposedElementType == 'label') {
                        elem.populateLabelWithResults(fkThing);

                    }
                    if (elem.exposedElementType == 'autocompleter') {
                        elem.populateAutocompleterWithResults(fkThing);
                    }

                    //elem.populateLabelWithResults(fkThing);
                }
            }
        }
        self.fkElems = [];
    };

    self.PopulateForeignKeyDependencies = function (callback) {

        console.log('Retrieving ValueThings');
        self.fkElems = [];
        var ids = [];
        for (var i = 0; i < self.formElements.length; i++) {

            if (self.formElements[i].hasValueThingDependencies) {

                self.fkElems.push(self.formElements[i]);

                if (self.formElements[i].atom.valueThingId != undefined
                    && !ids.includes(self.formElements[i].atom.valueThingId)
                    && !self.fkThingIds.includes(self.formElements[i].atom.valueThingId)) {

                    ids.push(self.formElements[i].atom.valueThingId);
                }
            }
        }
        if (ids.length == 0) {

            self.RefreshValueThingDependencies();

        } else {

            utilThing.InitializeByList(ids, function () {

                self.RefreshValueThingDependencies();
            });
        }

        if (callback != undefined) {
            callback();
        }

    };

    self.sendEmail = function (to, subject, plainTextBody, HTMLBody, callback) {
        //console.log(to, subject, HTMLBody, callback);
        return $.ajax('/NativeMoleculeDAL/SendEmail',
            {
                method: 'POST',
                data: {
                    'To': to,
                    'Subject': subject,
                    'PlainTextResponse': plainTextBody,
                    'HTMLResponse': `<html><body>${HTMLBody}</body></html>`
                },
                success: function (result) {
                    console.log(result);
                    //result is a boolean value
                    if (callback != undefined) {
                        callback(result);
                    }
                },
                error: function (result) {
                    console.log('email error' + result);
                }
            });
    };
};
/*
The main external function in this class is addElement, which allows the caller to pass in an args dictionary
that defines the element (i.e., select list, textbox, etc.)

args:
                elementType: '', string, one of: select, textbox, textarea, autocompleter
                label: '', string, display label
                thing: , the thing that is the datasource of the element being added
                property: '', string, the thing property that is the datasource and binding target of the element being added 
                appendTo: '', string, the DOM element that the element should be added to
                includeFormGroup: true, boolean, specifies whether the element should be padded with a set of containing divs.
                **** The following are all specific to autocompleters ***
                autocompleteTypeId: '', guid string, the typeOf identifier of the thing being searched (this is duplicative of valueThing and should be removed in future releases)
                displayFields : '', comma-delimited string, for select and autocompleter elements, a list of the properties that should be displayed in the select or autocomplete text box
                sortDefinition:  ,  specifies sort order (including secondary, tertiary, etc. sorts); see
                searchFields: '', comma-delimited string of properties specifying which attributes of the thing will be searched,
                valueThing: thing, the type of Thing being searched

*/
DTS.ThingElements = function () {
    var self = this;
    var displayItems = new DTS.Things();
    var _displayFields = '';
    var $select = {};
    var $ac = {};
    self.atom = {};
    self.atomMax = {};
    self.thingId = '';
    var attributeName = '';
    self.id = '';
    var _property = '';
    self.exposedThing = {};
    self.event = {};
    var _elementType = '';
    self.exposedProperty = '';
    self.exposedElementType = '';
    self.hrefLink = '';
    var _includeFormGroup = false;
    var _labelFKThing = new DTS.Molecule();
    var _delimiter = ' - ';
    var _autocompleteTypeId = '';
    var _searchFields = '';
    var _autocompleteSuggestionWidth = '';
    var _autocompleteBaseWidth = '';
    var _autocompleteMinimumCharacters = 5;
    var _autocompleteDelay = 500;
    self.required = false;
    self.hasValueThingDependencies = false;
    self.valueThing = new DTS.Molecule();
    var _addHandler = {};
    self.sortDefinition = '';
    self.typeFloat;
    self.treeDefinition = {};
    self.skipBinding = false;
    self.skipSave = false;
    self.nodeLookAhead = [];
    self.parentProperty = '';
    self.childElement = {};
    self.disableSelectValue = [];
    self.userDefault = false;
    self.addHelpText = false;
    self.doubleElement = false;
    self.tripleElement = false;
    self.uniqueId = '';
    self.helpTextAdded = false;
    self.parentProperty;
    self.populateAfterAutocomplete = false;
    self.selectElementThing = new DTS.Things();
    var isMultiSelect = false;
    var labelThing = new DTS.Molecule();
    var addhelpTextToLabel = false;
    var onHydrateCallback = null;
    self.jsTreeArray = [];
    var isFreeText = false;
    //var nativeDisplayColumns = '';

    self.addElement = function (args) {
        var label = args.label;
        var thing = args.thing;
        var property = args.property;
        var appendTo = args.appendTo;
        var prependTo = args.prependTo;
        var displayFields = args.displayFields;
        var includeFormGroup = args.includeFormGroup;
        var elementType = args.elementType;
        var uniqueSelect = args.uniqueSelect;
        var uniqueList = args.uniqueList;
        var iconString = args.iconString;
        var modalString = args.modalString;
        var shouldHighlight = args.shouldHighlight;
        _displayFields = displayFields;
        if (args.delimiter != undefined) {
            _delimiter = args.delimiter;
        }

        var callback = args.callback;
        var rowCount = args.rowCount;
        _searchFields = args.searchFields;
        _autocompleteTypeId = args.autocompleteTypeId;
        _autocompleteSuggestionWidth = args.autocompleteSuggestionWidth;
        _addHandler = args.addHandler;
        self.valueThing = args.valueThing;
        self.disableThing = args.disableThing;
        self.required = args.required;
        self.disableSelectValue = args.disableSelectValue;
        self.defaultOption = args.defaultOption;
        self.defaultOptionValue = args.defaultOptionValue;
        self.userDefault = args.userDefault;
        self.addHelpText = args.addHelpText;
        self.doubleElement = args.doubleElement;
        self.tripleElement = args.tripleElement;
        self.uniqueId = args.uniqueId;
        self.typeFloat = args.typeFloat;
        self.parentProperty = args.parentProperty;

        _autocompleteTypeId = args.autocompleteTypeId;
        self.exposedThing = args.thing;
        self.hrefLink = args.hrefLink;
        self.sortDefinition = args.sortDefinition;
        self.parentProperty = args.parentProperty;
        self.childElement = args.childElement;
        self.parentElement = args.parentElement;
        self.populateAfterAutocomplete = args.populateAfterAutocomplete;
        self.isSearch = false;

        if (args.isFreeText) {
            isFreeText = args.isFreeText;
        }

        //If the user doesn't have access to this atom, just don't return the element
        if (self.atom == undefined) {
            return;
        }

        if (self.uniqueId != '' && self.uniqueId != undefined) {
            if (property != undefined) {
                self.atom = thing.atomByPropertyName(property);
                self.thingId = thing.header.exposedId;
                attributeName = self.atom.name;
                self.id = 'input' + self.thingId + property + self.uniqueId;
            }
        } else if (elementType != 'tree' && elementType != 'iconHelper' && elementType != 'helpText') {
            if (property != undefined && !self.doubleElement && !self.tripleElement) {
                self.atom = thing.atomByPropertyName(property);
                self.thingId = thing.header.exposedId;
                attributeName = self.atom.name;
                self.id = 'input' + self.thingId + property;
            } else {
                if (property != undefined && self.doubleElement) {
                    self.atom = thing.atomByPropertyName(property);
                    self.thingId = thing.header.exposedId;
                    attributeName = self.atom.name;
                    self.id = 'input' + self.thingId + property + '2';
                }
                if (property != undefined && self.tripleElement) {
                    self.atom = thing.atomByPropertyName(property);
                    self.thingId = thing.header.exposedId;
                    attributeName = self.atom.name;
                    self.id = 'input' + self.thingId + property + '3';
                }
            }
        } else if (elementType == 'iconHelper') {
            if (property != undefined && !self.doubleElement && !self.tripleElement) {
                self.atom = thing.atomByPropertyName(property);
                self.thingId = thing.header.exposedId;
                attributeName = self.atom.name;
                self.id = 'icon' + self.thingId + property;
            } else {
                if (property != undefined && self.doubleElement) {
                    self.atom = thing.atomByPropertyName(property);
                    self.thingId = thing.header.exposedId;
                    attributeName = self.atom.name;
                    self.id = 'icon' + self.thingId + property + '2';
                }
                if (property != undefined && self.tripleElement) {
                    self.atom = thing.atomByPropertyName(property);
                    self.thingId = thing.header.exposedId;
                    attributeName = self.atom.name;
                    self.id = 'icon' + self.thingId + property + '3';
                }
            }
        } else if (elementType == 'helpText') {
            if (property != undefined && !self.doubleElement && !self.tripleElement) {
                self.atom = thing.atomByPropertyName(property);
                self.thingId = thing.header.exposedId;
                attributeName = self.atom.name;
                self.id = 'helpText' + self.thingId + property;
            } else {
                if (property != undefined && self.doubleElement) {
                    self.atom = thing.atomByPropertyName(property);
                    self.thingId = thing.header.exposedId;
                    attributeName = self.atom.name;
                    self.id = 'helpText' + self.thingId + property + '2';
                }
                if (property != undefined && self.tripleElement) {
                    self.atom = thing.atomByPropertyName(property);
                    self.thingId = thing.header.exposedId;
                    attributeName = self.atom.name;
                    self.id = 'helpText' + self.thingId + property + '3';
                }
            }
        }


        if (elementType == 'label' && displayFields != undefined) {

            var dNames = displayFields.split(',');
            self.id += dNames[0];
        }

        _property = property;
        self.exposedProperty = property;
        self.thing = thing;
        _elementType = elementType;
        self.exposedElementType = elementType;
        _includeFormGroup = includeFormGroup;
        self.treeDefinition = args.treeDefinition;
        if (args.skipBinding) {
            self.skipBinding = args.skipBinding;
        }
        if (args.isMultiSelect) {
            isMultiSelect = args.isMultiSelect;
        }
        if (args.labelThing != undefined) {
            labelThing = args.labelThing;
        }
        if (args.isSearch != undefined) {
            self.isSearch = args.isSearch;
        }
        if (args.dropDownContents != undefined) {
            displayItems.molecules = args.dropDownContents;
        }
        if (args.onHydrateCallback != null) {
            onHydrateCallback = args.onHydrateCallback;
        }


        if (elementType == 'autocompleter') {

            if (args.autocompleteMinimumCharacters != undefined) {
                _autocompleteMinimumCharacters = args.autocompleteMinimumCharacters;
            }
            if (args.autocompleteDelay != undefined) {
                _autocompleteDelay = args.autocompleteDelay;
            }
        }

        var returnVal = '';//we might want to just return the element and not append it for the caller

        switch (_elementType) {
            case 'textbox':
                returnVal = self.makeTextBox(label, thing, property, appendTo, prependTo);
                break;
            case 'datepicker':
                if (self.isSearch) {
                    self.atomMax = JSON.parse(JSON.stringify(self.atom));
                    self.atomMax.name += 'max';
                    returnVal = self.makeDatePickerRange(label, thing, property, appendTo, prependTo);
                }
                else {
                    returnVal = self.makeDatePicker(label, thing, property, appendTo, prependTo);
                }
                break;
            case 'select':
                returnVal = self.makeSelectList(label, thing, property, displayFields, appendTo, prependTo, uniqueSelect, uniqueList);
                break;
            case 'label':
                returnVal = self.makeLabel(label, thing, property, appendTo, prependTo);
                break;
            case 'radio':
                returnVal = self.addRadioSet(label, thing, property, appendTo, prependTo);
                break;
            case 'textarea':
                returnVal = self.makeTextArea(label, thing, property, appendTo, prependTo);
                break;
            case 'textarea_modal':
                returnVal = self.makeTextArea_modal(label, thing, property, appendTo, prependTo, rowCount);
                break;
            case 'autocompleter':
                self.id = self.id.replace(/-/g, '');
                returnVal = self.makeAutocompleter(label, thing, property, displayFields, appendTo, prependTo);
                break;
            case 'tree':
                self.treeDefinition.appendTo = appendTo;
                self.addTree(appendTo);
                break;
            case 'checkbox':
                returnVal = self.makeCheckBox(label, thing, property, appendTo, prependTo);
                break;
            case 'iconHelper':
                returnVal = self.makeIconWithModal(thing, property, iconString, appendTo, prependTo, shouldHighlight);
                break;
            case 'helpText':
                returnVal = self.createHelpText(thing, property, appendTo);
                break;
            default:
                break;
        }
        SaveElements.formElements.push(self);
        if (args.shouldReturnElement) {
            return self;

        }
    };

    self.createHelpText = function (thing, property, appendTo) {
        var helpTextString = '';
        var displayValue;
        if (thing[property] != undefined) {
            displayValue = thing[property];
        } else {
            displayValue = '';
        }

        helpTextString = '&nbsp&nbsp<span id="' + self.id + '" value="' + displayValue + '" data = "' + property + '"><i class="fa fa-question-circle grey infoText" style="font-size:small"></i></span>';

        var $elementLabel;

        if (appendTo != undefined) {
            $elementLabel = $('#' + appendTo + 'label');
            $elementLabel.append(helpTextString);
        }

        var $helpText = $(document.getElementById(self.id));
        $helpText.on('click', $helpText, self.helpTextClicked);

    };

    self.makeIconWithModal = function (thing, property, iconString, appendTo, prependTo, shouldHighlight) {

        if (thing[property] != undefined) {

            var displayValue = thing[property];

            var iconStr = `<span id="` + self.id + `">`
                + iconString +
                `</span>`;

        } else {
            if (self.id != undefined) {
                var iconStr = `<span id="` + self.id + `">`
                    + iconString +
                    `</span>`;
            } else {
                var iconStr = `<span id="` + property + `">`
                    + iconString +
                    `</span>`;
            }

        }

        if (appendTo != undefined) {
            $('#' + appendTo).append($(iconStr));
        }

        if (prependTo != undefined) {
            $('#' + prependTo).prepend($(iconStr));
        }

        if (thing[property] != undefined) {
            var $icon = $(document.getElementById(self.id));
            var icon = document.getElementById(self.id);
            if (thing[property] != '' && shouldHighlight) {
                icon.style.backgroundColor = "#FDFF47";
            }
        } else if (self.id != undefined) {
            var $icon = $(document.getElementById(self.id));
        } else {
            var $icon = $(document.getElementById(property));
        }

        if (self.skipBinding && _addHandler != undefined) {
            $icon.on('click', $icon, self.iconClicked);
        }

    };

    self.makeCheckBox = function (label, thing, property, appendTo, prependTo) {

        var displayValues = '';

        if (thing[property] != undefined) {
            displayValues = thing[property];
        }

        var formGroupStr = '';
        var checkBoxStr = '';

        formGroupStr = '<div class="form-group" id="' + self.id + '_formGroup">';
        formGroupStr += '<div class="row"><div class="col-12">';
        formGroupStr += '<div class="row"><div class="input-group col-sm-10">';
        checkBoxStr += '<input style = "width:25px; height:25px" type="checkbox" id="' + self.id + '" name="' + self.id + '" value="' + displayValues + '">';
        var labelStr = '&nbsp&nbsp&nbsp<label id = "' + self.id + 'label" > ' + label + '</label >';
        checkBoxStr += labelStr;
        if (self.required) {
            checkBoxStr += '<span style="font-color:#D92121; font-weight:bold">*</span>';
        }
        formGroupStr += checkBoxStr;
        formGroupStr += '</div> </div>';
        formGroupStr += '</div>';


        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }
            else {
                $('#' + appendTo).append($(checkBoxStr));
            }

            var $checkBox = $(document.getElementById(self.id));
            var checkBox = document.getElementById(self.id);

            if (displayValues) {
                $checkBox.attr("checked", true);
            } else {
                $checkBox.attr("checked", false);
            }

            if (!self.skipBinding && _addHandler == undefined) {
                $checkBox.on('change', $checkBox, self.checkBoxChanged);
            }
            if (self.required) {
                checkBox.style.borderStyle = 'solid';
                checkBox.style.borderWidth = '2px';
                checkBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $checkBox.attr('disabled', 'disabled');
            }
        }
        else {
            return checkBoxStr;
        }

        if (prependTo != undefined) {
            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }
            else {
                $('#' + prependTo).prepend($(checkBoxStr));
            }

            var $checkBox = $(document.getElementById(self.id));
            var checkBox = document.getElementById(self.id);

            if (displayValues) {
                $checkBox.attr("checked", true);
            } else {
                $checkBox.attr("checked", false);
            }

            if (!self.skipBinding && _addHandler == undefined) {
                $checkBox.on('change', $checkBox, self.checkBoxChanged);
            }
            if (self.required) {
                checkBox.style.borderStyle = 'solid';
                checkBox.style.borderWidth = '2px';
                checkBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $checkBox.attr('disabled', 'disabled');
            }
        }
        else {
            return checkBoxStr;
        }

    };

    self.makeAutocompleter = function (label, thing, property, displayFields, appendTo, prependTo, callback, doubleElement) {

        var searchReturnThings = new DTS.Things();
        var displayValueThing = {};
        var displayStr = '';
        if (thing[property] != undefined) {
            var dNames = _displayFields.split(',');
            for (var i = 0; i < dNames.length; i++) {
                displayValueThing = thing[property];
                displayStr += displayValueThing[dNames[i]];
                if (i < dNames.length - 1 && dNames.length > 1) {
                    displayStr += ' - ';
                }
            }
            if (self.childElement != undefined) {
                self.childElement.populateSelect(self.atom.valueThingId);
            }
        }
        var labelDisplay = label;
        if (label == undefined || label == '') {
            labelDisplay = 'label';
        }
        var formGroupStr = `<div class="form-group" id="${self.id}_formGroup">
                            <label id= "${self.id}${labelDisplay}">${label}</label>
                            <div class="row">
                                <div class="col-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend" id="${self.id}_fasearch"><span class="input-group-text"><i class="far fa-search" ></i></span></div>
                                        <input class="form-control" id="${self.id}" name="${self.id}" value="${displayStr}"><span><i style="visibility: hidden" class="far fa-spin fa-spinner fa-2x" id="${self.id}_spinner"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>`;

        var textBoxStr = `<div class="input-group">
                            <div class="input-group-prepend" id="${self.id}_fasearch"><span class="input-group-text"><i class="far fa-search" ></i></span></div>
                            <input class="form-control" id="${self.id}" name="${self.id}" value="${displayStr}"><span><i style="visibility: hidden" class="far fa-spin fa-spinner fa-2x" id="${self.id}_spinner"></i></span>
                         </div>`;

        if (appendTo != undefined || prependTo != undefined) {
            if (_includeFormGroup) {
                if (appendTo != undefined) {
                    $('#' + appendTo).append($(formGroupStr));
                } else if (prependTo != undefined) {
                    $('#' + prependTo).prepend($(formGroupStr));
                }
            }
            else {
                if (appendTo != undefined) {
                    $('#' + appendTo).append($(textBoxStr));
                } else if (prependTo != undefined) {
                    $('#' + prependTo).prepend($(textBoxStr));
                }
            }

            $('#' + self.id).attr('disabled', 'disabled');

            $ac = $(document.getElementById(self.id));
            var ac = document.getElementById(self.id);

            if (self.required) {
                ac.style.borderStyle = 'solid';
                ac.style.borderWidth = '2px';
                ac.style.borderColor = '#00CC99';
            }

            _autocompleteBaseWidth = $ac.css('width');
            if (labelThing != undefined && labelThing.attributes != undefined) {
                self.populateLabelWithResults(labelThing, true);
            }
            else {
                self.hasValueThingDependencies = true;
            }


            if (!self.disableThing) {
                $('#' + self.id).attr('disabled', false);
            }

            if (!self.isEditable) {
                $('#' + self.id).attr('disabled', 'disabled');
            }

            $('#' + self.id).on("input propertychange", function () {

                var input = $('#' + self.id);
                var string = input[0].value;
                if (string.length == 0) {

                    thing[property] = null;
                    thing.applyMappedValues();
                    self.addToSaveQueue(null);
                }

            });

            $('#' + self.id).autoComplete(
                {
                    autoFocus: true,
                    delay: _autocompleteDelay,
                    //minLength: _autocompleteMinimumCharacters,
                    minChars: _autocompleteMinimumCharacters,
                    appendTo: '#' + self.id,
                    classes: { "ui-autocomplete": "highlight" },
                    source: function (request, response) {
                        var ac = document.getElementById(self.id + '_spinner');
                        ac.style.visibility = "visible";//id="acSpinner"

                        var allQueryAttributes = [];
                        var s = _searchFields.split(',');
                        for (var i = 0; i < s.length; i++) {

                            var queryAttributes = {};
                            var searchTerm;
                            var searchAtom;
                            if (self.dynamic) {
                                searchTerm = s[i].replace(/\s/g, '');
                                searchAtom = self.valueThing.atomByPropertyName(searchTerm);
                                queryAttributes.attributeId = searchAtom.exposedId;
                                queryAttributes.valueText = request;
                                allQueryAttributes.push(queryAttributes);
                            } else {
                                searchTerm = s[i];//.replace(/\s/g, '');
                                queryAttributes.attributeName = searchTerm;
                                queryAttributes.valueText = request;
                                allQueryAttributes.push(queryAttributes);
                            }
                            
                        }

                        var displayAtts = [];
                        var dNames = _displayFields.split(',');

                        for (var i = 0; i < dNames.length; i++) {
                            var dName = dNames[i];
                            if (self.dynamic) {
                                dName = dNames[i].replace(/\s/g, '');
                                var displayAtom = self.valueThing.atomByPropertyName(dName);
                                displayAtts.push(displayAtom.exposedId);
                            } else {
                                dName = dNames[i];
                                displayAtts.push(dName);
                            }
                            //var displayAtom = self.valueThing.atomByPropertyName(dName);
                            //displayAtts.push(dName);
                        }

                        displayAtts.push('ValueExistsOnOff');

                        if (_additionalAttributes != undefined) {
                            var additionalAtt = _additionalAttributes.split(',');

                            for (var i = 0; i < additionalAtt.length; i++) {
                                var attr = additionalAtt[i];
                                if (self.dynamic) {
                                    attr = additionalAtt[i].replace(/\s/g, '');
                                    let displayAtom = self.valueThing.atomByPropertyName(attr);
                                    displayAtts.push(attr.exposedId);
                                } else {
                                    attr = additionalAtt[i];//.replace(/\s/g, '');
                                    displayAtts.push(attr);
                                }
                                
                                //let displayAtom = self.valueThing.atomByPropertyName(attr);
                                //displayAtts.push(attr);
                            }
                        }

                        var request = {};
                        request.attributes = allQueryAttributes;
                        if (self.dynamic) {
                            request.columns = displayAtts;
                        } else {
                            request.friendlyColumns = displayAtts;
                        }
                        //request.friendlyColumns = displayAtts;
                        request.DomainObjectId = _autocompleteTypeId;
                        request.RowCount = 500;
                        request.pageNumber = 1;
                        request.UseOr = true;

                        displayItems.InitializeByQuery(request, function (result) {
                            var returnLabel = [];

                            displayItems.removeInactive();
                            if (self.sortDefinition != undefined) {
                                displayItems.sortThingsByMultipleAttributes(self.sortDefinition);

                            }
                            
                            //searchReturnThings = result;
                            for (var i = 0; i < displayItems.molecules.length; i++) {
                                var displayStr = '';
                                for (var x = 0; x < dNames.length; x++) {
                                    if (x > 0) {
                                        displayStr += _delimiter;
                                    }
                                    var name = dNames[x].replace(/\s/g, '');
                                    displayStr += displayItems.molecules[i][name];
                                }

                                var displayResult = {
                                    label: displayStr,
                                    value: displayItems.molecules[i].header.exposedId
                                };

                                returnLabel.push(displayResult);
                            }
                            return response(returnLabel);
                        });

                    },
                    renderItem: function (item, search) {

                        if (_autocompleteSuggestionWidth != '' || _autocompleteSuggestionWidth != undefined) {

                            $ac.css('width', _autocompleteSuggestionWidth);
                        }
                        var divStr = '<div class="autocomplete-suggestion" data-val="' + item.value + '" data-label="' + item.label + '">' +
                            '<div class=\"col-12\">' + item.label + '</div>'
                            + '</div>';

                        //debugger;
                        return divStr;

                    },
                    onSelect: function (e, term, item) {
                        $ac.css('width', _autocompleteBaseWidth);
                        thing[property] = displayItems.byExposedId(term);
                        self.selectElementThing.molecules = displayItems.byExposedId(term);
                        thing.applyMappedValues();
                        if (!self.skipBinding) {
                            self.addToSaveQueue(term);
                        }
                        $ac.val(item[0].innerText);
                        var ac = document.getElementById(self.id + '_spinner');
                        ac.style.visibility = "hidden";
                        if (self.childElement != undefined) {

                            self.childElement.populateSelect(self.atom.valueThingId);
                        }
                        SaveElements.PopulateForeignKeyDependencies();
                        if (_addHandler != undefined) {
                            _addHandler(self);
                        }

                    },
                    noData: function (val) {
                        var ac = document.getElementById(self.id + '_spinner');
                        ac.style.visibility = "hidden";
                        return '<div>No results found for ' + val + ' </div>';
                    }
                }
            );

        }
    };

    self.addRadioSet = function (label, thing, property, appendTo, prependTo) {

        var displayValues = '';
        if (thing[property] != undefined) {
            displayValues = thing[property];
        }

        var trueChecked = '';
        var falseChecked = '';
        if (displayValues == true) {

            trueChecked = ' checked="checked" ';
        }
        else {
            if (!self.isSearch) {
                falseChecked = ' checked="checked" ';

            }
        }

        var radioElementStr = '';

        var formGroupStr = '<div class="form-group" id="' + self.id + '_formGroup">';
        formGroupStr += '<label id = "' + self.id + 'label">' + label + '</label>';
        formGroupStr += '<div class="row" ><div class="col-12">';

        radioElementStr += '<div class="row">';
        if (_includeFormGroup) {
            radioElementStr += '<div class="col-3">';
        }

        radioElementStr += '<label class="mr-1">Yes</label>';
        radioElementStr += '<input id="' + self.id + 'true" name="' + self.id + 'true" type="radio" value="' + displayValues + '" ' + trueChecked + '" >';
        if (_includeFormGroup) {
            radioElementStr += "</div>";
        }
        //
        radioElementStr += '<label class="mr-1">No</label>';
        radioElementStr += '<input id="' + self.id + 'false" name="' + self.id + 'true" type="radio" value="' + !displayValues + '" ' + falseChecked + '" >';
        radioElementStr += '</div>';
        radioElementStr += '</div>';

        formGroupStr += radioElementStr;
        formGroupStr += '</div> </div>'; //div class row/column
        formGroupStr += '</div>'; //form group

        if (appendTo != undefined) {

            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }
            else {
                //radioElementStr = '<span style="width:300px">' + radioElementStr + '</span>';
                $('#' + appendTo).append($(radioElementStr));
            }
            var $trueButton = $(document.getElementById(self.id + 'true'));
            var $falseButton = $(document.getElementById(self.id + 'false'));
            //debugger;
            $trueButton.on('click', $trueButton, self.radioToTrue);
            $falseButton.on('click', $falseButton, self.radioToFalse);
        }
        else {
            return radioElementStr;
        }

        if (prependTo != undefined) {

            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }
            else {
                $('#' + prependTo).prepend($(radioElementStr));
            }
            var $trueButton = $(document.getElementById(self.id + 'true'));
            var $falseButton = $(document.getElementById(self.id + 'false'));
            //debugger;
            $trueButton.on('click', $trueButton, self.radioToTrue);
            $falseButton.on('click', $falseButton, self.radioToFalse);
        }
        else {
            return radioElementStr;
        }

    };

    self.makeTextBox = function (label, thing, property, appendTo, prependTo) {

        var displayValues = '';
        if (thing[property] != undefined) {
            displayValues = thing[property];
        }

        var formGroupStr = '';
        var textBoxStr = '';
        var type = 'text';

        if (self.atom.dataType.toLowerCase() == 'valuefloat' || self.atom.dataType.toLowerCase() == 'valuebigint') {
            type = 'number';
        }

        formGroupStr = '<div class="form-group" id="_' + self.id + '_formGroup"  >';
        formGroupStr += '<label id = "' + self.id + 'label">' + label + '</label>';
        formGroupStr += '<div class="row"><div class="col-12">';
        textBoxStr += '<input class="form-control" id="' + self.id + '" name="' + self.id + '" type="' + type + '" value ="' + displayValues + '" spellcheck="false"></input>';
        formGroupStr += textBoxStr;
        formGroupStr += '</div> </div>';
        formGroupStr += '</div>';

        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }
            else {
                $('#' + appendTo).append($(textBoxStr));
            }

            var $textBox = $(document.getElementById(self.id));
            var textBox = document.getElementById(self.id);

            if (!self.skipBinding && self.addHandler == undefined) {
                    $textBox.on('change', $textBox, self.textChanged);
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            }
            if (self.defaultOption) {
                if (thing[property] == undefined || thing[property] == 0) {
                    if (self.defaultOptionValue == '' || self.defaultOptionValue == undefined) {
                        $textBox[0].defaultValue = '';
                    } else {
                        $textBox.val(self.defaultOptionValue);
                    }

                }
            }
        }
        else {
            return textBoxStr;
        }

        if (prependTo != undefined) {
            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }
            else {
                $('#' + prependTo).prepend($(textBoxStr));
            }

            var $textBox = $(document.getElementById(self.id));
            var textBox = document.getElementById(self.id);

            if (!self.skipBinding && self.addHandler == undefined) {
                    $textBox.on('change', $textBox, self.textChanged);
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            }
            if (self.defaultOption) {
                if (thing[property] == undefined) {
                    $textBox.text(self.defaultOptionValue);
                }
            }
        }
        else {
            return textBoxStr;
        }
    };

    self.makeTextArea = function (label, thing, property, appendTo, prependTo) {

        var displayValues = '';
        if (thing[property] != undefined) {
            displayValues = thing[property];
        }

        var formGroupStr = '';
        var textAreaStr = '';
        formGroupStr = '<div class="form-group" id="' + self.id + '_formGroup">';
        formGroupStr += '<label id = "' + self.id + 'label">' + label + '</label>';
        formGroupStr += '<div class="row"><div class="col-12">';
        //textAreaStr += '<textarea class="form-control" style="width:300px; height:300px; length:300px" id="' + self.id + '" name="' + self.id + '" value="' + displayValues + '">' + displayValues + '</textarea>';
        textAreaStr += '<textarea class="form-control" style="width:300px; height:100px; length:300px" id="' + self.id + '" name="' + self.id + '" value="' + displayValues + '">' + displayValues + '</textarea>';
        formGroupStr += textAreaStr;
        formGroupStr += '</div> </div>';
        formGroupStr += '</div>';

        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }
            else {
                $('#' + appendTo).append($(textAreaStr));
            }
            var $textBox = $(document.getElementById(self.id));
            var textBox = document.getElementById(self.id);

            if (!self.skipBinding) {
                $textBox.on('change', $textBox, self.textChanged);
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            }
        }
        else {
            return textAreaStr;
        }

        if (prependTo != undefined) {
            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }
            else {
                $('#' + prependTo).prepend($(textAreaStr));
            }
            var $textBox = $(document.getElementById(self.id));
            var textBox = document.getElementById(self.id);

            if (!self.skipBinding) {
                $textBox.on('change', $textBox, self.textChanged);
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            }
        }
        else {
            return textAreaStr;
        }

    };

    self.makeTextArea_modal = function (label, thing, property, appendTo, prependTo, rowCount) {
        var displayValues = '';
        var $textBox;
        var textBox;
        let rows = 4;
        if (rowCount !== undefined) {
            rows = rowCount;
        }
        if (thing[property] != undefined) {
            displayValues = thing[property];
        }

        var formGroupStr = '';
        var textAreaStr = '';
        formGroupStr = '<div class="form-group" id="_' + self.id + '_formGroup">';
        formGroupStr += '<label id = "' + self.id + 'label">' + label + '</label>';
        formGroupStr += '<div>';
        //textAreaStr += '<textarea class="form-control" style="width:300px; height:300px; length:300px" id="' + self.id + '" name="' + self.id + '" value="' + displayValues + '">' + displayValues + '</textarea>';
        textAreaStr += `<textarea class="form-control" rows="${rows}" id="${self.id}" name="${self.id}" value="${displayValues}">${displayValues}</textarea>`;
        formGroupStr += textAreaStr;
        formGroupStr += '</div>';
        formGroupStr += '</div>';

        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }
            else {
                $('#' + appendTo).append($(textAreaStr));
            }
            $textBox = $(document.getElementById(self.id));
            textBox = document.getElementById(self.id);

            if (!self.skipBinding) {
                $textBox.on('change', $textBox, self.textChanged);
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            }
        }
        else {
            return textAreaStr;
        }

        if (prependTo != undefined) {
            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }
            else {
                $('#' + prependTo).prepend($(textAreaStr));
            }
            $textBox = $(document.getElementById(self.id));
            textBox = document.getElementById(self.id);

            if (!self.skipBinding) {
                $textBox.on('change', $textBox, self.textChanged);
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            }
        }
        else {
            return textAreaStr;
        }

    };

    self.makeDatePicker = function (label, thing, property, appendTo, prependTo) {

        var displayValues = '';
        if (thing[property] != undefined) {
            displayValues = thing[property];
            for (var x = 0; x < thing.attributes.length; x++) {
                var att = thing.attributes[x];
                var attLabel = att.name;
                attLabel = attLabel.replace(/\s/g, '');
                if (att.dataType == 'ValueDatetime') {
                    if (displayValues != undefined) {
                        var newDate = new Date(displayValues);
                        try {
                            var getYear = newDate.getFullYear();
                            var getMonth = newDate.getMonth() + 1; //getmonth is 0 based
                            var getDay = newDate.getDate();
                            var formattedDate = getMonth + '-' + getDay + '-' + getYear;
                            displayValues = formattedDate;
                        }
                        catch (err) {
                            // add logging?
                        }
                    }
                }

            }
        }
        //var id = 'input' + self.thingId + '|' + attributeName;

        var keepDisabled = false;
        var formGroupStr = '';

        if (self.disableThing) {
            keepDisabled = true;
        }

        var datePickerStr = `<input class="make-me-a-date-picker" id="${self.id}" name="${self.id}" data-disable="${keepDisabled}" value="${displayValues}" aria-invalid="false">`;

        formGroupStr = `<div class="form-group" id="${self.id}_formGroup">
                            <label id="${self.id}${label}">${label}</label>
                                <div class="input-group">
                                    <i class="far fa-calendar-alt">&nbsp;</i> <input class="make-me-a-date-picker" id="${self.id}" name="${self.id}" data-disable="${keepDisabled}" value="${displayValues}" aria-invalid="false">
                                </div>
                        </div>`;

        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }

            if (!_includeFormGroup) {
                $('#' + appendTo).append(datePickerStr);
            }
            let $textBox = $(document.getElementById(self.id));
            let textBox = document.getElementById(self.id);

            if (!self.skipBinding) {
                $textBox.datepicker().on('changeDate', $textBox, self.dateChanged);
                if (!self.isSearch) { //Don't need to disable this by default if it's on a search form
                    $textBox.attr('disabled', 'disabled');
                }
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            } else {
                $textBox.attr('disabled', false);
            }
        }
        else {
            return datePickerStr;
        }

        if (prependTo != undefined) {
            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }

            if (!_includeFormGroup) {
                $('#' + prependTo).prepend(datePickerStr);
            }
            let $textBox = $(document.getElementById(self.id));
            let textBox = document.getElementById(self.id);

            if (!self.skipBinding) {
                $textBox.datepicker().on('changeDate', $textBox, self.dateChanged);

                if (!self.isSearch) { //Don't need to disable this by default if it's on a search form
                    $textBox.attr('disabled', 'disabled');
                }
            }
            if (self.required) {
                textBox.style.borderStyle = 'solid';
                textBox.style.borderWidth = '2px';
                textBox.style.borderColor = '#00CC99';
            }
            if (self.disableThing) {
                $textBox.attr('disabled', 'disabled');
            } else {
                $textBox.attr('disabled', false);
            }
        }

        else {
            return datePickerStr;
        }
    };

    self.makeDatePickerRange = function (label, thing, property, appendTo, prependTo) {

        var displayValues = '';
        if (thing[property] != undefined) {
            displayValues = thing[property];
        }

        var keepDisabled = false;
        var formGroupStr = '';
        var datePickerStr = '';

        if (self.disableThing) {
            keepDisabled = true;
        }

        formGroupStr = '<div class="form-group" id="' + self.id + '_formGroup">';
        formGroupStr += '<label id = "' + self.id + 'label">' + label + '</label>';
        formGroupStr += '<div class="row"><div class="col-12">';
        datePickerStr += '<span>';
        datePickerStr += '<input class="make-me-a-date-picker" id="' + self.id + '" name="' + self.id + '" data-disable = "' + keepDisabled + '" value="' + displayValues + '" aria-invalid="false">';
        datePickerStr += '&nbsp;&nbsp;&nbsp;&nbsp;';
        datePickerStr += '<input class="make-me-a-date-picker" id="' + self.id + 'max" name="' + self.id + '" data-disable = "' + keepDisabled + '" value="' + displayValues + '" aria-invalid="false">';
        datePickerStr += '</span>';
        formGroupStr += datePickerStr;
        formGroupStr += '</div> </div>';
        formGroupStr += '</div>';

        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            }

            if (!_includeFormGroup) {
                $('#' + appendTo).append(datePickerStr);
            }
            var $textBox = $(document.getElementById(self.id));
            $textBox.datepicker().on('changeDate', $textBox, self.dateChanged);
            var $textBoxMax = $(document.getElementById(self.id + 'max'));
            $textBoxMax.datepicker().on('changeDate', $textBoxMax, self.dateChangedMax);

        }
        else {
            return datePickerStr;
        }
        return datePickerStr;
    };

    self.makeLabel = function (label, thing, property, appendTo, prependTo, doubleElement) {

        var displayValues = '';
        if (thing[property] != undefined) {
            displayValues = thing[property];
            for (var x = 0; x < thing.attributes.length; x++) {
                var att = thing.attributes[x];
                var attLabel = att.name;
                attLabel = attLabel.replace(/\s/g, '');
                if (attLabel == property) {
                    if (att.dataType == 'ValueDatetime') {
                        if (displayValues != undefined) {
                            var newDate = new Date(displayValues);
                            try {
                                var getYear = newDate.getFullYear();
                                var getMonth = newDate.getMonth() + 1; //getmonth is 0 based
                                var getDay = newDate.getDate();
                                var formattedDate = getMonth + '-' + getDay + '-' + getYear;
                                displayValues = formattedDate;
                            }
                            catch (err) {
                                // add logging?
                            }
                        }
                    }
                }
            }
        }

        if (label != undefined) {
            var labelId = label.replace(/\s/g, '');
        }

        var value = displayValues;

        if (self.typeFloat != undefined && displayValues != '') {
            if (self.typeFloat == 'currency') {
                displayValues = displayValues.toFixed(2);
            }
        }

        var formGroupStr = '';
        var labelStr = '';

        formGroupStr = '<div class="form-group" id="' + self.id + '_formGroup" >';
        if (self.id == undefined || self.id == '') {
            self.id = labelId;
            formGroupStr += '<label style="width:20px" id="' + self.id + '">' + label + '</label>';
        } else {
            formGroupStr += '<label style="width:20px" id="' + self.id + '">' + label + '</label>';
        }
        formGroupStr += '<div class="row"><div class="col-12">';

        if (self.hrefLink != undefined) {

            displayValues = self.hrefLink + displayValues + '</a>';

        }//<a target=_blank href="/Container/SerialNo?id=' + boxItem.header.exposedId + '</a>

        //You have the option of passing in FKs
        //If you haven't, then just use the native displayValue;
        if (_displayFields == undefined || _displayFields == '') {

            labelStr += '<span id="' + self.id + '" name ="' + self.id + '" value="' + value + '">' + displayValues + '</span>';

        }
        else {

            labelStr += '<span id="' + self.id + '" name ="' + self.id + '" value=""></span>';
        }

        formGroupStr += labelStr;
        formGroupStr += '</div> </div>';
        formGroupStr += '</div>';

        if (appendTo != undefined) {
            if (_includeFormGroup) {
                $('#' + appendTo).append($(formGroupStr));
            } else {
                $('#' + appendTo).append($(labelStr));
            }
        }
        else if (prependTo == undefined) {
            return labelStr;
        }

        if (prependTo != undefined) {
            if (_includeFormGroup) {
                $('#' + prependTo).prepend($(formGroupStr));
            }
            else {
                $('#' + prependTo).prepend($(labelStr));
            }
        }
        else if (appendTo == undefined) {
            return labelStr;
        }

        if (_displayFields != undefined && _displayFields != '') {// && self.atom.valueThingId != undefined
            if (!addhelpTextToLabel) {

                //If we already have the FK data, populate it here
                if (labelThing != undefined && labelThing.attributes != undefined) {

                    self.populateLabelWithResults(labelThing, true);
                }
                else {
                    self.hasValueThingDependencies = true;

                }
            }
        }

    };

    self.populateAutocompleterWithResults = function (fkThing) {


        self.thing[_property] = fkThing;
        self.thing.applyMappedValues();
        var labelDisplayArray = _displayFields.split(',');
        var labelDisplayStr = '';

        for (var i = 0; i < labelDisplayArray.length; i++) {

            var labelProp = labelDisplayArray[i].replace(/\s/g, '');
            if (i > 0) {

                labelDisplayStr += _delimiter;
            }
            labelDisplayStr += fkThing[labelProp]; //_labelFKThing[labelProp];
        }
        if (labelDisplayStr == 'null') {
            labelDisplayStr = '';
        }

        var el = $('#' + self.id); //document.getElementById(self.id);
        el.val(labelDisplayStr);
    };

    self.populateLabelWithResults = function (fkThing, displayOnly) {

        //console.log('From populateLabelWithResults for elementType: ' + _elementType);
        self.thing[_property] = fkThing;

        if (!displayOnly) {
            self.thing.applyMappedValues();

        }

        var labelDisplayArray = _displayFields.split(',');
        var labelDisplayStr = '';

        for (var i = 0; i < labelDisplayArray.length; i++) {

            var labelProp = labelDisplayArray[i].replace(/\s/g, '');
            if (i > 0) {

                labelDisplayStr += _delimiter;
            }
            labelDisplayStr += fkThing[labelProp]; //_labelFKThing[labelProp];
        }
        if (labelDisplayStr == 'null') {
            labelDisplayStr = '';
        }
        //debugger;
        if (self.hrefLink != undefined && self.hrefLink.length > 0) {

            //labelDisplayStr = labelDisplayStr.link(self.hrefLink); //

            labelDisplayStr = self.hrefLink + labelDisplayStr + '</a>';
            //labelDisplayStr = labelDisplayStr.replace('"', '');
        }

        var el = document.getElementById(self.id);
        var baseElement = $(document.getElementById(self.id));
        var el = '';
        if (baseElement != undefined) {
            el = $(baseElement[0]);
        }
        if (self.exposedElementType == 'autocompleter') {
            el.val(labelDisplayStr);
            return;
        }

        el.empty();
        el.append(labelDisplayStr);


    };

    self.makeSelectList = function (label, thing, property, displayFields, appendTo, prependTo, uniqueSelect, uniqueList, doubleElement) {

        var typeId = self.atom.typeOfAttributeExposedId;

        var selectElement = '';
        var selectStr;
        var userDefaultStr;

        if (self.id == undefined || self.id.length == 0) {
            var x = label.replace(' ', '');
            self.id = x;
        }

        selectStr = '<div class="form-group" id="' + self.id + '_formGroup">';
        selectStr += '<label id = "' + self.id + 'label">' + label + '</label>';
        if (label != undefined) {
            label = label.replace(' ', '');
        }
        selectStr += '<div class="row"><div class="col-12">';

        var multiStr = '';
        if (isMultiSelect) {
            multiStr = ' multiple ';
        }

        if (uniqueSelect) {
            selectElement += '<select class="form-control" id= "' + self.id + '" name = "' + self.id + '" style="padding: 2px 4px">';

        } else if (self.userDefault) {
            selectElement += `<select ${multiStr} class="form-control" id= "${self.id}" name = "${self.id}" style="padding: 6px 10px">`;
        } else {
            selectElement += '<select ' + multiStr + ' class="form-control" id= "' + self.id + '" name = "' + self.id + '" style="padding: 6px 10px">';
        }

        selectElement += '</select>';

        if (self.userDefault) {
            selectElement += `<userdefault><i class="fas fa-star fa-lg blue userDefaultStar clickable" id="${self.id}userDefault"></i></userdefault>`
        }

        selectStr += selectElement;
        selectStr += '</div> </div>'; //row and col-12

        selectStr += '</div>';//form group close
        if (typeId == undefined) {
            typeId = '';
        }
        var el = $(selectStr);
        if (appendTo != undefined || prependTo != undefined) {
            var select;
            if (_includeFormGroup) {
                if (appendTo != undefined) {
                    $('#' + appendTo).append(el);
                } else if (prependTo != undefined) {
                    $('#' + prependTo).prepend(el);
                }
            }
            if (!_includeFormGroup) {
                if (appendTo != undefined) {
                    $('#' + appendTo).append($(selectElement));
                } else if (prependTo != undefined) {
                    $('#' + prependTo).prepend($(selectElement));
                }
            }
            if (uniqueSelect) {
                select = document.getElementById(self.id);

                if (self.required) {
                    select.style.borderStyle = 'solid';
                    select.style.borderWidth = '2px';
                    select.style.borderColor = '#00CC99';
                }

            } else {

                select = document.getElementById(self.id);

            }

            if (self.required) {
                select.style.borderStyle = 'solid';
                select.style.borderWidth = '2px';
                select.style.borderColor = '#00CC99';
            }

            if (self.userDefault) {
                var $userDefault = $('#' + self.id + 'userDefault');
                $userDefault.on('click', $userDefault, self.userDefaultSelected);
            }

            if (!self.skipBinding) {
                $(select).on('change', $(select), self.selectValueChanged);
            }
            if (self.skipBinding) {
                if (_addHandler != undefined) {
                    $(select).on('change', $(select), self.selectValueChanged);

                }
            }

        }

        if (uniqueSelect) {
            $select = $(document.getElementById(label));
            $select.attr('data-display-fields', uniqueList);
            if (self.disableThing) {
                $select.attr('disabled', 'disabled');
            }
            //self.populateWithResults(uniqueList, uniqueSelect);
            self.populateUniqueSelect(uniqueList);
        } else if (self.populateAfterAutocomplete) {

            $select = $(document.getElementById(self.id));

            $select.attr('data-type-id', typeId);
            $select.attr('data-display-fields', displayFields);
            $select.attr('disabled', 'disabled');

            if (self.parentElement.atom.valueThingId != undefined) {
                self.populateSelect(self.parentElement.atom.valueThingId);
            }

        } else {
            $select = $(document.getElementById(self.id));

            $select.attr('data-type-id', typeId);
            $select.attr('data-display-fields', displayFields);
            $select.attr('disabled', 'disabled');

            if (displayItems.molecules.length > 0) {
                //console.log('Pre-populating');
                self.populateWithResults(displayItems.molecules);
            }
            else {
                self.populateSelect();

            }
        }


    };

    self.populateSelect = function (valueThing) {

        var typeId = $select.data('type-id');
        var displayFields = $select.data('display-fields');

        //If we have multiple drop-downs of the same type per form (like in a search results form), let's only search for them once
        //Once we've done a query, we'll keep the molecule list in SaveElementsselectContents
        //The key is typeId+displayFields
        var savedKey = typeId + displayFields;

        if (displayFields == undefined) {
            displayFields = '';
        }
        var allQueryAttributes = [];
        var columns = [];
        var queryAttributes = {};
        //queryAttributes.attributeName = 'ValueExistsOnOff';
        //queryAttributes.BigIntValue = 1;
        //allQueryAttributes.push(queryAttributes);

        if (valueThing != undefined && valueThing != '') {
            queryAttributes = {};
            queryAttributes.attributeName = self.parentProperty;
            queryAttributes.valueThingId = valueThing;
            allQueryAttributes.push(queryAttributes);
        }

        var request = {};
        request.attributes = [];
        request.attributes = allQueryAttributes;
        request.columns = columns;//[];
        request.DomainObjectId = typeId;
        request.ExistsOnly = true;
        request.friendlyColumns = displayFields.split(',');
        request.friendlyColumns.push('ValueExistsOnOff');

        //If we have a parent/child dependency between two elements, we need
        //to include that element in the child's query;
        if (self.parentProperty != undefined && self.parentProperty != '') {

            request.friendlyColumns.push(self.parentProperty);
        }
        request.RowCount = 150000;
        request.pageNumber = 1;

        //displayItems.InitializeByQuery(request, self.populateWithResults);

        //console.log(SaveElements.selectContents);
        displayItems.InitializeByQuery(request, function (results) {
            SaveElements.selectContents.molecules = SaveElements.selectContents.molecules.concat(results);
            self.populateWithResults(results);
        });

    };

    self.populateUniqueSelect = function (uniqueList) {

        var d = uniqueList;
        var dispArray = uniqueList.split(',');
        for (var i = 0; i < dispArray.length; i++) {
            $select.append($('<option>', {
                value: dispArray[i],
                text: dispArray[i]
            }));
        }

    };

    self.populateWithResults = function (uniqueList) {//, uniqueSelect

        if (!self.disableThing) {
            $select.removeAttr('disabled');
        }

        var d = displayItems;
        var dispArray = _displayFields.split(',');

        self.selectElementThing.molecules = uniqueList;

        self.selectElementThing.sortThingsByMultipleAttributes([{
            property: dispArray[0],
            dir: 'asc'
        }]);

        var options = $select[0].options;
        $select.append($('<option>', {
            value: null,
            text: '(none selected)'
        }));

        for (var i = 0; i < self.selectElementThing.molecules.length; i++) {

            var mol = self.selectElementThing.molecules[i];
            var displayText = '';
            for (var x = 0; x < dispArray.length; x++) {

                if ((x > 0 && x < dispArray.length - 1) || x == 1) {
                    displayText += ' - ';
                }
                displayText += mol[dispArray[x]];
            }

            if (self.disableSelectValue != undefined) {
                for (var p = 0; p < self.disableSelectValue.length; p++) {
                    if (self.disableSelectValue[p].toLowerCase() == mol.header.exposedId.toLowerCase()) {
                        $select.append($('<option>', {
                            value: mol.header.exposedId,
                            text: displayText,
                            disabled: true
                        }));
                    } else {
                        $select.append($('<option>', {
                            value: mol.header.exposedId,
                            text: displayText
                        }));
                    }
                }
            } else {
                $select.append($('<option>', {
                    value: mol.header.exposedId,
                    text: displayText
                }));
            }


        }

        if (self.parentElement != undefined) {
            var parent = self.parentElement;
            parent.childElement.applySelectFilter(parent.atom.valueThingId);
        }

        if (self.atom.valueThingId != undefined) {

            var match = $.grep(options, function (obj) { return obj.value === self.atom.valueThingId; })[0];

            if (match != undefined) {
                var option = $(match);
                option.attr('selected', 'selected');

                if (self.childElement != undefined) {

                    self.childElement.applySelectFilter(self.atom.valueThingId);
                }

            }
        } else {
            if (self.defaultOption) {
                if ($select.val().includes('none')) {
                    $select.val(self.defaultOptionValue);
                }
            }
            if (self.userDefault && !self.applyDefault) {
                if (SaveElements.userDefaultThings.molecules.length != 0) {

                    for (var i = 0; i < SaveElements.userDefaultThings.molecules.length; i++) {
                        var currentDefault = SaveElements.userDefaultThings.molecules[i];
                        var getAttribute = eMESA.attributeByName(currentDefault.attributes, 'Default');
                        var getCurrentType = getAttribute.valueThingId;
                        var match = $.grep(options, function (obj) { return obj.value === getCurrentType; })[0];
                        var selectMatch = false;
                        if (self.disableSelectValue != undefined) {
                            for (var z = 0; z < self.disableSelectValue.length; z++) {
                                if (match != undefined) {
                                    if (match.value == self.disableSelectValue[z]) {
                                        selectMatch = true;
                                    }
                                }
                            }
                        }

                        if (!selectMatch) {
                            if (match != undefined) {
                                var option = $(match);
                                option.attr('selected', 'selected');
                                var selectionValue = $select.val();
                                if (!self.skipBinding) {
                                    self.thing[_property] = displayItems.byExposedId(selectionValue);//save this userDefault for this element in SaveElements
                                }
                                self.addToSaveQueue(selectionValue);
                                if (self.childElement != undefined) {

                                    self.childElement.applySelectFilter(self.atom.valueThingId);
                                }

                            }
                        }
                    }
                }
            }
        }
        if (onHydrateCallback != null) {
            onHydrateCallback(self);
        }
    };

    self.applySelectFilter = function (parentValue) {

        $select.empty();
        var dispArray = _displayFields.split(',');

        var options = $select[0].options;
        $select.append($('<option>', {
            value: null,
            text: '(none selected)'
        }));

        var filteredResults = self.selectElementThing.ByValueThingId(self.parentProperty, parentValue);

        for (var i = 0; i < filteredResults.length; i++) {

            var mol = filteredResults[i];
            var displayText = '';
            for (var x = 0; x < dispArray.length; x++) {

                if ((x > 0 && x < dispArray.length - 1) || x == 1) {
                    displayText += ' - ';
                }
                displayText += mol[dispArray[x]];
            }

            $select.append($('<option>', {
                value: mol.header.exposedId,
                text: displayText
            }));

        }

        if (self.atom.valueThingId != undefined) {

            var match = $.grep(options, function (obj) { return obj.value === self.atom.valueThingId; })[0];

            if (match != undefined) {
                var option = $(match);
                option.attr('selected', 'selected');
            }
        }
    };


    //Kick off the tree functions
    self.nodesFromMolecules = function (children, parent) {
        var tree = [];
        let nodeDefinition;
        var icon;
        var hasChildren;
        var isEndNode;
        if (self.treeDefinition.preLoad === true) {
            return self.treeDefinition.preLoadFunction(children, parent);
        }

        nodeDefinition = self.nodeDefinitionByTypeId(children[0].header.typeOf.exposedId);

        if (nodeDefinition != undefined) {
            if (nodeDefinition.endNode) {
                isEndNode = true;
            } else {
                isEndNode = false;
            }
            if (nodeDefinition.hasChildren) {
                hasChildren = true;
            } else {
                hasChildren = false;
            }
            if (nodeDefinition.callback) {
                //the user-defined callback let's you customize the node and perform sorting
                return nodeDefinition.callback(children, parent);
            }
        }
        icon = nodeDefinition.icon;
        if (icon == undefined) {
            icon = '';
        }


        if (parent === "#") {
            //If you don't need to customize the node, don't use the callback key and it will use this defualt object

            for (let i = 0; i < children.length; i++) {
                let textDisplay = '';
                if (nodeDefinition.displayFields != undefined || nodeDefinition.displayFields != '') {
                    let dNames = nodeDefinition.displayFields.split(',');
                    for (let x = 0; x < dNames.length; x++) {
                        let name = dNames[x];

                        if (name != undefined && name != '') {
                            var checkName = children[i][name];
                            if (checkName != undefined) {
                                if (x === 0) {
                                    textDisplay += children[i][name];
                                } else {
                                    textDisplay += ' - ' + children[i][name];
                                }
                            }
                        }
                    }
                }
                if (nodeDefinition.alias != undefined) {
                    var alias = nodeDefinition.alias.split(',');
                    var toReplace = alias[0];
                    var replaceTo = alias[1];
                    if (children[i][nodeDefinition.displayFields] == toReplace) {
                        textDisplay = replaceTo;
                    } else {
                        textDisplay = children[i][nodeDefinition.displayFields];
                    }
                }
                let node = {
                    'id': children[i].header.exposedId,
                    'nodeId': children[i].header.exposedId,
                    'parent': '#',
                    'children': true,
                    'hasChildren': true,
                    'text': textDisplay,
                    'nodeThing': children[i],
                    'addHandler': nodeDefinition.addHandler,
                    'icon': icon,
                    'endNode': isEndNode
                };
                tree.push(node);
            }
        } else {

            for (let i = 0; i < children.length; i++) {
                let textDisplay = '';
                if (nodeDefinition.childDisplayFields != undefined || nodeDefinition.childDisplayFields != '') {
                    let dNames = nodeDefinition.childDisplayFields.split(',');
                    for (let x = 0; x < dNames.length; x++) {
                        let name = dNames[x];
                        if (name != undefined && name != '') {
                            var checkName = children[i][name];
                            if (checkName != undefined) {
                                if (x === 0) {
                                    textDisplay += children[i][name];
                                } else {
                                    textDisplay += ' - ' + children[i][name];
                                }
                            }
                        }
                    }
                }
                let node = {
                    'id': children[i].header.exposedId,
                    'nodeId': children[i].header.exposedId,
                    'parent': parent,
                    'children': hasChildren,
                    'text': textDisplay,
                    'nodeThing': children[i],
                    'icon': icon,
                    'hasChildren': hasChildren,
                    'addHandler': nodeDefinition.addHandler,
                    'endNode': isEndNode
                };
                tree.push(node);
            }
        }
        //Sort the array by the text property
        tree.sort((a, b) => (a.text > b.text) ? 1 : ((b.text > a.text) ? -1 : 0));
        return tree;
    };

    //gets a refrence to the node defintion passed in from the originating js file
    self.nodeDefinitionByTypeId = function (nodeTypeId) {
        for (let i = 0; i < self.treeDefinition.nodeDefinitions.length; i++) {
            var currentDefinition = self.treeDefinition.nodeDefinitions[i];
            for (var p = 0; p < currentDefinition.nodeTypeId.length; p++) {

                if (currentDefinition.nodeTypeId[p] == nodeTypeId) {
                    return currentDefinition;

                }
            }
        }
    };

    //This function will be called recursively to break up AJAX calls into manageable chunks
    self.multipleCalls = function (array, parent, count = 0) {
        if (count > array.length - 1) {
            return;
        }

        
        
        var checkNode = false;
        var preloaded = false;
        var checkFunction;
        for (var x = 0; x < array.length; x++) {
            var t = array[x];
            for (var l = 0; l < t.length; l++) {
                var thisNode = t[l];
                var checkthisSearch = self.nodeDefinitionByTypeId(thisNode.header.typeOf.exposedId);
                
                if (checkthisSearch.handleAllNodes) {
                    if (checkthisSearch.alternateId) {
                        let emptyNode = $('#' + thisNode.header.exposedId + '-' + parent);
                        emptyNode.addClass('jstree-loading');
                        emptyNode.removeClass('jstree-closed');
                        checkNode = true;
                        checkFunction = checkthisSearch.function;
                    } else {
                        let emptyNode = $('#' + thisNode.header.exposedId);
                        emptyNode.addClass('jstree-loading');
                        emptyNode.removeClass('jstree-closed');
                        checkNode = true;
                        checkFunction = checkthisSearch.function;
                    }

                }

                if (checkthisSearch.preloaded) {
                    preloaded = true;
                }
            }
        }

        if (checkNode) {
            for (var o = 0; o < array.length; o++) {
                checkFunction(array[o], parent);
            }
            return;
        }

        if (preloaded) {
            return;
        }

        if (count == 0) {
            for (var a = 0; a < array.length; a++) {
                for (var b = 0; b < array[a].length; b++) {

                    var emptyNode = $('#' + array[a][b].header.exposedId);
                    emptyNode.addClass('jstree-loading');
                    emptyNode.removeClass('jstree-closed');
                }

            }
        }

        let arrayofChildNodes = [];
        let deferredArray = [];
        for (var x = 0; x < array[count].length; x++) {
            let currentArray = array[count];
            let currentNode = currentArray[x];
            let loadNodeDefinition = {};
            
            let searchNode = self.nodeDefinitionByTypeId(currentNode.header.typeOf.exposedId);

            loadNodeDefinition.nodeExposedId = currentNode.header.exposedId;
            if (loadNodeDefinition.icon != undefined) {
                $("#jstree").jstree(true).set_icon(currentNode.header.exposedId, self.icon);
            }

            //instantiate a new things object so we can make our calls
            loadNodeDefinition.childNodes = new DTS.Things();

            if (searchNode.function != undefined) {
                if (searchNode.handlAllNodes == undefined) {
                    if (loadNodeDefinition.icon != undefined) {
                        $("#jstree").jstree(true).set_icon(currentNode.header.exposedId, self.icon);
                    }
                    searchNode.function(currentNode.header.exposedId);
                    return;
                }
            } else {
                loadNodeDefinition.nodeExposedId = currentNode.header.exposedId;
                if (loadNodeDefinition.icon != undefined) {
                    $("#jstree").jstree(true).set_icon(currentNode.header.exposedId, self.icon);
                }
                loadNodeDefinition.childNodes = new DTS.Things();
            }

            var request = self.buildRequest(searchNode, currentNode, x);

            let deferreds = $.ajax('/NativeMoleculeDAL/QueryMoleculesNative',
                {
                    method: 'POST',
                    data: {
                        request
                    },
                    success: function (result) {
                        var thisResult = new DTS.Things();
                        thisResult.molecules = result;
                        if (thisResult.molecules.length != 0) {
                            
                            loadNodeDefinition.childNodes.setMolecules(thisResult.molecules);
                            
                        }
                        self.loadNodes(loadNodeDefinition, searchNode, currentNode);
                    }
                });
            deferredArray.push(deferreds);
        }
        $.when(...deferredArray).done(function () {
            self.multipleCalls(array, parent, count += 1);
        });
    };

    self.loadNodes = function (loadNodeDefinition, searchNode, currentNode) {
        var emptyNode = $('#' + currentNode.header.exposedId);
        let treeData = $('#jstree').jstree();
        let nodeId = currentNode.header.exposedId;
        if (searchNode.preloaded) {
            for (let i = 0; i < self.jsTreeArray.length; i++) {
                if (currentNode.header.exposedId === self.jsTreeArray[i].nodeExposedId) {
                    var molecules = self.jsTreeArray[i].childNodes;
                    if (molecules.loadingStatus === 'loaded') {
                        let children = molecules.molecules;
                        if (children.length === 0) {
                            emptyNode.removeClass('jstree-loading');
                            emptyNode.addClass('jstree-leaf');
                        } else {
                            emptyNode.removeClass('jstree-loading');
                            emptyNode.addClass('jstree-closed');
                        }
                    }
                } else {
                    emptyNode.removeClass('jstree-loading');
                    emptyNode.addClass('jstree-leaf');
                }
            }
            return;
        }
        if (searchNode.getListByParent) {
            loadNodeDefinition.childNodes.removeInactive();
            loadNodeDefinition.childNodes.molecules = loadNodeDefinition.childNodes.byTypeId(currentNode.header.exposedId);
            loadNodeDefinition.childNodes.loadingStatus = 'loaded';
            self.jsTreeArray.push(loadNodeDefinition);
            if (loadNodeDefinition.childNodes.molecules.length > 0) {
                emptyNode.removeClass('jstree-loading');
                emptyNode.addClass('jstree-closed');
                treeData._model.data[nodeId].original.hasChildren = true;
            }
            else {
                emptyNode.removeClass('jstree-loading');
                emptyNode.addClass('jstree-leaf');
                treeData._model.data[nodeId].original.hasChildren = false;
            }
            return;
        }
        if (searchNode.getListByValueThingOfTypeId) {
            loadNodeDefinition.childNodes.removeInactive();
            loadNodeDefinition.childNodes.allResultMolecules = loadNodeDefinition.childNodes.molecules;
            loadNodeDefinition.childNodes.molecules = loadNodeDefinition.childNodes.byTypeId(currentNode.header.kindOf.exposedId);
            loadNodeDefinition.childNodes.loadingStatus = 'loaded';
            self.jsTreeArray.push(loadNodeDefinition);
            if (loadNodeDefinition.childNodes.molecules.length > 0) {
                emptyNode.removeClass('jstree-loading');
                emptyNode.addClass('jstree-closed');
                treeData._model.data[nodeId].original.hasChildren = true;
            }
            else {
                emptyNode.removeClass('jstree-loading');
                emptyNode.addClass('jstree-leaf');
                treeData._model.data[nodeId].original.hasChildren = false;
            }
            return;
        }
        if (searchNode.getListofValueThing) {
            loadNodeDefinition.childNodes.loadingStatus = 'loaded';
            if (loadNodeDefinition.childNodes.molecules.length > 0) {
                var checkExists = loadNodeDefinition.childNodes.molecules[0].ValueExistsOnOff;
                if (checkExists != undefined) {
                    loadNodeDefinition.childNodes.removeInactive();
                }
                if (searchNode.nodeChildAttributeValue != undefined) {
                    self.jsTreeArray.push(loadNodeDefinition);
                } else {
                    loadNodeDefinition.childNodes.molecules = loadNodeDefinition.childNodes.ByValueThingId(currentNode.header.exposedId);
                    self.jsTreeArray.push(loadNodeDefinition);
                }
                emptyNode.removeClass('jstree-loading');
                emptyNode.addClass('jstree-closed');
                treeData._model.data[nodeId].original.hasChildren = true;
            }
            else {
                emptyNode.removeClass('jstree-loading');
                emptyNode.addClass('jstree-leaf');
                treeData._model.data[nodeId].original.hasChildren = false;
            }
            return;
        }
    };

    self.buildRequest = function (searchNode, currentNode, i) {

        let request = {};

        if (searchNode.getListByParent) {
            var friendlyColumns;
            if (searchNode.friendlyColumns.length == 1) {
                friendlyColumns = searchNode.friendlyColumns[0];
            } else {
                friendlyColumns = searchNode.friendlyColumns[i];
            }

            let allQueryAttributes = [];
            request.attributes = allQueryAttributes;
            request.columns = [];
            request.DomainObjectId = currentNode.header.exposedId;
            request.ExistsOnly = true;
            request.friendlyColumns = friendlyColumns;
            request.RowCount = 10000;
            request.pageNumber = 1;

            return request;
        }

        if (searchNode.getListByValueThingOfTypeId) {
            var domainObjectId;
            var friendlyColumns;
            if (typeof searchNode.domainObjectId === 'object') {
                domainObjectId = searchNode.domainObjectId[i];
            } else {
                domainObjectId = searchNode.domainObjectId;
            }

            if (searchNode.friendlyColumns.length == 1) {
                friendlyColumns = searchNode.friendlyColumns[0];
            } else {
                friendlyColumns = searchNode.friendlyColumns[i];
            }
            var fks = [];
            var objectName = '';
            if (searchNode.fkAlias != undefined) {
                for (var q = 1; q < searchNode.fkAlias.length; q++) {
                    var currentAliasKey = searchNode.fkAlias[q];
                    var splitAlias = currentAliasKey.split(',');

                    if (currentNode.Label == splitAlias[0]) {
                        objectName = splitAlias[1];
                    } else {
                        objectName = currentNode.Label;
                    }
                }
            } else {
                objectName = currentNode.Label;
            }
            if (searchNode.fkKeys != undefined) {
                for (var p = 1; p < searchNode.fkKeys.length; p++) {
                    var fkKey = searchNode.fkKeys[p];
                    fks.push(eMESA.fkQueryObjectFriendly(currentNode.Label, fkKey));
                }
            }

            request.attributes = [{
                attributeName: objectName,
                NonNullValuesOnly: true
            }];
            request.columns = [];
            request.ForeignKeys = fks;
            request.DomainObjectId = domainObjectId;
            request.ExistsOnly = true;
            request.friendlyColumns = friendlyColumns;
            request.RowCount = 20000;
            request.pageNumber = 1;

            return request;
        }

        if (searchNode.getListofValueThing) {
            var friendlyColumns;
            var nodeChildType;
            var nodeAttributeName;
            var nodeAttributeValue;

            if (searchNode.friendlyColumns.length == 1) {
                friendlyColumns = searchNode.friendlyColumns[0];
            } else {
                friendlyColumns = searchNode.friendlyColumns[i];
            }
            if (searchNode.nodeChildType.length == 1) {
                nodeChildType = searchNode.nodeChildType[0];
            } else {
                nodeChildType = searchNode.nodeChildType[i];
            }
            if (searchNode.nodeChildAttributeName.length == 1) {
                nodeAttributeName = searchNode.nodeChildAttributeName[0];
            } else {
                nodeAttributeName = searchNode.nodeChildAttributeName[i];
            }
            if (searchNode.nodeChildAttributeValue != undefined) {
                if (searchNode.nodeChildAttributeValue.length == 1) {
                    nodeAttributeValue = searchNode.nodeChildAttributeValue[0];
                } else {
                    nodeAttributeValue = searchNode.nodeChildAttributeValue[i];
                }
            }

            if (searchNode.valueType == 'valueThingId') {
                request.attributes = [{
                    attributeName: nodeAttributeName,
                    valueThingId: currentNode.header.exposedId
                }];
            }
            if (searchNode.valueType == 'valueText') {
                let valueAttr = nodeAttributeValue;
                let value = currentNode[valueAttr];
                request.attributes = [{
                    attributeName: nodeAttributeName,
                    valueText: value
                }];
            }

            request.columns = [];
            request.DomainObjectId = nodeChildType;
            request.ExistsOnly = true;
            request.friendlyColumns = friendlyColumns;
            request.RowCount = 1000;
            request.pageNumber = 1;

            return request;
        }


    };

    //lookAheadForChildNodes takes the current node and then querries the DB for the current node's children
    //This function can find all of the possible types of child chilren for the parent 
    //(for example, site > project > sub project, PWO and WO, sub project > WO and PWO )
    //the number of requests needs to be looked at 

    self.lookAheadForChildNodes = function (currentNodes, parent) {
        //get the length of the array once.For long arrays this is more performant and will allow us to later check the
        //array length and determine how we want to break up our ajax calls
        var nodeLength = currentNodes.length;
        //IF we have more than 15 calls to make at once, break them up. 
        if (nodeLength > 15) {
            //this is a magic number that tells us how many AJAX calls to make at once
            var size = 15;
            var callArray = [];
            
            for (let i = 0; i < currentNodes.length; i += size) {
                var currentNode = currentNodes[i];
                callArray.push(currentNodes.slice(i, i + size));
            }

            self.multipleCalls(callArray, parent);
            //debugger;
            return;
        }
        //otherwise loop though all of the current nodes
        for (let i = 0; i < nodeLength; i++) {
            //create the node definitions object

            let loadNodeDefinition = {};
            let currentNode = currentNodes[i];
            var emptyNode = $('#' + currentNode.header.exposedId);
            emptyNode.addClass('jstree-loading');
            emptyNode.removeClass('jstree-closed');

            let searchNode = self.nodeDefinitionByTypeId(currentNode.header.typeOf.exposedId);

            if (searchNode.overrideLookAhead != undefined) {
                for (var o = 0; o < searchNode.overrideLookAhead.length; o++) {
                    if (currentNode.header.typeOf.exposedId.toLowerCase() == searchNode.overrideLookAhead[o].toLowerCase()) {
                        let emptyNode = $('#' + currentNode.header.exposedId);
                        emptyNode.addClass('jstree-leaf');
                        emptyNode.removeClass('jstree-loading');
                        return;
                    }
                }
            }

            if (searchNode.function != undefined && !searchNode.handleAllNodes) {
                if (loadNodeDefinition.icon != undefined) {
                    $("#jstree").jstree(true).set_icon(currentNode.header.exposedId, self.icon);
                }
                searchNode.function(currentNode.header.exposedId);
            } else if (searchNode.preloaded) {
                self.loadNodes(loadNodeDefinition, searchNode, currentNode);
            } else if (searchNode.function != undefined && searchNode.handleAllNodes) {
                searchNode.function([currentNode], parent);
            } else {
                loadNodeDefinition.nodeExposedId = currentNode.header.exposedId;
                if (loadNodeDefinition.icon != undefined) {
                    $("#jstree").jstree(true).set_icon(currentNode.header.exposedId, self.icon);
                }
                loadNodeDefinition.childNodes = new DTS.Things();
                var request = self.buildRequest(searchNode, currentNode, i);
                loadNodeDefinition.childNodes.InitializeByQuery(request, function (result) {
                    self.loadNodes(loadNodeDefinition, searchNode, currentNode);
                    return;
                });
            }

        }

    };

    self.getNodeResultsByChild = function (nodeId) {

        var emptyThing = new DTS.Things();

        for (var i = 0; i < self.jsTreeArray.length; i++) {

            var thisNode = self.jsTreeArray[i].nodeExposedId;
            if (thisNode.toLowerCase() == nodeId.toLowerCase()) {
                return self.jsTreeArray[i].childNodes;
            }

        }

        return emptyThing;

    };

    self.addTree = function (appendTo) {
        $('#' + appendTo).append('<div id="jstree" style="height:720px; overflow-y: scroll" />');
        $('#jstree').jstree({
            'core': {
                'themes': {
                    'name': 'proton',
                    'responsive': true,
                    'variant': "large"
                },
                'check_callback': true,

                'data': function (node, cb) {
                    if (node.id === "#") {
                        //call user-supplied function to create the root node in the main page JS file
                        var getBaseNodes = self.treeDefinition.rootNodes.function;
                        getBaseNodes(function (result) {
                            if (result.molecules != undefined) {
                                var tree = self.nodesFromMolecules(result.molecules, node.id);
                            } else {
                                var tree = self.nodesFromMolecules(result, node.id);
                            }
                            //debugger;
                            cb(tree);
                            if (self.treeDefinition.nodeTypeId == undefined) {
                                return;
                            }
                            //look ahead for the next set of nodes from the root
                            if (result.molecules != undefined) {
                                self.lookAheadForChildNodes(result.molecules, node.id);
                            } else {
                                self.lookAheadForChildNodes(result, node.id);
                            }
                        });
                    }
                    else {

                        //Look to our automatic node loader and see if we've got something to display
                        for (let i = 0; i < self.jsTreeArray.length; i++) {
                            if (node.original.nodeId === self.jsTreeArray[i].nodeExposedId) {
                                var molecules = self.jsTreeArray[i].childNodes;
                                if (molecules.loadingStatus === 'loaded') {
                                    let children = molecules.molecules;
                                    if (children.length === 0) {
                                        return;
                                    }
                                    var tree = self.nodesFromMolecules(children, node.original.nodeId);
                                    cb(tree);
                                    if (node.original.endNode === true) {
                                        return;
                                    }
                                    self.lookAheadForChildNodes(children, node.original.nodeId);
                                }
                                else {
                                    //Our children haven't loaded yet
                                    //Set a timer to check on the status
                                    var intervalId = setInterval(function () {
                                        if (molecules.loadingStatus === 'loaded') {
                                            var children = molecules.molecules;
                                            if (children.length === 0) {
                                                clearInterval(intervalId);
                                                return;
                                            }
                                            var tree = self.nodesFromMolecules(children, node.original.nodeId);
                                            cb(tree);
                                            if (node.original.endNode === true) {
                                                clearInterval(intervalId);
                                                return;
                                            } else {
                                                self.lookAheadForChildNodes(children);
                                                clearInterval(intervalId);
                                            }
                                        }
                                    }, 400);
                                }
                            }
                        }
                    }
                },

            },
            "plugins": ["search", "types"],
            "types": {
                "default": {
                    "icon": self.icon
                }
            },
            "search": {
                "case_insensitive": true,
                "show_only_matches": true
            }
        }).on('select_node.jstree', self.onNodeSelected);
        $('#jstree').on('after_open.jstree', self.redrawNodeChildren);
        //$('#jstree').on('before_open.jstree', function (e, treedata) {
        //    console.log('open')
        //    treedata.node.children.forEach(child => {
        //        console.log(treedata.instance._model.data[child].children)
        //        if (typeof treedata.instance._model.data[child].children !== 'boolean') {
        //            if (treedata.instance._model.data[child].children.length < 1) {
        //                $(`#${child}`).removeClass('jstree-closed');
        //                $(`#${child}`).addClass('jstree-leaf');
        //            }
        //        }
        //    });
        //});
        if (_addHandler != undefined) {
            //_addHandler(e);
            _addHandler(self);
        }
    };
    self.redrawNodeChildren = function (e, data) {
        var nodeId = data.node.id;
        var thisNode = data.instance._model.data[nodeId];
        var state = data.node.state.loaded;
        var treeStuff = $("#jstree").jstree();


        for (var i = 0; i < thisNode.children.length; i++) {
            var childId = thisNode.children[i];
            var getChildNode = data.instance._model.data[childId];
            var childElement = $('#' + childId);
            var childState = childElement[0].classList.value;
            if (!childState.includes('jstree-loading')) {
                var hasChildren = getChildNode.original.hasChildren;
                if (!hasChildren) {
                    $(`#${childId}`).removeClass('jstree-closed');
                    $(`#${childId}`).addClass('jstree-leaf');
                } else {
                    if (!childState.includes('jstree-open')) {
                        $(`#${childId}`).addClass('jstree-closed');
                        $(`#${childId}`).removeClass('jstree-leaf');
                    } else {
                        $(`#${childId}`).addClass('jstree-open');
                        $(`#${childId}`).removeClass('jstree-leaf');
                    }
                }
            }
        }

        //    if (typeof treeStuff._model.data[childId].children !== 'boolean') {
        //        if (treeStuff._model.data[childId].children.length < 1) {
        //            $(`#${childId}`).removeClass('jstree-closed');
        //            $(`#${childId}`).addClass('jstree-leaf');
        //        }
        //    }
        //}



        //if (thisNode.original.nodeThing.header.typeOf.name == 'Code System') {
        //    var thisSystem = thisNode;

        //    var newLabel = thisSystem.original.nodeThing.Label;
        //    var systemMC = thisSystem.original.nodeThing.systemMC;
        //    var systemRFC = thisSystem.original.nodeThing.systemRFC;
        //    var systemRFSU = thisSystem.original.nodeThing.systemRFSU;

        //    var newText = `<span>&nbsp&nbsp</span>
        //                        <span style="font-size: 12px, text-align: center">` + newLabel + `</span>
        //                        <span class="systembox ` + systemMC + ` marginLeft" style="font-size: 12px, text-align: center">MC</span>
        //                        <span class="systembox ` + systemRFC + `" style="font-size: 12px, text-align: center">RFC</span> 
        //                        <span class="systembox ` + systemRFSU + `" style="font-size: 12px, text-align: center">RFSU</span>`;

        //    var thisElements = $('#' + nodeId);

        //    thisElements[0].children[1].children[0].nextSibling.nodeValue = '';
        //    thisElements[0].children[1].children[0].innerHTML = newText;
        //}

    };
    self.onNodeSelected = function (e, data) {

        var nodeId = data.node.id;
        var thisNode = data.instance._model.data[nodeId];
        var state = data.node.state.loaded;
        var treeStuff = $("#jstree").jstree();


        for (var i = 0; i < thisNode.children.length; i++) {
            var childId = thisNode.children[i];
            var getChildNode = data.instance._model.data[childId];
            var childElement = $('#' + childId);
            var checkChildElement = childElement[0];
            if (checkChildElement != undefined) {
                var childState = childElement[0].classList.value;
                if (!childState.includes('jstree-loading')) {
                    var hasChildren = getChildNode.original.hasChildren;
                    if (!hasChildren) {
                        $(`#${childId}`).removeClass('jstree-closed');
                        $(`#${childId}`).addClass('jstree-leaf');
                    } else {
                        $(`#${childId}`).addClass('jstree-closed');
                        $(`#${childId}`).removeClass('jstree-leaf');
                    }
                }
            }
        }

        var x = e;

        if (_addHandler != undefined) {
            //_addHandler(e);
            self.event = e;
            _addHandler(e, data);
        }

    };
    //End Tree functions

    self.renderDetailPane = function (thingDetail) {

        //Remove animation
        $('#detailSpinner').remove();

        var detailPaneId = 'paneDetail';
        var detailPaneColOneId = detailPaneId + 'colOne';
        var detailPaneColTwoId = detailPaneId + 'colTwo';

        $('#' + detailPaneId).remove();


        if (self.treeDefinition.useNativePane) {
            //Title
            var titleStr = '';
            if (self.treeDefinition.detailPaneTitle == undefined) {
                titleStr = thingDetail.header.label;
            }
            else {
                titleStr = self.treeDefinition.detailPaneTitle;
            }

            var appendStr = '<div id="' + detailPaneId + '" style="outline: 2px solid light-blue" ><p><h5>' + titleStr + '</h3></p><p>Exposed Id: ' + thingDetail.header.exposedId + '</p> ';
            appendStr += '<div class="row">';
            appendStr += '<div class="col-md bg-light" id = "' + detailPaneColOneId + '" style="width: 400px; padding-right:70px" ></div>';
            appendStr += '<div class="col-md bg-light" id = "' + detailPaneColTwoId + '"  style="width: 400px; padding-left:70px"></div>';
            appendStr += '</div>';
            appendStr += '</div>';

            $('#' + self.treeDefinition.appendDetailPaneTo).append(appendStr);
            thingDetail.attributes.sort(function (a, b) {

                return a.isDefault - b.isDefault;

            });
            for (var i = 0; i < thingDetail.attributes.length; i++) {

                var atom = thingDetail.attributes[i];
                var attLabel = '';
                if (atom.name == atom.label) {
                    attLabel = atom.label;
                }
                else {
                    attLabel = atom.name + '<br>' + atom.label;
                }

                var paneAppendTo = '';;

                if (i < (thingDetail.attributes.length / 2)) {

                    paneAppendTo = detailPaneColOneId;
                }
                else {
                    paneAppendTo = detailPaneColTwoId;
                }

                new DTS.ThingElements().addElement({
                    elementType: self.elementTypeByAtom(atom),
                    label: attLabel,
                    thing: thingDetail,
                    property: atom.name.replace(/\s/g, ''),
                    appendTo: paneAppendTo,
                    displayFields: 'Label',
                    includeFormGroup: true
                });
            }

            appendStr = '<div class="row">';
            appendStr += '<button class="btn btn-success btn-lg" id= "detailPaneSaveButton" name= "detailPaneSaveButton" style="width:300px" >Save</button>';
            appendStr += '</div>';
            $('#' + detailPaneId).append(appendStr);

            var $saveButton = $('#detailPaneSaveButton');
            $saveButton.on('click', $saveButton, self.detailPaneSaveButtonClicked);

        }
    };

    self.detailPaneSaveButtonClicked = function (e) {

        e.stopPropagation();
        e.preventDefault();

        $('#' + self.treeDefinition.appendDetailPaneTo).block({ message: 'Saving...' });
        SaveElements.save(function () {

            $('#' + self.treeDefinition.appendDetailPaneTo).unblock();
        });

    };

    self.elementTypeByAtom = function (atom) {

        var elType = '';
        switch (atom.dataType) {
            case 'ValueText':
                elType = 'textbox';
                break;
            case 'ValueLong':
                elType = 'textbox';
                break;
            case 'ValueDatetime':
                elType = 'datepicker'
                break;
            case 'ValueExistsOnOff':
                elType = 'radio';
                break;
            case 'ValueBoolean':
                elType = 'radio';
                break;
            case 'ValueLabel':
                elType = 'label';
                break;
            case 'ValueThingId':
                elType = 'select';
                break;
            case 'ValueFloat':
                elType = 'textbox';
                break;
            case 'ValueBigint':
                elType = 'textbox';
            default:
        }
        return elType;
    };

    self.addTable = function (headerLabel, id, headerFields, appendTo, prependTo, isSubTable, level) {
        var tableStr = '';
        var width = 100;
        if (level != undefined) {
            width = 100 - (15 * level);

        }

        tableStr += '<div class = "table-responsive">';
        tableStr += '<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row">';
        tableStr += '<table class="table table-sm dataTable no-footer" style="width: ' + width + '%; align:right" id="DataTables_Table_0" role="grid" id = "' + id + '" >';

        if (headerFields != null && headerFields != undefined) {

            tableStr += '<thead><tr role="row">';

            if (isSubTable != undefined && level != undefined) {

                //for (var x = 0; x < level; x++) {
                //    tableStr += '<th class="sorting_disabled" rowspan="1" colspan="1" ></th>';
                //}
            }
            for (var i = 0; i < headerFields.length; i++) {

                var headerField = headerFields[i];
                if (headerField == 'tableaction' || headerField == 'caption') {
                    tableStr += '<th class="sorting_disabled.custom-row-actions" rowspan="1" colspan="1" ><h5 >' + headerLabel + '</h5></th>';//style="width: 100%;"
                }
                else {
                    tableStr += '<th class="sorting_disabled" rowspan="1" colspan="1" >' + headerField + '</th>';//style="width: 347.4px;"
                }
            }

            tableStr += '</tr></thead>';
        }
        tableStr += '<tbody id = "' + id + 'body"></tbody>';
        tableStr += '</table>';
        tableStr += '</div>';
        tableStr += '</div>';

        if (appendTo != undefined || prependTo != undefined) {
            var el = $(tableStr);
            if (appendTo != undefined || prependTo != undefined) {

                if (isSubTable) {
                    if (level == undefined) {
                        level = 1;
                    }
                    var opacity = 1 - (level * .15);
                    var tdId = id + '_td';
                    var holderTdId = id + '_holder';
                    if (appendTo != undefined) {
                        $('#' + appendTo).after($('<td  id= "' + holderTdId + '" colspan="100"  ><div class ="col-sm-12" id="' + tdId + '"></div></td>'));
                        $('#' + tdId).append(el);
                    } else if (prependTo != undefined) {
                        $('#' + prependTo).after($('<td  id= "' + holderTdId + '" colspan="100"  ><div class ="col-sm-12" id="' + tdId + '"></div></td>'));
                        $('#' + tdId).prepend(el);
                    }

                    $(el).animate({
                        backgroundColor: 'rgb(201, 205, 211)',
                        opacity: opacity,
                        paddingTop: '20px',
                        paddingBottom: '5px',
                        paddingRight: '25px'

                    }, 150, function () {
                        // Animation complete.
                    });
                    $(el).animate({
                        paddingLeft: '40px',

                    }, 150, function () {
                        // Animation complete.
                    });

                }
                else {
                    if (appendTo != undefined) {
                        $('#' + appendTo).append(el);
                    } else if (prependTo != undefined) {
                        $('#' + prependTo).prepend(el);
                    }

                }
            }
        }
    };

    //Hide/Show Elements
    self.hideElement = function () {
        var $element = $('#' + self.id);
        $element.hide();
        var $elementLabel = $element.closest('div.form-group');
        $elementLabel.hide();
        if (self.exposedElementType == 'autocompleter') {
            $('#' + self.id + '_fasearch').hide();
        }
    };

    self.disableElement = function () {

        if (self.exposedElementType == 'iconHelper') {

            $('#' + self.exposedProperty).attr('disabled', 'disabled');

        } else {

            $('#' + self.id).attr('disabled', 'disabled');

        }

    };

    self.showElement = function () {
        var $element = $('#' + self.id);
        $element.show();
        var $elementLabel = $element.closest('div.form-group');
        $elementLabel.show();
        if (self.exposedElementType == 'autocompleter') {
            $('#' + self.id + '_fasearch').show();
        }
    };

    self.addTableDynamic = function (args) {

        var headerLabel = args.headerLabel;
        var id = args.id;
        var appendTo = args.appendTo;
        var isSubTable = args.isSubTable;
        var level = args.level;
        var hasExpandButton = args.hasExpandButton;
        var headerFields = args.headerFields;
        var hasBorder = false;
        if (args.hasBorder) {
            hasBorder = true;
        }
        var tableStr = '';
        var width = 100;
        if (level != undefined) {
            width = 100 - (15 * level);
        }

        if (args.overrideWidth != undefined) {
            width = args.overrideWidth;
        }

        if (isSubTable == undefined || !isSubTable) {
            tableStr += '<div class = "table-responsive">';
        }
        var expandButtonsToBind = [];
        var tableBorderStyle = 'table table-sm dataTable no-footer  table-striped';
        if (hasBorder) {
            tableBorderStyle = 'table table-sm dataTable no-footer table-bordered';
        }
        tableStr += '<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" style="overflow-y:hidden"><div class="row">';
        tableStr += '<table class="' + tableBorderStyle + '" style="width: ' + width + '%; align:right" id="DataTables_Table_' + id + '" role="grid" id = "' + id + '" >';
        tableStr += '<thead><tr><th><h5>' + headerLabel + '</h5></th><tr></thead>';

        if (headerFields != null && headerFields != undefined) {

            tableStr += '<thead><tr role="row">';

            if (isSubTable != undefined && level != undefined && hasExpandButton) {

                for (var x = 0; x < level; x++) {
                    //tableStr += '<th class="sorting_disabled" rowspan="1" colspan="1" ></th>';
                }
            }

            if (args.allowDelete) {
                tableStr += '<th class="sorting_disabled.custom-row-actions" rowspan="1" colspan="1" ></th>';

            }
            
            for (var i = 0; i < headerFields.length; i++) {

                var headerField = headerFields[i];
                if (headerField == 'tableaction' || headerField == 'caption') {
                    tableStr += '<th class="sorting_disabled.custom-row-actions" rowspan="1" colspan="1" ></th>';
                }
                else {
                    var sortStr = '';
                    if (args.headerSort != undefined && args.headerSort[headerField].includes('sorting')) {
                        sortStr = 'sorting';
                        //expandButtonsToBind.push('sort_' + headerField);
                        expandButtonsToBind.push(args.headerSort[headerField]);
                    }
                    tableStr += '<th class="' + sortStr + '" rowspan="1" colspan="1" id="' + args.headerSort[headerField] + '" >' + headerField + '</th>';
                }
            }

            tableStr += '</tr></thead>';
        }
        tableStr += '<tbody id = "' + id + 'body"></tbody>';
        tableStr += '</table>';
        tableStr += '</div>';
        if (isSubTable == undefined || !isSubTable) {
            tableStr += '</div>';
        }
        if (appendTo != undefined) {
            var el = $(tableStr);
            if (appendTo != undefined) {

                if (isSubTable) {
                    if (level == undefined) {
                        level = 1;
                    }
                    var opacity = 1 - (level * .2);
                    var tdId = id + '_td';
                    var holderTdId = id + '_holder';
                    $('#' + appendTo).after($('<td id= "' + holderTdId + '" colspan="100"   ><div class ="col-sm-12" id="' + tdId + '"></div></td>'));
                    $('#' + tdId).append(el);

                    $(el).animate({
                        backgroundColor: 'rgba(201, 205, 211, ' + opacity + ')',
                        //opacity: opacity,
                        paddingTop: '20px',
                        paddingBottom: '5px',
                        paddingRight: '25px'

                    }, 500, function () {
                        // Animation complete.
                    });
                    $(el).animate({
                        paddingLeft: '100px',

                    }, 500, function () {
                        // Animation complete.
                    });

                }
                else {
                    $('#' + appendTo).append(el);

                }
            }
        }

        for (var x = 0; x < expandButtonsToBind.length; x++) {

            var $expandButton = $('#' + expandButtonsToBind[x]);
            $expandButton.on('click', $expandButton, args.toggleHandler);

        }

    };

    self.addToSaveQueue = function (value, isMaxRange) {

        if (self.isSearch) {
            //This is for a dynamic search form context when we need min/max ranges for date and float attributes
            //We use the normal atom/element binding for the min value, but we need an extra one for the max value

            if (isMaxRange != undefined && isMaxRange) {
                self.atomMax.isMax = true;
                if (!self.atomMax.exposedId.includes('max')) {
                    self.atomMax.exposedId += 'max';
                }
                SaveElements.addThingToFilterList(self.thing, self.atomMax);
            }
            else {
                var atom = self.thing.atomByPropertyName(_property);
                if (atom.dataType == 'ValueThingId') {

                    //var valueThingProperty = 'property' + 'ExposedId';
                    var valueThingProperty = _property + 'ExposedId';
                    self.thing[valueThingProperty] = value;
                }
                self.thing[_property] = value;
                self.thing.syncMoleculeValue(_property);
                SaveElements.addThingToFilterList(self.thing, self.atom);
                //SaveElements.addThingToFilterList(self.thing, self.thing.atomByPropertyName(_property));
            }
            return;
        }

        //Maintain a list of saved things/attributes
        var atom = self.thing.atomByPropertyName(_property);
        if (atom.dataType == 'ValueThingId') {

            //var valueThingProperty = 'property' + 'ExposedId';
            var valueThingProperty = _property + 'ExposedId';
            self.thing[valueThingProperty] = value;
        }
        else {
            self.thing[_property] = value;

        }
        self.thing.syncMoleculeValue(_property);

        if (!self.skipSave && !self.isSearch) {

            SaveElements.addThingToSave(self.thing, self.thing.atomByPropertyName(_property));

        }
    };

    //Handlers
    self.treeNodeSelected = function (e, data) {

        console.log('Node selected');
        console.log(data);

        $('#' + self.treeDefinition.appendDetailPaneTo).empty();
        $('#' + self.treeDefinition.appendDetailPaneTo).append('<i class="fa fa-spin fa-spinner" id="detailSpinner" /> ');

        if (data.node.original.nodeThing.isHeaderOnly) {

            var passMol = new DTS.Molecule();
            passMol.CreateByExposedId(data.node.original.nodeThing.header.exposedId, self.renderDetailPane);

        }
        else {
            self.renderDetailPane(data.original.nodeThing);
        }
    };

    self.textChanged = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {
            var saveButton = SaveElements.saveBtnId;
            if (saveButton != undefined) {
                saveButton.attr('disabled', 'disabled');
            }

        }

        if (isFreeText) {

            console.log('Free text search ' + e.target.value);
            if (e.target.value.length == 0) {
                self.addToSaveQueue(e.target.value);
                return;
            }

            $('.dynamicSearch').attr('disabled', true);
            let spinnerId = self.id + '_searchSpinner';
            $('#' + self.id).after('<i class="fa fa-spin fa-spinner" id="' + spinnerId + '"></i>');
            var allQueryAttributes = [];
            var s = _searchFields.split(',');
            for (var i = 0; i < s.length; i++) {

                var queryAttributes = {};
                var searchAtom = self.valueThing.atomByPropertyName(s[i]);
                queryAttributes.attributeId = searchAtom.exposedId;
                queryAttributes.valueText = e.target.value;
                allQueryAttributes.push(queryAttributes);
            }

            var displayAtts = [];
            displayAtts.push('D9BCBDA0-2EBA-45B1-BFF6-4D52FD3EBA55');

            var request = {};
            request.attributes = allQueryAttributes;
            request.columns = displayAtts;
            request.DomainObjectId = _autocompleteTypeId;
            request.ExistsOnly = false;
            request.RowCount = 500;
            request.pageNumber = 1;
            request.UseOr = true;

            displayItems.InitializeByQuery(request, function (result) {
                var returnLabel = [];
                displayItems.removeInactive();
                console.log('Search results found: ' + result.length);
                var returnExposedIds = [];

                for (var i = 0; i < result.length; i++) {

                    returnExposedIds.push(result[i].header.exposedId);
                }
                self.addToSaveQueue(returnExposedIds);
                $('#' + spinnerId).remove();
                $('.dynamicSearch').attr('disabled', false);
            });


            return;
        }

        var t = e.target.id;
        if (!self.skipBinding) {
            self.addToSaveQueue(e.target.value);
            self.event = e;
        }
        if (_addHandler != undefined) {
            _addHandler(self);
        }
        if (saveButton != undefined) {

            saveButton.attr('disabled', false);

        }
    };

    self.checkBoxChanged = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {
            var saveButton = SaveElements.saveBtnId;
            if (saveButton != undefined) {
                saveButton.attr('disabled', 'disabled');
            }
        }
        var t = e.target.checked;
        if (!self.skipBinding) {
            self.thing[_property] = t;
            self.addToSaveQueue(t);
            self.event = e;
        }
        if (_addHandler != undefined) {
            _addHandler(self);
        }
        if (saveButton != undefined) {
            saveButton.attr('disabled', false);
        }
    };

    self.iconClicked = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {
            var saveButton = SaveElements.saveBtnId;
            if (saveButton != undefined) {
                saveButton.attr('disabled', 'disabled');
            }
        }
        var t = e.target.id;
        if (!self.skipBinding) {
            self.addToSaveQueue(e.target.checked);
            self.event = e;
        }
        if (_addHandler != undefined) {
            _addHandler(self);
        }
        if (saveButton != undefined) {
            saveButton.attr('disabled', false);

        }
    };

    self.userDefaultSelected = function (e) {

        $('#' + self.id + 'userDefault').removeClass("fa fa-star blue userDefaultStar clickable");
        $('#' + self.id + 'userDefault').addClass("fa fa-spin fa-spinner");
        var t = new DTS.Things();
        var theseDefaults = new DTS.Things();
        var userDefaultType = SaveElements.userDefaultType;
        userDefaultType.applyMappedValues();
        var defaultExists = false;
        $select = $('#' + self.id);
        var options = $select[0].options;
        if (SaveElements.userDefaultThings.molecules.length != 0) {

            for (var i = 0; i < SaveElements.userDefaultThings.molecules.length; i++) {
                var currentDefault = SaveElements.userDefaultThings.molecules[i];
                var user = currentDefault.PersonnelExposedId;
                var form = currentDefault.Form;
                var currentUser = userDefaultType.Personnel.header.exposedId;
                var currentForm = userDefaultType.Form;
                var getAttribute = eMESA.attributeByName(currentDefault.attributes, 'Default');
                var match = $.grep(options, function (obj) { return obj.value === getAttribute.valueThingId; })[0];

                if ((user.toLowerCase() == currentUser.toLowerCase()) && (form.toLowerCase() == currentForm.toLowerCase()) && match != undefined) {
                    var newDefault = new DTS.Molecule();
                    defaultExists = true;
                    newDefault.CreateByExposedId(self.atom.valueThingId, function () {
                        var getDefaultAttribute = eMESA.attributeByName(currentDefault.attributes, 'Default');
                        getDefaultAttribute.valueThingId = newDefault.header.exposedId;
                        currentDefault.applyMappedValues();
                        theseDefaults.molecules.push(currentDefault);
                        t.createOrModifyBySet(theseDefaults.molecules, function (result) {

                            if (result != 'Unauthorized') {
                                console.log('Save complete');

                            }
                            console.log('Post-save complete');
                            $('#' + self.id + 'userDefault').removeClass("fa fa-spin fa-spinner");
                            $('#' + self.id + 'userDefault').addClass("fa fa-check green");

                        });
                    });


                }
            }
        }
        if (!defaultExists) {
            var newGuid = eMESA.returnGuid();
            var newUserDefault = new DTS.Molecule();
            newUserDefault.CreateByType(userDefaultType, newGuid, 'newUserDefault');
            var newDefault = new DTS.Molecule();
            newDefault.CreateByExposedId(self.atom.valueThingId, function () {
                var getDefaultAttribute = eMESA.attributeByName(newUserDefault.attributes, 'Default');
                getDefaultAttribute.valueThingId = newDefault.header.exposedId;
                newUserDefault.applyMappedValues();
                theseDefaults.molecules.push(newUserDefault);
                t.createOrModifyBySet(theseDefaults.molecules, function (result) {

                    if (result != 'Unauthorized') {
                        console.log('Save complete');

                        SaveElements.userDefaultThings.molecules.push(newUserDefault);


                    }
                    console.log('Post-save complete');

                    $('#' + self.id + 'userDefault').removeClass("fa fa-spin fa-spinner");
                    $('#' + self.id + 'userDefault').addClass("fa fa-check green");

                });
            });
        }


    };

    self.helpTextClicked = function (e) {

        var getHelpTextModal = self.createHelpTextModal();
        var $checkModal = $('#helpTextModal');

        if ($checkModal != undefined) {
            $checkModal.remove();
        }

        $('#bodyContainer').append(getHelpTextModal);
        $('#helpTextModal').modal('toggle');
        var helpTexts = SaveElements.helpTexts;
        var helpTextId = e.currentTarget.id;
        helpTextId = helpTextId.replace('helpText', '');
        helpTextId = helpTextId.replace('Text', '');
        var helpText = helpTexts.byExposedId(helpTextId);
        var $label = $('#' + e.currentTarget.id).closest('label');
        var property = $label.text();
        $('#titleHelpText').text(property);
        if (helpText != undefined) {

            if (SaveElements.isLogInAdmin) {
                new DTS.ThingElements().addElement({
                    elementType: 'textarea',
                    label: null,
                    thing: helpText,
                    property: 'Text',
                    appendTo: 'helpText',
                    includeFormGroup: false
                });
            } else {
                new DTS.ThingElements().addElement({
                    elementType: 'label',
                    label: null,
                    thing: helpText,
                    property: 'Text',
                    appendTo: 'helpText',
                    includeFormGroup: false
                });
            }
        }

    };

    self.createHelpTextModal = function () {
        var helpTextModal = `<div class="modal fade" id="helpTextModal" role="dialog">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="titleHelpText"></h4>
                                            </div>
                                            <div class="modal-body" id="helpText">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" id="helpTextClose" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
        return helpTextModal;
    };

    self.selectValueChanged = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {

            var saveButton = SaveElements.saveBtnId;
            if (saveButton != undefined) {
                saveButton.attr('disabled', 'disabled');
            }

        }

        var selectionValue = {};
        var $target = $(e.target);

        if (isMultiSelect) {
            selectionValue = $target.val();
        }
        else {
            selectionValue = $target[0].value;
        }
        //var t = $target[0].selectedIndex;

        //var label = $target[0].options[$target[0].selectedIndex].label;
        if (!self.skipBinding) {
            if (!isMultiSelect) {
                self.thing[_property] = displayItems.byExposedId(selectionValue);//change the display value so it matches to selected valueThingId to avoid confusion
            }
            self.addToSaveQueue(selectionValue);
        }

        if (self.childElement != undefined) {

            self.childElement.applySelectFilter(selectionValue);
        }

        if (_addHandler != undefined) {
            //_addHandler(e);
            self.event = e;
            _addHandler(self);
        }
        if (saveButton != undefined) {
            saveButton.attr('disabled', false);

        }

    };

    self.radioToTrue = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {
            var saveButton = SaveElements.saveBtnId;
            if (saveButton != undefined) {
                saveButton.attr('disabled', 'disabled');
            }
        }

        var $target = $(e.target);
        var bv = $(e.currentTarget.checked);
        var setValue = bv[0];

        if (setValue) {
            console.log('Setting true');
            self.thing[_property] = setValue;
            self.addToSaveQueue(setValue);
        }


        if (_addHandler != undefined) {
            //_addHandler(e);
            self.event = e;
            _addHandler(self);
        }
        if (saveButton != undefined) {
            saveButton.attr('disabled', false);

        }

    };

    self.radioToFalse = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {

            var saveButton = SaveElements.saveBtnId;
            if (saveButton != undefined) {
                saveButton.attr('disabled', 'disabled');
            }

        }

        var $target = $(e.target);
        var bv = $(e.currentTarget.checked);
        var setValue = bv[0];
        if (setValue) {
            console.log('Setting false');
            self.thing[_property] = !setValue;
            self.addToSaveQueue(!setValue);

        }
        if (_addHandler != undefined) {
            //_addHandler(e);
            self.event = e;
            _addHandler(self);
        }
        if (saveButton != undefined) {
            saveButton.attr('disabled', false);

        }

    };

    self.dateChanged = function (e) {
        if (SaveElements.saveBtnId != undefined && SaveElements.saveBtnId.length > 0) {

            var saveButton = SaveElements.saveBtnId;
            saveButton.attr('disabled', 'disabled');
        }
        var $target = $(e);
        var selectedDate = $target[0].date.toISOString();//.format('YYYY-MM-DD') 
        self.thing[_property] = selectedDate;

        self.addToSaveQueue(selectedDate);

        if (_addHandler != undefined) {
            //_addHandler(e);
            self.event = e;
            _addHandler(self);
        }
        if (saveButton != undefined) {
            saveButton.attr('disabled', false);
        }
    };

    self.dateChangedMax = function (e) {
        var $target = $(e);
        var selectedDate = $target[0].date.toISOString();//.format('YYYY-MM-DD') 
        self.atomMax.valueDateTime = selectedDate;
        self.addToSaveQueue(selectedDate, true);

    };

    self.refresh = function (updatedThing) {

        //self.thing = updatedThing;
        var baseElement = $(document.getElementById(self.id));
        var $el = ''

        if (baseElement != undefined) {

            $el = $(baseElement[0]);
        }

        if ($el != undefined) {

            switch (_elementType) {
                case 'textbox':
                    $el.val(updatedThing[_property]);
                    break;
                case 'datepicker':
                    if (updatedThing[_property] != null) {
                        var dateVal = new Date(updatedThing[_property]).toISOString();
                        $el.val(dateVal);
                        $el.val(moment($el.val()).format('YYYY-MM-DD'));
                    }
                    break;
                case 'select':
                    var atom = updatedThing.atomByPropertyName(_property);
                    if (atom.valueThingId != undefined) {
                        if ($el[0] != undefined) {
                            var options = $el[0].options;
                            var match = $.grep(options, function (obj) { return obj.value === atom.valueThingId; })[0];

                            if (match != undefined) {
                                var option = $(match);
                                option.attr('selected', 'selected');
                            }
                        }
                    }
                    break;
                case 'label':
                    var displayText = '';
                    if (self.atom.dataType != 'ValueThingId') {
                        console.log('Refreshing label native for thingId ' + self.exposedThing.header.exposedId + 'property: ' + _property);

                        displayText = updatedThing[_property];
                        $el.text(displayText);
                    }
                    break;
                case 'radio':
                    break;
                case 'textarea':
                    $el.val(updatedThing[_property]);
                    break;
                case 'autocompleter':
                    var displayValueThing = {};
                    var displayStr = '';
                    if (updatedThing[_property] != undefined) {
                        var dNames = _displayFields.split(',');
                        displayValueThing = updatedThing[_property];
                        displayStr += displayValueThing[dNames[0]];
                        $el.val(displayStr);

                    }
                    break;
                default:
                    break;
            }
        }
    };

};


DTS.Molecule = function (source) {

    var self = this;
    var Thing = new DTS.Things();
    self.isHeaderOnly = false;
    //this.attributes = [];
    //this.header = {};
    //this.properties = [];
    //this.relationships = [];
    //var m = this;

    self.mapToObject = function () {

        //Let's create friendlier representations so we don't have to use the .atom accessor
        if (self.attributes == undefined) {
            return;
        }
        if (self.attributes[0].name == undefined) {
            self.isHeaderOnly = true;
            self.Label = self.header.label;
            self.name = self.header.name;
            self.treeLeafFlag = self.header.treeLeafFlag;
            return;
        }

        for (var i = 0; i < self.attributes.length; i++) {

            var att = self.attributes[i];
            var property = att.name.replace(/\s/g, '');

            switch (att.dataType) {
                case 'ValueText':
                    self[property] = att.valueText;
                    break;
                case 'ValueLong':
                    self[property] = att.valueLong;
                    break;
                case 'ValueDatetime':
                    self[property] = att.valueDateTime;
                    break;
                case 'ValueExistsOnOff':
                    self[property] = att.valueLong;
                    break;
                case 'ValueBoolean':
                    self[property] = att.valueLong === 1;
                    break;
                case 'ValueLabel':
                    self[property] = self.header.label;
                    break;
                case 'ValueThingId':
                    var valueThingName = property + 'Name';
                    var valueThingExposed = property + 'ExposedId';
                    var valueThingTypeAttributeId = property + 'TypeOfId';
                    self[valueThingExposed] = att.valueThingId;
                    self[valueThingName] = att.displayValue;
                    self[valueThingTypeAttributeId] = att.typeOfAttributeExposedId;
                    self[property] = null;
                    break;
                //case 'ValueText':
                //    self[property] = att.valueText;
                //    break;
                case 'ValueFloat':
                    self[property] = att.valueDecimal;
                    break;
                case 'ValueBigint':
                    self[property] = att.valueLong;
                    break;
                case 'ValueGuid':
                    self[property] = att.valueGuid;
                    break;
                case 'ValueBlob':
                    self[property] = att.valueBlob;
                    break;
                default:
            }

        }
    };

    self.atomByPropertyName = function (propertyName) {
        var returnAtt = {};

        for (var i = 0; i < self.attributes.length; i++) {
            var att = self.attributes[i];
            var attName = att.name.replace(/\s/g, '');
            if (attName == propertyName && att.existsOnOff) {
                returnAtt = att;
                break;
            }

        }
        return returnAtt;

    };

    self.atomById = function (atomExposedId) {

        for (var i = 0; i < self.attributes.length; i++) {

            var att = self.attributes[i];

            if (att.exposedId.toLowerCase() == atomExposedId.toLowerCase()) {

                return att;
            }

        }
    };

    self.syncMoleculeValue = function (property) {

        var atom = self.atomByPropertyName(property);

        if (atom.dataType != 'ValueThingId') {
            atom.displayValue = self[property];
        }

        switch (atom.dataType) {
            case 'ValueText':
                atom.valueText = self[property];
                break;
            case 'ValueLong':
                atom.valueLong = self[property];
                break;
            case 'ValueDatetime':
                atom.valueDateTime = self[property];
                break;
            case 'ValueExistsOnOff':
                atom.valueLong = self[property];
                break;
            case 'ValueBoolean':
                var vLong = 0;
                if (self[property] == true) {
                    vLong = 1;
                }
                atom.valueLong = vLong;
                break;
            case 'ValueLabel':
                //debugger;
                self.header.label = self[property];
                atom.valueText = null;
                break;
            case 'ValueThingId':
                if (self[property] != undefined && self[property].header != undefined) {
                    //debugger;
                    atom.valueThingId = self[property].header.exposedId;
                }
                else {
                    var valueThingProperty = property + 'ExposedId'
                    if (self[valueThingProperty] != undefined) {
                        atom.valueThingId = self[valueThingProperty];
                    }
                    else {
                        atom.valueThingId = null;
                    }

                }
                break;
            case 'ValueFloat':
                atom.valueDecimal = self[property];
                break;
            case 'ValueBigint':
                atom.valueLong = self[property];
                break;
            case 'ValueBlob':
                atom.valueBlob = self[property];
                break;
            default:
        }
    };

    self.applyMappedValues = function () {

        //console.log('Mapping values');
        for (var i = 0; i < self.attributes.length; i++) {

            var att = self.attributes[i];

            if (att.name != undefined) {
                var property = att.name.replace(/\s/g, '');
            }

            if (self[property] != undefined) {
                self.syncMoleculeValue(property);
            }


        }
    };

    self.applyValueThings = function (valueThingMols) {

        for (var i = 0; i < valueThingMols.length; i++) {

            var valueThingMol = valueThingMols[i];
            var atts = self.atomsByDataType('ValueThingId');
            for (var x = 0; x < atts.length; x++) {

                var att = atts[x];
                if (att.valueThingId == valueThingMol.header.exposedId) {

                    var propertyName = att.name.replace(/\s/g, '');
                    self[propertyName] = valueThingMol;
                }
            }
        }
    };

    if (source != undefined) {

        self.attributes = source.attributes;
        self.header = source.header;
        self.properties = source.properties;
        self.relationships = source.relationships;
        self.resultCount = source.resultCount;
        self.mapToObject();
    }

    self.atom = function (atomName) {

        for (var i = 0; i < self.attributes.length; i++) {

            if (self.attributes[i].name.toLowerCase() == atomName.toLowerCase() && self.attributes[i].existsOnOff) {

                return self.attributes[i];
                break;
            }
        }
        return {};
    };

    self.atomsByDataType = function (dataType) {

        var returnAtts = [];
        for (var i = 0; i < self.attributes.length; i++) {

            if (self.attributes[i].dataType.toLowerCase() == dataType.toLowerCase()) {

                returnAtts.push(self.attributes[i]);

            }
        }
        return returnAtts;
    };

    self.CreateByType = function (source, newExposedId, newLabel) {

        if (newExposedId == undefined) {

            newExposedId = eMESA.returnGuid();
        }

        var fromSource = JSON.parse(JSON.stringify(source));
        self.attributes = fromSource.attributes;
        self.header = fromSource.header;
        self.properties = fromSource.properties;
        self.relationships = fromSource.relationships;
        self.header.exposedId = newExposedId;
        self.header.typeOf.name = JSON.parse(JSON.stringify(source.header.name));
        self.header.typeOf.exposedId = JSON.parse(JSON.stringify(source.header.exposedId));
        self.header.kindOf.name = JSON.parse(JSON.stringify(source.header.name));
        self.header.kindOf.exposedId = JSON.parse(JSON.stringify(source.header.exposedId));
        self.header.label = newLabel;
        if (newLabel == '') {
            self.header.name = 'defaultname';
        }
        else {
            self.header.name = newLabel;

        }
        self.mapToObject();
    };

    self.CreateByExposedId = function (byExposedId, callback) {

        if (byExposedId == undefined) {

            callback(self);
        }

        Thing.getMolecule(byExposedId, function (fromMolecule) {
            self.attributes = fromMolecule.attributes;
            self.header = fromMolecule.header;
            self.properties = fromMolecule.properties;
            self.relationships = fromMolecule.relationships;

            self.mapToObject();
            if (callback != undefined) {
                callback(self);

            }
        });

    };

   

    self.applyAttributeValuesFromSource = function (sourceAttributes) {

        var fromAttributes = JSON.parse(JSON.stringify(sourceAttributes));
        self.attributes = fromAttributes;
        self.mapToObject();
    };

    return this;
};



DTS.Things = function () {

    var self = this;
    self.molecules = [];
    self.allResultMolecules = [];
    self.filteredMolecules = [];
    self.loadingStatus = 'idle';//idle

    self.authHeaders = {
        "Authorization": 'Bearer ' + token,
        "X-Client": clientId
    };

    //loading
    //loaded

    this.InitializeByQuery = function (queryRequest, callback) {

        self.loadingStatus = 'loading';


        if (queryRequest.friendlyColumns != undefined && !queryRequest.friendlyColumns.includes('ValueExistsOnOff')) {
            queryRequest.friendlyColumns.push('ValueExistsOnOff');
            let queryAttributes = {};
            queryAttributes.attributeName = 'ValueExistsOnOff';
            queryAttributes.BigIntValue = 1;
            queryRequest.attributes.push(queryAttributes); 
        }
        if (queryRequest.friendlyColumns == undefined) {
            queryRequest.columns.push('d9bcbda0-2eba-45b1-bff6-4d52fd3eba55');
            let queryAttributes = {};
            queryAttributes.AttributeId = 'd9bcbda0-2eba-45b1-bff6-4d52fd3eba55';
            queryAttributes.BigIntValue = 1;
            queryRequest.attributes.push(queryAttributes); 
        }

        self.queryMolecules(queryRequest, function (resultList) {
            //console.log('InitializeByQuery results received');
            self.setMolecules(resultList);
            self.removeInactive();
            if (callback != undefined) {
                callback(self.molecules);
            }
            self.loadingStatus = 'loaded';

        });
    };


    this.MoleculesForStoredProcedure = function (storedProcedureName, parameter, tableName, table, callback) {

        console.log('Active AJAX Calls ' + $.active);

        $.ajax('/NativeMoleculeDAL/MoleculesForStoredProcedure',
            {
                method: 'POST',
                data: {
                    storedProcedureName: storedProcedureName,
                    parameter: parameter,
                    tableName: tableName,
                    table: table
                },
                complete: function (result) {
                    if (callback != undefined) {
                        self.setMolecules(result.responseJSON);
                        callback();
                    }
                },
                error: function (result) {
                    console.log('ResetApplicationUniverse error' + result.statusText);
                }

            });

    };

    this.InitializeByTypeQuery = function (typeId, callback) {

        self.GetListOfByType(typeId, function (resultList) {

            self.setMolecules(resultList);
            if (callback != undefined) {
                callback(self);
            }
        });
    };

    this.AppendByTypeQuery = function (typeId, callback) {

        self.GetListOfByType(typeId, function (resultList) {

            self.appendToMolecules(resultList);
            if (callback != undefined) {
                callback(self);
            }
        });
    };

    this.InitializeWithMolecules = function (resultList) {
        self.setMolecules(resultList);
    };

    this.AppendMolecules = function (resultList) {

        self.appendToMolecules(resultList);
    };

    this.AppendByQuery = function (queryRequest, callback) {

        self.loadingStatus = 'loading';
        self.queryMolecules(queryRequest, function (resultList) {

            self.appendToMolecules(resultList);
            callback();
            self.loadingStatus = 'loaded';
        });
    };

    this.removeDuplicates = function () {

        var newArray = [];

        for (var i = 0; i < self.molecules.length; i++) {

            var current = self.molecules[i];
            var header = current.header.exposedId;
            var inArray = false;
            for (var p = 0; p < newArray.length; p++) {
                var thisHeader = newArray[p].header.exposedId;
                if (thisHeader == header) {
                    inArray = true;
                }
            }
            if (!inArray) {
                newArray.push(current);
            }

        }

        self.molecules = [];

        self.molecules = newArray;

    };

    this.removeInactive = function () {

        var newArray = [];

        for (var i = 0; i < self.molecules.length; i++) {

            var current = self.molecules[i];
            var exists = current.ValueExistsOnOff;
            if (exists) {
                newArray.push(current);
            }

        }

        self.molecules = [];

        self.molecules = newArray;

    };

    this.GetAttributeDataTypes = function (callback) {

        $.ajax('/NativeMoleculeDAL/GetAttributeDataTypes',
            {
                method: 'GET',
                complete: function (result) {
                    if (callback != undefined) {
                        callback(result.responseJSON);
                    }
                },
                error: function (result) {
                    console.log('GetTopLevelObjects error' + result.statusText);
                }

            });
    };

    this.initializeWithTopTree = function (callback) {

        $.ajax('/NativeMoleculeDAL/GetTopLevelObjects',
            {
                method: 'GET',
                complete: function (result) {
                    console.log(result.responseJSON);
                    self.setMolecules(result.responseJSON);
                    if (callback != undefined) {
                        callback(self);
                    }
                },
                error: function (result) {
                    console.log('GetTopLevelObjects error' + result.statusText);
                }

            });
    };

    this.InitializeByList = function (exposedIds, callback) {

        self.getList(exposedIds, function (result) {
            self.removeInactive();
            callback(self.molecules);
        });
    };

    this.AppendByList = function (exposedIds, callback) {

        self.getList(exposedIds, function (result) {
            self.appendToMolecules(resultList);
        });
    };

    this.applyValueThingsToAll = function (valueThings) {

        for (var i = 0; i < self.molecules.length; i++) {

            self.molecules[i].applyValueThings(valueThings);

        }
    };

    this.GetListOfByType = function (typeIdStr, callback) {
        
        var endpoint = apiEndpoint + 'api/Molecule/GetListOf/';
        console.log('Endpoint: ' + endpoint);
        var typeId = {typeIdStr};
        return $.ajax(
            {
                method: 'GET',
                url: endpoint,
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders,
                data: typeId,
                complete: function (result) {
                    self.setMolecules([result.responseJSON]);
                    if (callback != undefined) {
                        callback(self.molecules);
                    }
                },
                error: function (e) {
                    console.log('Get TypebyType error' + e);
                }

            });
    };

    this.queryMolecules = function (queryRequest, callback) {

        if (queryRequest.RowCount == undefined) {

            queryRequest.RowCount = 10000;
        }
        if (queryRequest.pageNumber == undefined) {
            queryRequest.pageNumber = 1;
        }

        var request = {
            "Attributes": queryRequest.attributes,
            "Columns": queryRequest.columns,
            "DomainObjectId": queryRequest.DomainObjectId,
            "ExistsOnly": queryRequest.ExistsOnly,
            "FriendlyColumns": queryRequest.friendlyColumns,
            "RowCount": queryRequest.RowCount,
            "PageNumber": queryRequest.pageNumber,
            "SortKeys": queryRequest.sortKeys,
            "UseOr": queryRequest.UseOr,
            "ForeignKeys": queryRequest.ForeignKeys

            //"UniverseId": '917edcf1-c1b1-4563-87da-7a76068c09da'

        };

        var endpoint = apiEndpoint + 'api/Eplus/QueryMolecules/';
        console.log('Endpoint: ' + endpoint);

        var startTime, endTime;

        startTime = new Date();

        var sessionId = eMESA.returnGuid();

        return $.ajax(
            {
                method: 'POST',
                url: endpoint,
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders,
                data: request,
                complete: function (result) {
                    endTime = new Date();
                    var timeDiff = endTime - startTime; //in ms
                    // strip the ms
                    //timeDiff /= 1000;

                    // get seconds 
                    var seconds = Math.round(timeDiff);
                    console.log(seconds + " milleseconds to RETURN: ResultCount " + result.responseJSON.length + " " +  sessionId);
                    
                    if (callback != undefined) {
                        callback(result.responseJSON);
                    }
                },
                error: function (e) {
                    console.log('Query molecules error' + e);
                }

            });
    };

    this.queryMoleculesCompressed = function (queryRequest, callback) {

        if (queryRequest.RowCount == undefined) {

            queryRequest.RowCount = 10000;
        }
        if (queryRequest.pageNumber == undefined) {
            queryRequest.pageNumber = 1;
        }

        var request = {
            "Attributes": queryRequest.attributes,
            "Columns": queryRequest.columns,
            "DomainObjectId": queryRequest.DomainObjectId,
            "ExistsOnly": queryRequest.ExistsOnly,
            "FriendlyColumns": queryRequest.friendlyColumns,
            "RowCount": queryRequest.RowCount,
            "PageNumber": queryRequest.pageNumber,
            "SortKeys": queryRequest.sortKeys,
            "UseOr": queryRequest.UseOr,
            "ForeignKeys": queryRequest.ForeignKeys

            //"UniverseId": '917edcf1-c1b1-4563-87da-7a76068c09da'

        };

        var BASE64_MARKER = ';base64,';

        var endpoint = apiEndpoint + 'api/Eplus/QueryMoleculesCompressed/';
        console.log('Endpoint: ' + endpoint);

        var startTime, endTime;

        startTime = new Date();

        var sessionId = eMESA.returnGuid();

        return $.ajax(
            {
                method: 'POST',
                url: endpoint,
                headers: self.authHeaders,
                contentType: "application/x-www-form-urlencoded",
                data: request,
                complete: function (result) {
                    
                    endTime = new Date();
                    var timeDiff = endTime - startTime; //in ms
                    // strip the ms
                    //timeDiff /= 1000;

                    // get seconds 
                    var seconds = Math.round(timeDiff);
                    //console.log(seconds + " milleseconds to RETURN " + sessionId);

                    var b64Data = result.responseJSON;
                    // Decode base64 (convert ascii to binary)

                    var strData  = atob(b64Data);

                    endTime = new Date();
                    var timeDiff = endTime - startTime; //in ms
                    // strip the ms
                    //timeDiff /= 1000;

                    // get seconds 
                    var seconds = Math.round(timeDiff);
                    //console.log(seconds + " milleseconds to ATOB: " +  sessionId);
                    

                    // Convert binary string to character-number array
                    var charData  = strData.split('').map(function(x){return x.charCodeAt(0);});

                    // Turn number array into byte-array
                    var binData  = new Uint8Array(charData);

                    // Pako magic
                    var data = pako.ungzip(binData);

                    endTime = new Date();
                    var timeDiff = endTime - startTime; //in ms
                    // strip the ms
                    //timeDiff /= 1000;

                    // get seconds 
                    var seconds = Math.round(timeDiff);
                    //console.log(seconds + " milleseconds to INFLATE: " +  sessionId);

                    var string = new TextDecoder().decode(data);

                    var parseString = JSON.parse(string);

                    endTime = new Date();
                    var timeDiff = endTime - startTime; //in ms
                    // strip the ms
                    //timeDiff /= 1000;

                    // get seconds 
                    var seconds = Math.round(timeDiff);
                    //console.log(seconds + " milleseconds to PARSESTRING: " +  sessionId);

                    endTime = new Date();
                    var timeDiff = endTime - startTime; //in ms
                    // strip the ms
                    //timeDiff /= 1000;

                    // get seconds 
                    var seconds = Math.round(timeDiff);
                    console.log(seconds + " milleseconds to DECODE: ResultCount " + parseString.length + " " +  sessionId);
                    
                    if (callback != undefined) {
                        callback(parseString);
                    }
                },
                error: function (e) {
                    console.log('Query molecules error' + e);
                }

            });
    };

    this.getUserMolecule = function (emailAddress, callback) {

        var endpoint = apiEndpoint + 'api/Eplus/GetUser/';
        console.log('Endpoint: ' + endpoint);
        var addressString = {emailAddress};
        return $.ajax(
            {
                method: 'GET',
                url: endpoint,
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders,
                data: addressString,
                complete: function (result) {
                    self.setMolecules([result.responseJSON]);
                    if (callback != undefined) {
                        callback(self.molecules);
                    }
                },
                error: function (e) {
                    console.log('Get User error' + e);
                }

            });
    };

    this.getNavigation = function (universeIdStr, callback) {

        var endpoint = apiEndpoint + 'api/Forms/GetNavigation/';
        console.log('Endpoint: ' + endpoint);
        var roleId = {universeIdStr};
        return $.ajax(
            {
                method: 'GET',
                url: endpoint,
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders,
                data: roleId,
                complete: function (result) {
                    self.setMolecules(result.responseJSON);
                    if (callback != undefined) {
                        callback(self.molecules);
                    }
                },
                error: function (e) {
                    console.log('Get Navigation error' + e);
                }

            });
    };

    this.byExposedId = function (exposedId) {

        for (var i = 0; i < self.molecules.length; i++) {
            var m = self.molecules[i];
            if (m.header.exposedId.toLowerCase() == exposedId.toLowerCase()) {
                return m;
            }
        }
        //return returnMol;
    };

    this.exposedIdExists = function (exposedId) {

        var exists = false;
        //var returnMol = new DTS.Molecule();
        for (var i = 0; i < self.molecules.length; i++) {

            var m = self.molecules[i];
            if (m.header.exposedId.toLowerCase() == exposedId.toLowerCase()) {
                exists = true;
                break;
            }
        }

        return exists;
        //return returnMol;
    };

    this.byPropertyValue = function (property, value) {

        var returnList = [];

        for (var i = 0; i < self.molecules.length; i++) {

            var m = self.molecules[i];
            if (m[property] != undefined && m[property] == value) {
                returnList.push(m);
            }
        }
        return returnList;
    };

    this.byTypeName = function (typeName) {

        var returnList = [];
        for (var i = 0; i < self.molecules.length; i++) {

            var m = self.molecules[i];
            if (m.header.typeOf.name.toLowerCase() == typeName.toLowerCase()) {

                returnList.push(m);
            }
        }
        return returnList;
    };

    this.byTypeId = function (typeId) {

        var returnList = [];
        for (var i = 0; i < self.molecules.length; i++) {

            var m = self.molecules[i];
            if (m.header.typeOf.exposedId == typeId) {

                returnList.push(m);
            }
        }
        return returnList;
    };

    this.byKindOf = function (kindOf) {

        var returnList = [];
        for (var i = 0; i < self.molecules.length; i++) {

            var m = self.molecules[i];
            if (m.header.kindOf.exposedId == kindOf) {

                returnList.push(m);
            }
        }
        return returnList;
    };

    //By default this just searches the molecules array of the instance
    //and returns matching results, but you can pass in subset if you want to search a smaller set
    this.attributedArraySortedByValue = function (byAttributeName, subset) {

        var dict = {};
        var setToParse = self.molecules;
        if (subset != undefined) {
            setToParse = subset;
        }
        for (var i = 0; i < setToParse.length; i++) {

            var thingToSort = setToParse[i];
            var thisAttribute = thingToSort.atom(byAttributeName);
            dict[thisAttribute.displayValue] = thingToSort;

        }

        return dict;
    };

    this.filterByUniverse = function (universe, append, shouldReturn) {

        var returnThings = [];
        for (var i = 0; i < self.molecules.length; i++) {

            var matchThing = self.molecules[i];

            if (matchThing.header.au.exposedId == universe) {

                returnThings.push(matchThing);
            }
        }
        if (shouldReturn) {
            return returnThings;
        }
        else {
            if (append) {
                self.filteredMolecules.concat(returnThings);
            }
            else {
                self.filteredMolecules = returnThings;
            }
        }

    };

    this.filterByAttributeNameAndValue = function (attributeName, value, append, shouldReturn) {

        var returnThings = [];
        for (var i = 0; i < self.molecules.length; i++) {

            var matchThing = self.molecules[i];

            if (matchThing[attributeName] == value) {
                returnThings.push(matchThing);
            }
        }
        if (shouldReturn) {
            return returnThings;
        }
        else {
            if (append) {

                self.filteredMolecules.concat(returnThings);
            }
            else {

                self.filteredMolecules = returnThings;

            }

        }

    };

    this.ByValueThingId = function (attributeName, valueThingId) {

        var returnThings = [];
        for (var i = 0; i < self.molecules.length; i++) {

            var matchThing = self.molecules[i];
            var matchAttribute = matchThing.atom(attributeName);
            if (matchAttribute == undefined) {
                continue;
            }
            if (matchAttribute.valueThingId == valueThingId) {
                returnThings.push(matchThing);
            }
        }
        //debugger;
        return returnThings;
    };

    self.sortThingsByAttribute = function (atomName, dir, applyTo) {

        var thingsToSort = [];

        if (applyTo == undefined || applyTo == null) {
            thingsToSort = self.molecules;
        }
        else {

            thingsToSort = applyTo;
        }

        var dict = {};
        for (var i = 0; i < thingsToSort.length; i++) {

            var thisOption = thingsToSort[i];

            var thisAttribute = thisOption.atom(atomName);
            if (thisAttribute.displayValue == undefined) {
                thisAttribute = thisOption.atomByPropertyName(atomName);
            }
            var key = thisAttribute.displayValue;// + '_' + thisOption.header.exposedId;

            if (key in dict) {
                key += '.1';
            }
            dict[key] = thisOption;
        }

        var sortedArray = Object.keys(dict);
        sortedArray.sort(function (a, b) {
            if (isNaN(a) && isNaN(b)) return a < b ? -1 : a == b ? 0 : 1;//both are string
            else if (isNaN(a)) return 1;//only a is a string
            else if (isNaN(b)) return -1;//only b is a string
            else return a - b;//both are num
        });

        if (dir != undefined) {
            sortedArray.reverse();
        }
        var newMoleculesArray = [];

        for (var i = 0; i < sortedArray.length; i++) {
            var thingForKey = dict[sortedArray[i]];
            newMoleculesArray.push(thingForKey);
        }

        if (applyTo == undefined) {

            self.molecules = newMoleculesArray;
        }
        else {

            return newMoleculesArray;
        }

    };

    /*
        Sorts self.molecules according to the array of properties definition passed in.

        [
            {property: '', property name to sort by
            dir: '', asc or desc; if not defined, asc
            }
        ]
        Will sort in order of list passed in, allowing for primary, secondary, tertiary etc sorts

    */
    self.sortThingsByMultipleAttributes = function (properties) {

        self.molecules.sort(function (a, b) {

            for (var i = 0; i < properties.length; i++) {

                var sortProperty = properties[i];
                var property = sortProperty.property;
                var dir = sortProperty.dir;
                if (dir == undefined || dir == 'asc') {
                    if (a[property] > b[property]) return 1;
                    if (a[property] < b[property]) return -1;
                }
                else {
                    if (a[property] > b[property]) return -1;
                    if (a[property] < b[property]) return 1;
                }
            }
        });
    };

    
    self.sortThingsByDate = function (properties) {

        self.molecules.sort(function (a, b) {

            for (var i = 0; i < properties.length; i++) {

                var sortProperty = properties[i];
                var property = sortProperty.property;
                var dir = sortProperty.dir;
                if (dir == undefined || dir == 'asc') {

                    a = new Date(a[property]);
                    b = new Date(b[property]);
                    return a > b ? 1 : a < b ? -1 : 0;

                }
                if (dir == 'desc') {

                    a = new Date(a[property]);
                    b = new Date(b[property]);
                    return a > b ? -1 : a < b ? 1 : 0;

                }

            }
        });

    };

    self.setMolecules = function (resultList) {

        self.molecules = [];
        for (var i = 0; i < resultList.length; i++) {

            var mol = new DTS.Molecule(resultList[i]);
            self.molecules.push(mol);
        }

    };

    self.appendToMolecules = function (resultList) {

        for (var i = 0; i < resultList.length; i++) {

            var mol = new DTS.Molecule(resultList[i]);
            self.molecules.push(mol);
        }

    };

    self.getMolecule = function (exposedId, callback) {

        var endpoint = apiEndpoint + 'api/Molecule/Get?exposedId=' + exposedId;
        console.log('Endpoint: ' + endpoint);
        return $.ajax(endpoint,
            {
                method: 'GET',
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders,
                //data: { exposedId: exposedId }, 
                complete: function (result) {
                    if (callback != undefined) {
                        callback(result.responseJSON);
                    }
                },
                error: function (result) {
                    console.log('getMolecules error' + result.statusText);
                }
            }); 

    };

    self.getForm = function (formId, callback) {

        var endpoint = apiEndpoint + 'api/Molecule/GetForm?formId=' + formId;
        console.log('Endpoint: ' + endpoint);
       
        return $.ajax(endpoint,
            {
                method: 'GET',
                //data: { formId },
                headers: self.authHeaders,
                complete: function (result) {
                    self.setMolecules(result.responseJSON);
                    if (callback != undefined) {
                        callback(self.molecules);
                    }
                },
                error: function (result) {
                    console.log('getForm error' + result.statusText);

                    if (result.status == 401) {
                        console.log('Unauthorized');
                        window.open('/Account/Logout', '_self');
                    }
                }
            });
    };


    self.getSystemStatus = function (systemIds, callback) {

        console.log('Active AJAX Calls ' + $.active);

        if (systemIds.length == 0) {
            return;
        }
        return $.ajax('/NativeMoleculeDAL/GetSystemStatus',
            {
                method: 'POST',
                data: { systemIds },
                complete: function (result) {
                    //debugger;
                    self.setMolecules(result.responseJSON);
                    if (callback != undefined) {
                        callback();
                    }
                },
                error: function (result) {
                    console.log('GetSystemStatus error' + result.statusText);
                }
            });

    };

    self.getList = function (exposedIds, callback) {
        
        var endpoint = apiEndpoint + 'api/Molecule/GetList/';
        console.log('Endpoint: ' + endpoint);
        var exposedIdList = { 
            "ExposedIds": exposedIds, 
            "SequenceId": null, 
            "UniverseId": null 
        }; 
        return $.ajax(
            {
                method: 'POST',
                url: endpoint,
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders,
                data: exposedIdList,
                complete: function (result) {
                    self.setMolecules(result.responseJSON);
                    if (callback != undefined) {
                        callback(self.molecules);
                    }
                },
                error: function (e) {
                    console.log('Get List error' + e);
                }

            });
        
    };

   

    self.scrubMolecules = function (passMolecules) {//We want to pass the native molecule representation, without the additional classes/properties we've added client-side

        var pMolecules = [];
        for (var i = 0; i < passMolecules.length; i++) {

            passMolecules[i].applyMappedValues();

            var pMolecule = {};
            pMolecule.attributes = passMolecules[i].attributes;
            pMolecule.header = passMolecules[i].header;
            pMolecule.properties = passMolecules[i].properties;
            pMolecule.relationships = passMolecules[i].relationships;
            pMolecules.push(pMolecule);
        }

        return pMolecules;

    };

    self.createOrModifyBySet = function (passMolecules, callback) {

        var molecules = self.scrubMolecules(passMolecules);

        $.ajax('/NativeMoleculeDAL/CreateOrModifyNative',
            {
                method: 'POST',
                data: { molecules },
                complete: function (result) {

                    console.log('Result for createOrModifyBySet');
                    var returnMolecules = [];

                    for (var i = 0; i < result.responseJSON.length; i++) {

                        var mol = new DTS.Molecule(result.responseJSON[i]);
                        returnMolecules.push(mol);
                    }
                    callback(returnMolecules);
                },
                error: function (result) {

                    console.log('Create result ' + result.statusText);
                    if (result.statusText == 'Unauthorized') {
                        callback(result.statusText);
                    }
                }
            });
    };

    self.saveSecurityLevelForThingAttribute = function (shouldApplyByType, forUsers, callback) {

        var passMolecules = self.scrubMolecules(self.molecules);

        $.ajax('/NativeMoleculeDAL/SaveSecurityForThings',
            {
                method: 'POST',
                data: {
                    'thingsToSave': passMolecules,
                    'shouldApplyByType': shouldApplyByType,
                    'forUsers': forUsers
                },
                complete: function (result) {

                    console.log('Result for SaveSecurityForThings');
                    callback(result.responseJSON);
                },
                error: function (result) {

                    console.log('Create result ' + result.statusText);
                    if (result.statusText == 'Unauthorized') {
                        callback(result.statusText);
                    }
                }
            });
    };

    self.CreateOrModifyInUniverse = function (createInNewUniverse, callback) {

        var passMolecules = self.scrubMolecules(self.molecules);

        $.ajax('/NativeMoleculeDAL/CreateOrModifyInUniverse',
            {
                method: 'POST',
                data: {
                    'molecules': passMolecules,
                    'createInNewUniverse': createInNewUniverse
                },
                complete: function (result) {
                    self.molecules = [];
                    console.log('CreateOrModifyInUniverse: ' + result.responseJSON);
                    for (var i = 0; i < result.responseJSON.length; i++) {
                        var mol = new DTS.Molecule(result.responseJSON[i]);
                        self.molecules.push(mol);
                    }
                    callback(self.molecules);
                },
                error: function (result) {

                    console.log('Create result ' + result.statusText);
                    if (result.statusText == 'Unauthorized') {
                        callback(result.statusText);
                    }
                }
            });
    };

    self.createTypeOfMolecule = function (molecule, newExposedId, newLabel) {

        var newMolecule = $.extend(true, {}, molecule);
        newMolecule.header.exposedId = newExposedId;
        newMolecule.header.name = newLabel;
        newMolecule.header.label = newLabel;
        newMolecule.header.typeOf.name = molecule.header.name;
        newMolecule.header.typeOf.exposedId = molecule.header.exposedId;
        return newMolecule;
    };

    self.cloneMolecule = function (molecule, newExposedId, newLabel) {

        if (newLabel == undefined) {
            newLabel = 'NewItem';
        }
        var newMolecule = $.extend(true, {}, molecule);
        newMolecule.header.exposedId = newExposedId;
        newMolecule.header.name = newLabel;
        newMolecule.header.label = newLabel;

        return newMolecule;
    };

    self.sumValuesByThingAndAttribute = function (attributeName, things) {

        var returnValue = 0;
        var setToParse = [];
        if (things != undefined) {
            setToParse = things;
        }
        else {
            setToParse = self.molecules;
        }


        for (var i = 0; i < setToParse.length; i++) {

            var addAttribute = setToParse[i].atom(attributeName);
            if (addAttribute.name == undefined) {
                addAttribute = setToParse[i].atomByPropertyName(attributeName);
            }

            var thing = setToParse[i];
            if (addAttribute.dataType == 'ValueFloat') {
                returnValue += parseFloat(addAttribute.valueDecimal);
            }

            if (addAttribute.dataType == 'ValueBigint') {
                returnValue += parseInt(addAttribute.valueLong);
            }
        }

        return returnValue;
    };

    self.maxValueByThingsForAttribute = function (attributeName, things) {

        var returnValue = 0;
        var property = attributeName.replace(/\s/g, '');
        var setToParse = [];
        if (things != undefined) {
            setToParse = things;
        }
        else {
            setToParse = self.molecules;
        }


        for (var i = 0; i < setToParse.length; i++) {

            var thing = setToParse[i];
            var addAttribute = thing.atom(attributeName);
            if (addAttribute.dataType == 'ValueFloat') {

                if (thing[property] > returnValue) {

                    returnValue = thing[property]; //addAttribute.valueDecimal;
                }
            }
            if (addAttribute.dataType == 'ValueBigint') {

                if (thing[property] > returnValue) {
                    returnValue = thing[property];
                }
            }
        }
        return returnValue;
    };

    self.createEmptyMoleculeFromType = function (molecule, newExposedId, newLabel, stripAttributes) {

        var newMolecule = $.extend(true, {}, molecule);
        newMolecule.header.exposedId = newExposedId;
        newMolecule.header.name = newLabel;
        newMolecule.header.label = newLabel;

        if (stripAttributes) {

            newMolecule.Attributes = [];

        }

        return newMolecule;
    };

    self.createEmptyMoleculeFromTypeWithNewValueThingId = function (molecule, newExposedId, newLabel, valueThingAttributeName, newValueThingId) {

        var newMolecule = $.extend(true, {}, molecule);
        newMolecule.header.exposedId = newExposedId;
        newMolecule.header.name = newLabel;
        newMolecule.header.label = newLabel;

        var replaceAttribute = newMolecule.atom(valueThingAttributeName).valueThingId = newValueThingId;

        return newMolecule;
    };

    return this;

};

window.SaveElements = new DTS.SaveElements();
window.ThingRegistry = new DTS.ThingRegistry();
window.ThingInterface = new DTS.ThingInterface();


