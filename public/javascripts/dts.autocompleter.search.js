﻿'use strict';

if (window.DTS === undefined) {
    window.DTS = {};
}

DTS.AutocompleterSearch = function () {
    var self = this;

    self.initialize = function() {
        self.setupListeners();
    }

    self.setupListeners = function() {
        $('body').on('click', 'span.autocomplete-search-button', self.onSearchClick);
        $('body').on('submit', 'form.autocomplete-search-form', self.onFormSearch);
    }

    self.onSearchClick = function(e) {
        var $target = $(e.target);
        //make sure we got the span, not the icons inside
        if ($target[0].tagName.toLowerCase() != 'span')
            $target = $target.parent('span');
        var searchObject = $target.data('search-object');
        var searchInput = $('#' + $target.data('search-for'));
        var modalId = 'searchModel' + searchInput.attr('id');
        $.ajax('/Dynamic/Search', {
            method: 'post',
            data: {
                domainType: searchObject,
                modalId: modalId
            },
            beforeSend: function() {
                $('body').block({ message: null });
            },
            success: function(searchForm) {
                //complex form here so I just injected the full HTML
                //With more time I'd love to just get the data back and work with that
                $('body').append('<div class="modal fade" id="' + modalId + '">' +
                    '  <div class="modal-dialog modal-lg" role="document">' +
                    '    <div class="modal-content small-container">' +
                    '      <div class="modal-header">' +
                    '        <h5 class="modal-title">Search ' + searchObject + '</h5>' +
                    '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                    '          <span aria-hidden="true">&times;</span>' +
                    '        </button>' +
                    '      </div>' +
                    '      <div class="modal-body">' +
                            searchForm + 
                    '      </div>' +
                    '      <div class="modal-footer">' +
                    '      </div>' +
                    '    </div>' +
                    '  </div>' +
                    '</div>');

                $('#' + modalId).modal('show');
            },
            complete: function() {
                $('body').unblock();
            }
        });
        
    }

    self.onFormSearch = function(e) {
        var $target = $(e.target);
        $.ajax('/Dynamic/SearchPost',
            {
                method: 'post',
                data: $target.serializeArray(),
                beforeSend: function() {
                    $target.block({ message: null });
                },
                success: function(data) {

                },
                complete: function() {
                    $target.unblock();
                }
            });
        e.stopPropagation();
        e.preventDefault();
        return false;
    }

    self.initialize();
}
