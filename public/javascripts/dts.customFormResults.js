﻿'use strict';

if (window.DTS === undefined) {
    window.DTS = {};
}

function setDefaults(options, defaults) {
    return _.defaults({}, _.clone(options), defaults);
}

var lastSelectedRow = '';
var copiedRowThingId = '';
//var activatedChildForms = {};
//Moving this to customForm
var activatedChildModels = {};
var activatedParents = {};
var customFormHeader = '';
var populatedChildForms = {};

DTS.CustomFormResults = function (options) {
    var self = this;
    var defaults = {
        reviewFormId: 'reviewForm',
        resultFormId: 'resultForm',
        paging: true,
        sorting: true,
        showInfo: true
    }

    self.options = setDefaults(options, defaults);

    self.refreshFilters = function (e) {
        if (self.reviewTable)
            self.reviewTable.draw();
        else
            self.setupTable();

        return false;
    }


    //self.setupColumns = function (vm, childVM, initOnly) {
    self.setupColumns = function (vm, childVM, initOnly, forParent) {
        self.Columns = [];
        var offset = 0;

        if ((self.Model.form.childFormKeyId != undefined &&
                self.Model.form.childFormKeyId != null &&
                self.Model.form.childFormKeyId != '00000000-0000-0000-0000-000000000000')
            || self.Model.form.allowInlineDelete) {
            self.Columns.push({
                "orderable": false,
                "data": 'ThingId',
                "defaultContent": '',
                "className": 'custom-row-actions',
                'parameters': {
                    'linkGuid': null,
                    'customLink': null,
                    'attributeId': null,
                    'editable': null,
                    'item': null,
                    'attribute': null,
                    'options': null,
                    'expand': (self.Model.form.childFormKeyId != undefined &&
                        self.Model.form.childFormKeyId != null &&
                        self.Model.form.childFormKeyId != '00000000-0000-0000-0000-000000000000'),
                    'delete': self.
                        Model.form.allowInlineDelete,
                    'childKey': self.Model.form.childFormKeyId
                },
                'render': eMESA.TableHelpers.customFormColumn,
                'targets': 0,
                'title': ''
                
            });
            offset = 1;
        }


        //var formToSerialize = ($('#' + self.options.reviewFormId));
        //debugger;
        $.ajax('/CustomForm/Results',
                {
                    method: 'post',
                    data: {
                        length: 1,
                        skipCount: true,
                        includeIds: true,
                        onlyShowHeader: true,
                        model: self.Model,
                        searchForm: JSON.stringify(eMESA.TableHelpers.objectifyForm($('#' + self.options.reviewFormId).serializeArray()))
                    },
                    beforeSend: function () {
                        $('button').prop('disabled', 'disabled');
                        //$('#' + self.options.reviewFormId + ' button').prop('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner" />');
                    },
                    success: function (results) {
                        var attributeIds = results.data[0];
                        //debugger;
                        //OK - the first result in our data now is the attribute ids, lets use that match up on the report fields
                        //way easier than string matching I was attempting.
                        var typeOf = self.Model.form.domainObject;
                        var parentIdSet = false;

                        $.each(self.Model.form.reportFields,
                                function (i, item) {
                                    try {

                                    var attributeProperty, attribute, attributeId, options;
                                    var sortable = true;

                                    for (var property in attributeIds) {
                                        if (!attributeIds.hasOwnProperty(property))
                                            continue;

                                        if (attributeIds[property].toLowerCase().trim() == item.attributeId.toLowerCase().trim()) {
                                            attributeProperty = property;
                                            attributeId = property;
                                            break;
                                        }
                                        if (attributeIds[property].indexOf(':') > -1) {
                                            //its a compound child column
                                            var ids = attributeIds[property].toLowerCase().trim().split(':');
                                            if (ids[1] == item.childAttributeId && ids[2] == item.parentAttributeId) {
                                                attributeProperty = property;
                                                attributeId = ids[2];
                                                sortable = false;
                                                options = $.grep(self.Model.allOptions,
                                                    function (option) {
                                                        return option.header.typeOf.exposedId == ids[0];
                                                    }).sort(function (a, b) {
                                                        return a.header.label > b.header.label ? 1 : -1;
                                                    });
                                                break;
                                            }
                                        }
                                    }
                                    var attributeGrep = $.grep(self.Model.molecule.attributes, function (attribute) {
                                        return attribute.exposedId == item.attributeId || attribute.exposedId == item.parentAttributeId;
                                    });
                                    if (attributeGrep.length > 0)
                                        attribute = attributeGrep[0];

                                    var childFormKeyId = "";
                                    var parentFormId = "";

                                    if (item.isEditable && !parentIdSet && vm != undefined) {

                                        childFormKeyId = vm.form.childFormKeyId;

                                        if (forParent == '') {
                                            parentFormId = vm.molecule.header.exposedId;
                                        }
                                        else {

                                            parentFormId = forParent;

                                        }
                                        parentIdSet = true;
                                    }
                                    self.Columns.push(
                                        {
                                            'targets': i + offset,
                                            'title': item.displayLabel.length > 0 ? item.displayLabel : item.name,
                                            'data': attributeProperty,
                                            'parameters': {
                                                'linkGuid': item.linkToFormId,
                                                'customLink': item.linkToCustom,
                                                'attributeId': attributeId,
                                                'editable': item.isEditable,
                                                'item': item,
                                                'attribute': attribute,
                                                'options': options,
                                                'expand': false,
                                                'delete': false,
                                                'typeOf': typeOf,
                                                'childFormKeyId': childFormKeyId,
                                                'parentFormId': parentFormId

                                            },
                                            'render': eMESA.TableHelpers.customFormColumn,
                                            'orderable': sortable
                                        });
                                    } catch (e) {

                                        debugger;
                                    }

                            });

                        
                        $('button').prop('disabled', '');
                        //return;
                        //finally if there were any defaulted filters we need to search now

                        var filters = eMESA.TableHelpers.objectifyForm($('#' + self.options.reviewFormId).serializeArray());
                        var runSearch = false;
                        for (var prop in filters) {
                            if (!filters.hasOwnProperty(prop) || prop.indexOf('input') != 0)
                                continue;

                            if (filters[prop].length > 1) {
                                runSearch = true;
                            }
                        }

                        if (runSearch)
                            self.refreshFilters();

                    }
            });
    }

    self.setupColumnsInBackground = function (vm, childVM, initOnly, forParent) {


    }


    self.expandSelectedTarget = function ($target, forRow) {

        $target.off("click");
        var storedForm = customFormHeader.activatedChildForms[forRow];
        $target.closest('tr').after($(storedForm));
        delete customFormHeader.activatedChildForms[forRow];
        //delete activatedParents[forRow];
    }

    self.expandChildForm = function (e) {

        e.stopPropagation();
        var $target = $(e.target);

        var forParent = $target.data('thing-id');
        lastSelectedRow = $target.data('thing-id');

        $target.removeClass('fa-expand').addClass('fa-compress').click(false);

        if (customFormHeader.activatedChildForms != undefined && customFormHeader.activatedChildForms[forParent] != undefined) {

            self.expandSelectedTarget($target, forParent);
            return;
            

        }
        //debugger;
        var modelToPass = {};
        if (activatedChildModels[forParent] != undefined) {

            modelToPass = activatedChildModels[forParent];
        }
        else {

            modelToPass = self.Model;
        }

        //var childForm = new DTS.ChildForm(self.Model,
        var childForm = new DTS.ChildForm(modelToPass,
            {
                parentMoleculeId: $target.data('thing-id')

            }, self, forParent);

        var newRow = $('<tr></tr>');
        var newCell = $('<td colspan="100"></td>');
        newRow.append(newCell);
        $target.parent('td').parent('tr').after(newRow);
        childForm.drawResultsTable(newCell);
        childForm.initialize();
    }

    self.collapseChildForm = function (e) {
        var $target = $(e.target);
        $target.removeClass('fa-compress').addClass('fa-expand');

        var contents = $target.parent('td').parent('tr').next('tr');
        var row = $target.closest('tr').next('tr');//.siblings().first();
        
        if (row != undefined) {
            //debugger;
            $target.off('click');
            $target.on("click", e.target, self.expandChildForm);
            customFormHeader.activatedChildForms[$target.data('thing-id')] = row;
            var $child = $(row).find('.fa-expand');
            activatedChildModels[$child.data('thing-id')] = self.Model;
            activatedParents[$target.data('thing-id')] = $target;
            //debugger;
            row.remove();
        }

    }

    self.setupListeners = function () {
        $('#' + self.options.resultFormId).on('click', '.fa-expand', self.expandChildForm);
        $('#' + self.options.resultFormId).on('click', '.fa-compress', self.collapseChildForm);
        $('#' + self.options.resultFormId).on('click', 'button.add-custom-row', self.addNewRow);
        $('#' + self.options.resultFormId).on('click', 'i.delete-custom-row', self.deleteRow);
    }

    self.setupTable =  function (vm, childVM) {
        var myTable = $('#' + self.options.resultFormId + ' .table');
        myTable.show();
        //debugger;


        self.reviewTable = myTable.DataTable({
            'processing': true,
            'serverSide': true,
            'searching': false,
            'paging': self.options.paging,
            'ordering': self.options.sorting,
            'info': self.options.showInfo,
            'language': {
                'processing': 'Loading Results'
            },
            'ajax': {
                type: 'POST',
                url: '/CustomForm/Results',
                data: function (d) {
                    d.model = self.Model;
                    d.searchForm = JSON.stringify(eMESA.TableHelpers.objectifyForm($('#' + self.options.reviewFormId).serializeArray()));
                    //debugger;

                },
                beforeSend: function () {
                    
                    $('#' + self.options.resultFormId).toggleClass('gray-out');
                },
                complete: function (results) {
                    var searchResults = JSON.parse(results.responseText);
                    //debugger;
                    $('#' + self.options.resultFormId).toggleClass('gray-out');
                    $('button.save-changes-button').show();
                    if (self.Model.form.allowInlineCreate) {
                        //jenky way to hide first row if there isn't real data
                        if ($('#' + self.options.resultFormId).find('input[name^="dataTypeinlineInput"]').first().attr('name').split(':')[0] === 'dataTypeinlineInput')
                            $('#' + self.options.resultFormId).find('tbody tr').first().addClass('dummy-row');
                        self.setupAddButton();
                    }
                    //Add collapse class
                    //

                    var t = JSON.stringify(eMESA.TableHelpers.objectifyForm($('#' + self.options.reviewFormId).serializeArray())).toString();
                    if (t.includes(":")) {

                        var keys = t.split(":");
                        var key = keys[1].replace("}", "");
                        
                        key = key.replace(/["']/g, "");

                        var section = $('#resultForm');
                        var allClicks = $(section).find('.fa-compress');

                        $.each(allClicks, function (i, result) {

                            var r = $(result);
                            var thingId = result.dataset["thingId"];

                            if (thingId == key) {
                                r.on("click", r, self.collapseChildForm);
                            }

                        });
                    }


                    $('#downloadToExcelContainer').show();
                    window.eMESA.setupDateTimePickers();
                    window.eMESA.setupAutoCompletersFor($('#' + self.options.resultFormId));


                    var dropDowns = $('[id^="inlineDropDown:"]');

                    var attributeArray  = [];
                    var dictDisplayValues = {};

                    for (var i = 0; i < dropDowns.length; i++) {

                        var attribute = dropDowns[i].id.split(":")[2] + ':' + dropDowns[i].id.split(":")[3];

                        if (!attributeArray.includes(attribute)) {

                            attributeArray.push(attribute);

                            var key = dropDowns[i].id.replace('inlineInput', 'inlineDropDown');
                            var displayFieldStr = $(document.getElementById(key)).attr('displayFields');
                            var displayFieldArray = displayFieldStr.split(":");

                            dictDisplayValues[attribute] = displayFieldArray;
                        }
                    }

                    for (var key in dictDisplayValues) {
                        //debugger;
                        var displayAtts = dictDisplayValues[key];
                        eMESA.dropDownThings(key.split(':')[1], key.split(':')[0], displayFieldArray, self.processInlineDropDowns, searchResults);
                    }

                    //debugger;
                }
            },
            'columnDefs': self.Columns
        });

        

        self.setupListeners();
    }

    self.processInlineDropDowns = function (dropDownThings, attributeId, forAttribute, displayAttributes, searchResults) {

        //debugger;
        if (dropDownThings.length > 0) {
            var findKey = 'select[id$="' + forAttribute + '"]';
            var $dropDowns = $(findKey);

            var resultsToPopulate = eMESA.sortedAttributeArray(dropDownThings, displayAttributes[0]);

            var sortedArray = Object.keys(resultsToPopulate);
            sortedArray.sort();

            for (var i = 0; i < sortedArray.length; i++) {

                var key = sortedArray[i];

                $dropDowns.append($('<option>', {
                    value: resultsToPopulate[key].header.exposedId,
                    text: eMESA.buildDropDownMenuDisplay(resultsToPopulate[key].attributes)

                }));
            }

            for (var i = 0; i < $dropDowns.length; i++) {

                var dId = $dropDowns[i].id.replace("inlineInput", "").split(":");

                var results = $.grep(searchResults.data, function (obj) { return obj.ThingId === dId[0]; })[0];
                var options = $dropDowns[i].options;
                $.each(results, function (i, result) {
                    //debugger;

                    if (i.toLowerCase() === dId[1].toLowerCase() && result.length > 0) {

                        var match = $.grep(options, function (obj) { return obj.value.toLowerCase() === result.toLowerCase(); })[0];
                        if (match != undefined) {
                            var option = $(match);
                            option.attr('selected', 'selected');
                        }

                    }

                });

            }

        }
    }

    self.setupAddButton = function () {

        $('#' + self.options.resultFormId + ' td').last().append($('<button class="btn btn-sm btn-outline-success add-custom-row">' + self.Model.molecule.header.label + '</button>'));
    }

    self.deleteRow = function (e) {

        if (confirm("Really delete this item?")) {

            e.stopPropagation();
            var $target = $(e.target);
            var row = $target.closest('tr').last('tr');
            var exposedId = $target.data('thing-id');
            row.remove()
            self.deleteRowExecute(exposedId);
        }
    }

    self.deleteRowExecute = function (exposedId) {

            $.ajax('/CustomForm/Delete',
                {
                    method: 'post',
                    data: {
                        thingId: exposedId
                    },
                    beforeSend: function () {
                        //$target.parents('.custom-form').last().block({ message: null });
                    },
                    complete: function () {
                        //$target.parents('.custom-form').last().unblock();
                        //row.remove();
                    },
                    success: function () {

                        //document.location.reload();
                    }
                });
    }

    self.addNewRow = function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax('/CustomForm/GenerateGUID',
            {
                method: 'GET',
                success: function (result) {
                    //debugger;

                    var newId = result;
                    var $target = $(e.target);
                    var $existingRow = $target.parents('tr').first();
                    $target.remove();
                    var $newRow = $existingRow.clone();

                    if ($newRow.find('.fa-compress').length > 0) {
                        $newRow.find('.fa-compress').remove();
                        $newRow.find('.custom-row-actions').append($('<i class="fa clickable fa-expand" data-thing-id="' + newId + '" :before </i>'));
                    }

                    $newRow.removeClass('dummy-row');


                    $newRow.find('td').each(function (index, element) {
                        self.clearNewRowCell(index, element, newId);
                    });


                    $newRow.find('td').first().append($('<input type="hidden" name="moleculeTypeinlineInput' + newId + '" value="' + $('#' + self.options.resultFormId + ' input[name="typeId"]').val() + '">'));

                    $existingRow.parents('table').first().append($newRow);
                    var existingButton = $('#' + self.options.resultFormId + ' td').last().find('.btn btn-sm btn-outline-success add-custom-row');

                    if ($('#' + self.options.resultFormId + ' td').last().find('.btn btn-sm btn-outline-success add-custom-row').length == 0) {

                        self.setupAddButton();
                    }

                    $newRow.find('.fa-expand').attr('data-thing-id', newId);
                    $newRow.find('.delete-custom-row').attr('data-thing-id', newId);


                    window.eMESA.setupDateTimePickers();
                }
            });
    }

    self.clearNewRowCell = function (index, element, newId) {
        var $element = $(element);
        
        //Don't remove the expand button
        if ($element.find('input, select').length == 0 && $element.find('.fa-expand').length === 0 && $element.find('span').length === 0) {

            if ($element.class != ".fa-expand") {
                $element.html('');
            }
            return;
        }

        $element.find('input, select, textarea, span').each(function (i, el) {
            var $el = $(el);
            //debugger;
            var currentId = $el.attr('id');
            if (currentId == undefined)
                currentId = $el.attr('name');

            $el.removeAttr("checked");

            if (currentId != undefined) {
                var idParts = currentId.split(':');

                if (idParts[1] != undefined) {

                    if (currentId.indexOf('inlineLabelInput') == -1) {//don't give labels the input tag

                        var namePart = currentId.indexOf('dataTypeinlineInput') == -1 ? "inlineInput" : "dataTypeinlineInput";
                        var newName = namePart + newId + ':' + idParts[1];
                        var newName = namePart + newId + ':' + idParts[1];
                        var nameReplace = newName.replace("true", "");
                        nameReplace = nameReplace.replace("false", "");

                        $el.attr('id', newName);
                        $el.attr('name', nameReplace);

                    }

                    //inlineLabelInput
                    if (currentId.indexOf('inlineLabelInput') != -1) {

                        var namePart = 'inlineLabelInput';//.indexOf('dataTypeinlineInput') == -1 ? "inlineInput" : "dataTypeinlineInput";
                        var newName = namePart + newId + ':' + idParts[1];
                        $el.attr('id', newName);
                        $el.attr('name', newName);
                    }

                    if (currentId.indexOf('inlineDropDown') != -1) {

                        var namePart = 'inlineDropDown';//.indexOf('dataTypeinlineInput') == -1 ? "inlineInput" : "dataTypeinlineInput";
                        var newName = namePart + newId + ':' + idParts[1];
                        $el.attr('id', newName);
                        $el.attr('name', newName);
                    }

                    if (currentId.indexOf('inlineLabelInputFK') != -1) {

                        var namePart = 'inlineLabelInputFK';//.indexOf('dataTypeinlineInput') == -1 ? "inlineInput" : "dataTypeinlineInput";
                        var newName = namePart + newId + ':' + idParts[1];
                        $el.attr('id', newName);
                        $el.attr('name', newName);
                    }

                    if (namePart != 'dataTypeinlineInput'
                        && currentId.indexOf('inlineParentAttribute') == -1
                        && currentId.indexOf('inlineTypeOfId') == -1
                        && namePart != 'inlineDropDown'
                        && !$el.is('select')
                    ) {
                        //debugger;

                        if (currentId.indexOf('inlineLabelInputFK') == -1) {
                            $el.val('');
                        }

                        $el.text('');
                    }


                    if (currentId.indexOf('inlineParentAttribute') != -1) {
                        namePart = "inlineParentAttribute";
                        newName = namePart + newId + ':' + idParts[1];
                        $el.attr('id', newName);
                        $el.attr('name', newName);
                    }
                    if (currentId.indexOf('inlineTypeOfIdinlineInput') != -1) {
                        namePart = "inlineTypeOfIdinlineInput";
                        newName = namePart + newId + ':' + idParts[1];
                        $el.attr('id', newName);
                        $el.attr('name', newName);
                    }


                    if (currentId.indexOf("true") != -1) {

                        $el.attr('value', "true");
                    }
                    if (currentId.indexOf("false") != - 1) {

                        $el.attr('value', "false");
                    }

                }
            }

        });

        if ($element.find('.autocompleter').length > 0)
            window.eMESA.setupAutoCompletersFor($element);

    }
}

DTS.ChildForm = function (parentVM, options, delegate, forParent) {
    //debugger;
    var self = this;
    var defaults = {
        reviewFormId: 'reviewForm' + Date.now(),
        resultFormId: 'resultForm' + Date.now(),
        parentMoleculeId: parentVM.molecule.header.exposedId
    }

    //These child classes are invoked from an instance of CustomForm, which is responsible for serializing the form and its contents;
    //But customForm doesn't retain any references to its children. We want to be able to pass along some hidden elements to include;
    var cameFrom = delegate.constructor.name;
    if (cameFrom === 'CustomForm') {
        customFormHeader = delegate;
    }

    if (customFormHeader == '') {

        customFormHeader = {};
        customFormHeader.childFormViewModels = {};
        customFormHeader.activatedChildForms = {};
    }

    self.options = setDefaults(options, defaults);

    var resultsHelper = new DTS.CustomFormResults({
        paging: false,
        sorting: false,
        showInfo: false,
        reviewFormId: self.options.reviewFormId,
        resultFormId: self.options.resultFormId
    });

    self.getModelsInBackground = function (parentVM) {

        $.ajax('/CustomForm/GetFormViewModel',
            {
                method: 'POST',
                data: {
                    formId: parentVM.form.childFormId
                },
                success: function (result) {

                    customFormHeader.childFormViewModels[parentVM.form.childFormId] = result;

                    if (result.form.childFormId != undefined && customFormHeader.childFormViewModels[result.form.childFormId] == undefined) {
                        self.getModelsInBackground(result);
                    }
                }
            });
    }

    self.initialize = function () {

        if (customFormHeader.childFormViewModels != undefined && customFormHeader.childFormViewModels[parentVM.form.childFormId] != undefined) {

            var form = customFormHeader.childFormViewModels[parentVM.form.childFormId];
            var prevId = form.molecule.header.exposedId;
            var newResult = $.extend(true, {}, form);
            var freshGuid = eMESA.returnGuid(); //customFormHeader.guidList[0];
            //customFormHeader.guidList.splice(0, 1);
            form.molecule.header.exposedId = freshGuid;
            self.buildChildForm(form);
        }
        else {
        
            $.ajax('/CustomForm/GetFormViewModel',
                {
                    method: 'POST',
                    data: {
                        formId: parentVM.form.childFormId
                    },
                    success: function (result) {
                        var newResult = $.extend(true, {}, result);
                        customFormHeader.childFormViewModels[parentVM.form.childFormId] = newResult;
                        self.buildChildForm(result);

                        if (result.form.childFormId != undefined && customFormHeader.childFormViewModels[result.form.childFormId] == undefined) {
                            self.getModelsInBackground(result);
                        }
                    }
                });
        }
    }

    self.buildChildForm = function (result) {

        self.internalVM = result;
        self.drawReviewTable();
        $('#' + self.options.resultFormId + ' input[name="typeId"]').val(self.internalVM.molecule.header.exposedId);
        if (self.internalVM.form.allowInlineCreate)
            $('#' + self.options.resultFormId).addClass('allowInlineCreate');
        resultsHelper.Model = self.internalVM;

        $('#' + self.options.resultFormId).find('h5.loading').removeClass('loading').html(self.internalVM.form.title);
        //debugger;
        if (forParent == undefined) {
            forParent = '';
        }
        resultsHelper.setupColumns(parentVM, self.internalVM, false, forParent);
    }

    self.drawReviewTable = function () {
        var $form = $('<form id="' + self.options.reviewFormId + '"></form>');
        var searchAttribute = $.grep(self.internalVM.molecule.attributes,
            function (attribute) {
                return attribute.exposedId == parentVM.form.childFormKeyId;
            });

        var typeOfId = self.internalVM.form.domainObject;

        if (searchAttribute.length == 0)
            return;
        var attributeWithThing = 'inlineParentAttribute' + self.internalVM.molecule.header.exposedId + ":" + searchAttribute[0].exposedId;
        var p = self.options.parentMoleculeId;

        var name = 'input' + searchAttribute[0].exposedId;
        var inlineName = 'inlineInput:' + searchAttribute[0].exposedId;

        $('#' + self.options.resultFormId).append($('<input type="hidden" name="dataType' + inlineName + '" id="dataType' + inlineName + '" value="ValueThingId">'));
        $form.append($('<input type="hidden" name="' + name + '" id="' + name + '" value="' + self.options.parentMoleculeId + '">'));
        $('body').append($form);

    }

self.drawResultsTable = function (appendTo, storedForm) {
        var $section = $('<section id="' + self.options.resultFormId + '" method="POST" class="custom-form"></section>');
        $section.append($('<h5 class="loading child-from-header">Loading Children</h5>'));
        $section.append($('<input type="hidden" name="typeId" value=""/>'));
        var $table = $('<div class="table-responsive">');
        $table.append($('<table class="table table-sm" style="display: none;width:100%;"></table>'));
        $section.append($table);
        appendTo.append($section);

        if (storedForm != undefined) {
            appendTo.append($(storedForm));
        }
    }
}