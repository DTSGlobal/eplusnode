﻿'use strict';

if (window.DTS === undefined) {
    window.DTS = {};
}

DTS.Favorites = function () {
    var self = this;

    self.initialize = function() {
        self.setupListeners();
    }

    self.setupListeners = function() {
        $('body').on('click', '.favorite-icon', self.favoriteIconClicked);
    }

    self.favoriteIconClicked = function(e) {
        var $target = $(e.target);
        var value = $('#' + $target.data('favorite-field')).val();
        $.ajax('/Favorite/Update', {
            method: 'post',
            data: {
                page: $target.data('favorite-page'),
                field: $target.data('favorite-field'),
                value: value
            },
            beforeSend: function() {
                $target.addClass('fa-spin');
            },
            success: function() {
                if (value == null || value == '') {
                    $target.removeClass('fa-star');
                    $target.addClass('fa-star-o');
                } else {
                    $target.addClass('fa-star');
                    $target.removeClass('fa-star-o');
                }
            },
            complete: function() {
                $target.removeClass('fa-spin');
            }
        });
    }
};