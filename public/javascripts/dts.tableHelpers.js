﻿'use strict';

if (window.DTS === undefined) {
    window.DTS = {};
}

DTS.TableHelpers = function() {
    var self = this;

    self.objectifyForm = function(formArray) { //serialize data function

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }

    self.renderDate = function(data) {
        if (data == null)
            return '';

        return moment(data).format('YYYY-MM-DD');
    }

    self.renderPersonColumn = function(data) {
        if (data == null || (data.lastName == '' && data.firstName == ''))
            return '';

        return data.lastName + ', ' + data.firstName;
    }

    self.renderCode = function (data) {
        if (data == null)
            return '';

        return data.label;
    }

    self.workOrderColumn = function(data) {
        if (data == null)
            return '';

        return '<a href="/WorkOrder/Edit/' + data.id + '">' + data.number + '</a>';
    }

    self.containerColumn = function(data, type, row) {
        if (data == null)
            return '';

        return '<a href="/CustomForm/5c548bef-be19-427c-a3c5-8ba97d7f3821/' + row.id + '">' + data + '</a>';
    }

    self.personnelColumn = function (data, type, row) {
        if (data == null)
            return '';

        return '<a href="/CustomForm/e1516dbe-e069-4ae7-8bd2-b5937ef4e8d5/' + row.id + '">' + data + '</a>';
    }

    self.jsaColumn = function (data, type, row) {
        if (data == null)
            return '';

        return '<a href="/JSA/Edit/' + row.id + '">' + data + '</a>';
    }

    self.customFormColumn = function (data, type, row, meta) {
        //debugger;
        function buildDisplayValue(attributes) {
            var displayValue = '';
            for (var i = 0; i < attributes.length; i++) {
                displayValue += attributes[i].displayValue + ' - ';

            }
            if (displayValue.length > 1)
                return displayValue.substr(0, displayValue.length - 3);

            return displayValue;
        }

        if (data == null)//|| data == ''
            return '';

        
        var parameters = meta.settings.oInit.columnDefs[meta.col].parameters;
        var linkGuid = parameters.linkGuid;
        var customLink = parameters.customLink;
        var pattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
        
        if (parameters.expand || parameters.delete) {
            var actionButtons = '';
            if (parameters.delete)
                actionButtons += '<i class="fa fa-times-circle fa-lg delete-custom-row" data-thing-id="' + row.ThingId + '"></i>';
            if(parameters.expand)
                actionButtons += '<i class="fa fa-expand clickable" data-thing-id="' + row.ThingId + '"></i>';
            return actionButtons;
        }

        if (linkGuid && linkGuid != '00000000-0000-0000-0000-000000000000') {
            if (parameters.attributeId != undefined && pattern.test(row[parameters.attributeId.toUpperCase()] ))
                return '<a target="_blank" href="/CustomForm/' + linkGuid + '/' + row[parameters.attributeId.toUpperCase()] + '">' + data + '</a>';
            else
                return '<a target="_blank" href="/CustomForm/' + linkGuid + '/' + row.ThingId + '">' + data + '</a>';
        }
        
        if (customLink != null && customLink != '') {

            var placeHolder = data;
            if (data == '') {
                placeHolder = 'Link';
            }
            return '<a target="_blank" href="' + customLink + '?id=' + row.ThingId + '">' + placeHolder + '</a>';
        }
        //debugger;
        //if (parameters.editable && parameters.attribute.userSecurityLevel > parameters.attribute.securityLevel) {
        var disabled = ''
        var type = 'text';

        var idString = 'inlineInput' + row.ThingId + ":" + parameters.attributeId;
        //debugger;
        if (parameters.editable && parameters.attribute.userSecurityLevel > parameters.attribute.securityLevel) {
            //debugger;
            //disabled = " readonly ";
            //type = 'label';

            var cssClass = '';
            var dataTypeInputString = '<input type="hidden" value="' + parameters.attribute.dataType + '" id="dataType' + idString + '" name="dataType' + idString + '">';
            var inlineTypeOfInputString = '<input type="hidden" value="' + parameters.typeOf + '" id="inlineTypeOfId' + idString + '" name="inlineTypeOfId' + idString + '">';
            var parentStr = '';

            if (parameters.childFormKeyId.length > 0) {

                var prevRow = $(row).prev()[0];
                var baseIdStr = row.ThingId + ":" + parameters.childFormKeyId;
                parentStr = '<input type="hidden" value="' + parameters.parentFormId + '" id="inlineParentAttribute' + baseIdStr + '" name="inlineParentAttribute' + baseIdStr + '">';
                //debugger;
                
            }
            
            if (parameters.item.inputType.toLowerCase() == 'textbox') {
                if (parameters.attribute && parameters.attribute.dataType.toLowerCase() == 'valuedatetime') {
                    cssClass = 'make-me-a-date-picker';
                }


                var numericWidth = '';
                if (parameters.attribute && (parameters.attribute.dataType.toLowerCase() == 'valuefloat' || parameters.attribute.dataType.toLowerCase() == 'valuebigint' )) {
                    type = 'number';
                    numericWidth = 'style="width: 120px;"'
                    //debugger;
                }
                //return parentStr + inlineTypeOfInputString + dataTypeInputString + '<input id="' + idString + '" name="' + idString + '" class="form-control ' + cssClass + '" type="' + type + '" value="' + data.replace("\"", "\"\"") + '"/>';
                return parentStr + inlineTypeOfInputString + dataTypeInputString + '<input id="' + idString + '" name="' + idString + '" class="form-control ' + cssClass + '" ' + numericWidth + ' type="' + type + '" value="' + data.replace("\"", "\"\"") + '"' + disabled + ' />';
            }

            if (parameters.item.inputType.toLowerCase() == 'textarea') {
                return parentStr + inlineTypeOfInputString + dataTypeInputString + '<textarea id="' + idString + '" name="' + idString + '" class="form-control">' + data + '</textarea>';
            }

            if (parameters.item.inputType.toLowerCase() == "radio") {

                var forTrueInput = idString + 'true';
                var forFalseInput = idString + 'false';

                var trueChecked = '';
                var falseChecked = '';

                if (data == '1') {
                    trueChecked = ' checked="checked" '
                }
                if (data == '0') {
                    falseChecked = '  checked="checked" '
                }

                var buildStr = '<span><label class="mr-1" for="' + forTrueInput + '">Yes</label><input id="' + forTrueInput + '" name="' + idString + '" type="radio" value="true" ' + trueChecked + ' > <label class="mr- 1" for="' + forFalseInput + '">No </label> <input id="' + forFalseInput + '" name="' + idString + '" type="radio" value="false" ' + falseChecked + ' ></span > ';

                //var buildStr = '<div class="col-3"><label class="mr-1" for="' + forTrueInput + '">Yes</label><input id="' + forTrueInput + '" name="' + idString + '" type="radio" value="true"></div>< div class="col- 3" > <label class="mr- 1" for="' + forFalseInput + '">No </label> <input id="' + forFalseInput + '" name="' + idString + '" type="radio" value="false"></div>';

                return parentStr + inlineTypeOfInputString + dataTypeInputString + buildStr;
                //var buildStr = 'div class="col-3"><label class="mr-1" for="inputc1b83e81-1d7c-44ad-b267-c461e62088a3true">Yes</label><input id="inputc1b83e81-1d7c-44ad-b267-c461e62088a3true" name="inputc1b83e81-1d7c-44ad-b267-c461e62088a3" type="radio" value="true"></div><div class="col-3"><label class="mr-1" for="inputc1b83e81-1d7c-44ad-b267-c461e62088a3false">No </label><input id="inputc1b83e81-1d7c-44ad-b267-c461e62088a3false" name="inputc1b83e81-1d7c-44ad-b267-c461e62088a3" type="radio" value="false"></div>'


            }

            if (parameters.item.inputType.toLowerCase() == 'dropdown') {
                var options = '<option value="">(none selected)</option>', selected;

                var selection = ''
                if (row[parameters.attributeId.toUpperCase()].length > 0) {

                    selection = ' currentSelection = "' + row[parameters.attributeId.toUpperCase()] + '"';
                }
                
                var idField = 'inlineDropDown:' + row.ThingId + ':' + parameters.attributeId + ':' + parameters.attribute.typeOfAttributeExposedId;
                var displayFields = '<input name ="' + idField + '" id = "' + idField + '" ' + selection + ' displayFields = "' + parameters.item.autoCompleteDisplayFields.join(':') + '" type="hidden" />';

                //<input displayfields="be74c3ae-cb14-409f-8bf5-b13f8f5209e5:6d2d74cc-5575-41ff-b9b1-12aa241e1996" id="dropDown:32054a2d-aa3b-4115-8712-c19a2be5210c" name="dropDown:32054a2d-aa3b-4115-8712-c19a2be5210c" type="hidden">
                /*
                for (var i = 0; i < parameters.options.length; i++) {
                    var option = parameters.options[i];
                    if (option.header.exposedId.toUpperCase() == row[parameters.attributeId.toUpperCase()]) { //TODO: what's the id
                        selected = 'selected';
                    } else
                        selected = '';

                    options += '<option value="' + option.header.exposedId + '" ' + selected + '>' + buildDisplayValue(option.attributes) + '</option>';
                }
                */
                //options += '<option selected>' + data + '</option>';
                // 
                return displayFields + parentStr + inlineTypeOfInputString + dataTypeInputString + '<select id="' + idString + '" name="' + idString + '" class="form-control">' + options + '</select>';
            }
            if (parameters.item.inputType.toLowerCase() == 'autocompleter') {
                //var $autoCompleter = $('<input id="' + idString + '_Label" name="' + idString + '_Label" class="form-control autocompleter" />');
                //var $autoCompleter = $('<input id="' + idString + '" name="' + idString + '" class="form-control autocompleter" />');
                var $autoCompleter = $('<input id="' + idString + '_autocompleter" name="' + idString + '_autocompleter" class="form-control autocompleter" />');
                $autoCompleter.attr('data-display-fields', parameters.item.autoCompleteDisplayFields.toString());
                $autoCompleter.attr('data-search-fields', parameters.item.autoCompleteSearchFields.toString());
                $autoCompleter.attr('data-search-type', parameters.attribute.typeOfAttributeExposedId);
                $autoCompleter.attr('data-search-object', "CustomForm");

                //TODO: where are you?
                var display = '';
                for (var i = 0; i < parameters.item.autoCompleteDisplayFields.length; i++) {
                    var displayField = parameters.item.autoCompleteDisplayFields[i];

                    var displayFieldValue = row[parameters.attribute.typeOfAttributeExposedId.toUpperCase() + ':' + displayField.toUpperCase() + ':' + parameters.attributeId.toUpperCase()];
                    //debugger;

                    if (displayFieldValue != undefined && displayFieldValue.length > 0) {
                        display += row[parameters.attribute.typeOfAttributeExposedId.toUpperCase() + ':' + displayField.toUpperCase() + ':' + parameters.attributeId.toUpperCase()] + ', ';
                    }
                }
                $autoCompleter.attr('value', display);

                var elementVal = row[parameters.attributeId.toUpperCase()];

                if (elementVal == undefined) {
                    elementVal = '';
                }

                var $icon = $('<span class="fa fa-circle-o-notch fa-spin typeahead-loading" id="' + idString + '_icon" style="display:none"></span>');
                var $hiddenInput = $('<input id="' + idString + '" name="' + idString + '" type="hidden" value="' + elementVal + '">');
                //debugger;

                return parentStr + inlineTypeOfInputString + dataTypeInputString + $autoCompleter[0].outerHTML + $icon[0].outerHTML + $hiddenInput[0].outerHTML;
            }
            
        }

        if (!parameters.editable) {
            
            //NEW ONE
            var attributeId = '';

            var labelIdStr = 'inlineLabelInput' + row.ThingId + ":" + parameters.item.childAttributeId;
            //var labelIdStr = 'inlineLabelInput' + row.ThingId + ":" + parameters.attributeId;

            var fkIdStr = 'inlineLabelInputFK' + row.ThingId + ":" + parameters.attributeId;
            var fk = '';
            var returnStr = '';
            if (parameters.item.childAttributeId != "00000000-0000-0000-0000-000000000000") {
                fk = '<input type="hidden" id="' + fkIdStr + '" name= "' + fkIdStr + '" value="' + parameters.item.childAttributeId + '">'
                //debugger;
            }
            
            labelIdStr = labelIdStr.replace("00000000-0000-0000-0000-000000000000", parameters.attributeId);
            returnStr = '<span id="' + labelIdStr + '" name="' + labelIdStr + '">' + data.replace("\"", "\"\"") + '</span >';

            var finalReturnStr = fk + returnStr;

            return finalReturnStr;
        }

        return data;
    }
};