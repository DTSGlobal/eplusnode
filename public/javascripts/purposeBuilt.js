$(function () {

    var self = this;
    var baseurl = window.location.pathname;

    var twoColumnLayout = `<div class="main-body mt-4">
                                        <h2 id="header"></h2>
                                        <div class="row" style="margin-left:3%" id="headerRow">
                                            <div class="col-sm-12 col-md-8 col-lg-3" id="columnOne">
                                            </div>
                                            <div class="col-sm-12 col-md-8 col-lg-3 offset-2 offset-md-3" id="columnTwo">
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left:3%" id="contentSeparator">
                                        </div>
                                        <div class="row" style="margin-left:3%" id="pbResultsGrid">
                                        </div>
                                    </div>
                                </div>`;

    $('#loadingTitle').append(twoColumnLayout);

    if(baseurl.includes("shipment")){

        $('#header').append("Shipment");

        var newScript = document.createElement("script");
        newScript.src = "/javascripts/shipment.js";
        document.head.appendChild(newScript);

    }

});