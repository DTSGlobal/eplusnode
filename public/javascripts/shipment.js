$(function() {
    //trackJs.track("This is a test of the emergency broadcast system")
    window.onbeforeunload = function() {
        //debugger;
        if(SaveElements.molecules.length > 0){
            return 'You have unsaved data on this form. Would you like to leave this form?';
        }
    };
    //Attempting something 1.1
    var Shipment = function() {


        var self = this;
        const id = $('#id').val();
        const userid = $('#userId').val();
        var checkBoxItemLoad = false;
        var saveNewContainer = false;
        var newContainer = false;
        var expandedKeys = [];
        var collapsedKeys = [];
        var initialBoxItemLoad = false;
        var checkContainer = false;

        var inQuickEditMode = false;
        var inMultiEditMode = false;
        var selectedItemsArray = [];
        var selectedBoxesArray = [];

        var formDataElements = new DTS.Things();
        var container = new DTS.Molecule();
        var pallets = new DTS.Things();
        var boxes = new DTS.Things();
        var boxItems = new DTS.Things();
        var customerOrderItems = new DTS.Things();
        var UtilityThing = new DTS.Things();
        var computedBoxItems = new DTS.Things();
        var computedBoxes = new DTS.Things();
        var computedPallets = new DTS.Things();
        var findComputed = new DTS.Things();

        var palletType = new DTS.Molecule();
        var palletTypeId = '337ef260-743f-40e7-adbb-8af5c158f36f';
        var boxType = new DTS.Molecule();
        var boxTypeId = '1A953C11-3688-4AD4-8A0C-61AE10C6A0F4';
        var boxItemType = new DTS.Molecule();
        var boxItemTypeId = '8A22DEF2-F084-4B2D-A89C-3F8E099170C6';
        var customerOrderTypeId = 'B0A35350-1E0C-40D0-B3A2-F03FCC58FAE8';
        var customerOrderTypeThing = new DTS.Molecule();
        var containerTypeId = 'E2A3840F-B06B-4E27-87B9-881F5F6AB7FC';
        var containerType = new DTS.Molecule();
        var customerOrderItemSearch = new DTS.Things();
        var existingContainers = new DTS.Things();

        self.initialize = function (e) {

            var containerThing = new DTS.Things();

            containerThing.molecules.push(container);

            SaveElements.baseFormData.push(containerThing);

            SaveElements.baseFormData.push(pallets);
            SaveElements.baseFormData.push(boxes);
            SaveElements.baseFormData.push(boxItems);
            SaveElements.baseFormData.push(customerOrderItems);

            self.startInitialLoadingAnimation();

            
            var loader = new DTS.Things();
            self.dependenciesLoaded = function () {

                var t = loader;
                palletType = loader.byExposedId(palletTypeId);
                boxType = loader.byExposedId(boxTypeId);
                boxItemType = loader.byExposedId(boxItemTypeId);
                customerOrderTypeThing = loader.byExposedId(customerOrderTypeId);
                if (id != undefined && id != "00000000-0000-0000-0000-000000000000") {
                    container.CreateByExposedId(id, self.containerReceived);
                }
                else {
                    newContainer = true;
                    checkContainer = true;
                }

                if (!newContainer) {
                    self.getPallets();
                }

                if (newContainer) {
                    containerType.CreateByExposedId(containerTypeId, self.createNewContainer);
                }

            };

            var ids = [
                palletTypeId,
                boxTypeId,
                boxItemTypeId,
                customerOrderTypeId,
                containerTypeId
            ];
            loader.InitializeByList(ids, self.dependenciesLoaded);

        };

        self.createNewContainer = function () {

            container.CreateByType(containerType, eMESA.returnGuid(), 'newContainer');
            self.getChangeMethod();
            
        };

        self.getOtherContainersForThisMonth = function () {

            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

            var queryAttributes = {};
            queryAttributes.attributeName = 'BornOnDate';
            queryAttributes.DateMin = firstDay.toISOString();
            var request = {};
            request.attributes = [];
            request.attributes.push(queryAttributes);
            request.columns = [];

            request.DomainObjectId = containerTypeId;
            request.ExistsOnly = true;
            request.friendlyColumns = [];
            request.friendlyColumns = [
                'Label'
            ];
            request.RowCount = 500;
            request.pageNumber = 1;
            existingContainers.InitializeByQuery(request, self.applyNewContainerPSINumber);
        };

        self.setPSINumber = function () {

            $('#header').append(`<i class="fa fa-spin fa-spinner" id="savePSISpinner"></i>
                                        <label id="savePSISpinnerLabel">Saving Shipment Number...Please wait</label>`);

            var thisElement = $('#input' + container.header.exposedId + 'ContainerChangeMethod');
            thisElement.attr('disabled', 'disabled');

            var d = new Date();
            var yearStr = d.getFullYear().toString();
            var monthStr = (d.getMonth() + 1).toString();

            yearStr = yearStr.substr(2, 2);
            if (monthStr.length == 1) {
                monthStr = '0' + monthStr;
            }

            var newPSINo = '';

            if (newContainer) {

                self.getOtherContainersForThisMonth();
                return;
            }
            else {
                var existing = container.Containercontainer_psi_no.substr(1, container.Containercontainer_psi_no.length - 1);
                newPSINo = container.ContainerChangeMethod.Label + existing;
            }

            if (container.Containercontainer_psi_no != newPSINo) {
                container.Containercontainer_psi_no = newPSINo;
                container.applyMappedValues();
                SaveElements.addThingToSave(container, container.atomByPropertyName('Containercontainer_psi_no'));
                SaveElements.RefreshFormElementByExposedId(container, 'Containercontainer_psi_no');
               
            }

            thisElement.attr('disabled', false);
        };

        self.applyNewContainerPSINumber = function () {

            var changedMethod = false;

            if (container.BornOnDate != undefined) {
                changedMethod = true;
            }

            var d = new Date();
            var yearStr = d.getFullYear().toString();
            var monthStr = (d.getMonth() + 1).toString();

            yearStr = yearStr.substr(2, 2);
            if (monthStr.length == 1) {
                monthStr = '0' + monthStr;
            }
            var containerNo;
            var sequentialNo = existingContainers.molecules.length.toString();

            if (sequentialNo == 0) {
                containerNo = '001';
            }

            sequentialNo++;

            if (sequentialNo.toString().length == 1) {
                containerNo = '00' + sequentialNo.toString();
            } else if (sequentialNo.toString().length == 2) {
                containerNo = '0' + sequentialNo.toString();
            } else if (sequentialNo.toString().length == 3) {
                containerNo = sequentialNo.toString();
            }

            var newPSINo = container.ContainerChangeMethod.Label + yearStr + monthStr + '-' + containerNo;


            if (container.Containercontainer_psi_no != newPSINo) {
                container.Containercontainer_psi_no = newPSINo;
                container.applyMappedValues();
                SaveElements.addThingToSave(container, container.atomByPropertyName('Containercontainer_psi_no'));
                var psiContainerNo = $('#input' + container.header.exposedId + 'Containercontainer_psi_no');
                if (!changedMethod) {
                    container.BornOnDate = new Date();
                    container.syncMoleculeValue('BornOnDate');
                    SaveElements.addThingToSave(container, container.atomByPropertyName('BornOnDate'));
                    container.Containercontainer_created_byExposedId = userid;
                    container.syncMoleculeValue('Containercontainer_created_by');
                    SaveElements.addThingToSave(container, container.atomByPropertyName('Containercontainer_created_by'));
                }
                psiContainerNo.text(newPSINo);
                //SaveElements.RefreshFormElementByExposedId(container, 'Containercontainer_psi_no');
                SaveElements.save(function () {

                    saveNewContainer = true;
                    var headerId = container.header.exposedId;
                    var currentURL = window.location.href;
                    if (!currentURL.includes(headerId)) {

                        var newUrl = currentURL;
                        if (newUrl.slice(-1) != '/') {
                            newUrl += '/';
                        }
                        newUrl += '?id=' + headerId;
                        history.replaceState(null, "", newUrl);
                        var thisElement = $('#input' + container.header.exposedId + 'ContainerChangeMethod');
                        thisElement.attr('disabled', false);

                    }
                    $('#savePSISpinner').remove();
                    $('#savePSISpinnerLabel').remove();

                    if (!changedMethod) {
                        self.containerReceived();
                        self.buildPalletTable();
                        SaveElements.RefreshFormElementByExposedId(container, 'BornOnDate');
                    } else {
                        SaveElements.RefreshFormElementByExposedId(container, 'Containercontainer_psi_no');
                        SaveElements.RefreshFormElementByExposedId(container, 'BornOnDate');
                    }

                });
            } else {
                var thisElement = $('#input' + container.header.exposedId + 'ContainerChangeMethod');
                thisElement.attr('disabled', false);
            }



        };

        self.startInitialLoadingAnimation = function () {

            $('#columnOne').append('<i class="fa fa-spin fa-spinner" id="loadSpinner"></i>');
        };

        self.stopLoadingAnimation = function () {

            $('#loadSpinner').remove();
        };

        self.getChangeMethod = function () {

            self.buildContainerSelections();

        };

        self.addTableDynamicShipment = function (args) {

            var headerLabel = args.headerLabel;
            var id = args.id;
            var appendTo = args.appendTo;
            var isSubTable = args.isSubTable;
            var level = args.level;
            var hasExpandButton = args.hasExpandButton;
            var headerFields = args.headerFields;
            var hasBorder = false;
            if (args.hasBorder) {
                hasBorder = true;
            }
            var tableStr = '';
            var width = 100;
            if (level != undefined) {
                width = 100 - (15 * level);
            }

            if (args.overrideWidth != undefined) {
                width = args.overrideWidth;
            }

            if (isSubTable == undefined || !isSubTable) {
                tableStr += '<div class = "table">';
            }

            var tableBorderStyle = 'table table-sm dataTable no-footer';
            if (hasBorder) {
                tableBorderStyle = 'table table-sm dataTable no-footer table-bordered';
            }
            tableStr += '<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" style="overflow-y:hidden"><div class="row">';
            tableStr += '<table class="' + tableBorderStyle + '" style="width: 100%; align:right" id="DataTables_Table_' + id + '" role="grid" id = "' + id + '" >';
            tableStr += '<thead><tr><th><h5>' + headerLabel + '</h5></th><tr></thead>';

            if (headerFields != null && headerFields != undefined) {

                tableStr += '<thead><tr role="row">';

                if (isSubTable != undefined && level != undefined && hasExpandButton) {

                    for (var x = 0; x < level; x++) {
                        //tableStr += '<th class="sorting_disabled" rowspan="1" colspan="1" ></th>';
                    }
                }

                for (var i = 0; i < headerFields.length; i++) {

                    var headerField = headerFields[i];
                    if (headerField == 'tableaction' || headerField == 'caption') {
                        tableStr += '<th class="sorting_disabled.custom-row-actions" rowspan="1" colspan="1" ></th>';
                    }
                    else {
                        tableStr += '<th class="sorting_disabled" rowspan="1" colspan="1" >' + headerField + '</th>';//style="width: 347.4px;"
                    }
                }

                tableStr += '</tr></thead>';
            }
            tableStr += '<tbody id = "' + id + 'body"></tbody>';
            tableStr += '</table>';
            tableStr += '</div>';
            if (isSubTable == undefined || !isSubTable) {
                tableStr += '</div>';
            }
            if (appendTo != undefined) {
                var el = $(tableStr);
                if (appendTo != undefined) {

                    if (isSubTable) {
                        if (level == undefined) {
                            level = 1;
                        }
                        var opacity = 1 - (level * .2);
                        var tdId = id + '_td';
                        var holderTdId = id + '_holder';
                        $('#' + appendTo).after($('<td id= "' + holderTdId + '" colspan="100"  "><div class ="col-sm-12" id="' + tdId + '"></div></td>'));
                        $('#' + tdId).append(el);

                        $(el).animate({
                            backgroundColor: 'rgba(201, 205, 211, ' + opacity + ')',
                            //opacity: opacity,
                            paddingTop: '20px',
                            paddingBottom: '5px',
                            paddingRight: '25px'

                        }, 500, function () {
                            // Animation complete.
                        });
                        $(el).animate({
                            paddingLeft: '55px',

                        }, 500, function () {
                            // Animation complete.
                        });

                    }
                    else {
                        $('#' + appendTo).append(el);

                    }
                }
            }
        };

        self.containerReceived = function () {

            if (!newContainer) {
                self.buildContainerSelections();
            }

            self.continueContainerBuild();
            
            self.addTableDynamicShipment({
                headerLabel: 'Pallets',
                id: 'palletTable',
                headerFields: ['tableaction', 'Pallet No', 'Length', 'Width', 'Height', 'Weight', 'tableaction', 'Duplicate'],
                appendTo: 'pbResultsGrid',
                isSubTable: false,
                level: null,
                headerSort: "nosort",
                hasExpandButton: true
            });
            $('#pbResultsGrid').append('<i class="fa fa-spin fa-spinner" id="gridLoadSpinner"></i>');
            
            //Save button
            $('#contentSeparator').append('<div class="col-lg-12 col-md-8 col-lg-6" >');//; 
            $('#contentSeparator').append('<button class="btn btn-success btn-lg" id= "saveCreateButton" name= "saveCreateButton" style="width:300px" >Save</button>');
            $('#contentSeparator').append('<div class="col-md-12 text-right"><button type="button" id="batchButton" name="batchButton" class="btn btn-secondary"><i class="fa fa-arrow-down" aria-hidden="true" id="batchButtonDownArrow">Add / Edit</i></button></div></div');
            $saveButton = $('#saveCreateButton');
            $saveButton.on('click', $saveButton, self.save);
            SaveElements.saveBtnId = $saveButton;

            var $batchButton = $('#batchButton');
            $batchButton.on('click', $batchButton, self.batchButtonClicked);
            //$batchButton.attr('disabled', 'disabled');
            
        };

        self.getPallets = function () {
            var queryAttributes = {};
            queryAttributes.attributeName = 'PalletContainer_id';
            queryAttributes.valueThingId = id;//container.header.exposedId;

            var request = {};
            request.attributes = [];
            request.attributes.push(queryAttributes);
            request.columns = [];

            request.DomainObjectId = palletTypeId;
            request.ExistsOnly = true;
            request.friendlyColumns = [];
            request.friendlyColumns = [
                'ValueExistsOnOff',
                'Palletpallet_weight',
                'ValueExistsOnOff',
                'Palletpallet_length',
                'Palletpallet_width',
                'Palletpallet_height',
                'Palletcontainer_id',
                'Palletpallet_id',
                'PalletPallet_Total_Value'
            ];
            request.RowCount = 1000;
            request.pageNumber = 1;
            pallets.InitializeByQuery(request, self.buildPalletTable);
        };

        self.getAllBoxes = function () {

            var allQueryAttributes = [];

            for (var i = 0; i < pallets.molecules.length; i++) {

                var thisPallet = pallets.molecules[i];
                var queryAttribute = {};
                queryAttribute.attributeName = 'BoxPalletPEKeyName4';
                queryAttribute.valueThingId = thisPallet.header.exposedId;
                allQueryAttributes.push(queryAttribute);

            }

            var request = {};
            request.attributes = [];
            request.attributes = allQueryAttributes;
            request.columns = [];
            request.DomainObjectId = boxTypeId;
            request.ExistsOnly = true;
            request.friendlyColumns = [
                'Boxbox_id',
                'BoxPalletPEKeyName4',
                'BoxBox_Total_Value',
                'ValueExistsOnOff'
            ];
            request.RowCount = 2000;
            request.pageNumber = 1;

            boxes.InitializeByQuery(request, self.getAllBoxItems);

        };


        self.getAllBoxItems = function () {

            //$('#batchButton').removeAttr('disabled');

            boxes.applyValueThingsToAll(pallets.molecules);
            self.enablePalletButtons();

            var allQueryAttributes = [];

            if (boxes.molecules.length == 0) {
                initialBoxItemLoad = true;
                console.log('No box items to load');
                return;
            }
            for (var i = 0; i < boxes.molecules.length; i++) {

                var queryAttributes = {};
                var thisBox = boxes.molecules[i];
                queryAttributes.attributeName = 'Box Itembox_id';
                queryAttributes.valueThingId = thisBox.header.exposedId;
                allQueryAttributes.push(queryAttributes);

            }

            var request = {};
            request.attributes = [];
            request.attributes = allQueryAttributes;
            request.columns = [];
            request.DomainObjectId = boxItemTypeId;
            request.ExistsOnly = true;
            request.friendlyColumns = [

                'Box Itemquantity',
                'Box ItemOrderItemPEKeyName4',
                'Box Itembox_id',
                'Box ItemBox_Item_Total_Value',
                'Box Itemorder_item_no',
                'ValueExistsOnOff'
            ];
            request.RowCount = 4000;
            request.pageNumber = 1;


            //Customer Order Itempo_number
            //FKs
            var FKs = [];
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itemitem_description_1'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itemudf_order_item_no'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itempo_number'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itemorder_quantity'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order ItemShippedQuantity'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itemline_number'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itempeoplesoft_item_no'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order ItemQuantityRemaining'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itemorder_item_price_each'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order ItemScheduleNumber'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order ItemItemCodeDisplay'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box ItemOrderItemPEKeyName4', 'Customer Order Itemitem_description_1'));

            //
            request.ForeignKeys = FKs;

            var findThings = new DTS.Things();

            findThings.InitializeByQuery(request, function (result) {
                console.log('Box Items received');
                checkBoxItemLoad = true;
                boxItems.AppendMolecules(findThings.byTypeName('Box Item'));
                boxItems.applyValueThingsToAll(findThings.byTypeName('Customer Order Item'));
                boxItems.applyValueThingsToAll(boxes.molecules);
                boxItems.removeInactive();
                customerOrderItems.AppendMolecules(findThings.byTypeName('Customer Order Item'));
                $('#batchButton').removeAttr('disabled');

                self.allDone();
            });

        };

        self.toggleBatchButton = function () {

            var down = $('#batchButtonDownArrow');
            var up = $('#batchButtonUpArrow');

            if (down.length != 0) {
                $('#batchButtonDownArrow').remove();
                $('#batchButton').append('<i class="fa fa-arrow-up" aria-hidden="true" id="batchButtonUpArrow">Close</i>');
            }
            if (up.length != 0) {
                $('#batchButtonUpArrow').remove();
                $('#batchButton').append('<i class="fa fa-arrow-down" aria-hidden="true" id="batchButtonDownArrow">Add / Edit</i>');

                //$('#pbResultsGrid').show(250);
                $('#orderSearchContainer').remove();

                $('#editButton').remove();
                $('#addMultiple').remove();
            }
        };

        self.batchButtonClicked = function (e) {

            var down = $('#batchButtonDownArrow');
            var up = $('#batchButtonUpArrow');

            if (down.length != 0) {
                $('#batchButtonDownArrow').remove();
                $('#batchButton').append('<i class="fa fa-arrow-up" aria-hidden="true" id="batchButtonUpArrow">Close</i>');
            }
            if (up.length != 0) {
                $('#batchButtonUpArrow').remove();
                $('#palletSelect').empty();
                $('#batchButton').append('<i class="fa fa-arrow-down" aria-hidden="true" id="batchButtonDownArrow">Add / Edit</i>');

                $('#pbResultsGrid').show(250);
                $('#orderSearchContainer').remove();

                $('#editButton').remove();
                $('#addMultiple').remove();
                
                return;
            }

            $('#pbResultsGrid').hide(250);

            $('#contentSeparator').append('<div class="container" id= "orderSearchContainer" ><div class="row"><div class="col-md" id="orderSearch"></div><div class="col-md" id="orderEdit"></div></div></div></div>');
            $('#orderSearch').append('<div row><h5>Add Or Modify</h5></div>');
            var poSearchId = 'poOrderSearch';
            var poSearchButtonId = 'poOrderSearchButton';

            //Search field
            var poSearchStr = '<div row>';
            poSearchStr += '<div class="form-group" id = "poSearchContainer" >';
            poSearchStr += '<input class="form-control" id= "' + poSearchId + '" name= "' + poSearchId + '" style= "width:200px; align:right" spellcheck= "false" autocomplete= "off" > ';
            poSearchStr += '<div row>';
            poSearchStr += '<br >';
            poSearchStr += '<button class="btn btn-success btn-sm btn-block" style= "width:100px; align:right" id= "' + poSearchButtonId + '" name= "' + poSearchButtonId + '_add" > Search</button > ';
            poSearchStr += '</div>';
            poSearchStr += '</div>';

            $('#orderSearch').append(poSearchStr);

            self.addTableDynamicShipment({
                headerLabel: 'Orders',
                id: 'orderTable',
                headerFields: ['tableaction', 'Order No', 'Item Code', 'Description', 'P21 #', 'DLTD #', 'Schedule #', 'Shipped Qty'],
                appendTo: 'orderSearch',
                hasBorder: true,
                overrideWidth: 45
            });

            var table = $('#DataTables_Table_orderTable');
            $('#DataTables_Table_orderTable').hide();

            var $textBox = $('#' + poSearchId);
            $textBox.on('change input', $textBox, self.poSearchTextChange);

            var $searchButton = $('#' + poSearchButtonId);
            $searchButton.on('click', $searchButton, self.poSearch);
            $searchButton.attr('disabled', 'disabled');

            var orderEditElements = '<span id ="orderEditActionButtons" style="white-space: nowrap">';
            orderEditElements += '<button type="button" id="editButton" name="editButton" class="btn btn-secondary"><i class="fa fa-arrow-down" aria-hidden="true">Quick Edit</i></button>';
            orderEditElements += '<button type="button" id="addMultiple" name="editButton" class="btn btn-secondary"><i class="fa fa-arrow-down" aria-hidden="true">Add Multiple</i></button>';
            orderEditElements += '</span>';
            $('#orderEdit').append(orderEditElements);

            var $editButton = $('#editButton');
            $editButton.on('click', $editButton, self.editButtonClicked);
            $editButton.attr('disabled', 'disabled');
            $('#editButton').hide();

            var $addMultipleButton = $('#addMultiple');
            $addMultipleButton.on('click', $addMultipleButton, self.addMultipleButtonClicked);
            $addMultipleButton.attr('disabled', 'disabled');

            $('#orderEdit').append('<div id="orderEditElements"</div>');

        };

        self.dismissEditPane = function (e) {

            e.stopPropagation();
            e.preventDefault();
            inQuickEditMode = false;

            self.toggleBatchButton();
            $('#batchButton').click();
            /*
            $('#editButton').removeAttr('disabled');
            $('#addMultiple').removeAttr('disabled');
            $('#orderEditElements').empty();
            */
        };

        self.addStatus = function () {
            $('#orderEditElements').append('<div id="editSuccessPaneHolder"></div>');
            $('#orderEditElements').append('<div class="alert alert-success" role="alert" id="editSuccessPane"><span id="editSuccessPaneMessage"></span><button type= "button" class="close" data-dismiss="alert" aria- label="Close" ><span aria-hidden="true">&times;</span></button ></div>');
            $('#editSuccessPane').hide();
            $('#editSuccessPane').on('closed.bs.alert', function () {
                $('#editSuccessPaneHolder').html('<div class="alert alert-success" role="alert" id="editSuccessPane"><span id="editSuccessPaneMessage"></span><button type="button" class="close" data-dismiss="alert" aria- label="Close" ><span aria-hidden="true">&times;</span></button ></div>');
                $('#editSuccessPane').hide();
            });

        };

        self.editButtonClicked = function (e) {

            inQuickEditMode = true;
            inMultiEditMode = false;
            self.enableItemCheckboxes();

            $('#addMultiple').attr('disabled', 'disabled');
            $('#editButton').attr('disabled', 'disabled');
            $('#orderEditElements').append('<br ><br ><br ><br >');

            self.addStatus();

            $('#orderEditElements').append('<div row><div class="col-md"><h5>Item: </h5><span id="customerOrderItemDisplay" name ="customerOrderItemDisplay" value=""></span> </div></div>');
            $('#orderEditElements').append('<div row><div class="col-md"><h5>Pallet: </h5></div></div><div row><div class="col-md"><select class="form-control" id= "palletSelect" name = "palletSelect" style= "width:100px"  ></div></div>');
            $('#orderEditElements').append('<div row><div class="col-md"><h5>From Box: </h5></div></div><div row><div class="col-md"><select class="form-control" id= "fromBoxSelect" name = "fromBoxSelect" style= "width:100px"  ></div></div>');
            $('#orderEditElements').append('<div row><div class="col-md"><h5>To Box: </h5></div></div><div row><div class="col-md"><select class="form-control" id= "toBoxSelect" name = "toBoxSelect" style= "width:100px"  ></div></div>');
            $('#orderEditElements').append('<div row><div class="col-md"><h5># Per Box</h5><input class="form-control" id="countToAdd" name="countToAdd" style="width:100px; align:right" spellcheck="false" value="1"></div></div>');

            $('#orderEditElements').append('<span id ="orderEditActionButtons" style="white-space: nowrap">');
            $('#orderEditElements').append('<div row><div class="col-md"><button class="btn btn-success btn-sm btn-block" style="width:100px; align:right" id="addBoxesButton" name="addBoxesButton">Add Boxes</button></div></div>');
            $('#orderEditElements').append('<div row><div class="col-md"></div><div class="col-md"></div><div class="col-md"><button class="btn btn-secondary btn-sm btn-block" style="width:100px; align:right" id="dismissEditPane" name="dismissEditPane">Dismiss</button></div></div>');
            $('#orderEditElements').append('</span>');

            var $addBoxesButton = $('#addBoxesButton');
            $addBoxesButton.on('click', $addBoxesButton, self.addBoxesButtonClicked);
            $('#addBoxesButton').attr('disabled', 'disabled');

            var $dismissButton = $('#dismissEditPane');
            $dismissButton.on('click', $dismissButton, self.dismissEditPane);

            var $palletSelect = $('#palletSelect');
            var palletListToSort = eMESA.sortedAttributeArrayByName(pallets.molecules, 'Palletpallet_id');
            var sortedArray = Object.keys(palletListToSort);
            sortedArray.sort(function (a, b) {
                if (isNaN(a) && isNaN(b)) return a < b ? -1 : a == b ? 0 : 1;//both are string
                else if (isNaN(a)) return 1;//only a is a string
                else if (isNaN(b)) return -1;//only b is a string
                else return a - b;//both are num
            });
            var options = $palletSelect[0].options;
            for (var i = 0; i < sortedArray.length; i++) {

                var key = sortedArray[i];
                $palletSelect.append($('<option>', {
                    value: palletListToSort[key].header.exposedId,
                    text: palletListToSort[key].Palletpallet_id
                }));
            }
            $palletSelect.on('change', $palletSelect, self.palletSelected);

            var $fromBoxSelect = $('#fromBoxSelect');
            self.populateBoxSelect('fromBoxSelect', pallets.molecules[0]);
            self.populateBoxSelect('toBoxSelect', pallets.molecules[0]);
        };

        self.addMultipleButtonClicked = function (e) {

            $('#addMultiple').attr('disabled', 'disabled');
            $('#editButton').attr('disabled', 'disabled');

            inQuickEditMode = false;
            inMultiEditMode = true;
            self.enableItemCheckboxes();

            $('#orderEditElements').append('<br ><br ><br ><br >');
            self.addStatus();
            $('#orderEditElements').append('<div row><div class="col-md"><h5>Items: </h5><span id="customerOrderItemDisplay" name ="customerOrderItemDisplay" value=""></span> </div></div><br >');
            $('#orderEditElements').append('<div row><div class="col-md"><h5>Pallets: Boxes </h5></div></div><div row><div class="col-md"><select multiple class="form-control" id= "palletSelect" name = "palletSelect" style= "width:300px"  ></div></div>');
            $('#orderEditElements').append('<div row><br></div>')
            $('#orderEditElements').append('<div row><div class="col-md"><h5>Selected Pallets and Boxes: </h5><span id="selectedPalletsBoxes" name ="selectedPalletsBoxes" value=""></span> </div></div>');
            $('#orderEditElements').append('<div row><br></div>')
            $('#orderEditElements').append('<div row><div class="col-md"><h5># Per Box</h5><input class="form-control" id="countToAdd" name="countToAdd" style="width:100px; align:right" spellcheck="false" value="1"></div></div>');

            selectedBoxesArray = [];
            var $palletSelect = $('#palletSelect');
            $palletSelect.on('change', $palletSelect, self.palletSelected);

            var options = $palletSelect[0].options;
            for (var i = 0; i < pallets.molecules.length; i++) {

                var palletMenu = pallets.molecules[i];

                var boxList = boxes.ByValueThingId('BoxPalletPEKeyName4', palletMenu.header.exposedId);
                var boxListToSort = eMESA.sortedAttributeArrayByName(boxList, 'Boxbox_id');

                var sortedArray = Object.keys(boxListToSort);
                sortedArray.sort(function (a, b) {
                    if (isNaN(a) && isNaN(b)) return a < b ? -1 : a == b ? 0 : 1;//both are string
                    else if (isNaN(a)) return 1;//only a is a string
                    else if (isNaN(b)) return -1;//only b is a string
                    else return a - b;//both are num
                });
                
                var key = sortedArray[i];
                for (var x = 0; x < sortedArray.length; x++) {

                    var key = sortedArray[x];
                    var displayLabel = 'Pallet: ' + palletMenu.Palletpallet_id + ' : Box : ' + boxListToSort[key].Boxbox_id;

                    $palletSelect.append($('<option>', {
                        value: boxListToSort[key].header.exposedId,
                        text: displayLabel
                    }));

                    //Make a default selection
                    if (x == 0 && i == 0) {
                        var match = $.grep(options, function (obj) { return obj.value === boxListToSort[key].header.exposedId; })[0];
                        if (match != undefined) {
                            var option = $(match);
                            option.attr('selected', 'selected');
                        }
                        selectedBoxesArray.push(boxListToSort[key].header.exposedId);
                        $('#selectedPalletsBoxes').append(displayLabel);
                    }
                }

            }

            $('#orderEditElements').append('<span id ="orderEditActionButtons" style="white-space: nowrap">');
            $('#orderEditElements').append('<div row><div class="col-md"><button class="btn btn-success btn-sm btn-block" style="width:100px; align:right" id="addBoxesButton" name="addBoxesButton">Add To Boxes</button></div></div>');
            $('#orderEditElements').append('<div row><div class="col-md"></div><div class="col-md"></div><div class="col-md"><button class="btn btn-secondary btn-sm btn-block" style="width:100px; align:right" id="dismissEditPane" name="dismissEditPane">Dismiss</button></div></div>');
            $('#orderEditElements').append('</span>');

            var $addBoxesButton = $('#addBoxesButton');
            $addBoxesButton.on('click', $addBoxesButton, self.addBoxesButtonClicked);
            $('#addBoxesButton').attr('disabled', 'disabled');

            var $dismissButton = $('#dismissEditPane');
            $dismissButton.on('click', $dismissButton, self.dismissEditPane);


        };

        self.palletSelected = function (e) {

            var $target = $(e.target);
            var displayValues = $target[0].options;
            selectedBoxesArray = [];
            var displayLabel = '';

            var matchLength = 0;
            for (var i = 0; i < displayValues.length; i++) {

                var displayValue = displayValues[i];
                if (displayValue.selected) {

                    if (matchLength > 0) {
                        displayLabel += '<br>';
                    }
                    displayLabel += displayValue.text;
                    selectedBoxesArray.push(displayValue.value);
                    matchLength++;
                }
            }

            $('#selectedPalletsBoxes').empty();
            $('#selectedPalletsBoxes').append(displayLabel);
            //

            if (inQuickEditMode) {

                var selectedPalletId = $target[0].value;
                var pallet = pallets.byExposedId(selectedPalletId);
                self.populateBoxSelect('fromBoxSelect', pallet);
                self.populateBoxSelect('toBoxSelect', pallet);

            }
        };

        self.populateBoxSelect = function (boxSelector, pallet) {

            var $boxSelect = $('#' + boxSelector);
            $boxSelect.empty();
            var boxList = boxes.ByValueThingId('BoxPalletPEKeyName4', pallet.header.exposedId);
            var boxListToSort = eMESA.sortedAttributeArrayByName(boxList, 'Boxbox_id');

            var sortedArray = Object.keys(boxListToSort);
            sortedArray.sort(function (a, b) {
                if (isNaN(a) && isNaN(b)) return a < b ? -1 : a == b ? 0 : 1;//both are string
                else if (isNaN(a)) return 1;//only a is a string
                else if (isNaN(b)) return -1;//only b is a string
                else return a - b;//both are num
            });
            var options = $boxSelect[0].options;
            for (var i = 0; i < sortedArray.length; i++) {

                var key = sortedArray[i];

                $boxSelect.append($('<option>', {
                    value: boxListToSort[key].header.exposedId,
                    text: boxListToSort[key].Boxbox_id
                }));
            }
        };

        self.addBoxesButtonClicked = function (e) {
            e.stopPropagation();
            e.preventDefault();

            var palletButton;
            var palletExpandButtonId;
            var addBoxesQtyTextBox;
            var addBoxesQty;
            var boxToAppend;
            var boxButtonId;
            var boxButton;
            var selectedCustomerItem;
            $('#addBoxesButton').attr('disabled', 'disabled');

            if (inQuickEditMode) {

                var selectedPalletId = $('#palletSelect')[0].value;
                var fromBox = $('#fromBoxSelect option:selected').text();
                var toBox = $('#toBoxSelect option:selected').text();

                var selectedPallet = pallets.byExposedId(selectedPalletId);
                addBoxesQtyTextBox = $('#countToAdd');
                addBoxesQty = addBoxesQtyTextBox.val();
                var candidateBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', selectedPallet.header.exposedId);

                var boxesToPopulate = [];
                for (var i = 0; i < candidateBoxes.length; i++) {

                    var candidateBox = candidateBoxes[i];
                    if (candidateBox.Boxbox_id >= fromBox && candidateBox.Boxbox_id <= toBox) {
                        boxesToPopulate.push(candidateBox);
                    }

                }

                selectedCustomerItem = selectedItemsArray[0];

                for (var x = 0; x < boxesToPopulate.length; x++) {
                    boxToAppend = boxesToPopulate[x];

                    //Expand the pallet
                    var checkId = boxToAppend.BoxPalletPEKeyName4;
                    if (checkId == undefined) {
                        palletExpandButtonId = boxToAppend.BoxPalletPEKeyName4ExposedId;
                    } else {
                        palletExpandButtonId = boxToAppend.BoxPalletPEKeyName4.header.exposedId + '_expand';
                    }
                    palletButton = $('#' + palletExpandButtonId);
                    palletButton.click();

                    boxButtonId = boxToAppend.header.exposedId + '_expand';
                    boxButton = $('#' + boxButtonId);
                    boxButton.click();

                    /*
                    var dummyBoxes = boxItems.ByValueThingId('Box Itembox_id', boxToAppend.header.exposedId);

                    if (dummyBoxes.length == 1 && dummyBoxes[0].BoxItemOrderItemPEKeyName4 == undefined) {

                        var thisBox = dummyBoxes[0];
                        thisBox.BoxItemOrderItemPEKeyName4 = selectedCustomerItem;
                        debugger;
                        thisBox.BoxItemquantity = addBoxesQty;
                        thisBox.applyMappedValues();
                        //SaveElements.RefreshFormElementsAllForThing(thisBox);
                        //SaveElements.RefreshFormElementByExposedId(thisBox, 'BoxItemquantity');
                        //SaveElements.RefreshFormElementByExposedId(thisBox, 'BoxItemOrderItemPEKeyName4');

                    }
                    else {
                    */
                    self.createNewBoxItem(null, boxToAppend, selectedCustomerItem, addBoxesQty, true);
                    //}
                }
            }

            if (inMultiEditMode) {

                addBoxesQtyTextBox = $('#countToAdd');
                addBoxesQty = addBoxesQtyTextBox.val();

                var selectedCustomerIds = [];
                var customerOrderItemQuantities = [];
                for (var i = 0; i < selectedBoxesArray.length; i++) {

                    var boxId = selectedBoxesArray[i];
                    boxToAppend = boxes.byExposedId(boxId);
                    var checkPallet = boxToAppend.BoxPalletPEKeyName4;
                    if (checkPallet == undefined) {

                        palletExpandButtonId = checkPallet;

                    } else {

                        palletExpandButtonId = boxToAppend.BoxPalletPEKeyName4.header.exposedId + '_expand';


                    }

                    palletButton = $('#' + palletExpandButtonId);
                    palletButton.click();

                    boxButtonId = boxToAppend.header.exposedId + '_expand';
                    boxButton = $('#' + boxButtonId);
                    boxButton.click();


                    for (var x = 0; x < selectedItemsArray.length; x++) {
                        selectedCustomerItem = selectedItemsArray[x];

                        var dummyBoxes = boxItems.ByValueThingId('Box Itembox_id', boxToAppend.header.exposedId);

                        if (dummyBoxes.length == 1 && dummyBoxes[0].BoxItemOrderItemPEKeyName4 == undefined) {

                            console.log('Found dummy box item');
                            var thisBox = dummyBoxes[0];
                            thisBox.BoxItemOrderItemPEKeyName4 = selectedCustomerItem;
                            thisBox.BoxItemquantity = addBoxesQty;
                            thisBox.applyMappedValues();

                            SaveElements.addThingToSave(thisBox, thisBox.atomByPropertyName('BoxItemorder_item_no'));
                            SaveElements.addThingToSave(thisBox, thisBox.atomByPropertyName('BoxItembox_id'));
                            SaveElements.addThingToSave(thisBox, thisBox.atomByPropertyName('BoxItemquantity'));
                            SaveElements.addThingToSave(thisBox, thisBox.atomByPropertyName('BoxItemOrderItemPEKeyName4'));


                            SaveElements.RefreshFormElementByExposedId(thisBox, 'BoxItemquantity');
                            SaveElements.RefreshFormElementByExposedId(thisBox, 'BoxItemOrderItemPEKeyName4');

                        }
                        else {

                            //Expand the pallet
                            console.log('Add new box item');
                            var isLast = false;
                            if (x == selectedItemsArray.length - 1) {
                                isLast = true;
                            }
                            self.createNewBoxItem(null, boxToAppend, selectedCustomerItem, addBoxesQty, null, null, isLast);

                        }

                        selectedCustomerIds.push(selectedCustomerItem.header.exposedId);
                        var customerOrderItemQty = {

                            customerOrderItemId: selectedCustomerItem.header.exposedId,
                            qty: addBoxesQty
                        };
                        customerOrderItemQuantities.push(customerOrderItemQty);
                    }
                    //these are being added, so we want to include them in our customerOrderItem array
                }
                customerOrderItems.molecules.concat(selectedItemsArray);
                self.computeOrderValues(null, selectedCustomerIds, [], '', false, customerOrderItemQuantities);
            }
            $('#addBoxesButton').attr('disabled', false);

            var message = 'Item Successfully Added';
            $('#editSuccessPaneMessage').empty();
            $('#editSuccessPaneMessage').append('<strong>' + message + '</strong>')
            $('#editSuccessPane').show();
        };

        self.poSearch = function (e) {
            e.stopPropagation();
            e.preventDefault();

            var myNode = document.getElementById("orderTablebody");
            while (myNode.firstChild) {

                myNode.firstChild.remove();

            }

            var myNode = document.getElementById("orderEditElements");
            while (myNode.firstChild) {

                myNode.firstChild.remove();

            }


            selectedItemsArray = [];

            var $textBox = $('#poOrderSearch');
            var poStr = $textBox.val().trim();
            $('#poSearchContainer').append('<i class="fa fa-spin fa-spinner" id="coSearchSpinner"></i>');

            var allQueryAttributes = [];
            var queryAttribute = {};
            queryAttribute.attributeName = 'Customer Order Itempo_number';
            queryAttribute.valueText = poStr;
            allQueryAttributes.push(queryAttribute);

            var request = {};
            request.attributes = [];
            request.attributes = allQueryAttributes;
            request.columns = [];
            request.DomainObjectId = customerOrderTypeId;
            request.ExistsOnly = true;
            request.friendlyColumns = [
                'Customer Order Itemitem_description_1',
                'Customer Order Itemudf_order_item_no',
                'Customer Order Itempo_number',
                'Customer Order Itemorder_quantity',
                'Customer Order ItemShippedQuantity',
                'Customer Order Itemline_number',
                'Customer Order Itempeoplesoft_item_no',
                'Customer Order ItemQuantityRemaining',
                'Customer Order Itemorder_item_price_each',
                'Customer Order ItemScheduleNumber',
                'Customer Order ItemItemCodeDisplay',
                'Customer Order Itemitem_description_1',
                'ValueExistsOnOff'
            ];
            request.RowCount = 500;
            request.pageNumber = 1;
            customerOrderItemSearch.InitializeByQuery(request, self.customerOrderItemsReceived);
        };

        self.customerOrderItemsReceived = function () {

            //Enable edit buttons
            $('#addMultiple').removeAttr('disabled');
            $('#editButton').removeAttr('disabled');
            var $textBox = $('#poOrderSearch');
            var poStr = $textBox.val().trim();
            $('#coSearchSpinner').remove();

            customerOrderItemSearch.sortThingsByMultipleAttributes([{
                property: 'CustomerOrderItemudf_order_item_no',
                dir: 'asc'
            },
            {
                property: 'CustomerOrderItemScheduleNumber',
                dir: 'asc'
            }]);
            //customerOrderItemSearch.sortThingsByAttribute('CustomerOrderItemudf_order_item_no');

            for (var i = 0; i < customerOrderItemSearch.molecules.length; i++) {

                var custItem = customerOrderItemSearch.molecules[i];
                var exists = custItem.ValueExistsOnOff;
                //debugger;
                var thisPO = custItem.CustomerOrderItempo_number;
                if (thisPO == poStr) {
                    if (exists) {
                        var rowStr = '<tr row id="' + custItem.header.exposedId + '" > ';
                        rowStr += '<td id = "itemrow' + i + 'itemrowCheckbox">';
                        rowStr += '<input type="checkbox" id = "check_' + custItem.header.exposedId + '" >';
                        rowStr += '</td>';

                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowitemrowCustomerOrderItempo_number"></td>';
                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowCustomerOrderItemItemCodeDisplay"></td>';
                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowCustomerOrderItemitem_description_1"></td>';
                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowCustomerOrderItemline_number"></td>';
                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowCustomerOrderItemudf_order_item_no"></td>';
                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowCustomerOrderItemScheduleNumber"></td>';
                        rowStr += '<td id = "' + custItem.header.exposedId + 'itemrowCustomerOrderItemShippedQuantity"></td>';

                        rowStr += '</td>';
                        rowStr += '</tr>';
                        $('#orderTablebody').append($(rowStr));

                        var checkedId = 'check_' + custItem.header.exposedId;
                        var $checkbox = $('#' + checkedId);
                        $checkbox.on('change', $checkbox, self.orderItemCheckedChanged);
                        $checkbox.attr('disabled', 'disabled');

                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItempo_number', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItempo_number', includeFormGroup: false });
                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItemItemCodeDisplay', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItemItemCodeDisplay', includeFormGroup: false });
                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItemitem_description_1', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItemitem_description_1', includeFormGroup: false });
                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItemline_number', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItemline_number', includeFormGroup: false });
                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItemudf_order_item_no', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItemudf_order_item_no', includeFormGroup: false });
                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItemScheduleNumber', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItemScheduleNumber', includeFormGroup: false });
                        new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: custItem, property: 'CustomerOrderItemShippedQuantity', appendTo: custItem.header.exposedId + 'itemrowCustomerOrderItemShippedQuantity', includeFormGroup: false });
                    }
                }
            }
            $('#DataTables_Table_orderTable').show(50);

        };

        self.enableItemCheckboxes = function () {

            for (var i = 0; i < customerOrderItemSearch.molecules.length; i++) {
                var thisItem = customerOrderItemSearch.molecules[i];
                var custId = 'check_' + thisItem.header.exposedId;
                var $checkbox = $(document.getElementById(custId));
                $checkbox.removeAttr('disabled');
            }
        };

        self.orderItemCheckedChanged = function (e) {

            var $target = $(e.target);
            var isChecked = $target[0].checked;
            var incomingId = $target[0].id.replace('check_', '');
            if (inQuickEditMode) {

                if (isChecked) {
                    selectedItemsArray = [];
                    selectedItemsArray.push(customerOrderItemSearch.byExposedId(incomingId));

                    //If we're in quick edit mode, only one should be selected at a time
                    for (var i = 0; i < customerOrderItemSearch.molecules.length; i++) {
                        var thisItem = customerOrderItemSearch.molecules[i];
                        var custId = thisItem.header.exposedId;
                        if (incomingId.toLowerCase() != custId.toLowerCase()) {
                            $('#check_' + custId).prop('checked', false);
                        }
                    }
                    var selectedItem = customerOrderItemSearch.byExposedId(incomingId);
                    var newLabel = selectedItem.CustomerOrderItempo_number + ' - ' + selectedItem.CustomerOrderItempeoplesoft_item_no + ' - ' + selectedItem.CustomerOrderItemitem_description_1 + ' - ' + selectedItem.CustomerOrderItemScheduleNumber + ' - ' + selectedItem.CustomerOrderItemShippedQuantity;
                    $('#customerOrderItemDisplay').text(newLabel);

                    $('#addBoxesButton').removeAttr('disabled');
                }
            }

            if (inMultiEditMode) {

                if (isChecked) {
                    selectedItemsArray.push(customerOrderItemSearch.byExposedId(incomingId));
                }
                else {

                    var spliceIndex = -1;
                    for (var i = 0; i < selectedItemsArray.length; i++) {
                        if (selectedItemsArray[i].header.exposedId.toLowerCase() == incomingId.toLowerCase()) {
                            spliceIndex = i;
                            break;
                        }
                    }
                    selectedItemsArray.splice(spliceIndex, 1);
                }


                var displayLabel = '';
                for (var i = 0; i < selectedItemsArray.length; i++) {

                    var selectedItem = selectedItemsArray[i];
                    if (i > 0) {
                        displayLabel += '<br >';
                    }
                    var shippedQty = selectedItem.CustomerOrderItemShippedQuantity;
                    if (shippedQty == null) {

                        shippedQty = '';
                    }
                    displayLabel += selectedItem.CustomerOrderItempo_number + ' - ' + selectedItem.CustomerOrderItempeoplesoft_item_no + ' - ' + selectedItem.CustomerOrderItemitem_description_1 + ' - ' + selectedItem.CustomerOrderItemScheduleNumber + ' - ' + shippedQty;
                }
                $('#customerOrderItemDisplay').empty();
                $('#customerOrderItemDisplay').append(displayLabel);
                $('#addBoxesButton').removeAttr('disabled');
            }
        };

        self.poSearchTextChange = function (e) {

            var $target = $(e.target);
            var poSearchStr = $target.val().trim();

            var $searchButton = $('#poOrderSearchButton');
            if (poSearchStr.length != 10) {
                $searchButton.attr('disabled', 'disabled');
            }
            else //if (poSearchStr.length == 10)
            {
                $searchButton.removeAttr('disabled');
            }
        };

        self.buildContainerSelections = function () {

            self.stopLoadingAnimation();
            $("#columnOne").append('<br/>');
            $("#columnTwo").append('<br/>');

            //Column 1
            //elementType, label, thing, property, appendTo, displayFields, includeFormGroup
            new DTS.ThingElements().addElement({ elementType: 'label', label: 'PSI No', thing: container, property: 'Containercontainer_psi_no', appendTo: 'columnOne', includeFormGroup: true });
            //new DTS.ThingElements().addElement({ elementType: 'label', label: 'Shipment Weight', thing: container, property: 'ContainerPalletWeight', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({
                elementType: 'select',
                label: 'Change Method',
                thing: container,
                property: 'ContainerChangeMethod',
                appendTo: 'columnOne',
                displayFields: 'Label',
                includeFormGroup: true,
                addHandler: self.setPSINumber
            });

        };

        self.continueContainerBuild = function () {

            new DTS.ThingElements().addElement({ elementType: 'radio', label: 'HazMat', thing: container, property: 'Containercontainer_hazmat', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'select', label: 'Route', thing: container, property: 'Containercontainer_route_id', appendTo: 'columnOne', displayFields: 'Label', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'select', label: 'Status', thing: container, property: 'Containercontainer_status_code', appendTo: 'columnOne', displayFields: 'Label', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'Seal', thing: container, property: 'Containercontainer_seal_no', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'Container #', thing: container, property: 'Containercontainer_car_no', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'Vessel Name', thing: container, property: 'Containercontainer_vessel_name', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'Voyage Number', thing: container, property: 'Containercontainer_voyage_number', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'Booking #', thing: container, property: 'Containercontainer_bol_number', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'datepicker', label: 'Export Date', thing: container, property: 'Containercontainer_export_date', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'datepicker', label: 'PSI/Vendor Depart Date', thing: container, property: 'Containercontainer_invoice_date', appendTo: 'columnOne', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'Final Destination', thing: container, property: 'Containercontainer_export_instr', appendTo: 'columnOne', includeFormGroup: true });


            //Column 2
            new DTS.ThingElements().addElement({ elementType: 'radio', label: 'Containerized', thing: container, property: 'Containercontainer_containerized', appendTo: 'columnTwo', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'radio', label: 'Shipper Owned Container', thing: container, property: 'Containercontainer_shipper_owned', appendTo: 'columnTwo', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'select', label: 'Point Origin ', thing: container, property: 'Containercontainer_point_origin', appendTo: 'columnTwo', displayFields: 'Label', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'select', label: 'Inco Terms', thing: container, property: 'Containercontainer_inco', appendTo: 'columnTwo', displayFields: 'Label', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'select', label: 'Loading Dock', thing: container, property: 'Containercontainer_loading_doc', appendTo: 'columnTwo', displayFields: 'Label', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: 'AFE/Project No', thing: container, property: 'Containercontainer_transshipment', appendTo: 'columnTwo', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'select', label: 'Truck Line', thing: container, property: 'Containercontainer_pre_carriage', appendTo: 'columnTwo', displayFields: 'Label', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'label', label: 'Shipment Value', thing: container, property: 'Containercontainer_place_pre', appendTo: 'columnTwo', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'textarea', label: 'Comments', thing: container, property: 'Containercontainer_external_comments', appendTo: 'columnTwo', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'label', label: 'Shipment Weight', thing: container, property: 'ContainerPalletWeight', appendTo: 'columnTwo', includeFormGroup: true });
            new DTS.ThingElements().addElement({ elementType: 'label', label: 'Created Date', thing: container, property: 'BornOnDate', appendTo: 'columnTwo', includeFormGroup: true });
            //new DTS.ThingElements().addElement({ elementType: 'label', label: 'Created By', thing: container, property: 'Containercontainer_created_by', displayFields:'Usernameemail_address', appendTo: 'columnTwo', includeFormGroup: true });
            //new DTS.ThingElements().addElement({ elementType: 'label', label: 'Modified By', thing: container, property: 'Containercontainer_modified_by', displayFields: 'Usernameemail_address', appendTo: 'columnTwo', includeFormGroup: true });
            var modifiedDate = '';

            var createName = container.Containercontainer_created_byName;
            if (createName == undefined) {
                createName = '';
            }

            var modifyName = container.Containercontainer_modified_byName;
            if (modifyName == undefined) {
                modifyName = '';
            }

            var createLabel = '<label style="width:20px" id="' + container.header.exposedId + 'createLabel">Created By</label></br>';
            var elCreateName = '<span id="' + container.header.exposedId + 'createName">' + createName + '</span></br></br>';
            $('#columnTwo').append(createLabel);
            $('#columnTwo').append(elCreateName);
            var ModifyLabel = '<label style="width:20px" id="' + container.header.exposedId + 'modifyLabel">Modified By</label></br>';
            var elModifyName = '<span id="' + container.header.exposedId + 'ModifiedDate">' + modifyName + '</span></br></br>';
            $('#columnTwo').append(ModifyLabel);
            $('#columnTwo').append(elModifyName);



            var labelforDate = '<label style="width:20px" id="' + container.header.exposedId + 'ModifiedDateLabel">ModifiedDate</label></br>';
            var labelModifiedDate = '<span id="' + container.header.exposedId + 'ModifiedDate">' + modifiedDate + '</span></br></br>';
            
            if (container.Containercontainer_modified_byName != undefined) {
                for (var b = 0; b < container.attributes.length; b++) {
                    let attr = container.attributes[b];
                    if (attr.name == 'Containercontainer_modified_by') {
                        modifiedDate = attr.enteredByTime;
                    }
                }


                if (modifiedDate != '' || modifiedDate != undefined) {
                    var newDate = new Date(modifiedDate);
                    var getYear = newDate.getFullYear();
                    var getMonth = newDate.getMonth() + 1; //getmonth is 0 based
                    var getDay = newDate.getDate();
                    var formattedDate = getMonth + '-' + getDay + '-' + getYear;
                    modifiedDate = formattedDate;
                }

                
                labelModifiedDate = '<span id="' + container.header.exposedId + 'ModifiedDate">' + modifiedDate + '</span></br></br>';
            }
            $('#columnTwo').append(labelforDate);
            $('#columnTwo').append(labelModifiedDate);

            //ContainerPalletWeight

            eMESA.setupDateTimePickers();

            //********************************************************************************************************************************************************//
            // Search for catalog 
            $('#columnTwo').append('<label>Search Catalog Item</label> <input class="form-control" type="text" id="searchForCatalog" name="searchForCatalog"> ');
            var $searchCatalog = $('#searchForCatalog');
            $searchCatalog.on('blur', $searchCatalog, self.searchForCatalog);

            //Search for po #
            $('#columnTwo').append('<label>Search PO #</label> <input class="form-control" type="text" id="searchForPo" name="searchForPo"> ');
            var $searchPo = $('#searchForPo');
            $searchPo.on('blur', $searchPo, self.searchForPo);

            //********************************************************************************************************************************************************//
            $('#columnOne').append('<br/><br/><br/>');
            $('#columnTwo').append('<br/><br/><br/>');

            SaveElements.PopulateForeignKeyDependencies();

        };
       
        self.buildPalletTable = function (e) {

            if (!newContainer && !pallets.molecules.length == 0) {
                self.getAllBoxes();
            }

            $('#pbResultsGrid').append('<br/><br/><br/>');
            $('#gridLoadSpinner').remove();

            pallets.removeInactive();

            pallets.sortThingsByAttribute('Palletpallet_id');

            if (pallets.molecules.length == 0) {

                self.addPallet();
                saveNewContainer = true;
            }
            else {
                for (var i = 0; i < pallets.molecules.length; i++) {

                    var pallet = pallets.molecules[i];

                    if (i == (pallets.molecules.length) - 1) {
                        var thisindex = i - 1;
                        self.addPalletRow(pallet, true, false, true);
                    }
                    else {
                        self.addPalletRow(pallet, false, false, true);
                    }
                }
            }

            self.bindElementsForType('pallet');
            
        };

        self.addPalletRow = function (pallet, addButton, bindExpandButton, bindDeleteButton, duplicate) {

            var rowElement = new DTS.ThingElements();
            var rowStyle = '';

            var rowStr = '<tr row class ="' + rowStyle + '" id="' + pallet.header.exposedId + '" > ';
            rowStr += '<td class = "custom-row-actions" id= "customRowActions' + pallet.header.exposedId + '">';
            rowStr += '<i class="fa fa-times-circle fa-lg delete-custom-row:before" id="' + pallet.header.exposedId + '_delete' + '"  data-thing-id="' + pallet.header.exposedId + '" data-thing-type="pallets" data-thing-order="Palletpallet_id" style="display:inline-block; color: darkred" ></i>';
            rowStr += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i9 class="fa fa-expand clickable" id="' + pallet.header.exposedId + '_expand' + '"  data-thing-id="' + pallet.header.exposedId + '" data-thing-type="pallet" ></i>';
            rowStr += '</td>';

            rowStr += '<td id = "palletrow' + pallet.header.exposedId + 'palletpallet_id"></td>';
            rowStr += '<td id = "palletrow' + pallet.header.exposedId + 'palletpallet_length" ></td>';
            rowStr += '<td id = "palletrow' + pallet.header.exposedId + 'palletpallet_width" ></td>';
            rowStr += '<td id = "palletrow' + pallet.header.exposedId + 'palletpallet_height" ></td>';
            rowStr += '<td id = "palletrow' + pallet.header.exposedId + 'palletpallet_weight" ></td>';


            rowStr += '<td class = "custom-row-actions" id="actions' + pallet.header.exposedId + '">';
            rowStr += '<button class="btn btn-success btn-sm btn-block" style="width:100px; background-color:dodgerblue; align:right" id="' + pallet.header.exposedId + '_clearPallet" name="' + pallet.header.exposedId + '_clearPallet">Clear Pallet</button>';
            if (addButton) {

                var buttonIdStr = pallet.header.exposedId + '_add';
                rowStr += '<button class="btn btn-success btn-sm btn-block" id="' + buttonIdStr + '" name="' + buttonIdStr + '_add">Add Pallet</button>';

                rowStr += '</td>';

                var textBoxItemId = pallet.header.exposedId + '_duplicateCount';
                var duplicateButtonId = pallet.header.exposedId + '_duplicateButton';

                var addMultiplePalletsStr = '<td><div id="palletAddSection"><span><input class="form-control" id="' + textBoxItemId + '" name= "' + textBoxItemId + '" style="width:100px; align:right" spellcheck= "false" value="0" >';
                //debugger;
                addMultiplePalletsStr += '<button class="btn btn-success btn-sm btn-block" style="width:100px; align:right" id="' + duplicateButtonId + '" name="' + duplicateButtonId + '_add">Duplicate</button>';
                addMultiplePalletsStr += '</span>';
                addMultiplePalletsStr += '</div></td>';
                rowStr += addMultiplePalletsStr;
            }
            rowStr += '</td>';
            rowStr += '</tr>';
            $('#palletTablebody').append($(rowStr));


            if (addButton) {
                var $addButton = $(document.getElementById(buttonIdStr));
                $addButton.on('click', $addButton, self.addPalletButtonClicked);

                var $duplicateButton = $(document.getElementById(duplicateButtonId));
                $duplicateButton.on('click', $duplicateButton, self.duplicatePalletClicked);

                if (boxes.molecules.length == 0) {

                    $('#palletAddSection').hide();
                    $('#' + buttonIdStr).hide();

                }
            }

            var $clearPalletButton = $(document.getElementById(pallet.header.exposedId + '_clearPallet'))
            $clearPalletButton.on('click', $clearPalletButton, self.clearPallet);

            if (bindExpandButton) {

                var $expandButton = $(document.getElementById(pallet.header.exposedId + '_expand'));
                $expandButton.on('click', $expandButton, self.expandPallet);

                if (boxes.molecules.length != 0) {
                    if (!duplicate) {
                        $expandButton.hide();

                        var id = $expandButton[0].id.replace('_expand', '');
                        var appendToid = 'customRowActions' + id;
                        $('#' + appendToid).append('<i class="fa fa-spin fa-spinner" id="pLoadSpinner' + $expandButton[0].id + '"></i>');
                    }
                }
            }

            if (bindDeleteButton) {

                var $deleteButton = $(document.getElementById(pallet.header.exposedId + '_delete'));
                $deleteButton.on('click', $deleteButton, self.deleteRow);
                if (boxes.molecules.length != 0) {
                    if (!duplicate) {
                        $deleteButton.hide();
                    }
                }
            }



            new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: pallet, property: 'Palletpallet_id', appendTo: 'palletrow' + pallet.header.exposedId + 'palletpallet_id', includeFormGroup: false });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: null, thing: pallet, property: 'Palletpallet_length', appendTo: 'palletrow' + pallet.header.exposedId + 'palletpallet_length', includeFormGroup: false });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: null, thing: pallet, property: 'Palletpallet_width', appendTo: 'palletrow' + pallet.header.exposedId + 'palletpallet_width', includeFormGroup: false });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: null, thing: pallet, property: 'Palletpallet_height', appendTo: 'palletrow' + pallet.header.exposedId + 'palletpallet_height', includeFormGroup: false });
            new DTS.ThingElements().addElement({ elementType: 'textbox', label: null, thing: pallet, property: 'Palletpallet_weight', appendTo: 'palletrow' + pallet.header.exposedId + 'palletpallet_weight', includeFormGroup: false });


        };

        self.clearPallet = function (e) {
            if (!checkBoxItemLoad) {
                alert('Loading Shipment, Please try again later.');
                e.stopPropagation();
                e.preventDefault();
            } else {
                if (confirm('Are you sure you wish to clear all Boxes and Box Items on this Pallet?')) {
                    if (confirm('Confirm Clear Pallet')) {
                        e.stopPropagation();
                        e.preventDefault();
                        var $target = $(e.target);
                        var matchId = $target[0].id.replace('_clearPallet', '');

                        var inactiveBoxes = [];
                        var thingType = 'boxes';
                        var beforeFilterOrders = [];

                        var findBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', matchId);

                        for (i = 0; i < findBoxes.length; i++) {

                            var currentBox = findBoxes[i];
                            currentBox.ValueExistsOnOff = 0;
                            inactiveBoxes.push(currentBox.header.exposedId);
                            currentBox.applyMappedValues();
                            SaveElements.addThingToSave(currentBox, currentBox.atomByPropertyName('ValueExistsOnOff'));

                            var findBoxItems = boxItems.ByValueThingId('Box Itembox_id', currentBox.header.exposedId);
                            for (var l = 0; l < findBoxItems.length; l++) {

                                var currentBoxItem = findBoxItems[l];
                                var boxOrder = currentBoxItem.BoxItemOrderItemPEKeyName4ExposedId;
                                if (boxOrder != undefined) {
                                    beforeFilterOrders.push(boxOrder);
                                }
                                
                            }
                            
                        }
                        var formElement = [];

                        var boxTable = $('#boxTable_' + matchId + 'body');
                        if (boxTable != undefined) {
                            boxTable.empty();
                            var fromPallet = pallets.byExposedId(matchId);
                            self.createNewBox(null, fromPallet, true);

                        }

                        var ordersToCompute = beforeFilterOrders.filter(function (item, pos) {
                            return beforeFilterOrders.indexOf(item) == pos;
                        });

                        if (ordersToCompute.length != 0) {
                            self.computeOrderValues(formElement, ordersToCompute, inactiveBoxes, thingType, true, []);
                        }
                    }
                }
            }


        }

        self.deleteRow = function (e) {

            if (!checkBoxItemLoad) {
                alert('Loading Shipment, Please try again later.');
            } else {

                if (confirm("Really delete this item?")) {

                    e.stopPropagation();
                    e.preventDefault();
                    var $target = $(e.target);
                    var exposedId = $target.data('thing-id');
                    var thingType = $target.data('thing-type');
                    var row = $target.closest('tr').last('tr');
                    var previousRow = row.prev('tr');
                    var prevRowId = previousRow.attr('id');
                    var thisId = exposedId;
                    var myArray = [];
                    var itemsToCompute = false;
                    myArray = eval(thingType);

                    var thisThing = myArray.byExposedId(exposedId);

                    //debugger;
                    thisThing.ValueExistsOnOff = 0;
                    thisThing.applyMappedValues();
                    var index = myArray.molecules.indexOf(thisThing);
                    SaveElements.addThingToSave(thisThing, thisThing.atomByPropertyName('ValueExistsOnOff'));
                    var inactivePallets = [];
                    var inactiveBoxes = [];
                    var beforeFilterOrder = [];
                    var formElement = [];


                    if (thingType == 'pallets') {
                        
                        inactivePallets.push(exposedId);
                        
                        var getThisThingBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', exposedId);
                        for (var l = 0; l < getThisThingBoxes.length; l++) {

                            var thisBox = getThisThingBoxes[l];
                            var getThisThingBoxItems = boxItems.ByValueThingId('Box Itembox_id', thisBox.header.exposedId);

                            for (var m = 0; m < getThisThingBoxItems.length; m++) {

                                var thisBoxItem = getThisThingBoxItems[m];
                                var thisOrder = thisBoxItem.BoxItemOrderItemPEKeyName4;
                                if (thisOrder != undefined) {
                                    var thisOrderId = thisOrder.header.exposedId;
                                    beforeFilterOrder.push(thisOrderId);
                                    itemsToCompute = true;
                                }

                            }

                        }
                        

                    } else if (thingType == 'boxes') {

                        inactiveBoxes.push(exposedId);
                        
                        var getThisThingBoxItems = boxItems.ByValueThingId('Box Itembox_id', exposedId);

                        for (var m = 0; m < getThisThingBoxItems.length; m++) {

                            var thisBoxItem = getThisThingBoxItems[m];
                            var thisOrder = thisBoxItem.BoxItemOrderItemPEKeyName4;
                            if (thisOrder != undefined) {
                                var thisOrderId = thisOrder.header.exposedId;
                                beforeFilterOrder.push(thisOrderId);
                                itemsToCompute = true;
                            }

                        }
                        

                    } else if (thingType == 'boxItems') {

                        if (thisThing.BoxItemOrderItemPEKeyName4 != undefined) {
                            getOrder = thisThing.BoxItemOrderItemPEKeyName4.header.exposedId;
                            beforeFilterOrder.push(getOrder);
                            itemsToCompute = true;
                        }
                        else {
                            //This is just an empty box item, it's okay to delete it without recomputing
                            $saveButton = $('#saveCreateButton');
                            $saveButton.removeAttr('disabled');

                            var idx = -1;
                            for (var z = 0; z < length; z++) {

                                if (boxItems.molecules[z].header.exposedId == thisId) {

                                    idx = z;
                                    break;
                                }
                            }
                            if (idx > -1) {
                                boxItems.molecules.splice(idx, 1);
                            }
                            idx = -1;
                            for (var z = 0; z < length; z++) {

                                if (SaveElements.molecules[z].header.exposedId == thisId) {

                                    idx = z;
                                    break;
                                }
                            }
                            if (idx > -1) {
                                SaveElements.molecules.splice(idx, -1);
                            }

                            row.remove();

                            var buttonIdStr = prevRowId + '_add';
                            var rowStr = '<button class="btn btn-success btn-sm btn-block" id="' + buttonIdStr + '" name="' + buttonIdStr + '_add">Add Box Item</button>';
                            $('#' + prevRowId + 'customRow').append(rowStr);
                            var $addButton = $('#' + buttonIdStr);
                            $addButton.on('click', $addButton, self.addBoxItemButtonClicked);
                            return;
                        }

                    }

                    var ordersToCompute = beforeFilterOrder.filter(function (item, pos) {
                        return beforeFilterOrder.indexOf(item) == pos;
                    });

                    if (thingType == 'pallets' || thingType == 'boxes') {
                        if (thingType == 'pallets') {
                            var thisOrderId = thisThing.Palletpallet_id;
                            var maxValueOrder = myArray.maxValueByThingsForAttribute('Palletpallet_id');

                            if (thisOrderId < maxValueOrder) {

                                row.remove();

                                var startNumber = thisOrderId + 1;
                                var endNumber = maxValueOrder + 1;

                                myArray.molecules.splice(index, 1);
                                
                                for (var i = 0; i < myArray.molecules.length; i++) {
                                    var currentThing = myArray.molecules[i];
                                    var currentId = currentThing.Palletpallet_id;

                                    if (currentId == startNumber && currentId < endNumber) {
                                        currentThing.Palletpallet_id = startNumber - 1;
                                        startNumber = startNumber + 1;
                                        currentThing.applyMappedValues();
                                        SaveElements.addThingToSave(currentThing, currentThing.atomByPropertyName('Palletpallet_id'));
                                        SaveElements.RefreshAllElements([currentThing]);
                                    }
                                }

                            } else if (thisOrderId == maxValueOrder) {
                                var findNumber = thisOrderId - 1;
                                if (findNumber == 0) {
                                    findNumber = 1;
                                }
                                for (var j = 0; j < myArray.molecules.length; j++) {

                                    var currentThing = myArray.molecules[j];
                                    var thisId = currentThing.Palletpallet_id;

                                    if (findNumber == thisId) {

                                        myArray.molecules.splice(index, 1);

                                        var x = document.getElementById('palletTablebody').rows.length;
                                        if (x == 1) {
                                            row.remove()
                                            self.addPallet(true);
                                        } else {
                                            row.remove()


                                            var buttonIdStr = currentThing.header.exposedId + '_add';
                                            var rowStr = '<button class="btn btn-success btn-sm btn-block" id="' + buttonIdStr + '" name="' + buttonIdStr + '_add">Add Pallet</button>';
                                            document.getElementById('actions' + currentThing.header.exposedId).insertAdjacentHTML('beforeend', rowStr);

                                            var rowStr2 = '</td>';

                                            var textBoxItemId = currentThing.header.exposedId + '_duplicateCount';
                                            var duplicateButtonId = currentThing.header.exposedId + '_duplicateButton';

                                            var addMultiplePalletsStr = '<td><span><input class="form-control" id="' + textBoxItemId + '" name= "' + textBoxItemId + '" style="width:100px; align:right" spellcheck= "false" value="0" >';

                                            addMultiplePalletsStr += '<button class="btn btn-success btn-sm btn-block" style="width:100px; align:right" id="' + duplicateButtonId + '" name="' + duplicateButtonId + '_add">Duplicate</button>';
                                            addMultiplePalletsStr += '</span>';
                                            rowStr2 += addMultiplePalletsStr;

                                            rowStr2 += '</td>';
                                            rowStr2 += '</tr>';

                                            document.getElementById('actions' + currentThing.header.exposedId).insertAdjacentHTML('afterend', rowStr2);

                                            var $addButton = $(document.getElementById(buttonIdStr));
                                            $addButton.on('click', $addButton, self.addPalletButtonClicked);

                                            var $duplicateButton = $(document.getElementById(duplicateButtonId));
                                            $duplicateButton.on('click', $duplicateButton, self.duplicatePalletClicked);
                                        }
                                    }
                                }


                            }

                            if (itemsToCompute) {
                                self.computeOrderValues(formElement, ordersToCompute, inactivePallets, thingType, true, []);
                            }
                        }

                        if (thingType == 'boxes') {
                            var thisOrderId = thisThing.Boxbox_id;
                            var palletGuid = thisThing.BoxPalletPEKeyName4.header.exposedId;
                            var unsortedPalletBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', palletGuid);

                            var tempArry = new DTS.Things();
                            var util = new DTS.Things();

                            var thisPalletBoxes = util.sortThingsByAttribute('Boxbox_id', null, unsortedPalletBoxes);

                            var maxValueOrder = tempArry.maxValueByThingsForAttribute('Boxbox_id', thisPalletBoxes);

                            if (thisOrderId < maxValueOrder) {

                                var startNumber = thisOrderId + 1;
                                var endNumber = maxValueOrder + 1;

                                for (var i = 0; i < thisPalletBoxes.length; i++) {
                                    var currentThing = thisPalletBoxes[i];
                                    var currentId = currentThing.Boxbox_id;
                                    //debugger;
                                    if (currentId == startNumber && currentId < endNumber) {
                                        currentThing.Boxbox_id = startNumber - 1;
                                        currentThing.applyMappedValues();
                                        startNumber = startNumber + 1;
                                        SaveElements.addThingToSave(currentThing, currentThing.atomByPropertyName('Boxbox_id'));
                                        SaveElements.RefreshAllElements([currentThing]);

                                    }

                                }

                                myArray.molecules.splice(index, 1);
                                
                                row.remove()

                            } else if (thisOrderId == maxValueOrder) {
                                var findNumber = thisOrderId - 1;
                                if (findNumber == 0) {
                                    findNumber = 1;
                                }
                                var palletGuid = thisThing.BoxPalletPEKeyName4.header.exposedId;
                                var thisPalletBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', palletGuid);

                                for (var j = 0; j < thisPalletBoxes.length; j++) {

                                    var currentThing = thisPalletBoxes[j];
                                    var thisId = currentThing.Boxbox_id;

                                    if (findNumber == thisId) {

                                        myArray.molecules.splice(index, 1);

                                        var palletId = thisThing.BoxPalletPEKeyName4.header.exposedId;
                                        var fromPallet = thisThing.BoxPalletPEKeyName4;

                                        var x = document.getElementById('boxTable_' + palletId + 'body').rows.length;
                                        if (x == 1) {
                                            row.remove()
                                            self.createNewBox(null, fromPallet, true);
                                        } else {
                                            row.remove();


                                            var buttonIdStr = currentThing.header.exposedId + '_add';
                                            var rowStr = '<button class="btn btn-success btn-sm btn-block" id="' + buttonIdStr + '" name="' + buttonIdStr + '_add" style="width:250px">Add Box</button>';
                                            document.getElementById('actions' + currentThing.header.exposedId).insertAdjacentHTML('beforeend', rowStr);

                                            var rowStr2 = '</td>';

                                            var textBoxItemId = currentThing.header.exposedId + '_duplicateCount';
                                            var duplicateButtonId = currentThing.header.exposedId + '_duplicateButton';

                                            var addMultiplePalletsStr = '<td><span><input class="form-control" id="' + textBoxItemId + '" name= "' + textBoxItemId + '" style="width:100px; align:right" spellcheck= "false" value="0" >';

                                            addMultiplePalletsStr += '<button class="btn btn-success btn-sm btn-block" style="width:100px; align:right" id="' + duplicateButtonId + '" name="' + duplicateButtonId + '_add">Duplicate</button>';
                                            addMultiplePalletsStr += '</span>';
                                            rowStr2 += addMultiplePalletsStr;

                                            rowStr2 += '</td>';
                                            rowStr2 += '</tr>';

                                            document.getElementById('actions' + currentThing.header.exposedId).insertAdjacentHTML('afterend', rowStr2);

                                            var $addButton = $(document.getElementById(buttonIdStr));
                                            $addButton.on('click', $addButton, self.addBoxButtonClicked);

                                            var $duplicateButton = $(document.getElementById(duplicateButtonId));
                                            $duplicateButton.on('click', $duplicateButton, self.duplicateBoxClicked);
                                        }

                                    }
                                }


                            }

                            if (itemsToCompute) {
                                self.computeOrderValues(formElement, ordersToCompute, inactiveBoxes, thingType, true, []);
                            }
                        }




                    } else if (thingType == 'boxItems') {
                        //need to adjust computed columnns here before removing the row
                        var myArray = [];
                        myArray.push(thisId);

                        if (itemsToCompute) {
                            self.computeOrderValues(formElement, ordersToCompute, myArray, thingType, true, []);
                        }

                        var boxId = thisThing.BoxItembox_id.header.exposedId;
                        var fromBox = thisThing.BoxItembox_id;

                        var x = document.getElementById('boxItemTable_' + boxId + 'body').rows.length;
                        if (x == 1) {
                            row.remove()
                            self.createNewBoxItem(null, fromBox, null, null, false, true);
                        } else {
                            row.remove();
                        }
                      
                    }

                }
            }
        }

        self.duplicatePalletClicked = function (e) {

            e.stopPropagation();
            e.preventDefault();
            var $target = $(e.target);
            var matchId = $target[0].id.replace('_duplicateButton', '');
            var textBoxId = matchId + '_duplicateCount';
            var duplicateTextBox = $(document.getElementById(textBoxId));
            var duplicateCount = duplicateTextBox.val();

            var addButtonId = matchId + '_add';
            var addButton = $(document.getElementById(addButtonId));

            var skipAddButton = true;
            for (var i = 0; i < duplicateCount; i++) {

                if (i == (duplicateCount - 1)) {
                    skipAddButton = false;
                }
                self.addPallet(true, skipAddButton, true, matchId);
            }
            duplicateTextBox.remove();
            addButton.remove();
            $(e.target).remove();

        }

        self.duplicateBoxClicked = function (e) {

            e.stopPropagation();
            e.preventDefault();
            var $target = $(e.target);
            var matchId = $target[0].id.replace('_duplicateButton', '');
            var textBoxId = matchId + '_duplicateCount';
            var duplicateTextBox = $(document.getElementById(textBoxId));
            var duplicateCount = duplicateTextBox.val();

            var addButtonId = matchId + '_add';
            var addButton = $(document.getElementById(addButtonId));

            var skipAddButton = true;
            for (var i = 0; i < duplicateCount; i++) {

                if (i == (duplicateCount - 1)) {
                    skipAddButton = false;
                }
                self.addBox(true, skipAddButton, matchId);
            }
            duplicateTextBox.remove();
            addButton.remove();
            $(e.target).remove();

        }
      
        self.addPalletButtonClicked = function (e) {


            e.stopPropagation();
            e.preventDefault();

            var $target = $(e.target);
            var matchId = $target[0].id.replace('_add', '');
            var textBoxId = matchId + '_duplicateCount';
            var duplicateTextBox = $(document.getElementById(textBoxId));
            var duplicateButtonid = matchId + '_duplicateButton';
            var duplicateButton = $(document.getElementById(duplicateButtonid));

            if (duplicateButton != undefined) {
                duplicateTextBox.remove();
                duplicateButton.remove();
            }


            self.addPallet(true);
            $(e.target).remove();

        }

        self.addBox = function (bindExpandButton, skipAddButton, boxGuid) {

            var newBox = new DTS.Molecule();
            var newGuid = eMESA.returnGuid();
            newBox.CreateByType(boxType, newGuid, 'newBox');
            var lastBox;
            var boxValue;
            var boxId;

            var anyBoxItems = boxItems.ByValueThingId('Box Itembox_id', boxGuid);

            //debugger;
            var palletGuid;
            var boxToDuplicate = boxes.byExposedId(boxGuid);
            var palletID = boxToDuplicate.BoxPalletPEKeyName4;
            if (palletID == undefined) {

                palletGuid = boxToDuplicate.BoxPalletPEKeyName4ExposedId;
            } else {
                palletGuid = boxToDuplicate.BoxPalletPEKeyName4.header.exposedId;
            }
            
            var thisPallet = pallets.byExposedId(palletGuid);
            var thisPalletBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', palletGuid);
            //debugger;
            var tempArry = new DTS.Things();

            if (thisPalletBoxes.length > 0) {
                boxId = tempArry.maxValueByThingsForAttribute('Boxbox_id', thisPalletBoxes) + 1;

                lastBox = boxes.byExposedId(boxGuid);
                boxValue = lastBox.BoxBox_Total_Value;
                

            }
            newBox.BoxBox_Total_Value = boxValue;
            newBox.Boxbox_id = boxId;
            newBox.BoxPalletPEKeyName4 = thisPallet;
            var boxTableId = 'boxTable_' + palletGuid;

            newBox.applyMappedValues();
            boxes.molecules.push(newBox);
            SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('BoxPalletPEKeyName4'));
            SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('Boxbox_id'));
            SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('BoxBox_Total_Value'));

            //debugger;
            if (skipAddButton) {
                self.addBoxRow(newBox, false, boxTableId);
            }
            else {
                self.addBoxRow(newBox, true, boxTableId);
            }

            var newDuplicateBoxItems = [];
            var selectedCustomerIds = [];
            var customerOrderItemQuantities = [];
            for (var j = 0; j < anyBoxItems.length; j++) {

                var newBoxItem = new DTS.Molecule();

                currentBoxItem = anyBoxItems[j];
                newBoxItemGuid = eMESA.returnGuid();
                newBoxItem.CreateByType(boxItemType, newBoxItemGuid, 'newBoxItem');
                newBoxItem.BoxItembox_id = newBox;
                var boxQuantity = currentBoxItem.BoxItemquantity;
                var boxOrder;
                var boxOrderGuid;
                var checkOrder = currentBoxItem.BoxItemOrderItemPEKeyName4;
                if (checkOrder == undefined) {
                    boxOrder = currentBoxItem.BoxItemOrderItemPEKeyName4ExposedId;
                    boxOrderGuid = boxOrder;
                } else {
                    boxOrder = currentBoxItem.BoxItemOrderItemPEKeyName4;
                    boxOrderGuid = boxOrder.header.exposedId;
                }

                var boxOrderId = currentBoxItem.BoxItemorder_item_no;
                newBoxItem.BoxItemquantity = boxQuantity;
                newBoxItem.BoxItemOrderItemPEKeyName4 = boxOrder;
                newBoxItem.BoxItemorder_item_no = boxOrderId;

                newBoxItem.applyMappedValues();
                boxItems.molecules.push(newBoxItem);

                SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItembox_id'));
                SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemOrderItemPEKeyName4'));
                SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemquantity'));
                SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemorder_item_no'));
                newDuplicateBoxItems.push(newBoxItem);
                
                if (checkOrder != undefined || boxQuantity != undefined) {
                    if (checkOrder != '' || boxQuantity != '') {
                        selectedCustomerIds.push(boxOrderGuid);

                        var customerOrderItemQty = {

                            customerOrderItemId: boxOrderGuid,
                            qty: boxQuantity
                        };
                        customerOrderItemQuantities.push(customerOrderItemQty);
                    }
                }
                
            }

            if (customerOrderItemQuantities.length != 0) {
                self.computeOrderValues(null, selectedCustomerIds, [], '', false, customerOrderItemQuantities);
            }
        }

        self.addPallet = function (bindExpandButton, skipAddButton, duplicate, palletGuid) {
            //debugger;
            var newDuplicateBoxItems = [];
            //debugger;
            var selectedCustomerIds = [];
            var customerOrderItemQuantities = [];
            var newPallet = new DTS.Molecule();
            var newGuid = eMESA.returnGuid();
            newPallet.CreateByType(palletType, newGuid, 'newPallet');
            newPallet.Palletcontainer_id = container;
            var palletLength;
            var palletWidth;
            var palletHeight;
            var palletWeight;
            var palletId = 1;

            if (duplicate) {
                if (pallets.molecules.length > 0) {
                    palletId = pallets.maxValueByThingsForAttribute('Palletpallet_id') + 1;
                    lastPallet = pallets.byExposedId(palletGuid);

                    palletLength = lastPallet.Palletpallet_length;
                    palletWidth = lastPallet.Palletpallet_width;
                    palletHeight = lastPallet.Palletpallet_height;
                    palletWeight = lastPallet.Palletpallet_weight;

                }
                newPallet.Palletpallet_id = palletId;
                newPallet.Palletpallet_length = palletLength;
                newPallet.Palletpallet_width = palletWidth;
                newPallet.Palletpallet_height = palletHeight;
                newPallet.Palletpallet_weight = palletWeight;

                newPallet.applyMappedValues();
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletcontainer_id'));
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletpallet_id'));
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletpallet_length'));
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletpallet_width'));
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletpallet_height'));
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletpallet_weight'));

                pallets.molecules.push(newPallet);

                if (skipAddButton) {
                    self.addPalletRow(newPallet, false, bindExpandButton, true, true);
                }
                else {
                    self.addPalletRow(newPallet, true, bindExpandButton, true, true);
                }

                var boxTableId = 'boxTable_' + newGuid;

                var anyBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', palletGuid);
                var newDuplicateBoxes = [];
                for (var k = 0; k < anyBoxes.length; k++) {

                    var newBox = new DTS.Molecule();

                    currentBox = anyBoxes[k];
                    currentBoxGuid = currentBox.header.exposedId;
                    newBoxGuid = eMESA.returnGuid();
                    newBox.CreateByType(boxType, newBoxGuid, 'newBox');
                    newBox.BoxPalletPEKeyName4 = newPallet;
                    var boxId = currentBox.Boxbox_id;
                    var boxValue = currentBox.BoxBox_Total_Value;
                    newBox.BoxBox_Total_Value = boxValue;
                    newBox.Boxbox_id = boxId;

                    newBox.applyMappedValues();
                    boxes.molecules.push(newBox);
                    //make changes
                    SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('BoxPalletPEKeyName4'));
                    SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('Boxbox_id'));
                    SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('BoxBox_Total_Value'));
                    newDuplicateBoxes.push(newBox);

                    var anyBoxItems = boxItems.ByValueThingId('Box Itembox_id', currentBoxGuid);
                    for (var j = 0; j < anyBoxItems.length; j++) {

                        var newBoxItem = new DTS.Molecule();

                        currentBoxItem = anyBoxItems[j];
                        newBoxItemGuid = eMESA.returnGuid();
                        newBoxItem.CreateByType(boxItemType, newBoxItemGuid, 'newBoxItem');
                        newBoxItem.BoxItembox_id = newBox;
                        var boxQuantity = currentBoxItem.BoxItemquantity;
                        var boxOrder;
                        var boxOrderGuid;
                        var checkOrder = currentBoxItem.BoxItemOrderItemPEKeyName4;
                        if (checkOrder == undefined) {

                            boxOrder = currentBoxItem.BoxItemOrderItemPEKeyName4ExposedId;
                            boxOrderGuid = boxOrder;
                        } else {
                            boxOrder = currentBoxItem.BoxItemOrderItemPEKeyName4;
                            boxOrderGuid = boxOrder.header.exposedId;
                        }

                        var boxOrderId = currentBoxItem.BoxItemorder_item_no;

                        newBoxItem.BoxItemquantity = boxQuantity;
                        newBoxItem.BoxItemOrderItemPEKeyName4 = boxOrder;
                        newBoxItem.BoxItemorder_item_no = boxOrderId;

                        newBoxItem.applyMappedValues();
                        boxItems.molecules.push(newBoxItem);

                        SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItembox_id'));
                        SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemOrderItemPEKeyName4'));
                        SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemquantity'));
                        SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemorder_item_no'));
                        newDuplicateBoxItems.push(newBoxItem);

                        if (boxOrderGuid != undefined) {
                            selectedCustomerIds.push(boxOrderGuid);

                            var customerOrderItemQty = {

                                customerOrderItemId: boxOrderGuid,
                                qty: boxQuantity
                            };
                            customerOrderItemQuantities.push(customerOrderItemQty);
                        }


                    }

                }

                if (selectedCustomerIds.length != 0) {
                    self.computeOrderValues(null, selectedCustomerIds, [], '', false, customerOrderItemQuantities);
                }

            } else {
                if (pallets.molecules.length > 0) {
                    palletId = pallets.maxValueByThingsForAttribute('Palletpallet_id') + 1;

                }
                newPallet.Palletpallet_id = palletId;

                newPallet.applyMappedValues();
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletcontainer_id'));
                SaveElements.addThingToSave(newPallet, newPallet.atomByPropertyName('Palletpallet_id'));

                pallets.molecules.push(newPallet);

                let newBox = new DTS.Molecule();
                
                newBoxGuid = eMESA.returnGuid();
                newBox.CreateByType(boxType, newBoxGuid, 'newBox');
                newBox.BoxPalletPEKeyName4 = newPallet;
                newBox.syncMoleculeValue('BoxPalletPEKeyName4');
                newBox.Boxbox_id = 1;

                newBox.applyMappedValues();
                boxes.molecules.push(newBox);
                //make changes
                SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('BoxPalletPEKeyName4'));
                SaveElements.addThingToSave(newBox, newBox.atomByPropertyName('Boxbox_id'));

                if (skipAddButton) {
                    self.addPalletRow(newPallet, false, bindExpandButton, true, true);
                }
                else {
                    self.addPalletRow(newPallet, true, bindExpandButton, true, true);
                }

            }

        };

        self.addBoxButtonClicked = function (e, bindExpandButton, skipAddButton, duplicate, boxGuid) {

            e.stopPropagation();
            e.preventDefault();

            //debugger;

            var fromBoxId = e.target.id;
            fromBoxId = fromBoxId.replace('_add', '');
            var textBoxId = fromBoxId + '_duplicateCount';
            var duplicateTextBox = $(document.getElementById(textBoxId));
            var duplicateButtonid = fromBoxId + '_duplicateButton';
            var duplicateButton = $(document.getElementById(duplicateButtonid));

            if (duplicateButton != undefined) {
                duplicateTextBox.remove();
                duplicateButton.remove();
            }

            self.createNewBox(fromBoxId);
            $(e.target).remove();
        }

        self.createNewBox = function (fromBoxId, fromPallet, afterDelete) {

            var newBox = new DTS.Molecule();
            newBox.CreateByType(boxType, eMESA.returnGuid(), 'newBox');

            if (fromPallet == undefined) {
                var fromBox = boxes.byExposedId(fromBoxId);
                fromPallet = fromBox.BoxPalletPEKeyName4;
            }

            newBox.BoxPalletPEKeyName4 = fromPallet;
            var boxTableId = 'boxTable_' + newBox.BoxPalletPEKeyName4.header.exposedId;
            var fromBoxesSet = boxes.ByValueThingId('BoxPalletPEKeyName4', newBox.BoxPalletPEKeyName4.header.exposedId);
            if (!afterDelete) {
                if (fromBoxesSet.length > 0) {

                    var util = DTS.Things();
                    var newBoxId = util.maxValueByThingsForAttribute('Boxbox_id', fromBoxesSet);
                    newBox.Boxbox_id = newBoxId + 1;
                }
                else {
                    newBox.Boxbox_id = 1;
                }
            }
            if (afterDelete) {
                newBox.Boxbox_id = 1;
            }
            newBox.applyMappedValues();
            boxes.molecules.push(newBox);
            //They don't ever modify this thing on the form, so we don't have handlers for it
            //But it needs to be saved
            SaveElements.molecules.push(newBox);
            self.addBoxRow(newBox, true, boxTableId);

        };

        self.addBoxItemButtonClicked = function (e) {
            e.stopPropagation();
            e.preventDefault();

            var fromBoxItemId = e.target.id;
            fromBoxItemId = fromBoxItemId.replace('_add', '');
            self.createNewBoxItem(fromBoxItemId);
            $(e.target).remove();

        };

        self.createNewBoxItem = function (fromBoxItemId, fromBox, orderItem, orderItemQty, shouldTriggerCompute, fromDelete, lastAddMultiple) {

            var newBoxItem = new DTS.Molecule();
            newBoxItem.CreateByType(boxItemType, eMESA.returnGuid(), 'newBoxItem');
            var idx = 0;
            if (!fromDelete) {
                if (fromBoxItemId != undefined) {

                    var fromBoxItem = boxItems.byExposedId(fromBoxItemId);
                    newBoxItem.BoxItembox_id = fromBoxItem.BoxItembox_id;
                    var boxItemTableId = 'boxItemTable_' + newBoxItem.BoxItembox_id.header.exposedId;
                    var fromBoxItemsSet = boxItems.ByValueThingId('Box Itembox_id', newBoxItem.BoxItembox_id.header.exposedId);
                    var util = DTS.Things();

                    var newBoxItemId = util.maxValueByThingsForAttribute('Box Itemorder_item_no', fromBoxItemsSet);
                    newBoxItem.BoxItemorder_item_no = newBoxItemId + 1;
                    idx = fromBoxItemsSet.length + 1;
                }
                else {

                    newBoxItem.BoxItembox_id = fromBox;
                    var boxItemTableId = 'boxItemTable_' + newBoxItem.BoxItembox_id.header.exposedId;
                    newBoxItem.BoxItemorder_item_no = 1;
                    newBoxItem.BoxItembox_id = fromBox;
                }
            }

            if (fromDelete) {
                newBoxItem.BoxItembox_id = fromBox;
                var boxItemTableId = 'boxItemTable_' + newBoxItem.BoxItembox_id.header.exposedId;
                newBoxItem.BoxItemorder_item_no = 1;
                newBoxItem.BoxItembox_id = fromBox;
            }

            //orderItem, orderItemQty
            if (orderItem != undefined) {
                newBoxItem.BoxItemOrderItemPEKeyName4 = orderItem;
                newBoxItem.BoxItemquantity = orderItemQty;
            }

            newBoxItem.applyMappedValues();
            SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemorder_item_no'));
            SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItembox_id'));

            if (orderItem != undefined) {
                SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemOrderItemPEKeyName4'));
                SaveElements.addThingToSave(newBoxItem, newBoxItem.atomByPropertyName('BoxItemquantity'));
            }

            boxItems.molecules.push(newBoxItem);

            if (lastAddMultiple) {
                var table = $('#' + boxItemTableId + 'body');
                var rows = table[0].rows;
                for (var u = 0; u < rows.length; u++) {

                    var rowId = rows[u].id;
                    var rowAddButton = document.getElementById(rowId + '_add');
                    var $rowAddButton = $('#' + rowId + '_add');
                    if (rowAddButton != undefined) {
                        $rowAddButton.remove();
                    }

                }
            }

            if (lastAddMultiple || lastAddMultiple == undefined) {
                self.addBoxItemRow(newBoxItem, true, boxItemTableId, shouldTriggerCompute);
            } else {
                self.addBoxItemRow(newBoxItem, false, boxItemTableId, shouldTriggerCompute);
            }

        };

        self.bindElementsForType = function (type) {

            var expandButtons = $('.fa-expand');

            $.each(expandButtons,
                function (i, input) {

                    var $expandButton = $(input);
                    if ($expandButton.data('thing-type') == 'pallet') {
                        $expandButton.on('click', expandButtons, self.expandPallet);

                        if (!newContainer && !saveNewContainer) {
                            $expandButton.hide();
                            var id = $expandButton[0].id.replace('_expand', '');
                            var appendToid = 'customRowActions' + id;
                            $('#' + appendToid).append('<i class="fa fa-spin fa-spinner" id="pLoadSpinner' + $expandButton[0].id + '"></i>');

                        }
                    }
                });
        };

        self.enablePalletButtons = function () {

            var expandButtons = $('.fa-expand');

            $.each(expandButtons,
                function (i, input) {
                    var $expandButton = $(input);
                    if ($expandButton.data('thing-type') == 'pallet') {
                        $expandButton.show();
                        $('#pLoadSpinner' + $expandButton[0].id).remove();
                        var id = $expandButton[0].id.replace('_expand', '');
                        $('#' + id + '_delete').show();

                        var addButtonIdStr = id + '_add';
                        $('#palletAddSection').show();
                        $('#' + addButtonIdStr).show();
                    }
                });
        }

        self.showBoxButtons = function () {

            var expandButtons = $('.fa-expand');

            $.each(expandButtons,
                function (i, input) {
                    var $expandButton = $(input);
                    if ($expandButton.data('thing-type') == 'box') {
                        $expandButton.show();
                        $('#bLoadSpinner' + $expandButton[0].id).remove();
                        var id = $expandButton[0].id.replace('_expand', '');
                        $('#' + id + '_delete').show();

                    }
                });
        };

        self.shouldRenderItems = function (e) {

            var shouldRender = true;
            var $target = $(e.target);
            var matchId = $target[0].id.replace('_expand', '');
            //Check to see if we need to collapse the row
            if ($target.hasClass('fa-compress')) {

                var idx = -1;
                var newCollapseKey = '';
                for (var i = 0; i < expandedKeys.length; i++) {

                    var key = expandedKeys[i];
                    if (key.includes(matchId)) {

                        idx = i;
                        var elToCollapse = $('#' + key + '_holder');
                        elToCollapse.hide('fast');
                        newCollapseKey = key;
                        break;

                    }
                }
                //expandedKeys.splice(idx);
                collapsedKeys.push(newCollapseKey);
                //$target.off('click');
                $target.removeClass('fa-compress').addClass('fa-expand').click(false);//
                shouldRender = false;
            }
            else if (collapsedKeys.length > 0) {
                console.log('Checking expand');
                var matchId = $target[0].id.replace('_expand', '');
                var idx = -1;
                var newExpandKey = '';
                var matchFound = false;
                for (var i = 0; i < collapsedKeys.length; i++) {

                    var key = collapsedKeys[i];
                    if (key.includes(matchId)) {

                        idx = i;
                        var elToExpand = $('#' + key + '_holder');
                        elToExpand.show('fast');
                        newCollapseKey = key;
                        matchFound = true;
                        console.log('Found element to expand');
                        break;

                    }
                }
                if (matchFound) {
                    //collapsedKeys.splice(idx);
                    expandedKeys.push(newCollapseKey);
                    //$target.off('click');
                    $target.removeClass('fa-expand').addClass('fa-compress').click(false);
                    shouldRender = false;

                }

            }

            return shouldRender;
        };

        self.expandPallet = function (e) {

            var $target = $(e.target);
            if (self.shouldRenderItems(e)) {

                boxes.removeInactive();

                var selectedPalletId = e.target.id.replace('_expand', '');
                var selectedBoxes = boxes.ByValueThingId('BoxPalletPEKeyName4', selectedPalletId);

                var util = DTS.Things();


                selectedBoxes = util.sortThingsByAttribute('Boxbox_id', null, selectedBoxes);

                self.renderBoxesForPallet(selectedPalletId, selectedBoxes);

                $target.removeClass('fa-expand').addClass('fa-compress').click(false);
            }


        };

        self.addBoxRow = function (box, addButton, boxTableId) {

            var t = $('#' + boxTableId);

            var rowStyle = '';
            var rowStr = '<tr row class ="' + rowStyle + '" id="' + box.header.exposedId + '" > ';
            rowStr += '<td class = "custom-row-actions" id="customRowActions' + box.header.exposedId + '">';
            rowStr += '<i class="fa fa-times-circle fa-lg delete-custom-row:before" id="' + box.header.exposedId + '_delete' + '"  data-thing-id="' + box.header.exposedId + '" data-thing-type="boxes" data-thing-order="Boxbox_id" style="display:inline-block; color: darkred" ></i>';
            rowStr += '&nbsp;&nbsp;&nbsp;<i class="fa fa-expand clickable" id="' + box.header.exposedId + '_expand' + '"  data-thing-id="' + box.header.exposedId + '" data-thing-type="box" ></i>';
            rowStr += '</td>';

            rowStr += '<td id = "bowrow' + box.header.exposedId + 'Boxbox_id"></td>';
            rowStr += '<td id = "bowrow' + box.header.exposedId + 'BoxBox_Total_Value"></td>';

            if (addButton) {

                var buttonIdStr = box.header.exposedId + '_add';
                rowStr += '<td><button class="btn btn-success btn-sm btn-block" id="' + buttonIdStr + '" name="' + buttonIdStr + '_add" style="width:250px">Add Box</button>';


                rowStr += '</td>';

                var textBoxItemId = box.header.exposedId + '_duplicateCount';
                var duplicateButtonId = box.header.exposedId + '_duplicateButton';

                var addMultiplePalletsStr = '<td><span><input class="form-control" id="' + textBoxItemId + '" name= "' + textBoxItemId + '" style="width:100px; align:right" spellcheck= "false" value="0" >';
                //debugger;
                addMultiplePalletsStr += '<button class="btn btn-success btn-sm btn-block" style="width:100px; align:right" id="' + duplicateButtonId + '" name="' + duplicateButtonId + '_add">Duplicate</button>';
                addMultiplePalletsStr += '</span>';
                rowStr += addMultiplePalletsStr;
            }

            rowStr += '</tr>';
            $('#' + boxTableId + 'body').append($(rowStr));

            if (addButton) {
                var $addButton = $(document.getElementById(buttonIdStr));
                $addButton.on('click', $addButton, self.addBoxButtonClicked);

                var $duplicateButton = $(document.getElementById(duplicateButtonId));
                $duplicateButton.on('click', $duplicateButton, self.duplicateBoxClicked);
            }


            new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: box, property: 'Boxbox_id', appendTo: 'bowrow' + box.header.exposedId + 'Boxbox_id', includeFormGroup: false });
            new DTS.ThingElements().addElement({ elementType: 'label', label: null, thing: box, property: 'BoxBox_Total_Value', appendTo: 'bowrow' + box.header.exposedId + 'BoxBox_Total_Value', includeFormGroup: false });

            var $expandButton = $('#' + box.header.exposedId + '_expand');
            $expandButton.on('click', $expandButton, self.expandBox);

            var $deleteButton = $(document.getElementById(box.header.exposedId + '_delete'));
            $deleteButton.on('click', $deleteButton, self.deleteRow);

            if (!initialBoxItemLoad) {
                if (!checkBoxItemLoad && !checkContainer && !saveNewContainer) {
                    $expandButton.hide();
                    $deleteButton.hide();

                    var id = $expandButton[0].id.replace('_expand', '');
                    var appendToid = 'customRowActions' + id;
                    $('#' + appendToid).append('<i class="fa fa-spin fa-spinner" id="bLoadSpinner' + $expandButton[0].id + '"></i>');

                }
            }
            //if (boxItems.molecules.length == 0 && !newContainer && !initialBoxItemLoad && boxes.molecules.length > 1) {
            //    $expandButton.hide();
            //    $deleteButton.hide();

            //    var id = $expandButton[0].id.replace('_expand', '');
            //    var appendToid = 'customRowActions' + id;
            //    $('#' + appendToid).append('<i class="fa fa-spin fa-spinner" id="bLoadSpinner' + $expandButton[0].id + '"></i>');

            //}
        };

        self.renderBoxesForPallet = function (palletId, selectedBoxes) {

            var boxTableId = 'boxTable_' + palletId;
            self.addTableDynamicShipment({
                headerLabel: 'Boxes',
                id: boxTableId,
                headerFields: ['tableaction', 'Box #', 'Box Total Value', 'tableaction'],
                appendTo: palletId,
                isSubTable: true,
                level: 1, hasExpandButton: true
            });


            if (selectedBoxes.length == 0) {

                var fromPallet = pallets.byExposedId(palletId);
                self.createNewBox(null, fromPallet);

            }
            else {

                for (var i = 0; i < selectedBoxes.length; i++) {

                    var box = selectedBoxes[i];
                    var exists = box.ValueExistsOnOff;
                    if (exists) {
                        var addBoxButton = false;
                        if (i == (selectedBoxes.length) - 1) {
                            addBoxButton = true;
                        }

                        self.addBoxRow(box, addBoxButton, boxTableId);
                    }


                }
            }
            expandedKeys.push(boxTableId);
        };

        self.expandBox = function (e) {

            var $target = $(e.target);
            if (self.shouldRenderItems(e)) {
                boxItems.removeInactive();
                var selectedBoxId = e.target.id.replace('_expand', '');
                var selectedBoxItems = boxItems.ByValueThingId('Box Itembox_id', selectedBoxId);

                var util = DTS.Things();
                var selectedDisplayItems = util.sortThingsByAttribute('Box Itemorder_item_no', null, selectedBoxItems);

                if (selectedDisplayItems.length == selectedBoxItems.length) {

                    self.renderBoxItemsForBox(selectedBoxId, selectedDisplayItems);
                }
                else {
                    self.renderBoxItemsForBox(selectedBoxId, selectedBoxItems);
                    
                }

                $target.removeClass('fa-expand').addClass('fa-compress').click(false);
            }
        }

        self.renderBoxItemsForBox = function (boxId, selectedBoxItems) {

            var boxItemTableId = 'boxItemTable_' + boxId;
            var cols = [
                '',
                'Item Entry',
                'PO #',
                'P21 #',
                'DLTD #',
                'Sch #',
                'Item Code',
                'Item Description',
                'Total Value',
                'Order Qty',
                'Shipped Qty',
                'Qty Remaining',
                'Quantity'
            ];

            self.addTableDynamicShipment({ headerLabel: 'Box Items', id: boxItemTableId, headerFields: cols, appendTo: boxId, isSubTable: true, level: 2, hasExpandButton: false });



            if (selectedBoxItems.length == 0) {

                var fromBox = boxes.byExposedId(boxId);
                self.createNewBoxItem(null, fromBox);
            }

            for (var i = 0; i < selectedBoxItems.length; i++) {
                var boxItem = selectedBoxItems[i];
                var exists = boxItem.ValueExistsOnOff;
                if (exists) {
                    var addAddButton = false;
                    if (i == (selectedBoxItems.length - 1)) {
                        addAddButton = true;
                    }
                    self.addBoxItemRow(boxItem, addAddButton, boxItemTableId);
                }

            }
            SaveElements.PopulateForeignKeyDependencies();
            expandedKeys.push(boxItemTableId);

        };

        self.addBoxItemRow = function (boxItem, addButton, boxItemTableId, shouldTriggerCompute) {

            $('#batchButton').removeAttr('disabled');
            
            var rowStyle = '';
            

            var rowStr = '<tr row class ="' + rowStyle + '" id="' + boxItem.header.exposedId + '" > ';
            rowStr += '<td style="width:50px">';
            rowStr += '<i class="fa fa-times-circle fa-lg delete-custom-row:before" id="' + boxItem.header.exposedId + '_delete' + '"  data-thing-id="' + boxItem.header.exposedId + '" data-thing-type="boxItems" style="display:inline-block; color: darkred" ></i>';
            rowStr += '</td>';
            //rowStr += '<td id = "bowitemrow' + i + boxItem.header.exposedId + 'BoxItembox_id"></td>'; //Item Entry
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemorder_item_no"></td>'; //Item Entry
            //
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItempo_number"></td>'; //PO
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemline_number"></td>'; //P21 #
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemudf_order_item_no"></td>';//DLTD #
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemScheduleNumber"></td>';//Schedule #
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemItemCodeDisplay"</td>';//Item Code
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemitem_description_1"></td>';//Item Description
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemBox_Item_Total_Value"></td>';//Total value
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemorder_quantity"></td>';//Order qty
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemShippedQuantity"></td>';//Shipped qty
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemQuantityRemaining"></td>';//qty remaining
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxItemquantity"></td>';//qty 
            rowStr += '<td id = "bowitemrow' + boxItem.header.exposedId + 'BoxBox_Total_Value"></td>';
            rowStr += '<td id = "' + boxItem.header.exposedId + 'customRow"></td>';

            rowStr += '</tr>';

            $('#' + boxItemTableId + 'body').append($(rowStr));

            if (addButton) {
                
                var buttonIdStr = boxItem.header.exposedId + '_add';
                var deleteButton = '<button class="btn btn-success btn-sm btn-block" id="' + buttonIdStr + '" name="' + buttonIdStr + '_add">Add Box Item</button>';
                $('#' + boxItem.header.exposedId + 'customRow').append(deleteButton);
            }
            
            

            var poTypeId = '';
            var poTypeAtom = boxItem.BoxItemOrderItemPEKeyName4;
            if (poTypeAtom != undefined) {
                poTypeId = poTypeAtom.header.typeOf.exposedId;
            }
            else {

                poTypeId = boxItem.BoxItemOrderItemPEKeyName4TypeOfId;

            }

            if (addButton) {
                var $addButton = $(document.getElementById(buttonIdStr));
                $addButton.on('click', $addButton, self.addBoxItemButtonClicked);

            }

            var $deleteButton = $(document.getElementById(boxItem.header.exposedId + '_delete'));
            $deleteButton.on('click', $deleteButton, self.deleteRow);

            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemorder_item_no',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemorder_item_no',
                includeFormGroup: false
            });

            new DTS.ThingElements().addElement({
                elementType: 'autocompleter',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItempo_number',
                includeFormGroup: false,
                autocompleteTypeId: poTypeId,
                displayFields: 'CustomerOrderItempo_number,CustomerOrderItemudf_order_item_no,CustomerOrderItemScheduleNumber,CustomerOrderItemItemCodeDisplay,CustomerOrderItemitem_description_1',
                sortDefinition:
                [{
                        property: 'CustomerOrderItempo_number',
                        dir: 'asc'
                    },
                    {
                        property: 'CustomerOrderItemudf_order_item_no', 
                        dir: 'asc'
                    },
                    {
                        property: 'CustomerOrderItemScheduleNumber',
                        dir: 'asc'}],
                searchFields: 'CustomerOrderItempo_number',
                valueThing: customerOrderTypeThing,
                //Following are optional parameters that will be defaulted if not provided
                autocompleteSuggestionWidth: '600px',
                autocompleteMinimumCharacters: 9,
                autocompleteDelay: 500,
                addHandler : self.orderItemSelected

            });

            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemudf_order_item_no',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItemudf_order_item_no'
            });

            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItempeoplesoft_item_no',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItempeoplesoft_item_no'
            });
            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemScheduleNumber',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItemScheduleNumber'
            });

            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemItemCodeDisplay',
                includeFormGroup: false,
                hrefLink: '<a target="_blank" href="/Container/SerialNo?id=' + boxItem.header.exposedId + '">',
                //hrefLink: baseUrl + '/Container/SerialNo?id=' + boxItem.header.exposedId,
                displayFields: 'CustomerOrderItemItemCodeDisplay'
            });

            //var hrefLink = '<a target=_blank href="/Container/SerialNo?id=' + boxItem.header.exposedId + '</a>>';
            //$('#'+ boxItem.header.exposedId + 'BoxItemCustomerOrderItemItemCodeDisplay').append(hrefLink);

            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemitem_description_1',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItemitem_description_1'
            });

            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemBox_Item_Total_Value',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemBox_Item_Total_Value',
                includeFormGroup: false
            });
            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemorder_quantity',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItemorder_quantity'
            });
            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemShippedQuantity',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItemShippedQuantity'
            });
            new DTS.ThingElements().addElement({
                elementType: 'label',
                label: null,
                thing: boxItem,
                property: 'BoxItemOrderItemPEKeyName4',
                appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemCustomerOrderItemQuantityRemaining',
                includeFormGroup: false,
                displayFields: 'CustomerOrderItemQuantityRemaining'
            });

            
            //shouldReturnElement
            if (shouldTriggerCompute) {//We want to trigger a function with this element, since its value has been set via a batch process rather than user interaction

                var qtyField = new DTS.ThingElements().addElement({
                    elementType: 'textbox',
                    label: null,
                    thing: boxItem,
                    property: 'BoxItemquantity',
                    appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemquantity',
                    includeFormGroup: false,
                    addHandler: self.computeOrderValues,
                    shouldReturnElement: true
                });
                self.computeOrderValues(qtyField);
            }
            else {
                new DTS.ThingElements().addElement({
                    elementType: 'textbox',
                    label: null,
                    thing: boxItem,
                    property: 'BoxItemquantity',
                    appendTo: 'bowitemrow' + boxItem.header.exposedId + 'BoxItemquantity',
                    includeFormGroup: false,
                    addHandler: self.computeOrderValues
                });

            }
            
        }

        self.computeOrderValues = function (formElement, ordersToCompute, myArray, thingType, isDelete, customerOrderItemQuantities) {

            console.log('Start computeOrderValues');
            var passOrders = [];
            if (!isDelete && formElement != undefined) {

                var getOrder = formElement.exposedThing.BoxItemOrderItemPEKeyName4;
                if (getOrder != undefined) {
                    var getOrderId = getOrder.header.exposedId;
                    passOrders.push(getOrderId);
                } else {
                    var myId = formElement.exposedThing.BoxItemOrderItemPEKeyName4ExposedId;
                    if (myId != undefined) {
                        passOrders.push(myId);
                    } else {
                        return;
                    }
                }

            }

            if (ordersToCompute != undefined) {

                passOrders = ordersToCompute;
            }

            $saveButton = $('#saveCreateButton');
            $saveButton.prop('disabled', true);
            $('#batchButton').prop('disabled', false);

            self.getComputedBoxItems(formElement, passOrders, myArray, thingType, isDelete, customerOrderItemQuantities);
            //self.getComputedBoxItems(formElement, ordersToCompute, myArray, thingType, isDelete);


        }

        self.getComputedBoxItems = function (formElement, ordersToCompute, myArray, thingType, isDelete, customerOrderItemQuantities) {

            var allQueryAttributes = [];
            
            var queryAttribute = {};

            for (var i = 0; i < ordersToCompute.length; i++) {
                var getOrder = ordersToCompute[i];
                queryAttribute.attributeName = 'Box ItemOrderItemPEKeyName4';
                queryAttribute.valueThingId = getOrder;
                allQueryAttributes.push(queryAttribute);
            }
            
            var request = {};
            request.attributes = [];
            request.attributes = allQueryAttributes;
            request.columns = [];
            request.DomainObjectId = boxItemTypeId;
            request.ExistsOnly = true;
            request.friendlyColumns = [
                'Box Itemquantity',
                'Box ItemOrderItemPEKeyName4',
                'Box Itembox_id',
                'ValueExistsOnOff'
            ];
            var FKs = [];
            FKs.push(eMESA.fkQueryObjectFriendly('Box Itembox_id', 'Boxbox_id'));
            FKs.push(eMESA.fkQueryObjectFriendly('Box Itembox_id', 'BoxPalletPEKeyName4'));
            request.ForeignKeys = FKs;
            
            request.RowCount = 500;
            request.pageNumber = 1;

            findComputed.InitializeByQuery(request, function () { self.getComputedPallets(formElement, ordersToCompute, myArray, thingType, isDelete, customerOrderItemQuantities) });
            
        }
        
        self.getComputedPallets = function (formElement, ordersToCompute, myArray, thingType, isDelete, customerOrderItemQuantities) {

            findComputed.removeInactive();
 
            computedBoxes.InitializeWithMolecules(findComputed.byTypeName('Box'));

            var array = [];
            for (var i = 0; i < computedBoxes.molecules.length; i++) {

                var currentBox = computedBoxes.molecules[i];
                var palletId = currentBox.BoxPalletPEKeyName4ExposedId;
                array.push(palletId);

            };

            var exposedIds = array.filter(function (item, pos) {
                return array.indexOf(item) == pos;
            });
            computedPallets.InitializeByList(exposedIds, function (result) {

                self.computeQuantities(formElement, ordersToCompute, myArray, thingType, isDelete, customerOrderItemQuantities);

            });

        }

        self.computeQuantities = function (formElement, ordersToCompute, myArray, thingType, isDelete, customerOrderItemQuantities) {
            console.log('Computing quantities');

            boxes.applyValueThingsToAll(pallets);
            boxItems.applyValueThingsToAll(boxes);
            boxItems.applyValueThingsToAll(customerOrderItems);

            SaveElements.fkThings = [];
            SaveElements.fkThingIds = []; 
            var thisOrder;
            var thisBox;
            var boxId;
            var currentSavedItems = new DTS.Things();
            var currentSavedBoxItems = new DTS.Things();
            var savedByOrder = [];
            var getallboxItems = [];
            var getIndex;
            var orderQuantity;
            var checkOrder;
            var getBoxItem;
            var orderId;
            var thisBoxItemQuantity;
            var exists;

            if (customerOrderItemQuantities == undefined) {
                customerOrderItemQuantities = [];
            }

            currentSavedItems.InitializeWithMolecules(SaveElements.molecules);
            currentSavedBoxItems.molecules = currentSavedItems.byTypeName('Box Item')

            var orderThingsToRefresh = [];

            if (!isDelete) {
                //debugger;
                if (customerOrderItemQuantities.length == 0) {
                    if (formElement != undefined) {
                        var thisId = formElement.exposedThing.header.exposedId;
                        getBoxItem = boxItems.byExposedId(formElement.exposedThing.header.exposedId);
                        checkOrder = getBoxItem.BoxItemOrderItemPEKeyName4;
                        if (checkOrder == undefined) {

                            thisOrder = getBoxItem.BoxItemOrderItemPEKeyName4ExposedId;

                        } else {

                            thisOrder = getBoxItem.BoxItemOrderItemPEKeyName4;

                        }
                        
                        thisBox = getBoxItem.BoxItembox_id;
                        boxId = thisBox.header.exposedId;
                        var newQuantityInt = parseInt(formElement.exposedThing.BoxItemquantity);
                        orderThingsToRefresh.push(thisOrder);

                    }

                } else {
                    if (thingType == 'pallets') {

                        for (var t = 0; t < myArray.length; t++) {

                            var removePallet = computedPallets.byExposedId(myArray[t]);
                            if (removePallet != undefined) {
                                removePallet.ValueExistsOnOff = 0;
                            }

                        }

                    } else if (thingType == 'boxes') {

                        for (var t = 0; t < myArray.length; t++) {

                            var removeBox = computedBoxes.byExposedId(myArray[t]);
                            if (removeBox != undefined) {
                                removeBox.ValueExistsOnOff = 0;
                            }

                        }

                    } else if (thingType == 'boxItems') {

                        getBoxItem = boxItems.byExposedId(myArray[0]);

                        checkOrder = getBoxItem.BoxItemOrderItemPEKeyName4;
                        
                        if (checkOrder == undefined) {

                            thisOrder = getBoxItem.BoxItemOrderItemPEKeyName4ExposedId;

                        } else {

                            thisOrder = getBoxItem.BoxItemOrderItemPEKeyName4;

                        }
                        
                        thisBox = getBoxItem.BoxItembox_id;
                        boxId = thisBox.header.exposedId;
                        getallboxItems = boxItems.ByValueThingId('Box Itembox_id', boxId);
                        getIndex = getallboxItems.indexOf(getBoxItem);
                        orderQuantity = thisOrder.CustomerOrderItemorder_quantity;
                        orderThingsToRefresh.push(thisOrder);
                    }

                }
            }
            var computedThisBox;
            var unsavedBox = false;

            if (customerOrderItemQuantities.length != 0) {
                unsavedBox = true;
            }

            if (!isDelete && customerOrderItemQuantities.length == 0) {
                computedThisBox = findComputed.byExposedId(thisId);
            } else {
                if (myArray != undefined && customerOrderItemQuantities.length == 0) {
                    computedThisBox = findComputed.byExposedId(myArray[0]);
                }
                
            }
            //debugger;
            if (computedThisBox == undefined && customerOrderItemQuantities.length == 0 && !isDelete) {
                if (myArray != undefined) {
                    computedThisBox = boxItems.byExposedId(myArray[0]);
                } else{
                    //debugger;
                    computedThisBox = formElement.exposedThing;
                    unsavedBox = true;

                }
                
            }

            if (computedThisBox != undefined) {
                if (!isDelete && customerOrderItemQuantities.length == 0) {
                    computedThisBox.BoxItemquantity = newQuantityInt;
                    computedThisBox.applyMappedValues();
                } else if (customerOrderItemQuantities.length == 0) {
                    computedThisBox.ValueExistsOnOff = 0;
                }
            }

            computedBoxItems.molecules = [];
            computedBoxItems.AppendMolecules(findComputed.byTypeName('Box Item'));
            computedBoxItems.applyValueThingsToAll(computedBoxes.molecules);

            for (var x = 0; x < ordersToCompute.length; x++) {
                var compareId = ordersToCompute[x];

                var boxitemTotals = 0;
                var remainingQuantity = 0;

                //for unsaved boxes, need to check unsaved against computed box items, add them before computing the rest here
                savedByOrder = currentSavedBoxItems.ByValueThingId('Box ItemOrderItemPEKeyName4', compareId);
                for (var a = 0; a < savedByOrder.length; a++) {

                    thisBox = savedByOrder[a];
                    var thisBoxId = thisBox.header.exposedId;

                    var findBoxes = computedBoxItems.byExposedId(thisBoxId);

                    if (findBoxes == undefined) {
                        var quantity = thisBox.BoxItemquantity;
                        boxitemTotals = boxitemTotals + parseInt(quantity);
                    }

                }
                
                if (customerOrderItemQuantities.length != 0) {
                    //debugger;
                    for (var y = 0; y < customerOrderItemQuantities.length; y++) {
                        var key = customerOrderItemQuantities[y];
                        var id = key.customerOrderItemId;
                        var value = key.qty;
                        if (id != undefined) {
                            if (id.toLowerCase() == compareId.toLowerCase()) {
                                boxitemTotals = boxitemTotals + parseInt(value);

                            }
                        }
                    }

                }
 
                var boxToOrder = boxItems.ByValueThingId('Box ItemOrderItemPEKeyName4', compareId);

                if (boxToOrder.length != 0) {
                    var checkOrderId = boxToOrder[0].BoxItemOrderItemPEKeyName4;
                    if (checkOrderId == undefined) {

                        thisOrder = boxToOrder[0].BoxItemOrderItemPEKeyName4ExposedId;

                    } else {

                        thisOrder = boxToOrder[0].BoxItemOrderItemPEKeyName4;

                    }

                    orderQuantity = thisOrder.CustomerOrderItemorder_quantity;

                    for (var j = 0; j < computedPallets.molecules.length; j++) {

                        var thisPallet = computedPallets.molecules[j];
                        var palletId = thisPallet.header.exposedId;
                        var palletexists = thisPallet.ValueExistsOnOff;
                        if (palletexists) {
                            var theseBoxes = computedBoxes.ByValueThingId('BoxPalletPEKeyName4', palletId);

                            for (var k = 0; k < theseBoxes.length; k++) {

                                thisBox = theseBoxes[k];
                                if (thisBox != undefined) {
                                    boxId = thisBox.header.exposedId;
                                    var theseBoxItems = computedBoxItems.ByValueThingId('Box Itembox_id', boxId)
                                    var boxexists = thisBox.ValueExistsOnOff;
                                    if (boxexists) {
                                        for (var i = 0; i < theseBoxItems.length; i++) {
                                            var inBoxList = false;
                                            var thisBoxItem = theseBoxItems[i];
                                            var boxItemHeader = thisBoxItem.header.exposedId;


                                            for (y = 0; y < savedByOrder.length; y++) {
                                                var thisSavedItem = savedByOrder[y];
                                                var savedHeader = thisSavedItem.header.exposedId;
                                                if (savedHeader != undefined && boxItemHeader != undefined) {
                                                    if (savedHeader.toLowerCase() == boxItemHeader.toLowerCase()) {
                                                        orderId = thisSavedItem.BoxItemOrderItemPEKeyName4ExposedId;

                                                        thisBoxItemQuantity = thisSavedItem.BoxItemquantity;

                                                        exists = thisSavedItem.ValueExistsOnOff;
                                                        if (orderId != undefined) {
                                                            if (orderId.toLowerCase == ordersToCompute[x].toLowerCase) {
                                                                if (exists) {
                                                                    boxitemTotals = boxitemTotals + thisBoxItemQuantity;
                                                                }
                                                            }

                                                            inBoxList = true;
                                                        }
                                                    }
                                                }

                                            }

                                            if (inBoxList == false) {
                                                orderId = thisBoxItem.BoxItemOrderItemPEKeyName4ExposedId;

                                                thisBoxItemQuantity = thisBoxItem.BoxItemquantity;

                                                exists = thisBoxItem.ValueExistsOnOff;
                                                if (orderId != undefined) {
                                                    if (orderId.toLowerCase() == ordersToCompute[x].toLowerCase()) {
                                                        if (exists) {
                                                            boxitemTotals = boxitemTotals + thisBoxItemQuantity;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    remainingQuantity = orderQuantity - boxitemTotals;
                    thisOrder.CustomerOrderItemQuantityRemaining = remainingQuantity;
                    thisOrder.CustomerOrderItemShippedQuantity = boxitemTotals;
                }

                

                if (typeof thisOrder == 'object') {
                   
                    thisOrder.applyMappedValues();
                    SaveElements.addThingToSave(thisOrder, thisOrder.atomByPropertyName('CustomerOrderItemQuantityRemaining'));
                    SaveElements.addThingToSave(thisOrder, thisOrder.atomByPropertyName('CustomerOrderItemShippedQuantity'));
                
                    var boxItemsToUpdate = boxItems.ByValueThingId('Box ItemOrderItemPEKeyName4', thisOrder.header.exposedId);

                    for (var z = 0; z < boxItemsToUpdate.length; z++) {

                        thisBoxItem = boxItemsToUpdate[z];

                        checkOrder = thisBoxItem.BoxItemOrderItemPEKeyName4;
                        if (checkOrder != undefined) {

                            thisBoxItem.BoxItemOrderItemPEKeyName4 = thisOrder;
                            thisBoxItem.applyMappedValues();

                            SaveElements.RefreshValueThingDependenciesForThing(thisOrder);

                        }
                    
                    }

                } else if (typeof thisOrder == 'string') {

                    var getOrderNow = new DTS.Molecule();
                    getOrderNow.CreateByExposedId(thisOrder, function () {
                        getOrderNow.applyMappedValues();
                        SaveElements.addThingToSave(getOrderNow, getOrderNow.atomByPropertyName('CustomerOrderItemQuantityRemaining'));
                        SaveElements.addThingToSave(getOrderNow, getOrderNow.atomByPropertyName('CustomerOrderItemShippedQuantity'));

                        boxItemsToUpdate = boxItems.ByValueThingId('Box ItemOrderItemPEKeyName4', getOrderNow.header.exposedId);
                     
                        for (var z = 0; z < boxItemsToUpdate.length; z++) {

                            thisBoxItem = boxItemsToUpdate[z];

                            checkOrder = thisBoxItem.BoxItemOrderItemPEKeyName4;
                            if (checkOrder != undefined) {

                                thisBoxItem.BoxItemOrderItemPEKeyName4 = getOrderNow;
                                thisBoxItem.applyMappedValues();

                                SaveElements.RefreshValueThingDependenciesForThing(getOrderNow);

                            }

                        }

                        $('#saveCreateButton').removeAttr('disabled');

                    });

                }

            }

            $('#saveCreateButton').removeAttr('disabled');

        }

        self.orderItemSelected = function (e) {

            //debugger;
        };

        //Boxes and box items
        self.allDone = function () {

            initialBoxItemLoad = true;
            self.showBoxButtons();
        };

        self.computeValues = function() {

            var boxItemsToSave = [];
            var boxIds = [];
            var palletIds = [];
            boxes.applyValueThingsToAll(pallets);
            boxItems.applyValueThingsToAll(boxes);
            boxItems.applyValueThingsToAll(customerOrderItems);

            for (var i = 0; i < SaveElements.molecules.length; i++) {

                var mol = SaveElements.molecules[i];
                if (mol.header.typeOf.name == 'Box Item') {

                    var fullBoxItem = boxItems.byExposedId(mol.header.exposedId);
                    boxItemsToSave.push(fullBoxItem);
                }

                //Palletpallet_weight

            }
            for (var i = 0; i < boxItemsToSave.length; i++) {

                var bItem = boxItemsToSave[i];

                var bOrder = bItem.BoxItemOrderItemPEKeyName4;
                if (bOrder == undefined) {
                    bOrder = bItem.BoxItemOrderItemPEKeyName4ExposedId;
                    if (bOrder != undefined) {
                        var getOrder = customerOrderItems.byExposedId(bOrder);
                        if (getOrder != undefined) {
                            bItem.BoxItemOrderItemPEKeyName4 = getOrder;
                            var totalValue = bItem.BoxItemOrderItemPEKeyName4.CustomerOrderItemorder_item_price_each * bItem.BoxItemquantity;
                            bItem.BoxItemBox_Item_Total_Value = totalValue;
                            bItem.applyMappedValues();
                            SaveElements.addThingToSave(bItem, bItem.atomByPropertyName('BoxItemBox_Item_Total_Value'));
                        }
                    }

                }
                if (bItem.BoxItemOrderItemPEKeyName4 != undefined) {

                    var totalValue = bItem.BoxItemOrderItemPEKeyName4.CustomerOrderItemorder_item_price_each * bItem.BoxItemquantity;
                    bItem.BoxItemBox_Item_Total_Value = totalValue;
                    bItem.applyMappedValues();
                    SaveElements.addThingToSave(bItem, bItem.atomByPropertyName('BoxItemBox_Item_Total_Value'));

                }
          
                if (!boxIds.includes(bItem.BoxItembox_id.header.exposedId)) {

                    boxIds.push(bItem.BoxItembox_id.header.exposedId);
                }
            }

            //Box total values

            for (var i = 0; i < boxIds.length; i++) {

                var utilThing = new DTS.Things();
                var boxId = boxIds[i];

                var boxToSum = boxes.byExposedId(boxId);
                var boxItemsToSum = boxItems.ByValueThingId('Box Itembox_id', boxId);

                var boxTotalValue = utilThing.sumValuesByThingAndAttribute('BoxItemBox_Item_Total_Value', boxItemsToSum);

                if (boxTotalValue != undefined) {

                    var thisBox = boxes.byExposedId(boxId);
                    thisBox.BoxBox_Total_Value = boxTotalValue;
                    thisBox.applyMappedValues();
                    SaveElements.addThingToSave(thisBox, thisBox.atomByPropertyName('BoxBox_Total_Value'));
                }

                if (!palletIds.includes(thisBox.BoxPalletPEKeyName4.header.exposedId)) {
                    palletIds.push(thisBox.BoxPalletPEKeyName4.header.exposedId);
                }

            }

            //Pallet values

            for (var i = 0; i < palletIds.length; i++) {
               
                var utilThing = new DTS.Things();
                var palletId = palletIds[i];
                var palletToSum = pallets.byExposedId(palletId);
                if (palletToSum.ValueExistsOnOff) {
                    var boxesToSum = boxes.ByValueThingId('BoxPalletPEKeyName4', palletId);
                    var palletTotalValue = utilThing.sumValuesByThingAndAttribute('BoxBox_Total_Value', boxesToSum);

                    palletToSum.PalletPallet_Total_Value = palletTotalValue;
                    palletToSum.applyMappedValues();
                    SaveElements.addThingToSave(palletToSum, palletToSum.atomByPropertyName('PalletPallet_Total_Value'));
                }
            }

            var containerTotalValue = pallets.sumValuesByThingAndAttribute('PalletPallet_Total_Value');

            var containerTotalWeight = pallets.sumValuesByThingAndAttribute('Palletpallet_weight');
            container.Containercontainer_place_pre = containerTotalValue;
            container.ContainerPalletWeight = containerTotalWeight;
            container.applyMappedValues();

            SaveElements.addThingToSave(container, container.atomByPropertyName('Containercontainer_place_pre'));
            SaveElements.addThingToSave(container, container.atomByPropertyName('ContainerPalletWeight'));


            if (container.Containercontainer_place_pre == undefined || container.Containercontainer_place_pre == 0 || isNaN(container.Containercontainer_place_pre)) {
                var boxItemsTotal = 0;
                for (var t = 0; t < boxItems.molecules.length; t++) {
                    var boxItem = boxItems.molecules[t];
                    if (boxItem.BoxItemOrderItemPEKeyName4ExposedId != undefined) {
                        var boxTotal = boxItem.BoxItemquantity * boxItem.BoxItemOrderItemPEKeyName4.CustomerOrderItemorder_item_price_each;
                        boxItem.BoxItemBox_Item_Total_Value = boxTotal.toFixed(2);
                        boxItem.syncMoleculeValue('BoxItemBox_Item_Total_Value');
                        SaveElements.addThingToSave(boxItem, boxItem.atomByPropertyName('BoxItemBox_Item_Total_Value'));
                        boxItemsTotal = boxItemsTotal + boxTotal;
                    }
                }

                container.Containercontainer_place_pre = boxItemsTotal.toFixed(2);
                container.syncMoleculeValue('Containercontainer_place_pre');
                SaveElements.addThingToSave(container, container.atomByPropertyName('Containercontainer_place_pre'));

            }

            //Containercontainer_place_pre
            //Palletpallet_weight
            //Container sum pallet weight
            //ContainerPalletWeight
        }

        //Save/refresh functions
        self.save = function (e) {

            e.stopPropagation();
            e.preventDefault();
            $('#bodyContainer').block({ message: 'Saving...' });
            try {
                self.computeValues();

            } catch (e) {
                console.log('Error computingValues: ' + e);
            }
            
            container.Containercontainer_modified_byExposedId = userid;
            container.syncMoleculeValue('Containercontainer_modified_by');
            SaveElements.addThingToSave(container, container.atomByPropertyName('Containercontainer_modified_by'));

            container.ContainerPalletCount = pallets.molecules.length;
            container.syncMoleculeValue('ContainerPalletCount');
            SaveElements.addThingToSave(container, container.atomByPropertyName('ContainerPalletCount'));
            
            SaveElements.save(self.postSave);
        };

        self.postSave = function (result) {
            console.log('postSave');

            if (result == 'Unauthorized') {

                alert('Your session has timed out. Please log in and retry.');
                $('#bodyContainer').unblock();

            }
            else {

                
                newContainer = false;
                checkContainer = false;
                var headerId = container.header.exposedId;
                var currentURL = window.location.href;
                if (!currentURL.includes(headerId)) {

                    var newUrl = currentURL;
                    if (newUrl.slice(-1) != '/') {
                        newUrl += '/';
                    }
                    newUrl += '?id=' + headerId;
                    history.replaceState(null, "", newUrl);

                }
                SaveElements.PopulateForeignKeyDependencies();
                $('#bodyContainer').unblock();
            
            }
            


        };

        //********************************************************************************************************************************************************//
        // Function search for searchForCatalog

        self.searchForCatalog = function () {
            var palletArr = [];
            var boxArr = [];
            var myPalletHeader;
            var myBoxHeader;
            var matches = false;

            for (var i = 0; i < boxItems.molecules.length; i++) {

                var thisBoxItem = boxItems.molecules[i];

                var thisOrder = thisBoxItem.BoxItemOrderItemPEKeyName4;

                if (thisOrder != undefined) {
                    var itemCode = thisOrder.CustomerOrderItemItemCodeDisplay;
                    var myHeader = thisOrder.header.exposedId;

                    var mySearchField = $('#searchForCatalog').val();

                    if (mySearchField == itemCode) {
                        matches = true;
                        var thisBox = thisBoxItem.BoxItembox_id;
                        myBoxHeader = thisBoxItem.BoxItembox_id.header.exposedId;
                        boxArr.push(myBoxHeader);

                        var myPallet = thisBox.BoxPalletPEKeyName4;
                        myPalletHeader = myPallet.header.exposedId;
                        palletArr.push(myPalletHeader);

                    }
                }

            }

            if (!matches) {
                alert('Sorry no matches found!')
            }

            var newPalletArray = palletArr.filter(function (item, pos) {
                return palletArr.indexOf(item) == pos;
            });

            var newBoxArray = boxArr.filter(function (item, pos) {
                return boxArr.indexOf(item) == pos;
            });

            for (var i = 0; i < newPalletArray.length; i++) {

                var thisPalletArr = newPalletArray[i];
                var palletExpand = $('#' + thisPalletArr + '_expand');
                palletExpand.click();

            }

            for (var i = 0; i < newBoxArray.length; i++) {

                var thisBoxArr = newBoxArray[i];
                var boxExpand = $('#' + thisBoxArr + '_expand');
                boxExpand.click();

            }

        };

        // Function search for PO #
        self.searchForPo = function () {

            var palletArray = [];
            var boxArray = [];
            var PalletHeader;
            var BoxHeader;
            var matches = false;

            for (var i = 0; i < boxItems.molecules.length; i++) {

                var thisBoxItem = boxItems.molecules[i];

                var thisOrder = thisBoxItem.BoxItemOrderItemPEKeyName4;

                if (thisOrder != undefined) {
                    var itemPo = thisOrder.CustomerOrderItempo_number;

                    var mySearchForPo = $('#searchForPo').val();

                    if (mySearchForPo == itemPo) {
                        matches = true;
                        var thisBox = thisBoxItem.BoxItembox_id;
                        BoxHeader = thisBoxItem.BoxItembox_id.header.exposedId;
                        boxArray.push(BoxHeader);

                        var myPallet = thisBox.BoxPalletPEKeyName4;
                        PalletHeader = myPallet.header.exposedId;
                        palletArray.push(PalletHeader);


                    };
                };

            };

            if (!matches) {
                alert('Sorry no matches found!')
            }

            var newPalletArray = palletArray.filter(function (item, pos) {
                return palletArray.indexOf(item) == pos;
            });

            var newBoxArray = boxArray.filter(function (item, pos) {
                return boxArray.indexOf(item) == pos;
            });

            for (var i = 0; i < newPalletArray.length; i++) {

                var thisPalletArray = newPalletArray[i];
                var palletExpand = $('#' + thisPalletArray + '_expand');
                palletExpand.click();

            };

            for (var i = 0; i < newBoxArray.length; i++) {

                var thisBoxArray = newBoxArray[i];
                var boxExpand = $('#' + thisBoxArray + '_expand');
                boxExpand.click();

            };


        };

        //********************************************************************************************************************************************************//

    }

    var shipment = new Shipment();
    shipment.initialize();

});
