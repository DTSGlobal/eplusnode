﻿"use strict";

if (window.DTS === undefined) {
    window.DTS = {};
}



DTS.eMESA = function () {
    var self = this;
    var prevWidth = 0;
    self.guidList = [];
    self.queryDefinition = {};

    self.authHeaders = {
        "Authorization": 'Bearer ' + token,
        "X-Client": clientId
    };

    self.initialize = function () {
        self.setupListeners();
        self.setupDateTimePickers();
        self.setupAutoCompleters();
        self.TableHelpers = new DTS.TableHelpers();
        self.AutocompleterSearch = new DTS.AutocompleterSearch();
        self.DTSFavorites = new DTS.Favorites();
        self.DTSFavorites.initialize();
        self.retrieveGuids();

        [].slice.call(document.querySelectorAll('.dropdown .nav-link')).forEach(function (el) {
            el.addEventListener('click', self.onClick, false);
        });
    }

    self.setupListeners = function () {
        $('#sessionProject').change(self.sessionProjectChanged);
        $('#refreshProjects').click(self.refreshSessionProjects);
        $('.userDefaultStar').click(self.userDefaultStarClicked);
    }
    self.sessionProjectChanged = function () {
        var projectId = $('#sessionProject').val();
        $.ajax('/CustomForm/ChangeSessionProject',
            {
                method: 'post',
                data: { projectId }
            });
    }

    self.refreshSessionProjects = function () {
        $.ajax('/CustomForm/RefreshSessionProjects',
            {
                method: 'post',
                beforeSend: function () {
                    $('#refreshProjects').addClass('fa-spin');
                    $('#sessionProject')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">(Please Select)</option>');
                },
                success: function (result) {
                    $.each(result,
                        function (i, v) {
                            $('#sessionProject').append('<option value="' + v.id + '">' + v.label + '</option>');
                        });
                },
                complete: function () {
                    $('#refreshProjects').removeClass('fa-spin');
                }
            });
    }

    self.userDefaultStarClicked = function (evt) {
        var elem = evt.target;
        var starElem = $(elem);
        var domainTypeId = starElem.data('domainTypeId');
        var selectBoxId = starElem.data('target');
        var formName = starElem.data('formName');
        var defaultValue = $('#' + selectBoxId).val();
        var isDelete = defaultValue !== '' && defaultValue !== '0' ? false : true;
        var url = isDelete ? '/UserDefault/DeleteUserDefault' : '/UserDefault/SaveUserDefault';

        $.ajax(url,
            {
                method: 'post',
                data: {
                    domainTypeId: domainTypeId,
                    defaultValue: defaultValue,
                    formName: formName
                },
                beforeSend: function () {
                    starElem.removeClass('fa-star').addClass('fa-spinner').addClass('fa-spin');
                },
                complete: function () {
                    starElem.removeClass('fa-spinner').removeClass('fa-spin').removeClass('blue').addClass('fa-check').addClass('green');
                }
            });
    }
    self.setupDateTimePickers = function () {
        //these are ones we need to format manually here, incase of return from ReportByObject pivoted
        var manualDatePickers = $('input.make-me-a-date-picker');
        $.each(manualDatePickers,
            function (i, input) {
                var $input = $(input);
                if ($input.val() == '') {
                    if (!$input.data('disable')) {
                        $input.attr('disabled', false);
                    }
                    return;
                }

                $input.val(moment($input.val()).format('YYYY-MM-DD'));
                if (!$input.data('disable')) {
                    $input.attr('disabled', false);
                }
            });
        manualDatePickers.datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });
        $('input[type="date"]').removeAttr('type').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    }

    self.setupAutoCompleters = function () {
        $('.autocompleter').each(function (i, item) { self.autoCompleteMe(item); });
    }

    self.setupAutoCompletersFor = function ($container) {
        $container.find('.autocompleter').each(function (i, item) { self.autoCompleteMe(item); });
    }

    self.autoCompleteMe = function (input, callback) {
        var $input = $(input);
        var objectName = $input.data('search-object');
        var type = $input.data('search-type');
        var searchFields = $input.data('search-fields');
        var displayFields = $input.data('display-fields');
        var xhr;
        $input.autoComplete({
            delay: 300,
            source: function (term, response) {
                try {
                    xhr.abort();
                } catch (e) { }

                xhr = $.ajax('/' + objectName + "/Autocomplete", {
                    data: {
                        q: term,
                        typeId: type,
                        searchFields: searchFields,
                        displayFields: displayFields
                    },
                    success: function (data) {
                        response(data);
                    },
                    beforeSend: function () {
                        $('#' + $input.attr('id').replace('_Label', '').replace(':', '\\:') + '_icon').show();
                    },
                    complete: function () {
                        $('#' + $input.attr('id').replace('_Label', '').replace(':', '\\:') + '_icon').hide();
                    }
                });
            },
            renderItem: function (item, search) {
                search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                return '<div class="autocomplete-suggestion" data-val="' + item.id + '" data-label=\'' + item.name + '\'>' +
                    '<div class=\"col-12\">' + item.name.replace(re, "<b>$1</b>") + '</div>'
                    //'<div class=\"col-9\">' + item.description.replace(re, "<b>$1</b>") + '</div>'
                    + '</div>';
            },
            onSelect: function (e, term, item) {
                var label = item.data('label');
                if (label.indexOf('<br') !== -1)
                    label = label.substr(0, label.indexOf('<br'));
                $input.val(label);

                //$('#' + $input.attr('id').replace('_Label', '').replace(':', '\\:')).val(item.data('val'));
                if ($input.attr('id').search('_Label') > -1) {
                    var value = item.data('val');
                    $('#' + $input.attr('id').replace('_Label', '').replace(':', '\\:')).val(item.data('val'));
                }

                if ($input.attr('id').search('_autocompleter') > -1) {
                    var value = item.data('val');
                    $('#' + $input.attr('id').replace('_autocompleter', '').replace(':', '\\:')).val(item.data('val'));
                }

                if (typeof callback === 'function') {
                    callback.call();
                }
            }
        });
    }


    self.onClick = function (e) {
        var t = e.target;
        if (!t.classList.contains('pagelink')) {
            e.preventDefault();
            var el = this.parentNode;
            el.classList.contains('show-submenu') ? self.hideSubMenu(el) : self.showSubMenu(el);
        }

    }
    self.showSubMenu = function (el) {
        el.classList.add('show-submenu');
        //document.addEventListener('click', function onDocClick(e) {
        //    e.preventDefault();
        //    if (el.contains(e.target)) {
        //        return;
        //    }
        //    document.removeEventListener('click', onDocClick);
        //    self.hideSubMenu(el);
        //});
    }

    self.hideSubMenu = function (el) {
        el.classList.remove('show-submenu');
    }

    self.blockPageOnButtonPress = function ($button) {
        $button.html('<i class="fa fa-spin fa-spinner"/>');
        $(":input").prop("disabled", true);
        $("a").css("cursor", "default").click(false);
    }

    //Methods to populate drop-down menus asynchronously

    self.dropDownThings = function (typeToGet, forAttribute, displayAttributes, callback, searchResults) {

        var args = [typeToGet];
        //var dispArray = displayAttributes.split(':');
        for (var i = 0; i < displayAttributes.length; i++) {
            args.push(displayAttributes[i]);
        }

        //input096734a8-42c9-4787-b504-ec80ece8ac5b
        //input23e0c642-8dd2-482d-a145-c08b9ab41645
        //debugger;
        $.ajax("/CustomForm/GetDropDowns",
            {
                data: {
                    thingsToGet: JSON.stringify(args)
                },
                method: "post",
                complete: function (result) {

                    callback(JSON.parse(result.responseText), typeToGet, forAttribute, displayAttributes, searchResults);
                    //var resultData = JSON.parse(result.responseText);
                }

            });
    }

    self.getDropDowns = function (selectDropDown, typeToGet, displayAttributes, selectKey, callback) {

        var $menu = $(selectDropDown);
        var args = [typeToGet];
        var dispArray = displayAttributes.split(':');
        for (var i = 0; i < dispArray.length; i++) {
            args.push(dispArray[i]);
        }

        //debugger;
        $.ajax("/CustomForm/GetDropDowns",
            {
                data: {
                    thingsToGet: JSON.stringify(args)
                },
                method: "post",
                complete: function (result) {

                    var resultData = JSON.parse(result.responseText);
                    var resultsToPopulate = self.sortedAttributeArray(resultData, dispArray[0]);

                    var sortedArray = Object.keys(resultsToPopulate);
                    sortedArray.sort();

                    var options = $menu[0].options;
                    for (var i = 0; i < sortedArray.length; i++) {

                        var key = sortedArray[i];

                        $menu.append($('<option>', {
                            value: resultsToPopulate[key].header.exposedId,
                            text: self.buildDropDownMenuDisplay(resultsToPopulate[key].attributes)

                        }));

                        var optionSelectedKey = selectKey.replace("input", "selectedDropDownattName");
                        //debugger;
                        var selectedOption = document.getElementById(optionSelectedKey);
                        var matchKey = selectedOption.value;
                        var match = $.grep(options, function (obj) { return obj.value === matchKey; })[0];

                        if (match != undefined) {
                            var option = $(match);
                            option.attr('selected', 'selected');
                        }
                    }
                    if (typeof callback == 'function')
                        callback.call();
                }
            });
    }

    //General utility methods for Things/Molecules, etc.
    //We need Guids sometimes. This is the store
    self.retrieveGuids = function () {
        
        var endpoint = apiEndpoint + 'api/Molecule/GenerateGUIDStore/';
        console.log('Endpoint: ' + endpoint);
        return $.ajax(endpoint,
            {
                method: 'GET',
                contentType: "application/x-www-form-urlencoded",
                headers: self.authHeaders, 
                complete: function (result) {
                    self.guidList = result.responseJSON;
                }
            }); 

        

    }

    self.returnGuid = function () {
        //Take one off the top and then remove it so the next one is unique
        var returnGuid = self.guidList[0];
        self.guidList.splice(0, 1);
        return returnGuid;

    }

    self.getList = function (exposedIds, callback) {
        var ids = exposedIds;
        $.ajax('/CustomForm/GetList',
            {
                method: 'POST',
                data: { ids },
                complete: function (result) {

                    callback(result.responseJSON);

                }
            });
    }

    self.sortedAttributeArrayByName = function (dropdownThings, displayFiedldAttributeName) {
        var dict = {};
        for (var i = 0; i < dropdownThings.length; i++) {

            var thisOption = dropdownThings[i];
            var thisAttribute = $.grep(thisOption.attributes, function (obj) { return obj.name === displayFiedldAttributeName; })[0];
            dict[thisAttribute.displayValue] = thisOption;
        }

        return dict;

    }
    /*
    self.maxValueByThingsForAttribute = function (things, attributeName) {

        var returnValue = 0;

        for (var i = 0; i < things.length; i++) {

            var thing = things[i];
            var addAttribute = self.attributeByName(thing.attributes, attributeName);
            if (addAttribute.dataType == 'ValueFloat') {

                if (addAttribute.valueDecimal > returnValue) {

                    returnValue = addAttribute.valueDecimal;
                }

            }
            if (addAttribute.dataType == 'ValueBigint') {

                if (addAttribute.valueLong > returnValue) {

                    returnValue = addAttribute.valueLong;

                }

            }
        }

        return returnValue;
    }
     */
    /*
   self.sumValuesByThingAndAttribute = function (things, attributeName) {

       var returnValue = 0;

       for (var i = 0; i < things.length; i++) {

           var thing = things[i];
           var addAttribute = self.attributeByName(thing.attributes, attributeName);
           if (addAttribute.dataType == 'ValueFloat') {

               returnValue += addAttribute.valueDecimal;

           }
           if (addAttribute.dataType == 'ValueBigint') {

               returnValue += addAttribute.valueLong;

           }

           
       }

       return returnValue;
   }
   */



    self.createEmptyMoleculeFromType = function (molecule, newExposedId, newLabel, stripAttributes) {


        var newMolecule = $.extend(true, {}, molecule);
        newMolecule.header.exposedId = newExposedId;
        newMolecule.header.name = newLabel;
        newMolecule.header.label = newLabel;

        if (stripAttributes) {

            newMolecule.Attributes = [];

        }

        return newMolecule;
    }

    self.createEmptyMoleculeFromTypeWithNewValueThingId = function (molecule, newExposedId, newLabel, valueThingAttributeName, newValueThingId) {

        var newMolecule = $.extend(true, {}, molecule);
        newMolecule.header.exposedId = newExposedId;
        newMolecule.header.name = newLabel;
        newMolecule.header.label = newLabel;

        var replaceAttribute = newMolecule.atom(valueThingAttributeName).valueThingId = newValueThingId;

        return newMolecule;
    }


    self.attributeByName = function (attributes, attributeName) {

        return $.grep(attributes,
            function (attributes) {
                return attributes.name == attributeName;
            })[0];
    }

    self.attributeByLabel = function (attributes, attributeLabel) {

        return $.grep(attributes,
            function (attributes) {
                return attributes.name == attributeLabel;
            })[0];
    }


    self.attributeValueByName = function (attributes, attributeName) {

        var returnAttribute = $.grep(attributes,
            function (attributes) {
                return attributes.name == attributeName;
            })[0];


        var returnStr = '';

        if (returnAttribute.displayValue != null) {

            returnStr = returnAttribute.displayValue;
        }

        return returnStr;
    }

    self.attributeValueByLabel = function (attributes, attributeLabel) {

        var returnAttribute = $.grep(attributes,
            function (attributes) {
                return attributes.name == attributeLabel;
            })[0];

        return returnAttribute.displayValue;
    }

    self.moleculeByExposedId = function (things, exposedId) {

        return $.grep(things,
            function (thing) {
                return thing.header.exposedId == exposedId;
            })[0];

    }

    self.moleculesByTypeName = function (things, typeName) {

        return $.grep(things,
            function (things) {
                return things.header.typeOf.name == typeName;
            });
    }

    self.MoleculesByValueThingId = function (things, attributeName, valueThingId) {

        var returnThings = [];

        for (var i = 0; i < things.length; i++) {

            var matchThing = things[i];

            var matchAttribute = self.attributeByName(matchThing.attributes, attributeName);

            if (matchAttribute == undefined) {
                continue; //debugger;
            }

            if (matchAttribute.valueThingId == valueThingId) {

                returnThings.push(matchThing);
            }

        }
        return returnThings;
    }

    self.thingAttributeGuidByName = function (thing, attributeName) {


    }

    self.fkQueryObjectFriendly = function (foreignKeyTypeName, foreignKeyAttributeName) {

        var fk = {};
        fk.ForeignKeyTypeName = foreignKeyTypeName;
        fk.TypeId = null;
        fk.ForeignKeyAttributeName = foreignKeyAttributeName;
        fk.AttributeId = null;
        fk.IsChild = false;

        return fk;
    }


    self.fkQueryObject = function (foreignKeyTypeName, typeId, foreignKeyAttributeName, attributeId) {

        var fk = {};
        fk.ForeignKeyTypeName = foreignKeyTypeName;
        fk.TypeId = typeId;
        fk.ForeignKeyAttributeName = foreignKeyAttributeName;
        fk.AttributeId = attributeId;
        fk.IsChild = false;

        return fk;
    }

    self.sortedAttributeArray = function (dropdownThings, displayFiedldAttributeId) {
        var dict = {};
        for (var i = 0; i < dropdownThings.length; i++) {

            try {
                var thisOption = dropdownThings[i];
                var thisAttribute = $.grep(thisOption.attributes, function (obj) { return obj.exposedId === displayFiedldAttributeId; })[0];
                dict[thisAttribute.displayValue] = thisOption;
            } catch (err) {
            }
        }

        return dict;

    }

    self.attributedArraySortedByValue = function (thingsToSort, byAttributeName) {

        var dict = {};
        for (var i = 0; i < thingsToSort.length; i++) {

            var thingToSort = thingsToSort[i];
            var thisAttribute = $.grep(thingToSort.attributes, function (obj) { return obj.name === byAttributeName; })[0];
            dict[thisAttribute.displayValue] = thingToSort;

        }

        return dict;
    }



    self.buildDropDownMenuDisplay = function (attributes) {

        var returnLabel = '';
        var incrementer = 0;
        $.each(attributes, function (i, attribute) {
            if (i > 0) {
                if (attribute.displayValue != undefined && attribute.displayValue.trim().length > 0) {
                    returnLabel = returnLabel + ' - ' + attribute.displayValue;
                }
            }
            else {
                if (attribute.displayValue != undefined && attribute.displayValue.trim().length > 0) {
                    returnLabel = returnLabel + attribute.displayValue;
                }
            }
            incrementer++;
        });

        return returnLabel;
    }



    self.updateValues = function (resultValues, e, collapsedElements) {

        //debugger;
        var $element = $(e);
        var thingDict = {};//This stores references to the Things with valueThing attributes
        //In the form, they're represented with ThingId:FKAttributeId
        var fkDict = {};    //This represents the FKThings and their display attributes

        var radioInputKeyTrue = '';
        var radioInputKeyFalse = '';
        var radioValue = false;


        for (var t = 0; t < resultValues.length; t++) {
            var thing = resultValues[t];
            var header = thing.header;
            var exposedId = header.exposedId;
            //debugger;

            for (var a = 0; a < thing.attributes.length; a++) {

                //debugger;
                var thisAttribute = thing.attributes[a];
                var matchKey = '';
                var matchLabelKey = '';
                var matchInlineLabel = '';
                var inlineLabelInput = '';
                var inlineLabelInputFK = '';

                if (t == 0) {

                    matchKey = 'input' + thing.attributes[a].exposedId;
                    matchLabelKey = 'inputLabel' + thing.attributes[a].exposedId;
                }
                else {
                    matchKey = 'inlineInput' + exposedId + ":" + thing.attributes[a].exposedId;
                    matchLabelKey = 'inputLabel' + exposedId + ":" + thing.attributes[a].exposedId;
                    inlineLabelInput = 'inlineLabelInput' + exposedId + ":" + thing.attributes[a].exposedId;
                    inlineLabelInputFK = 'inlineLabelInputFK' + exposedId + ":" + thing.attributes[a].exposedId;

                }


                if (thisAttribute.dataType != "ValueThingId") {

                    $element.each(function (i, el) {

                        var $el = $(el);
                        var currentId = $el.attr('id');
                        if (currentId == undefined)
                            currentId = $el.attr('name');

                        if (currentId != undefined) {


                            if (currentId == matchLabelKey || currentId == inlineLabelInput || currentId == inlineLabelInputFK || currentId.toLowerCase() == inlineLabelInput.toLowerCase()) {
                                $el.text(thisAttribute.displayValue);
                            }
                            if (currentId == matchKey) {
                                $el.val(thisAttribute.displayValue);
                            }
                        }



                    });

                    /*

                    if (thisAttribute.dataType == 'ValueBoolean') {

                    radioInputKeyTrue = 'inlineInput' + exposedId + ":" + thing.attributes[a].exposedId + 'true';
                    radioInputKeyFalse = 'inlineInput' + exposedId + ":" + thing.attributes[a].exposedId + 'false';

                    if (thisAttribute.valueLong == 1) {
                        radioValue = true;
                    }

                }

                    if (radioInputKeyFalse.length > 0) {

                            if (currentId == radioInputKeyFalse) {
                                debugger;

                                if (radioValue) {
                                    $el.removeAttr('checked');
                                }
                                else {
                                    $el.attr('checked', 'checked');

                                }
                            }

                            if (currentId == radioInputKeyTrue) {
                                debugger;

                                if (radioValue) {
                                    $el.attr('checked', 'checked');
                                }
                                else {
                                    $el.removeAttr('checked');
                                }

                            }
                        }
                    */

                    //New for collapsed elements
                    var keys = Object.keys(collapsedElements);
                    for (var x = 0; x < keys.length; x++) {
                        var key = keys[x];
                        var $collapsed = $(collapsedElements[key]).find('input, select, textarea, span');
                        $collapsed.each(function (i, el) {

                            var $el = $(el);
                            var currentId = $el.attr('id');
                            if (currentId == undefined)
                                currentId = $el.attr('name');

                            if (currentId != undefined) {

                                if (currentId == matchLabelKey || currentId == inlineLabelInput || currentId == inlineLabelInputFK || currentId.toLowerCase() == inlineLabelInput.toLowerCase()) {
                                    $el.text(thisAttribute.displayValue);
                                }
                                if (currentId == matchKey) {
                                    $el.val(thisAttribute.displayValue);
                                }
                            }
                        });
                    }



                }
                else {
                    matchKey = 'inlineLabelInputFK' + exposedId + ":" + thing.attributes[a].exposedId;

                    $element.each(function (i, el) {

                        var $el = $(el);
                        var currentId = $el.attr('id');
                        if (currentId == undefined)
                            currentId = $el.attr('name');

                        if (currentId == matchKey) {
                            var matchAttribute = thing.attributes[a];
                            if (matchAttribute.valueExposedId != undefined) {

                                if (!(matchAttribute.valueExposedId in fkDict)) {
                                    var fkAttributes = [];
                                    var thisValue = $el.val();
                                    fkAttributes.push($el.val());
                                    fkDict[matchAttribute.valueExposedId] = fkAttributes;
                                }
                                else {
                                    var fkAttributes = fkDict[matchAttribute.valueExposedId];
                                    fkAttributes.push($el.val());
                                }
                                thingDict[matchAttribute.valueExposedId] = thing.header.exposedId;
                                //debugger;
                            }
                        }
                    });


                    var keys = Object.keys(collapsedElements);
                    for (var x = 0; x < keys.length; x++) {
                        var key = keys[x];
                        var $collapsed = $(collapsedElements[key].find('input, select, textarea, span'));
                        //debugger;
                        $collapsed.each(function (i, el) {

                            var $el = $(el);
                            var currentId = $el.attr('id');
                            if (currentId == undefined)
                                currentId = $el.attr('name');

                            if (currentId == matchKey) {
                                var matchAttribute = thing.attributes[a];
                                if (matchAttribute.valueExposedId != undefined) {

                                    if (!(matchAttribute.valueExposedId in fkDict)) {
                                        var fkAttributes = [];
                                        var thisValue = $el.val();
                                        fkAttributes.push($el.val());
                                        fkDict[matchAttribute.valueExposedId] = fkAttributes;
                                    }
                                    else {
                                        var fkAttributes = fkDict[matchAttribute.valueExposedId];
                                        fkAttributes.push($el.val());
                                    }
                                    thingDict[matchAttribute.valueExposedId] = thing.header.exposedId;
                                    //debugger;
                                }
                            }
                        });
                    }


                }
            }
        }


        if (Object.keys(fkDict).length > 0) {

            $.ajax("/CustomForm/GetMoleculesBySet",
                {
                    data: {
                        thingsToGet: JSON.stringify(fkDict)
                    },
                    method: "post",
                    complete: function (result) {
                        //debugger;
                        var resultData = JSON.parse(result.responseText);

                        for (var thisKey in fkDict) {

                            //keyHolder.push(thisKey);
                            var attributes = fkDict[thisKey];

                            var thing = $.grep(resultData, function (obj) { return obj.header.exposedId === thisKey; })[0];
                            //debugger;
                            for (var a = 0; a < attributes.length; a++) {

                                var selectedAttribute = $.grep(thing.attributes, function (obj) { return obj.exposedId === attributes[a]; })[0];
                                var displayValue = selectedAttribute.displayValue; //self.attributeValueById(thing, attributes[a].exposedId);
                                inlineLabelInput = 'inlineLabelInput' + thingDict[thisKey] + ":" + selectedAttribute.exposedId; //attributes[a].exposedId;

                                //debugger;

                                $element.each(function (i, el) {
                                    var $el = $(el);
                                    var currentId = $el.attr('id');
                                    if (currentId == undefined)
                                        currentId = $el.attr('name');

                                    if (currentId == inlineLabelInput) {

                                        $el.text(displayValue);
                                    }
                                });


                                //New for collapsed elements
                                var collapsedKeys = Object.keys(collapsedElements);
                                for (var x = 0; x < keys.length; x++) {
                                    var collapsedKey = collapsedKeys[x];
                                    var $collapsed = $(collapsedElements[collapsedKey]).find('input, select, textarea, span');
                                    //debugger;
                                    $collapsed.each(function (i, el) {
                                        var $el = $(el);
                                        var currentId = $el.attr('id');
                                        if (currentId == undefined)
                                            currentId = $el.attr('name');

                                        /*
                                        if (currentId != undefined && currentId.includes('fe36c6db-94f1-452c-92b5-a79e33b8e466')) {

                                            debugger;
                                        }
                                        */
                                        if (currentId == inlineLabelInput) {
                                            //debugger;

                                            $el.text(displayValue);
                                        }
                                    });
                                }

                            }
                        }
                    }
                });
        }
    }

    self.queryMoleculeDefinition = function (callback) {

        $.ajax('/CustomForm/QueryMoleculesGetDefinition',
            {
                method: 'POST',

                complete: function (result) {
                    self.queryDefinition = result.responseJSON; //JSON.parse(result.responseText);
                    callback();
                    //debugger;

                }
            });
    };

};



//var renderWorkOrderSearch = function () {

//    var isPopulated = $('#havePopulated').val();

//    if (isPopulated == '0') {

//        var id = $('#navPersonnel').val();

//        var openCObyUser = new DTS.Things();
//        var constructionObjectTypeId = 'D81DADB4-1790-46A5-92F4-A6B54366578C';
//        var statusOpen = 'd16f9f93-bea0-454c-84a8-99c122184fa0';

//        var allQueryAttributes = [];

//        var queryAttribute = {};
//        queryAttribute.attributeName = 'Assigned To';
//        queryAttribute.valueThingId = id;//CO.header.exposedId;
//        allQueryAttributes.push(queryAttribute);

//        queryAttribute = {};

//        queryAttribute.attributeName = 'Status';
//        queryAttribute.valueThingId = statusOpen;
//        allQueryAttributes.push(queryAttribute);

//        var request = {};
//        request.attributes = [];
//        request.attributes = allQueryAttributes;
//        request.columns = [];
//        request.DomainObjectId = constructionObjectTypeId;
//        request.ExistsOnly = true;
//        request.friendlyColumns = [];
//        request.friendlyColumns = [
//            'ValueExistsOnOff',
//            'Status',
//            'Assigned To'
//        ];
//        request.RowCount = 1000;
//        request.pageNumber = 1;
//        console.log('Cancelling query');

//        openCObyUser.InitializeByQuery(request, function () {

//            openCObyUser.removeInactive();

//            var countCO = openCObyUser.molecules.length;
//            //var openWTbyCompany = new DTS.Things();

//            $('#userOpenCOs').append('<label style="color: darkblue; font-size:18px"><bold>&nbsp;&nbsp;&nbsp;&nbsp; Open Construction Objects for ' + userName.outerText + ':</bold></label><label style="color: darkred; font-size: 20px"><bold>&nbsp;&nbsp;' + countCO + '</bold></label>')

//            var companyId = $('#navigationCompanyId').val();
//            var companyName = $('#navigationCompanyLabel').val();

            //var workTaskObjectTypeId = 'CACC7FAD-0473-4B96-8992-24CFB013AF00';
            //var statusOpen = '0361ff5e-0633-4e57-9121-59add4300752';

            //var allQueryAttributes = [];

            //var queryAttribute = {};
            //queryAttribute.attributeName = 'Company Code';
            //queryAttribute.valueThingId = companyId;
            //allQueryAttributes.push(queryAttribute);

            //queryAttribute = {};

            //queryAttribute.attributeName = 'Task Status';
            //queryAttribute.valueThingId = statusOpen;
            //allQueryAttributes.push(queryAttribute);

            //var request = {};
            //request.attributes = [];
            //request.attributes = (allQueryAttributes);
            //request.columns = [];

            //request.DomainObjectId = workTaskObjectTypeId;
            //request.ExistsOnly = true;
            //request.friendlyColumns = [];
            //request.friendlyColumns = [
            //    'ValueExistsOnOff',
            //    'Company Code',
            //    'Task Status'
            //];
            //request.RowCount = 1000;
            //request.pageNumber = 1;

            //openWTbyCompany.InitializeByQuery(request, function () {

            //    $('#havePopulated').val('1');
            //    var openWT = openWTbyCompany.molecules.length;

            //    $('#openTasksByCompany').append('<label style="color: darkblue; font-size:18px"><bold>&nbsp;&nbsp;&nbsp;&nbsp; Open Tasks for ' + companyName + ':</bold></label><label style="color: darkred; font-size: 20px"><bold>&nbsp;&nbsp;' + openWT + '</bold></label>')

//        });

//    }

//};

window.eMESA = new DTS.eMESA();
window.eMESA.initialize();

//this.renderWorkOrderSearch();

