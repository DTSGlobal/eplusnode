
var express = require('express');
var router = express.Router();

var zlib = require('zlib');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('purposebuilt', { title: 'PFT' });
});

const { pipeline } = require('stream');
const {
  createReadStream,
  createWriteStream
} = require('fs');

const gzip = createGzip();
const source = createReadStream('input.txt');
const destination = createWriteStream('input.txt.gz');

pipeline(source, gzip, destination, (err) => {
  if (err) {
    console.error('An error occurred:', err);
    process.exitCode = 1;
  }
});

module.exports = router;
